@php
$PublicIP = $_SERVER['REMOTE_ADDR'];
$json     = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$PublicIP);
$json     = json_decode($json, true);
$ipCountry = $json['geoplugin_countryName'];
//dd($game);
@endphp
@extends('layouts.app') @section('content') @include('includes.navbar')
<link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.min.css"
/>
<style>
  .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
  }

  .rating input[type="radio"]:not(:nth-of-type(0)) {
    /* hide visually */
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }

  .rating [type="radio"]:not(:nth-of-type(0)) + label {
    display: none;
  }

  label[for]:hover {
    cursor: pointer;
  }

  .rating .stars label:before {
    content: "★";
  }

  .stars label {
    color: lightgray;
    font-size: 45px;
  }

  .stars label:hover {
    text-shadow: 0 0 1px #000;
  }
.react-reveal{
    background: #262626;
}
  .rating
    [type="radio"]:nth-of-type(1):checked
    ~ .stars
    label:nth-of-type(-n + 1),
  .rating
    [type="radio"]:nth-of-type(2):checked
    ~ .stars
    label:nth-of-type(-n + 2),
  .rating
    [type="radio"]:nth-of-type(3):checked
    ~ .stars
    label:nth-of-type(-n + 3),
  .rating
    [type="radio"]:nth-of-type(4):checked
    ~ .stars
    label:nth-of-type(-n + 4),
  .rating
    [type="radio"]:nth-of-type(5):checked
    ~ .stars
    label:nth-of-type(-n + 5) {
    color: orange;
  }

  .rating [type="radio"]:nth-of-type(1):focus ~ .stars label:nth-of-type(1),
  .rating [type="radio"]:nth-of-type(2):focus ~ .stars label:nth-of-type(2),
  .rating [type="radio"]:nth-of-type(3):focus ~ .stars label:nth-of-type(3),
  .rating [type="radio"]:nth-of-type(4):focus ~ .stars label:nth-of-type(4),
  .rating [type="radio"]:nth-of-type(5):focus ~ .stars label:nth-of-type(5) {
    color: darkorange;
  }

  @media (max-width: 1200px) {
    .bUIiDt > button img {
      max-width: 100%;
      height: auto;
    }

    .bUIiDt > button {
      position: unset;
      width: 100%;
      height: auto;
    }
  }

  @media (max-width: 1024px) {
    .media-carousel {
      padding: 0 10px 30px 10px;
    }

    .c-product-card {
      max-width: 33%;
      flex: 0 0 33%;
    }
  }

  @media (max-width: 767px) {
    .hlDzFF {
      font-size: 2.5rem;
      line-height: 2.5rem;
    }

    .media-carousel img {
      height: auto;
    }

    .media-carousel {
      padding: 0 10px 30px 10px;
    }

    .oicw95-0.jVapfN {
      margin: 0;
    }

    nav#nav-tabs {
      flex-wrap: wrap;
      display: grid;
    }

    .c-product-card {
      max-width: 50%;
      flex: 0 0 50%;
    }

    .sc-4dil8t-1.gdwDyM {
      margin-left: auto;
      margin-right: auto;
    }
  }

  @media (max-width: 380px) {
    .c-product-card {
      max-width: 100%;
      flex: 0 0 100%;
    }
  }
.swiper_main_wrap2 .swiper_main{
    max-width: 1920px !important;     
}
.swiper_main {
    height: auto;
    position: relative;
    width: 100%;
    max-width: 350px;
    height: 400px;
}

  .swiper_main_wrap {
    padding-top: 70px;
  }

  .swiper_main .swiper-slide {
    background: transparent !important;
    height: auto;
  }

  .swiper_main_wrap .thumbnail {
    margin: 0;
    padding: 0;
    background: transparent;
    border: 0;
  }

  .col-md-12.swiper_main_wrap.swiper_main_wrap2 .right,
  .swiper_main_wrap .left.swiper-button-next {
    top: 120px;
  }

  div#media {
    padding: 0;
  }

  .swiper_main .swiper-slide {
    border-radius: 15px;
    overflow: hidden;
  }

  .swiper_main .swiper-slide img {
    width: 100%;
    border-radius: 15px;
  }

  .swiper_main .swiper-button-next,
  .swiper_main .swiper-button-prev {
        width: 40px;
    height: 40px;
    border-radius: 100%;
    background-size: 15px 25px;
    background-color: white;
  }

  .swiper-container {
    width: 100%;
    height: 100%;
  }

  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }

  .swiper_main .swiper-pagination-clickable.swiper-pagination-bullets {
    bottom: auto !important;
    top: -25px;
    z-index: 99;
    right: 0;
    text-align: right;
  }

  .swiper_main .swiper-pagination-bullet {
    width: 11px;
    height: 11px;
    background: transparent;
    opacity: 1;
    border: 1px solid #fff;
    margin-right: 5px;
  }

  .swiper_main .swiper-pagination-bullet-active {
    opacity: 1;
    background: white;
  }

  .swiper_main_wrap .right.swiper-button-prev,
  .left.swiper-button-next {
    background: transparent;
    font-size: 22px;
    left: 10px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #ffffff;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width: 40px;
    margin-top: 0px;
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    vertical-align: middle;
    opacity: 0.7;
  }

  .swiper_main_wrap .right.swiper-button-prev:hover,
  .left.swiper-button-next:hover {
    opacity: 1;
  }

  .swiper_main_wrap.swiper_main_wrap2 {
    padding-top: 0;
    padding-bottom: 50px;
  }

  .swiper_main_wrap .left.swiper-button-next {
    left: auto;
  }

  @media (max-width: 767px) {
    .swiper_main {
      height: auto;
    }

    .swiper_main .swiper-wrapper {
      height: auto;
    }
  }
.recommended_creep{
      margin-top:75px !important;       
}
.othr_txt{
      margin: 50px 0 0 33px;   
}
.othr_txt span{ 
      color:#fff;  
}


.MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
    .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
        .MultiCarousel .MultiCarousel-inner .item { float: left;}
        .MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:#f1f1f1; color:#666;}
    .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
    .MultiCarousel .leftLst { left:0; }
    .MultiCarousel .rightLst { right:0; }
    
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background:#ccc; }
</style>
<div id="c-page__wrapper"
  class="sc-1e6fvcw-0 hzKfzr c-page__wrapper c-page__wrapper--dark c-page__wrapper--search-in-content"
>
  <div id="c-page__content">
    <div class="container">
      <div class="naz0ou-5 eCljye">
        <br />
        <input
          type="hidden"
          id="gameUniqueId"
          value="{{ $game[0]->uniqueId }}"
        />
        <div class="row">
          <div class="sc-1s4zbsp-1 iPmuZJ col-12 d-none d-xl-block">
            <div class="sc-1s4zbsp-3 fRbizP">
              <a href="<?php echo URL('/'); ?>" class="sc-1s4zbsp-0 kfYpdE"
                >Home</a
              >
              <span class="sc-1s4zbsp-2 hlVQTK">&gt;</span>
            </div>
            <!--<div class="sc-1s4zbsp-3 fRbizP">-->
            <!--    <a href="https://www.kinguin.net/c/14/battlefield-series" class="sc-1s4zbsp-0 kfYpdE">Battlefield Series</a>-->
            <!--    <span class="sc-1s4zbsp-2 hlVQTK">&gt;</span>-->
            <!--</div>-->
            <!--<div class="sc-1s4zbsp-3 fRbizP">-->
            <!--    <a href="https://www.kinguin.net/c/82009/battlefield-2042" class="sc-1s4zbsp-0 kfYpdE">Battlefield 2042</a>-->
            <!--    <span class="sc-1s4zbsp-2 hlVQTK">&gt;</span>-->
            <!--</div>-->
            <div class="sc-1s4zbsp-3 fRbizP">
              <a class="sc-1s4zbsp-0 kfYpdE">{{ $game[0]->title }}</a>
              <span class="sc-1s4zbsp-2 hlVQTK">&gt;</span>
            </div>
          </div>
          <h1 class="kl1e9v-2 kzrKxy col-12 d-none d-xl-block">
            &nbsp;<span>{{ $game[0]->title }}</span>
          </h1>
        </div>
        <div class="row">
          <!--- show image section start ------------->
          <div class="sc-1899ue3-0 gALTvL col-xl-8">
            <div class="sc-139i622-0 bUIiDt">
              <div class="react-reveal" style="opacity: 1">
                <div class="sc-139i622-1 buILmw">
                  <!--<img src="https://i.ytimg.com/vi/UnA7tepsc7s/maxresdefault.jpg" alt="">-->
                  <!--<image width="734" height="518"-->
                  <!--    src="{{ asset('images/' . $game[0]->background_image) }}" alt=""></image>-->
                  <div class="swiper_main" style="
    position: absolute;
    max-width: 100%;">
        <div class="swiper-pagination"></div>
    <!-- Slider main container -->
    <div class="swiper-container">
  
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper" style="transition-duration:2s !important;">
            @foreach($game as $val2 )
                <div class="swiper-slide"> <img src="{{asset('images/'.$val2->image)}}" class="img-fluid"></div>
                <!--<div class="swiper-slide"> <img src="{{ URL('images/2.jpg') }}"></div>-->
                <!--<div class="swiper-slide"> <img src="{{ URL('images/3.jpg') }}"></div>-->
          @endforeach
        </div>

        <!-- If we need navigation buttons -->
       <div class="swiper-button-prev"> <i class="fas fa-chevron-left"></i></div>
        <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></i></div>
    </div>
 </div>
                </div>
              </div>
              <button></button>
            </div>

            <div class="sc-1899ue3-1 fBlcjK">
              <div class="sc-1gk2hwg-0 dpLTZy">
			  @php
					$ex = explode(",",$game[0]->genres);
					
			  @endphp
				
					@foreach($ex as $exx)
							
								@php 
									foreach($genres as $genrelist){
										if($genrelist->uniqueId == $exx){
											$exx_name =  $genrelist->genresName;
										}
									}
								@endphp
							
								<span class="sc-1gk2hwg-1 bZGjqc"
								  ><a href="@php echo URL('/all-games?genresId='. $exx); @endphp"
									> {{ $exx_name }} </a
								  ></span>
							
					@endforeach
				
				<!--<span class="sc-1gk2hwg-1 bZGjqc"
                  ><a href="https://www.kinguin.net/rpg-games/">RPG</a></span>
				<span class="sc-1gk2hwg-1 bZGjqc"
                  ><a href="https://www.kinguin.net/fps-games/">FPS</a></span>
				<span class="sc-1gk2hwg-1 bZGjqc"
                  ><a href="https://www.kinguin.net/open-world-games/"
                    >Open World</a
                  >
				  </span>-->
              </div>
              <span class="sc-1gk2hwg-1 sc-9qu0r0-1 bZGjqc cGRJNb">
                <img
                  content="German"
                  src="https://static.kinguin.net/media/language/5409a417f1128.png"
                  alt="German"
                  class="sc-6sv52k-0 bRiDYu"
                /><img
                  content="English"
                  src="https://static.kinguin.net/media/language/540d53c73d2b9.png"
                  alt="English"
                  class="sc-6sv52k-0 bRiDYu"
                /><img
                  content="French"
                  src="https://static.kinguin.net/media/language/5409a908d54ec.png"
                  alt="French"
                  class="sc-6sv52k-0 bRiDYu"
                />
                <div class="sc-9qu0r0-2 iKuPOU">
                  +
                  <!-- -->14
                  <div class="sc-9qu0r0-0 jzbMfz">
                    <img
                      content="Spanish"
                      src="https://static.kinguin.net/media/language/5409a8ee37963.png"
                      alt="Spanish"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Czech"
                      src="https://static.kinguin.net/media/language/5409a9724e604.png"
                      alt="Czech"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Hungarian"
                      src="https://static.kinguin.net/media/language/5409a9ee61a6c.png"
                      alt="Hungarian"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Japanese"
                      src="https://static.kinguin.net/media/language/5409aa0c25f76.png"
                      alt="Japanese"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Polish"
                      src="https://static.kinguin.net/media/language/5409ab0b28df5.png"
                      alt="Polish"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Russian"
                      src="https://static.kinguin.net/media/language/5409ab95b23d4.png"
                      alt="Russian"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Chinese"
                      src="https://static.kinguin.net/media/language/5409abe5a1e5a.png"
                      alt="Chinese"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Bulgarian"
                      src="https://static.kinguin.net/media/language/5409adb842a2b.png"
                      alt="Bulgarian"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Korean"
                      src="https://static.kinguin.net/media/language/540d4b1e28b4f.png"
                      alt="Korean"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Turkish"
                      src="https://static.kinguin.net/media/language/540d4b3c3e645.png"
                      alt="Turkish"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Thai"
                      src="https://static.kinguin.net/media/language/540d4cad82d85.png"
                      alt="Thai"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Italian"
                      src="https://static.kinguin.net/media/language/5410066ed35a8.png"
                      alt="Italian"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Portuguese - Brazil"
                      src="https://cdn.kinguin.net/media/language/54100734a62bb.png"
                      alt="Portuguese - Brazil"
                      class="sc-6sv52k-0 bRiDYu"
                    /><img
                      content="Arabic"
                      src="https://static.kinguin.net/media/language/54128fd558b8c.png"
                      alt="Arabic"
                      class="sc-6sv52k-0 bRiDYu"
                    />
                  </div>
                </div>
              </span>
            </div>
          </div>
          <!---- show image section end ----->

          <!------ add to cart section start ---------------->

          <div class="col-xl-4">
            <div class="sc-1lo74h5-0 bLjxrt">
              <div class="sc-1lo74h5-3 dcNOcH">
                <div class="sc-1lo74h5-1 gBjlDQ">
                  <div class="sc-1lo74h5-2 bYyfAf">
                    <!--<span class="sc-1eae489-0 ktEPVT">-->
                    <!--  <div class="sc-1eae489-1 eGVBYF">-->
                    <!--    <svg-->
                    <!--      xmlns="http://www.w3.org/2000/svg"-->
                    <!--      viewBox="0 0 67.3 63.25"-->
                    <!--      width="20"-->
                    <!--      height="20"-->
                    <!--    >-->
                    <!--      <path-->
                    <!--        d="M0 37.04l6.45-6.82 3.9 3.58q-1.05-7.84 3.73-12.89a13 13 0 015.53-3.63 12 12 0 016.46-.11 17.47 17.47 0 01.37-7.11 13.28 13.28 0 013.19-5.59 14 14 0 016.2-4.05 10.76 10.76 0 016.74.26q2.55.91 6.61 4.66L67.35 22.1l-7 7.45-16.27-15c-2.82-2.6-4.87-4-6.18-4.27a4.9 4.9 0 00-4.55 1.65 6.86 6.86 0 00-1.8 3.82 7 7 0 001 4.57 27.44 27.44 0 005.29 6l13.51 12.61-7 7.41-15.52-14.37c-2.77-2.55-4.67-4.06-5.72-4.54a4.78 4.78 0 00-2.95-.43 5.48 5.48 0 00-2.72 1.71 7.37 7.37 0 00-2 4 6.86 6.86 0 00.91 4.4 27.34 27.34 0 005.3 6l13.7 12.74-7 7.4z"-->
                    <!--        fill="#fff"-->
                    <!--      ></path>-->
                    <!--    </svg>-->
                    <!--    <span>93</span>-->
                    <!--  </div>-->
                    <!--</span>-->
                  </div>
                  <div>
                    <div class="sc-1oex0f2-0 kzRKQU">
                      <div class="sc-1hk4sym-0 cuMaYk">
                        <div class="sc-1hk4sym-4 fZCBIE">
                          <div class="sc-9vx9ai-0 dQzvsF toggle">
                              <!------------- category icon ------------------>
                            @if($game[0]->name == 'Android')
                            <div title="android" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 42 49.97"
                                width="42"
                                height="50"
                              >
                                <path
                                  d="M6.34 19.2a2.84 2.84 0 00-.93-2.12 3 3 0 00-2.19-.89 3.27 3.27 0 00-2.24.89 2.8 2.8 0 00-1 2.12v12.95a2.88 2.88 0 001 2.23 3.32 3.32 0 002.3.89 3 3 0 002.19-.89 3 3 0 00.87-2.23V19.2zm1.09 17.52v-20h27.13v20a3.3 3.3 0 01-3.25 3.35h-2.22v6.81a2.74 2.74 0 01-1.53 2.73 3.27 3.27 0 01-3 0 2.76 2.76 0 01-1.48-2.73v-6.81h-4.16v6.81a2.8 2.8 0 01-1.48 2.68 3.18 3.18 0 01-3 0 2.74 2.74 0 01-1.53-2.73v-6.76h-2.2a3.3 3.3 0 01-3.28-3.32zm27.13-21.09a11.88 11.88 0 00-1.86-6.42 13 13 0 00-5-4.63l2.08-3.9a.4.4 0 00-.05-.55l-.12-.07c-.25-.11-.42-.06-.49.17l-2.19 3.9a14 14 0 00-5.92-1.22 14.89 14.89 0 00-6 1.22L12.82.23c-.07-.23-.24-.28-.49-.17a.4.4 0 00-.23.5.75.75 0 00.07.12l2.07 3.9a13.39 13.39 0 00-5.08 4.63 11.64 11.64 0 00-1.92 6.42zM16.01 9.49a1 1 0 01-.32.78 1.16 1.16 0 01-1.64 0 1.05 1.05 0 01-.33-.78 1.29 1.29 0 01.32-.84 1.06 1.06 0 011.49-.15 1.14 1.14 0 01.16.15 1.29 1.29 0 01.32.84zm12.36 0a1.08 1.08 0 01-.32.78 1 1 0 01-.77.34 1.24 1.24 0 01-.82-.34 1 1 0 01-.45-.78 1.18 1.18 0 01.38-.84 1.16 1.16 0 01.82-.39 1 1 0 01.77.39 1.29 1.29 0 01.34.84zm10.34 6.7a3.32 3.32 0 012.3.89 2.8 2.8 0 011 2.12v12.95a2.88 2.88 0 01-1 2.23 3.27 3.27 0 01-2.24.89 3 3 0 01-2.19-.89 3 3 0 01-.93-2.23V19.31a3.11 3.11 0 013.06-3.12z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Origin')
                            <div title="ea origin" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 38 50.02"
                                width="38"
                                height="50"
                              >
                                <path
                                  d="M18.15.07a.56.56 0 01.76.22v.06a.76.76 0 01-.22.7 12 12 0 00-2.06 4.87c-.14.72 0 .54.1.57a21 21 0 016.07.28 19 19 0 019.61 5.08 18.35 18.35 0 013.89 20.8 39.05 39.05 0 01-16.44 17.29.58.58 0 01-.72-.12.62.62 0 01.08-.78 12 12 0 002-4.42 3.72 3.72 0 00.14-1 .56.56 0 00-.48-.1 19.3 19.3 0 01-10.18-1.76 18.88 18.88 0 01-8.1-7.33 18.4 18.4 0 01-.21-18.5A38.89 38.89 0 0118.15.07M18 17.81a7.49 7.49 0 00-5.17 3.2 7.17 7.17 0 00.78 9A7.51 7.51 0 0023 31.13a7.19 7.19 0 00.45-11.94A7.52 7.52 0 0018 17.81z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Steam')
                            <div title="steam" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 50"
                                width="50"
                                height="50"
                              >
                                <path
                                  d="M50.01 25.01a24.19 24.19 0 00-3.38-12.5 25.26 25.26 0 00-9.12-9.13A24.18 24.18 0 0025.01.01a24.2 24.2 0 00-12 3.07 25.73 25.73 0 00-9 8.32 24.06 24.06 0 00-4 11.61l13.4 5.52a7.42 7.42 0 014.44-1.21l5.94-8.67v-.1a9 9 0 012.78-6.65 9.48 9.48 0 0113.3 0 8.93 8.93 0 012.77 6.65 9.21 9.21 0 01-2.82 6.76 9 9 0 01-6.81 2.7l-8.47 6a7.14 7.14 0 01-7 7.36A6.85 6.85 0 0113 39.76a7.07 7.07 0 01-2.42-4l-9.67-3.9a24.18 24.18 0 004.94 9.32 24.46 24.46 0 008.52 6.5 24.87 24.87 0 0010.64 2.33 24.18 24.18 0 0012.5-3.37 25.26 25.26 0 009.12-9.13 24.18 24.18 0 003.38-12.5zm-34.28 12.9a3.83 3.83 0 003 0 3.87 3.87 0 002.12-2.12 3.58 3.58 0 000-3 4 4 0 00-2.07-2.07l-3.23-1.31a5 5 0 014 0 5.25 5.25 0 012.82 2.87 5 5 0 010 4 5.25 5.25 0 01-2.87 2.87 5 5 0 01-4 .05 5.41 5.41 0 01-2.87-2.62zm17.54-13.1a6.12 6.12 0 004.38-1.8 6.19 6.19 0 000-8.87 6.33 6.33 0 00-8.87 0 5.91 5.91 0 00-1.87 4.42 6 6 0 001.86 4.45 6.2 6.2 0 004.5 1.81zm0-1.51a4.52 4.52 0 01-3.38-1.41 4.74 4.74 0 010-6.66 4.46 4.46 0 013.33-1.41 4.74 4.74 0 013.48 8l-.11.11a4.57 4.57 0 01-3.32 1.4z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Battle.Net')
                            <div title="battle.net" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 50"
                                width="30"
                                height="30"
                              >
                                <path
                                  d="M44.05 21.72c7.91-.1 2.09-6.8-1.56-8.5 0 0 4.82-11.22 4.32-11.52-.07 0-5.75 11-5.75 11a35.79 35.79 0 00-8.94-2.21C28.45 3.2 24.59-.51 21.62.04s-4.85 2.7-5.13 8.15c-4.7-5.06-7.25 1.88-6.64 6.11A18.27 18.27 0 000 18.12s6.69-2.56 10-2.42a23.91 23.91 0 002.41 9.2c-5.87 8.54-8.07 20.6 6.13 14.4-2.08 5.39 2.67 5.48 8.33 2.65l6.86 8-5.48-9a26.07 26.07 0 006.62-7c16.24 1.12 19.56-4.88 9.17-12.26zM29.17 10.36a20.39 20.39 0 00-8.93 2.25c.28-8.24 4.37-10.07 8.93-2.25zm-12.65 3.93l-3.82-.1c-.56-2.81 1.45-5.9 3.63-4-.42.81.05 3.31.19 4.1zm-2.67 13c3.93 6.39 5.29 6.71 5.29 6.71-7 3-9.08.28-5.29-6.71zm9.62 8.63l2.08 3.51c-4.82 2.4-7.91-.11-2.08-3.5zm3.43 2.63c-.49-1.12-.8-1.72-1.24-2.77 1.46-.67 4.4-1.78 4.4-1.78s-3.48.3-4.32.42c3.27-2.35 11-11.66 11-11.66l-2.15-1.09c-4 4.41-7.93 8.29-13.25 11.42-6.94-7.61-7.76-11.65-8.63-17.27 0 0 2.6.21 3.6.32a43.54 43.54 0 00-.8 5.2s.54-3 1.8-4.3c1 5.11 4.42 15 4.42 15l2-1.39c-1.59-5.71-3.2-11.16-3.35-17.11 7.35-2.72 14-.86 19.21 1.51-.7.9-2 2.65-2 2.65a16.79 16.79 0 00-4.14-3.42 23.27 23.27 0 013.23 4c-5.1-1.62-15.47-3.5-15.47-3.5l.01 2.22a59.72 59.72 0 0116.68 5.74c-2.36 7.38-5.22 12.42-11 15.81zM36 32a24.43 24.43 0 003.07-8.64c5.59 4.23 7.11 8.23-3.07 8.64zm6.6-11.12l-3.6-1.6s1.47-2.25 2.2-3.34c4.07 3.13 3.13 4.57 1.47 5z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'NCSoft')
                            <div title="ncsoft" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 40 50"
                                width="40"
                                height="50"
                              >
                                <path
                                  d="M40 27.5a.7.7 0 00-.32-.59l-5-3.29v-7.9a.71.71 0 00-.38-.62L11.18.12a.73.73 0 00-.78 0L2 5.36a.7.7 0 00-.24 1 .77.77 0 00.22.22l22.88 15.03v7.46l-5.16 3.35v-7.4a.7.7 0 00-.32-.59L8 16.89a.73.73 0 00-.74 0 .71.71 0 00-.38.62v7L.36 28.7a.7.7 0 000 1.18l17.4 11.72v7.7a.71.71 0 00.39.62.73.73 0 00.73 0l20.8-13.73a.7.7 0 00.32-.59v-8a.25.25 0 000-.1zM10.77 1.55l21.79 14.14-7.07 4.64L3.73 6.02zm15.53 20l6.93-4.53v6.6l-6.93 4.51zm-12.4 7.93l4.37-3.09v6zM8.29 18.8l3.65 2.4-3.65 2.34zm4.95 3.22l4.51 3-5.53 3.9a.7.7 0 00-.17 1 .66.66 0 00.19.18l6.34 4.24a.73.73 0 00.8 0L34 24.85l4 2.65-19.52 12.87L2 29.26zm5.96 26V41.6l19.36-12.78v6.37z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'UPlay')
                            <div title="uplay" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 48 49.94"
                                width="48"
                                height="50"
                              >
                                <path
                                  d="M42.41 33.86a19.31 19.31 0 01-18.2 11.91C9.61 45 5.37 28.14 15.12 22l.43.43a10 10 0 00-1.65 2.47 11.63 11.63 0 00-1.26 5.7c.76 11.82 17.25 14.22 22.93 2.54 7.23-16-11.93-32.3-28.94-19.86l-.39-.39c4.47-7 13.21-10.19 21.56-8.09a21.33 21.33 0 0114.61 29zm-13.65-.26a6.55 6.55 0 11-5.85-10.29A5.34 5.34 0 0127 32.37l1.77 1.23zM5.93 29.27A14.56 14.56 0 005.81 33l-.58.21a14 14 0 01-1-4 14.92 14.92 0 0112.66-16c7.35-1.09 14.38 3.5 16 9.9l-.58.21a17.49 17.49 0 00-1.59-1.84c-8.86-8.93-22.79-4.83-24.79 7.79zM48 25C47.45-.64 13.53-10.19.47 13.81c.57.42 1.37 1 2 1.4A25.36 25.36 0 000 25.93a24 24 0 0048 0v-1"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'XBOX 360')
                            <div title="xbox one" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50.01 49.97"
                                width="50"
                                height="50"
                              >
                                <path
                                  d="M36.49 31.23a84.6 84.6 0 00-6.36-6.76q-4.23-4.12-4.94-4.23c-.27-.07-1.31.71-3.13 2.32s-3.53 3.26-5.14 4.94a56.44 56.44 0 00-6.61 8.32q-2.37 3.78-2.37 6a2.86 2.86 0 00.66 2 15.23 15.23 0 002.77 2.17 24.83 24.83 0 0011.1 3.93 29.53 29.53 0 005.15 0 25.34 25.34 0 005-1 27.88 27.88 0 005.7-2.72c2-1.21 3.18-2.18 3.58-2.92q.81-1.52-.65-4.69a37.34 37.34 0 00-4.76-7.36zM18.23 13.69a93.41 93.41 0 00-8.07 11.39 54.36 54.36 0 00-4.14 8.16 19.63 19.63 0 00-1 3.84 5.94 5.94 0 00.05 2.62c.14.4.14.51 0 .35s-.47-.62-1-1.36a23.91 23.91 0 01-2.67-5.5 31.74 31.74 0 01-1.36-6.7 17.93 17.93 0 010-2.32c0-.74.18-2 .45-3.73a23 23 0 012.2-6.65 23.54 23.54 0 013.93-5.75 3.82 3.82 0 011.32-1 3.4 3.4 0 011.51.2 14.15 14.15 0 014 2.32l4.13 3.52zm31.37 6.35a21.54 21.54 0 01.4 4.74 29 29 0 01-.3 4.74 24.07 24.07 0 01-1.91 5.84 21 21 0 01-1.21 2.42 15.93 15.93 0 01-1.32 2c-.13.2-.2.26-.2.2l.1-.71a12 12 0 00-.8-5.24 50.3 50.3 0 00-5.34-10.56 102.84 102.84 0 00-7.06-9.78l.6-.5a36.89 36.89 0 016-4.84 7.29 7.29 0 011.91-1 5.29 5.29 0 011.55-.36c.34 0 1 .54 1.92 1.61a23.21 23.21 0 013.13 4.54 23.94 23.94 0 012.53 6.9zM13.38 3.51c-.74.06-1.06 0-.95-.1a9.09 9.09 0 012-1A24.16 24.16 0 0126.8.08a24.49 24.49 0 0111.22 3.43h-.4a15.24 15.24 0 00-3.63.2 24.9 24.9 0 00-6.86 2.41 7.51 7.51 0 01-2.11.87l-.91-.3q-6.79-3.38-10.73-3.18z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'PlayStation Vita')
                            <div title="playstation 4" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 38"
                                width="50"
                                height="38"
                              >
                                <path
                                  d="M49.56 29.1a1.69 1.69 0 00.18-2 6.11 6.11 0 00-3.47-2.14 23.16 23.16 0 00-5.56-1.29 23.88 23.88 0 00-5.73 0 29.29 29.29 0 00-6.6 1.46v4.79l8.68-3a8.63 8.63 0 012.56-.51 6.42 6.42 0 012.35.21q.9.3.78.81a1.65 1.65 0 01-1.26.86l-13.11 4.58v4.62l17.8-6.25 1.3-.6a6.44 6.44 0 002.08-1.54zm-19.44-7.87a7.43 7.43 0 004.56.81 4.85 4.85 0 003.25-2.23 8.94 8.94 0 001.22-5c0-3.25-.68-5.74-2-7.45s-3.79-3.17-7.2-4.37a88.59 88.59 0 00-11.29-3v35.53l8.16 2.47V8.3a2.67 2.67 0 01.48-1.71 1 1 0 011.17-.34c1.1.28 1.65 1.31 1.65 3.08v11.9zM3.73 32.35a26.39 26.39 0 0013.37.86v-4.22l-4 1.45a8.63 8.63 0 01-2.56.51 6.16 6.16 0 01-2.34-.21c-.61-.2-.87-.47-.79-.81a1.65 1.65 0 011.26-.86l8.42-3v-4.59L5.47 25.59a15 15 0 00-4.52 2.06 1.93 1.93 0 00-1 1.58 2.41 2.41 0 001 1.75 6.91 6.91 0 002.78 1.37z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'EPIC')
                            <div title="epic games" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 647.17 750.98"
                                width="65"
                                height="75"
                              >
                                <path
                                  d="M276.68 138.46h-18v106.13h18c11.05 0 16.37-5.35 16.37-16.87v-72.4c0-11.51-5.32-16.86-16.37-16.86z"
                                ></path>
                                <path
                                  fill-rule="evenodd"
                                  d="M244.07 507.98l-.47-1.2-.48-1.07-.35-1.08-.47-1.08-.48-1.2-.36-1.08-.46-1.07-.47-1.08-.36-1.08-.48-1.2-.35-1.07-.48-1.09-.48-1.07-.35-1.08-.48-1.2-.47-1.08-.36-1.07-.48-1.08-.47-1.2-.36-1.08-.48-1.08-.47 1.08-.36 1.08-.48 1.2-.47 1.08-.36 1.07-.47 1.08-.48 1.2-.47 1.08-.36 1.07-.48 1.09-.48 1.07-.35 1.2-.48 1.08-.48 1.08-.35 1.07-.48 1.08-.47 1.2-.48 1.08-.35 1.08-.48 1.07-.48 1.2-.36 1.08-.47 1.08h20.11l-.5-1.08-.36-1.08z"
                                ></path>
                                <path
                                  d="M588.4 0H58.77C15.84 0 0 15.84 0 58.79V577a133.49 133.49 0 00.63 13.56c1 9.38 1.16 18.46 9.88 28.8.85 1 9.75 7.64 9.75 7.64 4.79 2.35 8.06 4.08 13.46 6.25l260.79 109.29c13.54 6.2 19.2 8.62 29 8.43h.08c9.83.19 15.5-2.23 29-8.43l260.86-109.27c5.4-2.17 8.67-3.9 13.46-6.25 0 0 8.9-6.62 9.75-7.64 8.72-10.34 8.9-19.42 9.88-28.8a133.49 133.49 0 00.63-13.56V58.79C647.17 15.84 631.32 0 588.4 0zM356.61 99.8h43.8v287.93h-43.8zm-19.08 392.14l.59.95.6-.95.6-1.07.71-1 .59-1.07.6-1 .59-1.08.71-1 .6-1.07.6-1 .59-.95.72-1.07.59-1 .6-1.07.59-1 .71-1.07.6-1 .6-1.07.59-1 .6-1 .71-1.07.59-1 .6-1.08.59-1 .72-1.06.59-1 .6-1 .6-1.07.71-.95.59-1.08.59-1 .6-1.07.72-1 .59-1.08.6-.95h24.39v87l-1.19-.21h-21.78v-51.33l-.59.95-.71 1.09-.6.95-.71 1-.6 1.07-.72 1-.59 1-.59 1.08-.71 1-.6.95-.72 1-.59 1.08-.72 1-.59 1-.6 1.08-.71 1-.59 1-.72 1.07-.59 1-.72 1-.59 1.08-.6 1-.71 1-.59 1.08-.72 1-.59 1-.72 1-.6 1.07-.59 1-.71 1-.6 1.08-.71.95-.6 1-.71 1.08-.6 1h-.47l-.72-1.08-.59-1-.72-1.08-.59-1-.71-1.08-.6-1-.71-1.08-.6-1-.72-1.07-.59-1-.71-1.08-.59-1-.72-1.07-.6-1-.71-1.08-.6-1-.71-1.07-.71-1-.6-1.08-.71-1-.6-1.07-.71-1-.6-1.08-.71-1-.6-1.08-.71-.95-.59-1.08-.72-1-.59-1.08-.72-1-.6-1.08-.71-.95-.6-1.09-.7-1v51.21h-22.62v-87h24.4l.59.95.6 1.08.71.95.6 1.07.6 1 .59 1.08.59.95.72 1.07.59 1 .6 1 .59 1.06.6 1 .71 1.08.6 1 .59 1.07.59 1 .72 1 .6 1.07.59 1 .6 1.07.59 1 .72 1.07.59 1 .59 1.07.6.95.59 1 .72 1.07.6 1 .59 1.08.59 1 .6 1.07.71 1zM214.87 99.8h68.8c35.62 0 53.22 17.68 53.22 53.47v76.51c0 35.79-17.6 53.47-53.22 53.47h-25v104.48h-43.8zm-119.39 0h97.43v39.89h-53.64v81.41h51.58V261h-51.58v86.83h54.45v39.9H95.48zm82.9 433.29l-.83.72-1 .71-.83.6-.95.72-1 .6-1 .71-1 .6-1 .6-1.07.6-1.07.59-1.07.6-1.08.47-1.07.6-1.18.48-1.07.48-1.2.47-1.07.48-1.07.36-1.07.36-1.07.36-1.19.36-1.07.36-1.19.24-1.07.24-1.19.23-1.2.24-1.18.12-1.31.12-1.19.12-1.31.12-1.19.12-1.31.12h-5.12l-1.19-.12-1.31-.12-1.19-.12-1.18-.12-1.2-.12-1.19-.24-1.19-.24-1.06-.23-1.2-.24-1.07-.36-1.19-.24-1.07-.36-1.07-.48-1.07-.36-1.19-.48-1.07-.47-1.07-.48-1.07-.48-1.08-.6-1-.59-1.07-.6-1-.59-1-.6-1-.72-1-.71-1-.72-.83-.72-.83-.72-.84-.83-.83-.72-.84-.84-.83-.83-.71-.84-.71-1-.72-.83-.6-1-.71-1-.6-1-.59-1-.27-.87-.6-1-.47-1-.45-1.07-.48-1.08-.47-1.07-.36-1-.48-1.08-.21-1.07-.35-1.08-.24-1.19-.41-1.08v-1.18l-.16-1.1-.24-1.2-.12-1.07-.12-1.2-.12-1.19-.11-1.2v-3.94l.11-1.32v-1.19l.12-1.2.12-1.31.24-1.19.12-1.2.23-1.07.36-1.2.24-1.19.35-1.08.36-1.19.36-1.08.48-1.2.47-1.07.48-1.08.48-1.07.47-1.08.6-1.08.59-.95.59-1 .6-.95.6-1 .71-1 .72-.95.71-1 .71-.84.83-.83.72-.84.83-.84.84-.83.83-.72 1-.84.83-.71 1-.72 1-.6 1-.72 1-.59.94-.6 1.07-.6 1.08-.6 1.07-.59 1.07-.48 1.07-.48 1.19-.6 1-.36 1.19-.36 1.07-.47 1.07-.24 1.07-.36 1.2-.24 1.06-.36 1.19-.24 1.19-.12 1.2-.24 1.19-.12 1.18-.12 1.19-.12 1.2-.12h5.11l1.31.12 1.31.12h.95l1.19.12 1.31.24 1.07.12 1.19.24 1.19.24 1.07.24 1.07.24 1.07.24 1.08.36 1.07.23.95.36 1.07.36 1.07.48 1.07.48 1.08.48 1.07.59 1.06.48 1 .6 1.07.6 1 .59 1.07.6 1 .72 1 .6 1 .72 1 .71.95.84 1 .72-.71.95-.83.84-.72 1-.83 1-.72 1-.71.83-.84 1-.7 1-.72.84-.83 1-.72 1-.83.95-.72.84-.71 1-.83.95-.71 1-.84.84-.71 1-1-.72-1-.83-.95-.6-1-.72-1-.59-1-.6-1-.6L154 480l-.95-.48-1-.47-1-.36-1.07-.36-1-.33-1.08-.24-1.19-.24-1.19-.24-1.19-.12L143 477l-1.32-.11h-2.49L138 477l-1.08.12-1.19.24-1.07.24-1.07.36-1.07.36-1.07.48-1.07.47-.95.6-1 .6-1 .6-.83.72-.84.71-.83.72-.71.84-.83.83-.72 1-.59.84-.6.95-.59 1.08-.6.95-.47 1.08-.36 1.08-.47 1.07-.36 1.2-.24 1.19-.24 1.07-.24 1.32-.12 1.19-.11 1.2v2.75l.11 1.19.12 1.08.12 1.19.24 1.08.24 1.08.24 1.07.36 1.08.35.95.48 1.2.47 1.07.59 1.08.6 1 .6 1 .71 1 .72.84.83.83.71.84.83.72 1 .71 1 .72 1 .6.95.6 1.07.47 1.07.48 1.08.48 1.19.36 1.18.36 1.2.24 1.19.24 1.19.12 1.3.12h2.74l1.31-.12 1.31-.12 1.19-.12 1.19-.24 1.19-.24 1.19-.36 1.07-.24 1.07-.48 1-.48 1.07-.47.83-.48.95-.6v-11h-17.42v-17.46h39.27zm9.33 10.78l.47-1.08.48-1.07.47-1.2.36-1.07.48-1.07.48-1.08.47-1.07.48-1.08.48-1.19.47-1.08.48-1.07.35-1.08.48-1.08.47-1.07.48-1.2.48-1.07.47-1.08.48-1.08.48-1.07.36-1.08.47-1.19.47-1.08.48-1.07.47-1.08.48-1.08.48-1.07.47-1.2.36-1.07.48-1.08.48-1.07.47-1.08.48-1.08.47-1.19.48-1.08.47-1.07.36-1.08.48-1.07.47-1.08.48-1.2.48-1.07.47-1.08.48-1.07.47-1.08.36-1.08.48-1.06.47-1.2.48-1.07.47-1.08.48-1.08.48-1.07.47-1.08.36-1.19.48-1.08.47-1.08.48-1.07.47-1.08.48-1.07.47-1.2.48-1.07.36-1.08.48-1.08.47-1.07.48-1.08.47-1.19.48-1.08.47-1.07.48-1.08.35-1.08.48-1.07.48-1.2.48-1.07.47-1.08.48-1.08.48-1.07.47-1.08.36-1.19.47-1.08.48-1.07.47-1.08h22l.48 1.08.48 1.07.47 1.08.47 1.19.36 1.08.47 1.07.48 1.08.48 1.08.48 1.07.47 1.2.48 1.07.48 1.08.35 1.08.48 1.07.47 1.08.48 1.19.5 1.09.48 1.07.48 1.08.47 1.08.36 1.07.48 1.2.47 1.07.48 1.08.47 1.07.48 1.08.47 1.08.48 1.19.36 1.08.47 1.07.48 1.08.48 1.08.48 1.07.47 1.2.48 1.06.47 1.08.36 1.08.47 1.07.48 1.08.47 1.07.48 1.2.48 1.08.48 1.07.47 1.08.36 1.07.47 1.08.48 1.19.47 1.08.48 1.08.48 1.07.47 1.08.48 1.07.36 1.2.47 1.07.48 1.08.48 1.08.47 1.07.48 1.08.47 1.19.48 1.08.35 1.07.48 1.08.48 1.08.47 1.07.48 1.2.48 1.07.47 1.08.48 1.08.36 1.07.47 1.08.47 1.19.48 1.08.48 1.07.47 1.08.48 1.07.48 1.07.36 1.2.47 1.07.48 1.08.43 1.12h-24.52l-.48-1.08-.36-1.08-.47-1.07-.48-1.2-.35-1.07-.48-1.07-.48-1.08-.38-1.15-.48-1.08-.48-1.07-.35-1.08-.48-1.19-.48-1.08-.35-1.08-.48-1.07h-34.12l-.48 1.07-.3 1.08-.48 1.08L216 533l-.35 1.08-.48 1.07-.48 1.08-.36 1.07-.47 1.08-.48 1.07-.35 1.07-.48 1.2-.47 1.07-.36 1.08-.48 1.08h-24zm133.83 146.42L198.8 648.08h250.74zm149.11-145.37h-69.38v-87h68.79v19.72H424v14.23h41.41v18.52H424v14.81h46.65zm-47.09-208.19V150.8c0-35.79 17.61-53.47 53.22-53.47h21.31c35.61 0 52.8 17.28 52.8 53.06v58.82h-43v-56.35c0-11.52-5.32-16.87-16.38-16.87h-7.36c-11.46 0-16.79 5.35-16.79 16.87v181.82c0 11.52 5.33 16.86 16.79 16.86h8.19c11 0 16.37-5.34 16.37-16.86v-65h43v67.05c0 35.79-17.6 53.47-53.21 53.47h-21.7c-35.63 0-53.24-17.68-53.24-53.47zm129.68 183.1l-.32 1.17-.12 1.19-.12 1.32-.24 1.19-.35 1.2-.24 1.07-.36 1.2-.47 1.07-.48 1-.47 1.08-.6 1-.6 1-.71 1-.6 1-.71.84-.85.84-.71.71-1 .84-.83.72-1 .71-.94.59-1 .72-1.08.6-1.07.48-1.07.59-1.19.48-1.19.48-1.19.36-1.19.36-1.07.36-1.19.24-1.07.24-1.07.23-1.14.29-1.19.12-1.07.12-1.2.12-1.3.12-1.19.12h-5l-1.19-.12h-1.31l-1.19-.12-1.19-.12-1.31-.12-1.19-.24-1.19-.12-1.19-.23-1.09-.53-1.2-.2-1.19-.24-1.19-.36-1.07-.24-1.19-.36-1.19-.36-1.07-.48-1.19-.35-1.07-.36-1.19-.48-1.08-.48-1.06-.48-1.07-.59-1.08-.48-1.06-.54-.95-.6-1.07-.6-.95-.6-1-.59-.95-.72-.95-.72-1-.72-.95-.71-.84-.72-1-.84.72-1 .83-.84.71-1 .84-1 .71-.84.71-1 .84-1 .71-.84.84-1 .71-.84.83-1 .71-1 .72-.84.83-1 .72-1 .83-.84.71-1 .95.72 1.07.72 1 .72 1.07.71.95.6 1.07.72.95.6 1.08.47.95.6 1.07.48 1.07.48 1.07.48 1.07.35 1.08.48 1.18.36 1.19.36 1.07.36 1.2.24 1.18.24 1.19.24 1.31.12 1.2.23h1.18l1.31.12h2.62l1.31-.12 1.19-.23 1.07-.24 1.07-.24 1-.36.84-.48 1-.72.71-.83.48-1 .36-1.08.12-1.19v-.24l-.12-1.44-.48-1.19-.6-.84-.83-.71-1-.72-1-.48-1.07-.48-1.2-.48-1.42-.59-.84-.24-.94-.24-1.08-.36-1.07-.24-1.19-.36-1.19-.24-1.31-.36-1.19-.23-1.31-.36-1.18-.24-1.2-.36-1.19-.24-1.19-.36-1.07-.36-1.19-.24-1.07-.36-1.07-.35-1.07-.36-1.31-.48-1.19-.48-1.19-.48-1.08.06-1.07-.59-1.19-.6-1-.6-1.07-.6-.95-.71-1-.6-.83-.72-.84-.83-.83-.84-.72-.84-.7-.84-.6-.83-.59-1-.6-1.07-.48-.83-.36-1-.35-1.08-.36-.95-.24-1.2-.22-1.07-.12-1.2-.12-1.19-.12-1.32v-2.75l.12-1.19.12-1.08.12-1.2.24-1.07.24-1.08.36-1.07.24-1.08.47-1.07.48-1.08.48-1.07.59-1.08.59-1.08.71-.95.72-1 .83-1 .84-.83.83-1 1-.84.82-.59 1-.72.95-.72.95-.6 1.08-.59 1.06-.48 1.07-.6 1.08-.48 1.19-.36 1.18-.47 1.08-.36.95-.24 1.07-.24 1.08-.24 1.18-.24 1.07-.24 1.19-.12 1.2-.12 1.18-.12 1.19-.12h5.12l1.31.12 1.31.12h1.19l1.31.12 1.19.24 1.38.08 1.19.24 1.07.24 1.19.24 1.19.24 1.07.24 1.19.35 1.07.36 1.07.24 1.19.48 1.07.36 1.07.48 1.07.48 1.2.47 1.07.6.94.48 1.08.6 1.07.59 1 .6 1.07.72 1 .6 1 .72 1 .71 1 .72-.71 1-.6.95-.71 1-.72 1-.71 1.07-.59 1-.72 1-.71 1-.6 1-.71 1-.72 1-.6 1-.7 1.08-.72 1-.71.95-.6 1-.71.95-1-.71-1.06-.6-1-.6-1-.71-1.07-.48-1-.6-1.06-.48-1-.48-1-.74-1-.47-1.07-.36-1-.48-1.31-.36-1.19-.36-1.19-.35-1.19-.24-1.19-.24-1.19-.24-1.19-.12-1.19-.12-1.07-.12h-2.62l-1.31.24-1.19.24-1.07.24-1 .47-.84.48-1.07 1-.71 1.07-.47 1.08-.12 1.2v.24l.12 1.55.59 1.31.47.72.83.84 1.08.6 1 .59 1.19.48 1.31.48 1.43.48 1 .24 1 .35 1.02.2 1.07.36 1.19.24 1.32.36 1.3.36 1.31.36 1.31.24 1.19.36 1.31.35 1.19.24 1.19.36 1.19.36 1.07.36 1.19.36 1.07.36 1.1.33 1.31.48 1.19.59 1.19.48 1.19.6 1.08.6 1.07.59.94.6 1 .72 1 .6 1 .83 1 .84.82 1 .84.83.71 1 .72 1.08.59.95.6 1.08.48.95.35 1.08.36 1.08.23 1.07.24 1.2.24 1.19.12 1.2.12 1.31z"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Playstation 3' || $game[0]->name == 'Playstation 4' || $game[0]->name == 'Playstation 5')
                                <div title="playstation 4" class="sc-1oex0f2-0 kzRKQU">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 50 38"
                                    width="50"
                                    height="38"
                                  >
                                    <path
                                      d="M49.56 29.1a1.69 1.69 0 00.18-2 6.11 6.11 0 00-3.47-2.14 23.16 23.16 0 00-5.56-1.29 23.88 23.88 0 00-5.73 0 29.29 29.29 0 00-6.6 1.46v4.79l8.68-3a8.63 8.63 0 012.56-.51 6.42 6.42 0 012.35.21q.9.3.78.81a1.65 1.65 0 01-1.26.86l-13.11 4.58v4.62l17.8-6.25 1.3-.6a6.44 6.44 0 002.08-1.54zm-19.44-7.87a7.43 7.43 0 004.56.81 4.85 4.85 0 003.25-2.23 8.94 8.94 0 001.22-5c0-3.25-.68-5.74-2-7.45s-3.79-3.17-7.2-4.37a88.59 88.59 0 00-11.29-3v35.53l8.16 2.47V8.3a2.67 2.67 0 01.48-1.71 1 1 0 011.17-.34c1.1.28 1.65 1.31 1.65 3.08v11.9zM3.73 32.35a26.39 26.39 0 0013.37.86v-4.22l-4 1.45a8.63 8.63 0 01-2.56.51 6.16 6.16 0 01-2.34-.21c-.61-.2-.87-.47-.79-.81a1.65 1.65 0 011.26-.86l8.42-3v-4.59L5.47 25.59a15 15 0 00-4.52 2.06 1.93 1.93 0 00-1 1.58 2.41 2.41 0 001 1.75 6.91 6.91 0 002.78 1.37z"
                                      fill-rule="evenodd"
                                    ></path>
                                  </svg>
                                </div>
                            @elseif($game[0]->name == 'GOG COM')
                            <div title="gog com" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 48"
                                width="50"
                                height="48"
                              >
                                <path
                                  d="M14.9 30.82H9.08a.83.83 0 00-.82.83v4.19a.83.83 0 00.82.84h5.82v2.79H7.64a2.16 2.16 0 01-2.15-2.16v-7.13A2.15 2.15 0 017.64 28h7.26zM17 23a2.18 2.18 0 01-2.17 2.2H5.49v-2.79h7.87a.85.85 0 00.83-.85V12.2a.83.83 0 00-.83-.84H9.13a.85.85 0 00-.84.85v4.26a.84.84 0 00.83.84h3.39v2.83H7.66A2.19 2.19 0 015.49 18v-7.28a2.19 2.19 0 012.17-2.2h7.16a2.2 2.2 0 012.18 2.2zm27.51 16.47h-2.77v-8.65H39.8a.81.81 0 00-.82.83v7.82h-2.77v-8.65h-2a.82.82 0 00-.82.83v7.82h-2.72v-9.29A2.15 2.15 0 0132.82 28h11.69v11.47zm0-16.43a2.18 2.18 0 01-2.17 2.2H33v-2.83h7.87a.85.85 0 00.84-.85V12.2a.84.84 0 00-.84-.84h-4.23a.85.85 0 00-.83.85v4.26a.85.85 0 00.83.85H40v2.83h-4.82A2.19 2.19 0 0133 18v-7.28a2.19 2.19 0 012.17-2.2h7.17a2.19 2.19 0 012.17 2.2V23zM27.94 12.21v4.25a.84.84 0 01-.82.85h-4.23a.84.84 0 01-.84-.84V12.2a.84.84 0 01.83-.84h4.23a.83.83 0 01.83.83zm.64-3.68h-7.16a2.18 2.18 0 00-2.18 2.19V18a2.2 2.2 0 002.17 2.2h7.17a2.2 2.2 0 002.18-2.2v-7.28a2.19 2.19 0 00-2.17-2.2m-2.9 23.13v4.19a.82.82 0 01-.82.83H20.7a.82.82 0 01-.82-.83v-4.19a.82.82 0 01.82-.83h4.17a.83.83 0 01.82.84zm.62-3.65h-7a2.16 2.16 0 00-2.15 2.16v7.14a2.16 2.16 0 002.15 2.16h7a2.16 2.16 0 002.15-2.16v-7.12A2.15 2.15 0 0026.31 28m22.63 16.3a2.57 2.57 0 01-.76 1.83 2.53 2.53 0 01-1.82.76H3.64a2.53 2.53 0 01-1.82-.76 2.57 2.57 0 01-.76-1.83V3.67a2.59 2.59 0 012.58-2.6h42.72a2.59 2.59 0 012.58 2.6v40.66zm0-43.26A3.61 3.61 0 0046.36 0H3.64a3.63 3.63 0 00-2.57 1.07A3.68 3.68 0 000 3.67v40.66a3.68 3.68 0 001.07 2.6A3.63 3.63 0 003.64 48h42.72a3.58 3.58 0 002.57-1.08A3.64 3.64 0 0050 44.33V3.67a3.68 3.68 0 00-1.07-2.6"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Bethesda')
                            <div title="bethesda" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 116.95 117.24"
                                width="50"
                                height="28"
                              >
                                <g data-name="Warstwa 2">
                                  <path
                                    class="bethesda_svg__cls-1"
                                    d="M117 58.74c.27 32.11-26.53 58.85-59.12 58.5C26.24 116.9.13 90.74 0 59A58.72 58.72 0 0159.42 0C91.31.48 117.29 26.85 117 58.74zM79 89.06l6.82 6.41c1.16 1.11 1.71 1.13 2.83 0q3.5-3.51 7-7c1.29-1.31 1.23-2.11-.16-3.41-1.87-1.77-3.75-3.54-5.55-5.38a1.69 1.69 0 01-.3-1.47c1.55-3.9 3.2-7.76 4.78-11.65a1.4 1.4 0 011.52-1h8.28c1.64 0 2-.35 2-2v-9.63c0-1.75-.5-2.25-2.25-2.26h-8.14a1.17 1.17 0 01-1.3-.85c-1.72-4-3.46-8-5.27-12a1.18 1.18 0 01.34-1.61c2-1.9 4-3.84 6-5.75.83-.79 1.21-1.54.22-2.51C93.3 26.6 91 24.27 88.71 22c-1.36-1.34-1.91-1.31-3.27 0-1.85 1.85-3.75 3.67-5.54 5.58a1.32 1.32 0 01-1.84.28c-3.69-1.68-7.4-3.36-11.15-4.91-1.07-.44-1.53-.88-1.48-2.1.11-2.48 0-5 0-7.46 0-2-.44-2.42-2.5-2.43h-8.8c-2.11 0-2.59.49-2.6 2.67v7.46a1.59 1.59 0 01-1.12 1.81c-4 1.64-8 3.45-12 5.13a1.53 1.53 0 01-1.31-.34c-1.92-1.91-3.78-3.89-5.66-5.85-1.23-1.29-1.89-1.3-3.15 0l-6.58 6.65c-1.23 1.24-1.22 1.75.05 3 1.85 1.85 3.67 3.74 5.56 5.56a1.46 1.46 0 01.35 2c-1.77 3.86-3.44 7.76-5.09 11.66a1.27 1.27 0 01-1.39.93h-8.28c-1.7 0-2.05.34-2.06 2.08v9.64c0 1.84.36 2.18 2.26 2.19h8.14a1.79 1.79 0 011.3.78c1.82 4 3.54 8.11 5.34 12.15a1.11 1.11 0 01-.32 1.5c-2 1.9-4 3.83-5.94 5.75-1.13 1.12-1.14 1.84 0 3q3.56 3.56 7.12 7.07c1.17 1.16 1.78 1.13 2.95-.07 1.85-1.91 3.73-3.81 5.56-5.75a1.3 1.3 0 011.75-.35c3.77 1.62 7.55 3.22 11.37 4.74a1.51 1.51 0 011.11 1.7v7.74c0 2 .38 2.4 2.34 2.4h9.37c1.78 0 2.26-.46 2.27-2.22v-8.27a1.72 1.72 0 01.82-1.26c4.32-1.88 8.6-3.68 12.63-5.4z"
                                    data-name="Warstwa 1"
                                  ></path>
                                </g>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Rockstar Games')
                            <div title="rockstar games" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 129.38 118.34"
                                width="50"
                                height="50"
                              >
                                <g data-name="Warstwa 2">
                                  <g data-name="Warstwa 1">
                                    <path
                                      class="rockstar_svg__cls-1"
                                      d="M86.48 60.83c-.19.29-.32.47-.43.65l-6 10.14a.87.87 0 01-.88.49H69.11c2.1 2.21 4.07 4.3 6.07 6.37a.73.73 0 01.16 1c-1.41 3-2.79 5.93-4.19 8.9l-.89 1.94 1-.57 10.17-6.07a.63.63 0 01.86.05c1.62 1.28 3.26 2.54 4.89 3.8L91 90.39a1.79 1.79 0 000-.31c-.51-3.48-1-7-1.55-10.42a1.07 1.07 0 01.49-1.24c2.73-1.86 5.43-3.77 8.15-5.67.19-.13.37-.28.7-.53h-10.5c-.6-3.79-1.2-7.53-1.81-11.39zM66.25 32a14.18 14.18 0 00-3.94-.64c-2.85-.1-5.7 0-8.56 0a.6.6 0 00-.7.57c-.22 1.12-.46 2.22-.7 3.33-.5 2.39-1 4.79-1.54 7.33h8.95a29.55 29.55 0 003.4-.15c2.77-.36 5.51-1.6 5.89-5.52.21-2.21-.47-4.19-2.8-4.92z"
                                    ></path>
                                    <path
                                      class="rockstar_svg__cls-1"
                                      d="M129.35 21a25.28 25.28 0 00-.4-4.68 19.42 19.42 0 00-7-11.67A21 21 0 00108.22 0H22.77a37.22 37.22 0 00-5.48.4A19.89 19.89 0 006.46 5.72 19.81 19.81 0 000 20.75v77a21.3 21.3 0 00.42 4.29 19.66 19.66 0 008.3 12.59 18.72 18.72 0 0010.63 3.62c15.08.1 30.17 0 45.25 0h43.42a23.72 23.72 0 005.32-.53 20.19 20.19 0 0016-19.88q.06-38.36.01-76.84zm-23.69 49.37c-4.34 3-8.67 6.07-13 9.09a1.16 1.16 0 00-.6 1.29c.39 2.4.72 4.81 1.08 7.22s.69 4.77 1 7.16v.73l-3.27-2.5-8.71-6.7a.8.8 0 00-1.08-.07q-7.75 4.64-15.52 9.22a1.21 1.21 0 01-.67.2c.09-.22.17-.44.27-.66q3.6-7.71 7.22-15.44a.74.74 0 00-.13-1c-2.67-2.8-5.29-5.65-8-8.41a4.38 4.38 0 01-1.35-3c-.06-1.83 0-3.66.07-5.5 0-2.18.4-4.37-.1-6.54a4.65 4.65 0 00-4.46-3.83c-3.11-.12-6.24-.09-9.36-.15-.4 0-.46.23-.53.54-.64 3-1.29 6.09-1.94 9.13q-.75 3.54-1.49 7.08c-.08.37-.23.52-.62.52H32.81c-.14 0-.28 0-.52-.07.15-.77.28-1.52.44-2.25Q35.88 51.59 39 36.71q1.56-7.35 3.1-14.7a.75.75 0 01.87-.7h22.61a29.55 29.55 0 018.1.87 9.89 9.89 0 017.67 8.86 17.89 17.89 0 01-.45 7.36c-1.35 4.66-4.6 7.25-9.23 8.27l-.94.18c.17.08.27.15.38.19a5.58 5.58 0 014.08 5.24c.2 2.71.09 5.45.06 8.17s-.16 5.63 1 8.3a1.36 1.36 0 001.89.89c.2-.07.36-.32.48-.52l8.53-14.42a1.91 1.91 0 01.61-.68c.13.73.27 1.46.39 2.2l2 12.83c.09.57.3.76.92.76h15.01l.07.14c-.15.14-.31.29-.49.42z"
                                    ></path>
                                  </g>
                                </g>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'MOG Station')
                            <div title="mog station" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 119.01 119.01"
                                width="50"
                                height="28"
                              >
                                <path
                                  class="mog_station_svg__cls-1"
                                  d="M65.57 35c.81-.67 1.64-1.32 2.46-2l.21.2c-.94 1.5-1.89 3-3 4.77 1 .39 1.68.76 2.45 1a11.71 11.71 0 0010-20.82 11.53 11.53 0 00-14.64 1 12.11 12.11 0 00-1.48 15.31 2.52 2.52 0 004 .54zM59.52 67.76a5.48 5.48 0 105.41 5.59 5.51 5.51 0 00-5.41-5.59z"
                                ></path>
                                <path
                                  class="mog_station_svg__cls-1"
                                  d="M59.5 0A59.51 59.51 0 10119 59.5 59.5 59.5 0 0059.5 0zM35.62 40.49c4.42 1.58 7.12 5 10.21 8A22.91 22.91 0 0158 44.18a1.44 1.44 0 001-.58c.92-1.42 1.78-2.88 2.73-4.45-.31-.28-.62-.6-1-.89a13.91 13.91 0 01-4.35-15.09A15.09 15.09 0 0169 13.24c7.07-.56 13.17 2.69 15.85 8.44 4 8.69-1.82 18.79-11.63 20.12a16.48 16.48 0 01-9.17-1.32c-.2.32-.33.47-.42.65C62.27 44 62.39 44 65.38 45c2.64.92 5.16 2.22 7.91 3.43 2.79-2.83 5.61-6.14 10-8l3 13.8-.47.29a7.25 7.25 0 01-1-1.14c-1.13-1.91-2.2-3.85-3.31-5.76-.9-1.54-1.31-1.72-2.78-.67a44.1 44.1 0 00-3.94 3.44c-.46.41-.84.9-1.3 1.4-9.26-6.81-18.54-6.79-27.9-.1-.89-1-1.74-1.91-2.66-2.79a15.64 15.64 0 00-2.14-1.76c-1.88-1.27-2.32-1.14-3.45.84s-2.07 3.68-3.15 5.49a6.41 6.41 0 01-1 1l-.44-.26c.92-4.41 1.87-8.91 2.87-13.72zm51.93 51.4a11.59 11.59 0 01-5.7 3.53 17.92 17.92 0 01-1.93.27l-.38-.43A11.4 11.4 0 0181 93.43a22.46 22.46 0 008-12.71 16.41 16.41 0 00-3.63-14.44C80 59.7 70 61.9 65.94 68.05c-.26.4.09 1.41.43 2a8.27 8.27 0 01-4.17 12.28 8.5 8.5 0 01-9.2-2.7 8.14 8.14 0 01-.3-9.79 1.81 1.81 0 00-.13-2.64c-6.23-7.85-17.51-6.15-21.49 3.32-3 7.08-1.13 13.42 3.34 19.21a53.62 53.62 0 003.89 4 13.45 13.45 0 011.16 1.61l-.35.58a30.19 30.19 0 01-3.82-1.17 13.92 13.92 0 01-6.25-6c-4-6.85-5.06-14-1.85-21.44 2.47-5.73 6.6-9.27 13-9.57s10.6 3.25 13.61 8.51c.25.45.47.9.8 1.52a9 9 0 019.7.09c.38-.71.72-1.37 1.08-2 4.3-7.77 12.74-10.4 20-6.23A14.53 14.53 0 0190.61 65c5.95 9.93 2.39 21-3.06 26.89z"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'XBOX Series')
                            <div title="xbox series" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 212.73 300"
                                width="50"
                                height="50"
                              >
                                <g data-name="Layer 2">
                                  <path
                                    d="M121.38 227.06l15.32-21.15a41.21 41.21 0 01-14.22-6.2 15.67 15.67 0 01-6.2-13.86 15.22 15.22 0 016.93-13.49 33.6 33.6 0 0119.69-4.74c27 0 33.92 14.22 38.66 20l14.59-20A42.62 42.62 0 00187 157c-9.85-7.66-24.8-11.67-44.49-11.67-17.15 0-30.64 3.65-40.49 11.3a37.16 37.16 0 00-14.58 30.64c.03 20.1 11.33 32.86 33.94 39.79zm74.4-1.09c-6.93-6.2-17.14-10.58-31.36-14.23l-15.68 21.52a58.78 58.78 0 0122.61 9.48 15 15 0 016.56 13.5 18.55 18.55 0 01-7.66 16 36.86 36.86 0 01-22.25 5.51c-22.62 0-33.19-7.29-43.77-27l-15.67 21.17a46.07 46.07 0 0011.67 14.59c10.94 9.11 26.62 13.49 46.68 13.49 19 0 33.55-4 44.13-12a39.19 39.19 0 0015.68-33.56c1.46-10.57-2.92-21.15-10.94-28.44zM36.81.19a13.26 13.26 0 0111 5.15C53 14.91 53 26 47.12 34.79 44.18 38.47 39 40.68 34.61 40v-8.89c2.94 0 5.89-.73 7.36-2.94a19.66 19.66 0 002.21-8.84A26 26 0 0042.7 12c-1.47-1.47-2.94-3-5.15-2.21-1.47 0-3.68.74-4.42 2.21-1.47 2.21-2.21 5.15-2.94 8.1l-.74 4.42a24.25 24.25 0 01-5.15 11.75C22.09 39.21 18.41 40 14.73 40a14.54 14.54 0 01-10.31-4.47C2.21 30.38.74 26 .74 20.8s.73-10.3 4.41-14.72C8.1 2.4 12.52.92 16.93.92v8.84c-2.94.74-5.15 1.47-6.62 3.68S8.1 18.59 8.84 20.8a21.41 21.41 0 001.47 6.63c.73 1.47 2.94 2.21 4.42 2.21s3.68-.74 4.41-2.21a22 22 0 002.95-7.36l.73-4.42c0-4.42 2.21-8.1 4.42-11.78 3-2.95 5.89-4.42 9.57-3.68zm-9.57 264.33A26.67 26.67 0 0022.09 277l-.74 4.42a21.92 21.92 0 01-2.94 7.36c-.74 1.51-2.95 2.22-4.41 2.22s-3.68-.73-4.42-2.21c-1.48-2.19-1.48-4.39-1.48-6.6a14.86 14.86 0 012.21-8.1c1.47-1.48 3.68-2.95 5.89-2.95v-8.83a17.38 17.38 0 00-11.78 5.15A26.52 26.52 0 000 282.19c0 4.41.74 9.57 3.68 13.25A10.67 10.67 0 0014 299.86a14 14 0 009.57-3.68 24.26 24.26 0 005.16-11.78l.73-4.42a35.57 35.57 0 012.95-8.1c.73-1.47 2.94-2.21 4.41-2.21a6.7 6.7 0 015.18 2.21 8.56 8.56 0 011.47 7.36 19.66 19.66 0 01-2.21 8.84c-1.47 2.21-4.42 2.94-7.36 2.94v8.84c5.15 0 9.57-1.48 12.52-5.16a28.26 28.26 0 00.73-29.45 13.23 13.23 0 00-11-5.15 11.5 11.5 0 00-8.84 4.42zM50.8 146v9.57a13.8 13.8 0 01-5.89 1.43h-5.15a8.32 8.32 0 00-5.89 2.21 5.72 5.72 0 00-1.47 5.89v11.8h19.14v9.57H2.21v-24.3a16.25 16.25 0 012.94-9.57l-.73.74a1.58 1.58 0 011.47-1.48c2.21-2.94 5.89-4.41 10.31-4.41 5.15 0 10.31 2.94 11.78 8.09a13.3 13.3 0 013.68-5.15c2.21-1.47 4.42-1.47 6.63-1.47l7.36-.74c1.47-.73 3-.73 5.15-2.21zm-33.87 11a6.66 6.66 0 00-5.15 2.21 5.72 5.72 0 00-1.47 5.89v11.8H25v-12.52a8.32 8.32 0 00-2.21-5.89c-2.2-1.47-3.68-2.21-5.89-1.47zM2.21 126.83H50.8v-9.57H2.21zm8.1-67.74h-8.1v36.08H50.8V59.09h-8.1V85.6H30.19V61.3h-8.1v24.3H10.31zm0 147.26h-8.1v36.08H50.8v-36.08h-8.1v26.5H30.19v-24.29h-8.1v24.29H10.31zm67.32-67.48h30l22.46-31-15-20.41zM208.31.71h-29.95L160 26.23l15 20.42zm-96.65 0H81.72l101.06 138.16h29.95z"
                                    data-name="Layer 3"
                                  ></path>
                                </g>
                              </svg>
                            </div>
                            @elseif($game[0]->name == 'Nintendo')
                            <div title="nintendo" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 50.04"
                                width="50"
                                height="50"
                              >
                                <path
                                  d="M10.71.15v.11a53.23 53.23 0 017-.23h5.5a1.64 1.64 0 01.89.28q.11.18.11 24.72v20.88a19.22 19.22 0 01-.17 4 3.29 3.29 0 01-1.17.12h-5.35c-4.24-.08-6.62-.15-7.15-.23a13.07 13.07 0 01-6.64-3.57 13.53 13.53 0 01-3.51-6.59 18.21 18.21 0 01-.11-2.56l-.1-11.15v-12.5a18.64 18.64 0 01.39-3.8 12.8 12.8 0 013.78-6.19A13.41 13.41 0 0110.71.15zm9.38 24.87v-21h-4c-2.24 0-3.7 0-4.41.11a7.14 7.14 0 00-2.07.56A8.48 8.48 0 006.15 7.2a8.88 8.88 0 00-1.9 3.85q-.11.78-.11 14t.12 14a8.8 8.8 0 002.28 4.24 9.11 9.11 0 004.19 2.45c.45.08.93.15 1.45.22l7.92.12V25.02zM11.6 10.41a4.29 4.29 0 013.57.73 4.65 4.65 0 012 3.18 4.72 4.72 0 01-3.23 5.13 5.33 5.33 0 01-1.68.23 5.06 5.06 0 01-1-.06 3.15 3.15 0 01-.83-.39 4.62 4.62 0 01-2.18-2.21 3.74 3.74 0 01-.33-1.68 4.2 4.2 0 011.45-3.79 4.64 4.64 0 012.23-1.14zM29.24.15a39.71 39.71 0 014.63-.13 34.88 34.88 0 015.41.23 12.76 12.76 0 016.92 3.47 12.58 12.58 0 013.68 6.92q.13.78.13 14.38v.67q0 10.33-.07 12.24a10.41 10.41 0 01-.5 3.18l-.11.22a19 19 0 01-.78 1.79 12.8 12.8 0 01-4.08 4.63 12.16 12.16 0 01-5.86 2.17c-.45.07-2.07.11-4.86.11H29.4a19.89 19.89 0 01-.17-4.08V.15zm11.05 22.54a4.63 4.63 0 00-3.69.45 5 5 0 000 8.7 5 5 0 003.69.56 5 5 0 003.46-3.24 4.14 4.14 0 000-3.24 4.15 4.15 0 00-1.29-2.06 5.46 5.46 0 00-2.17-1.17z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>

                            @else
                            <div title="other" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 50 27.51"
                                width="50"
                                height="28"
                              >
                                <path
                                  d="M20 12.5a.67.67 0 00-.62-.62h-3.75V8.13A.68.68 0 0015 7.5h-2.5a.67.67 0 00-.62.63v3.75H8.13a.67.67 0 00-.63.62V15a.68.68 0 00.63.63h3.71v3.75a.67.67 0 00.62.62h2.5a.68.68 0 00.63-.62v-3.75h3.75a.68.68 0 00.62-.63zm18.75-5a3.06 3.06 0 11-2.23.9 3.06 3.06 0 012.23-.9zm-5 6.25a3.06 3.06 0 11-2.23.9 3.06 3.06 0 012.23-.91zm2.5-10a10 10 0 015 1.33 9.73 9.73 0 013.67 3.66 10.23 10.23 0 010 10.08 9.73 9.73 0 01-3.63 3.63 9.85 9.85 0 01-5 1.33 9.7 9.7 0 01-4.88-1.25 13.39 13.39 0 01-2.81-2.27l-.32-.23h-6.56l-.31.31a14.85 14.85 0 01-2.81 2.19 9.73 9.73 0 01-4.89 1.21 9.88 9.88 0 01-5-1.33 9.66 9.66 0 01-3.63-3.63 10.23 10.23 0 010-10.08 9.73 9.73 0 013.63-3.63 10 10 0 015-1.33zm0-3.75h-22.5A13.68 13.68 0 000 13.62v.12A13.68 13.68 0 0013.62 27.5h.13a13.6 13.6 0 005.16-1 13.39 13.39 0 004.3-2.76h3.63a13.39 13.39 0 004.3 2.77A13.76 13.76 0 1036.29-.01z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @endif
                            <!------------- category icon ------------------>
                          </div>
                        </div>
                        <div class="sc-1hk4sym-1 gXytPz">
                          <div class="sc-1hk4sym-2 kVuKms tooltip">Other</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="sc-1oex0f2-0 kzRKQU">
                      <div class="sc-1hk4sym-0 cuMaYk">
                        <div class="sc-1hk4sym-4 fZCBIE">
                          <div class="sc-9vx9ai-0 dQzvsF toggle">
                              
                              <!--country icon-->
                            @if($game[0]->iso == 'WW')  
                            <div
                              title="region free"
                              class="sc-1oex0f2-0 kzRKQU"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 41 41.02"
                                width="30"
                                height="30"
                              >
                                <path
                                  d="M0 20.51a20.5 20.5 0 0135-14.5l-1.25 1.17a18.79 18.79 0 00-32 13.33zm41 0a20.5 20.5 0 01-35 14.5l1.23-1.19a18.79 18.79 0 0032.06-13.31zM20.49 5.14a14.85 14.85 0 017.69 2.08 15.55 15.55 0 015.61 5.61 15.27 15.27 0 010 15.38 15.63 15.63 0 01-5.61 5.61 15.25 15.25 0 01-15.37 0 15.66 15.66 0 01-5.62-5.61 15.25 15.25 0 010-15.37 15.58 15.58 0 015.61-5.62 14.88 14.88 0 017.69-2.08zm12 9.43a13.26 13.26 0 00-3.2-4.19 12.64 12.64 0 00-4.64-2.57 17.12 17.12 0 012.72 6.76zm-6.51 6a32.93 32.93 0 00-.25-4H15.28a31.88 31.88 0 000 7.93H25.7a32.77 32.77 0 00.3-3.98zM20.49 7.13c-.86 0-1.75.66-2.66 2a15 15 0 00-2.17 5.46h9.67a15.15 15.15 0 00-2.17-5.48q-1.36-1.98-2.67-1.98zm-4.15.68a12.6 12.6 0 00-4.62 2.57 13.08 13.08 0 00-3.19 4.19h5.08a17.66 17.66 0 012.73-6.76zM7.1 20.52a13.11 13.11 0 00.62 4h5.58a32.44 32.44 0 01-.25-3.94 33.43 33.43 0 01.25-4H7.72a13 13 0 00-.62 3.94zm1.43 5.95a13.08 13.08 0 003.19 4.19 12.6 12.6 0 004.62 2.57 17.29 17.29 0 01-2.73-6.76zm12 7.44q1.31 0 2.67-2a15.22 15.22 0 002.17-5.46h-9.71a15 15 0 002.17 5.46c.91 1.34 1.79 2 2.66 2zm4.22-.68a12.68 12.68 0 004.55-2.57 13.11 13.11 0 003.2-4.19h-5.13a18.57 18.57 0 01-2.66 6.76zm3-8.74h5.58a13 13 0 000-7.94h-5.65a30.54 30.54 0 01.25 3.94 31.47 31.47 0 01-.25 4zm9.96-14.48L35 4.31l-3 3zm-34.42 21L6 36.71l3-3z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @elseif($game[0]->iso == 'ES' || $game[0]->iso == 'DE' || $game[0]->iso == 'FR' || $game[0]->iso == 'IT' || $game[0]->iso == 'PL' || $game[0]->iso == 'CH' || $game[0]->iso == 'SE' || $game[0]->iso == 'BE' || $game[0]->iso == 'FI' || $game[0]->iso == 'NO' || $game[0]->iso == 'IE')
                            <div title="europe" class="sc-1oex0f2-0 kzRKQU">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 36 36"
                                width="30"
                                height="30"
                              >
                                <path
                                  d="M18.01-.01a17.47 17.47 0 019 2.43 18.31 18.31 0 016.56 6.57 17.88 17.88 0 010 18 18.31 18.31 0 01-6.56 6.58 17.88 17.88 0 01-18 0 18.31 18.31 0 01-6.58-6.58 17.88 17.88 0 010-18 18.31 18.31 0 016.58-6.56 17.47 17.47 0 019-2.44zm14 11a15.45 15.45 0 00-3.74-4.9 14.83 14.83 0 00-5.4-3 20.32 20.32 0 013.19 7.9zm-14-8.67c-1 0-2.06.78-3.12 2.32a17.7 17.7 0 00-2.55 6.35h11.32a17.7 17.7 0 00-2.54-6.39c-1.06-1.5-2.11-2.28-3.11-2.28zm-4.86.8a14.8 14.8 0 00-5.41 3 15.71 15.71 0 00-3.73 4.87h5.93a20.87 20.87 0 013.2-7.91zM4.01 24.99a15.71 15.71 0 003.74 4.9 14.8 14.8 0 005.41 3 20.18 20.18 0 01-3.22-7.9zm14 8.71c1 0 2.06-.78 3.12-2.33a17.59 17.59 0 002.53-6.38H12.34a17.59 17.59 0 002.54 6.38c1.06 1.53 2.13 2.31 3.13 2.31zm4.94-.8a14.92 14.92 0 005.33-3 15.71 15.71 0 003.73-4.91h-5.95a21.84 21.84 0 01-3.12 7.91zM9.58 22.65H3.01a15.19 15.19 0 010-9.29h29.9a15.19 15.19 0 010 9.29zm3.12-8.06h4v1.3h-2.69v1.3h2.67v1.3h-2.67v1.3h2.67v1.3h-4v-6.5zm5.81 0h1.34v5.2h-1.34v-5.2zm1.34 5.2h2.66v1.3h-2.66v-1.3zm2.66-5.2h1.33v5.2h-1.33z"
                                  fill-rule="evenodd"
                                ></path>
                              </svg>
                            </div>
                            @else
                            <div title="row" class="sc-4n09jf-0 kEFYxa">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 36 39.35"
                                width="36"
                                height="39"
                              >
                                <defs>
                                  <style>
                                    .row_svg__a {
                                      fill: none;
                                    }
                                  </style>
                                </defs>
                                <path
                                  class="row_svg__a"
                                  d="M13.16 6.5a14.63 14.63 0 00-5.41 3 15.6 15.6 0 00-3.73 4.9h5.94a20.82 20.82 0 013.2-7.9zM23.68 14.41a17.89 17.89 0 00-2.54-6.39c-1.06-1.54-2.12-2.32-3.12-2.32s-2.06.78-3.12 2.32a17.89 17.89 0 00-2.54 6.39zM30.17 7.24a2.08 2.08 0 001.52-.63 2.12 2.12 0 000-3 2.08 2.08 0 00-1.52-.65 2.16 2.16 0 00-2.15 2.14 2.16 2.16 0 002.15 2.14zM11.02 22.21a2.06 2.06 0 00-1.47.59 1.92 1.92 0 00-.61 1.44 1.87 1.87 0 00.58 1.43 2.1 2.1 0 002.94 0 1.94 1.94 0 00.62-1.43 2 2 0 00-2.06-2z"
                                ></path>
                                <path
                                  class="row_svg__a"
                                  d="M16.02 28.35l1.06-2.33h7a37.9 37.9 0 00.29-4.64 38.07 38.07 0 00-.29-4.65H11.92c-.06.54-.16 1.47-.16 1.47l-2.32.07s.1-1 .16-1.54H3.07a15.19 15.19 0 000 9.29h1.87l1.08 2.33h-2a15.6 15.6 0 003.74 4.9 14.63 14.63 0 005.41 3c-.42-.59-1.15-1.93-1.15-1.93l1.44-2.36a26.11 26.11 0 001.46 2.77c1.1 1.54 2.1 2.32 3.1 2.32s2.06-.78 3.12-2.33a17.77 17.77 0 002.54-6.38zm-1-1.14c-.28.42-.85 1.24-1.72 2.46l-1.78 2.44a.6.6 0 01-.52.25.63.63 0 01-.52-.25c-.45-.65-1-1.46-1.74-2.44-.87-1.22-1.45-2-1.72-2.46a6.86 6.86 0 01-.81-1.54 4.35 4.35 0 01-.19-1.43 4.65 4.65 0 01.68-2.44 5.15 5.15 0 011.84-1.78 5.06 5.06 0 015 0 5.12 5.12 0 011.83 1.78 4.92 4.92 0 01.47 3.87 6.86 6.86 0 01-.82 1.54zM22.96 36.26a14.81 14.81 0 005.33-3 15.6 15.6 0 003.73-4.9h-6a21.7 21.7 0 01-3.06 7.9zM33.02 16.73h-6.58a37.42 37.42 0 01.29 4.61 38.56 38.56 0 01-.29 4.68h6.58a15.19 15.19 0 000-9.29z"
                                ></path>
                                <path
                                  d="M26.07 8.23c.29.45.88 1.32 1.77 2.6l1.8 2.57a.6.6 0 00.53.27.62.62 0 00.54-.27l1.82-2.53c.9-1.28 1.48-2.15 1.77-2.6a7.51 7.51 0 00.83-1.62 4.84 4.84 0 00.21-1.51 5 5 0 00-.69-2.57A5.29 5.29 0 0032.77.69a5.13 5.13 0 00-5.14 0 5.24 5.24 0 00-1.9 1.84 4.87 4.87 0 00-.71 2.57 4.84 4.84 0 00.21 1.51 7.75 7.75 0 00.84 1.62zm2.59-4.64a2 2 0 011.51-.63 2.08 2.08 0 011.52.63 2.12 2.12 0 010 3 2.08 2.08 0 01-1.52.63 2.16 2.16 0 01-2.15-2.12 2 2 0 01.64-1.51z"
                                ></path>
                                <path
                                  d="M33.59 12.38l-1.56 2h-6.01a20.18 20.18 0 00-3.2-7.91 7 7 0 011.2.87 13.39 13.39 0 01-.47-1.38 10.75 10.75 0 010-1.39 12.4 12.4 0 00-5.53-1.22 17.47 17.47 0 00-9 2.43 18.15 18.15 0 00-6.57 6.57 17.88 17.88 0 000 18 18.15 18.15 0 006.57 6.61 17.88 17.88 0 0018 0 18.15 18.15 0 006.57-6.57 17.88 17.88 0 000-18zM18.02 5.7c1 0 2.06.78 3.12 2.32a17.89 17.89 0 012.54 6.39H12.36a17.89 17.89 0 012.54-6.39c1.06-1.54 2.12-2.32 3.12-2.32zM7.75 9.51a14.63 14.63 0 015.41-3 20.87 20.87 0 00-3.2 7.91H4.02a15.49 15.49 0 013.73-4.91zm10.27 27.55c-1 0-2.06-.78-3.12-2.33a26.11 26.11 0 01-1.46-2.77l-1.42 2.37s.75 1.34 1.18 1.93a14.63 14.63 0 01-5.41-3 15.6 15.6 0 01-3.77-4.9h2l-1.1-2.33H3.07a15.19 15.19 0 010-9.29H9.6c-.06.52-.16 1.54-.16 1.54l2.32-.07s.1-.93.16-1.47h12.2a38.07 38.07 0 01.29 4.65 37.9 37.9 0 01-.29 4.64h-7l-1.1 2.32h7.65a17.77 17.77 0 01-2.54 6.38c-1.05 1.55-2.11 2.33-3.11 2.33zm10.27-3.81a14.81 14.81 0 01-5.33 3 21.77 21.77 0 003.12-7.91h6a15.64 15.64 0 01-3.79 4.91zm4.73-7.23h-6.58a38.56 38.56 0 00.29-4.68 37.42 37.42 0 00-.29-4.61h6.58a15.19 15.19 0 010 9.29z"
                                ></path>
                                <path
                                  d="M15.36 21.8a5.12 5.12 0 00-1.83-1.78 5 5 0 00-6.82 1.78 4.65 4.65 0 00-.69 2.44 4.35 4.35 0 00.21 1.43 6.86 6.86 0 00.79 1.54c.27.42.85 1.24 1.72 2.46.71 1 1.29 1.79 1.74 2.44a.63.63 0 00.52.25.6.6 0 00.52-.25l1.75-2.44c.87-1.22 1.44-2 1.72-2.46a6.86 6.86 0 00.81-1.54 4.92 4.92 0 00-.47-3.87zm-2.83 3.87a2.1 2.1 0 01-2.94 0 1.9 1.9 0 01-.57-1.43 2 2 0 01.55-1.44 2.14 2.14 0 012.94 0 2 2 0 01.62 1.44 1.92 1.92 0 01-.6 1.43z"
                                ></path>
                              </svg>
                            </div>

                            @endif
                            
                            <!--country icons-->
                            
                          </div>
                        </div>
                        <div class="sc-1hk4sym-1 gXytPz">
                          <div class="sc-1hk4sym-2 kVuKms tooltip">
                            Region Free
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="sc-4n09jf-0 kEFYxa">
                      <div class="sc-1hk4sym-0 cuMaYk">
                        <div class="sc-1hk4sym-4 fZCBIE">
                          <div class="rskl68-0 fCwDIU toggle">
                            <div class="rskl68-2 hpKIBi">
                              <!--<div class="rskl68-1 gPkvYn">-->
                              <!--  <svg-->
                              <!--    aria-hidden="true"-->
                              <!--    focusable="false"-->
                              <!--    data-prefix="fal"-->
                              <!--    data-icon="share-alt"-->
                              <!--    class="svg-inline--fa fa-share-alt fa-w-14"-->
                              <!--    role="img"-->
                              <!--    xmlns="http://www.w3.org/2000/svg"-->
                              <!--    viewBox="0 0 448 512"-->
                              <!--  >-->
                              <!--    <path-->
                              <!--      fill="currentColor"-->
                              <!--      d="M352 320c-28.6 0-54.2 12.5-71.8 32.3l-95.5-59.7c9.6-23.4 9.7-49.8 0-73.2l95.5-59.7c17.6 19.8 43.2 32.3 71.8 32.3 53 0 96-43 96-96S405 0 352 0s-96 43-96 96c0 13 2.6 25.3 7.2 36.6l-95.5 59.7C150.2 172.5 124.6 160 96 160c-53 0-96 43-96 96s43 96 96 96c28.6 0 54.2-12.5 71.8-32.3l95.5 59.7c-4.7 11.3-7.2 23.6-7.2 36.6 0 53 43 96 96 96s96-43 96-96c-.1-53-43.1-96-96.1-96zm0-288c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zM96 320c-35.3 0-64-28.7-64-64s28.7-64 64-64 64 28.7 64 64-28.7 64-64 64zm256 160c-35.3 0-64-28.7-64-64s28.7-64 64-64 64 28.7 64 64-28.7 64-64 64z"-->
                              <!--    ></path>-->
                              <!--  </svg>-->
                              <!--</div>-->
                            </div>
                          </div>
                        </div>
                        <div class="sc-1hk4sym-1 gXytPz">
                          <div class="sc-1hk4sym-2 kVuKms tooltip">
                            <a class="rskl68-3 gGiBjb">
                              <div>
                                <svg
                                  aria-hidden="true"
                                  focusable="false"
                                  data-prefix="fab"
                                  data-icon="twitter"
                                  class="svg-inline--fa fa-twitter fa-w-16"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 512 512"
                                >
                                  <path
                                    fill="currentColor"
                                    d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                                  ></path>
                                </svg>
                              </div>
                            </a>
                            <a class="rskl68-4 iPJkeY">
                              <div>
                                <svg
                                  aria-hidden="true"
                                  focusable="false"
                                  data-prefix="fab"
                                  data-icon="facebook-f"
                                  class="svg-inline--fa fa-facebook-f fa-w-10"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 320 512"
                                >
                                  <path
                                    fill="currentColor"
                                    d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
                                  ></path>
                                </svg>
                              </div>
                            </a>
                            <a class="rskl68-5 ekpARY">
                              <div>
                                <svg
                                  aria-hidden="true"
                                  focusable="false"
                                  data-prefix="fas"
                                  data-icon="link"
                                  class="svg-inline--fa fa-link fa-w-16"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 512 512"
                                >
                                  <path
                                    fill="currentColor"
                                    d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"
                                  ></path>
                                </svg>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <span id="wishlist_button">
                    <button class="wishlist_button" type="button">
                      <div class="sp24lz-1 fUpJtP">
                        <!--<div class="sp24lz-0 hviSEU">-->
                        <!--  <span element="span" class="fa-2x">-->
                        <!--    <svg-->
                        <!--      aria-hidden="true"-->
                        <!--      focusable="false"-->
                        <!--      data-prefix="fal"-->
                        <!--      data-icon="heart"-->
                        <!--      class="svg-inline--fa fa-heart fa-w-16"-->
                        <!--      role="img"-->
                        <!--      xmlns="http://www.w3.org/2000/svg"-->
                        <!--      viewBox="0 0 512 512"-->
                        <!--    >-->
                        <!--      <path-->
                        <!--        fill="currentColor"-->
                        <!--        d="M462.3 62.7c-54.5-46.4-136-38.7-186.6 13.5L256 96.6l-19.7-20.3C195.5 34.1 113.2 8.7 49.7 62.7c-62.8 53.6-66.1 149.8-9.9 207.8l193.5 199.8c6.2 6.4 14.4 9.7 22.6 9.7 8.2 0 16.4-3.2 22.6-9.7L472 270.5c56.4-58 53.1-154.2-9.7-207.8zm-13.1 185.6L256.4 448.1 62.8 248.3c-38.4-39.6-46.4-115.1 7.7-161.2 54.8-46.8 119.2-12.9 142.8 11.5l42.7 44.1 42.7-44.1c23.2-24 88.2-58 142.8-11.5 54 46 46.1 121.5 7.7 161.2z"-->
                        <!--      ></path>-->
                        <!--    </svg>-->
                        <!--  </span>-->
                        <!--</div>-->
                        <span class="sp24lz-2 bKAiEf">Add to the Wishlist</span>
                      </div>
                    </button>
                  </span>
                </div>
                <!------------------------>
                @if($game[0]->country == $ipCountry || $game[0]->iso == 'WW')
                <div class="sc-1lo74h5-5 kxxMuU">
                  <span class="fa fa-2x allow">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fal"
                      data-icon="check-circle"
                      class="svg-inline--fa fa-check-circle fa-w-16"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                    >
                      <path
                        fill="currentColor"
                        d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
                      ></path>
                    </svg>
                  </span>
                  
                  <span>Can be activated in - <strong>{{ $ipCountry }}</strong></span>
                  
                </div>
                @else
                <div class="sc-1lo74h5-5 kxxMuU">
                  <span class="fa fa-2x forbidden"
                      ><svg
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fal"
                        data-icon="ban"
                        class="svg-inline--fa fa-ban fa-w-16"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"modal-content modal-content2
                        viewBox="0 0 512 512"
                      >
                        <path
                          fill="currentColor"
                          d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zM103.265 408.735c-80.622-80.622-84.149-208.957-10.9-293.743l304.644 304.643c-84.804 73.264-213.138 69.706-293.744-10.9zm316.37-11.727L114.992 92.365c84.804-73.263 213.137-69.705 293.743 10.9 80.622 80.621 84.149 208.957 10.9 293.743z"
                        ></path></svg
                    ></span>
                  
                  <span style="color:red;">Cannot be activated in - <strong style="color:red;" >{{ $ipCountry }}</strong></span>
                </div>
                @endif
                <!------------------->
                <div class="sc-1lo74h5-5 kxxMuU">
                  <button type="button" data-toggle="modal" data-target="#myModal2">
                    <span>Check country restriction</span>
                  </button>
                </div>
                
                
                
    <div class="modal fade" id="myModal2" role="dialog" data-keyboard="false" data-backdrop="false">
        <div class="modal-dialog2">
          <!-- Modal content-->
          <div class="modal-content modal-content2">
            <div class="modal-header">
            <h3 class="text-center text-white">Product activation check</h3>
              <button type="button" class="close close_button_module text-white"  data-dismiss="modal" >&times;</button>
            </div>
            <div class="modal-body jwAxcm">
              <div class="modal_inr">
			  
				@if($game[0]->country == $ipCountry || $game[0]->iso == 'WW')
					
                <p><svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fal"
                      data-icon="check-circle"
                      class="svg-inline--fa fa-check-circle fa-w-16"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
					  style="color:green;"
                      viewBox="0 0 512 512"
                    >
                      <path
                        fill="currentColor"
                        d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
                      ></path>
                    </svg> This version of product can be activated in - <strong>{{ $ipCountry }}</strong></p> 
					@else
						
					<p><svg
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fal"
                        data-icon="ban"
                        class="svg-inline--fa fa-ban fa-w-16"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 512 512"
                      >
                        <path
                          fill="currentColor"
                          d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zM103.265 408.735c-80.622-80.622-84.149-208.957-10.9-293.743l304.644 304.643c-84.804 73.264-213.138 69.706-293.744-10.9zm316.37-11.727L114.992 92.365c84.804-73.263 213.137-69.705 293.743 10.9 80.622 80.621 84.149 208.957 10.9 293.743z"
                        ></path></svg
                    > This version of product cannot be activated in - <strong>{{ $ipCountry }}</strong></p>

					
					@endif
					
                <div class="row main_row">
                  <div class="col-md-8 col-sm-12"><h3>List of allowed countries for this product version:</h3></div>
                  <div class="col-md-4 col-sm-12">
                   <div class="srch_bar"><input type="text" class="sc-n6vpkd-0 driaIc" value=""><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="search" class="svg-inline--fa fa-search fa-w-16 fa fa-2x" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M508.5 481.6l-129-129c-2.3-2.3-5.3-3.5-8.5-3.5h-10.3C395 312 416 262.5 416 208 416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c54.5 0 104-21 141.1-55.2V371c0 3.2 1.3 6.2 3.5 8.5l129 129c4.7 4.7 12.3 4.7 17 0l9.9-9.9c4.7-4.7 4.7-12.3 0-17zM208 384c-97.3 0-176-78.7-176-176S110.7 32 208 32s176 78.7 176 176-78.7 176-176 176z"></path></svg></div></div>
                </div>
				
                <div class="row cuntry_lst">
                  <div class="col-4"><span>Afghanistan</span></div>
                  <div class="col-4"><span>American Samoa</span></div>
                  <div class="col-4"><span>Albania</span></div>
                  <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                  <div class="col-4"><span>Gibraltar</span></div>
                  <div class="col-4"><span>Guinea-Bissau</span></div>
                  <div class="col-4"><span>Latvia</span></div>
                  <div class="col-4"><span>Lebanon</span></div>
                  <div class="col-4"><span>Maldives</span></div>
                  <div class="col-4"><span>North Korea</span></div>
                  <div class="col-4"><span>Panama</span></div>
                  <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                  <div class="col-4"><span>Gibraltar</span></div>
                  <div class="col-4"><span>Guinea-Bissau</span></div>
                  <div class="col-4"><span>Latvia</span></div>
                  <div class="col-4"><span>Lebanon</span></div>
                  <div class="col-4"><span>Maldives</span></div>
                  <div class="col-4"><span>North Korea</span></div>
                   <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                  <div class="col-4"><span>Gibraltar</span></div>
                  <div class="col-4"><span>Guinea-Bissau</span></div>
                  <div class="col-4"><span>Latvia</span></div>
                  <div class="col-4"><span>Lebanon</span></div>
                  <div class="col-4"><span>Maldives</span></div>
                  <div class="col-4"><span>North Korea</span></div>
                  <div class="col-4"><span>Panama</span></div>
                  <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                   <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                  <div class="col-4"><span>Gibraltar</span></div>
                  <div class="col-4"><span>Guinea-Bissau</span></div>
                  <div class="col-4"><span>Latvia</span></div>
                  <div class="col-4"><span>Lebanon</span></div>
                  <div class="col-4"><span>Maldives</span></div>
                  <div class="col-4"><span>North Korea</span></div>
                  <div class="col-4"><span>Panama</span></div>
                  <div class="col-4"><span>Andorra</span></div>
                  <div class="col-4"><span>Antigua & Barbuda</span></div>
                  <div class="col-4"><span>Bahamas</span></div>
                  <div class="col-4"><span>Croatia</span></div>
                  <div class="col-4"><span>Ghana</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <!--<button type="button" class="btn btn-default close_button_module text-dark" data-dismiss="modal">-->
              <!--  Close-->
              <!--</button>-->
            </div>
          </div>
        </div>
      </div>
                
<style>
.modal_inr{
    width:100%;
}
.modal_inr p{
    background: #fff;
    font-size: 17px;
    font-weight: normal;
    border-radius: 10px;
    padding: 13px 10px 10px;
    color: #000;
}
.modal-content2 .modal-header h3{
    font-size: 23px;
    font-weight: normal;
    color: #fff !important;
}
.modal_inr p strong{
    color: #000;
}
.modal_inr svg{
    color: #EF4324;
    font-size: 25px;
    margin-right: 10px;
    margin-top: -1px;
    vertical-align: middle;
}
.modal-content2{
    padding: 0px 40px;     
}
.modal-content2 .modal-header{
    padding: 5px 0px 0px 0px !important;
}
.srch_bar{
    background: #fff;
    font-size: 17px;
    font-weight: normal;
    border-radius: 30px;
    padding: 10px 2px 10px 17px;
    color: #000;
    color: rgb(138, 138, 138);
}
.main_row{
    border-top: 1px solid #666;
    margin: 30px 0 0 0;
    padding: 25px 0 10px 0;
}
.main_row h3{
    font-size: 18px;
    font-weight: 600;
    color: #fff;
    margin: 0;
}
.cuntry_lst{
    margin: 30px 0 0 0; 
    overflow: auto; 
    height: auto; 
    max-height: 200px;
    font-size: 14px;
}
.modal-content2{
    margin-top: 5% !important;
}


.modal.fade.in .modal-content {
  bottom: 0;
  opacity: 1;
}

.modal-content {
  bottom: -250px;
  opacity: 0;
  -webkit-transition: opacity 0.2s ease-out, bottom 0.1s ease-out;
  -moz-transition: opacity 0.2s ease-out, bottom 0.1s ease-out;
  -o-transition: opacity 0.2s ease-out, bottom 0.1s ease-out;
  transition: opacity 0.2s ease-out, bottom 0.1s ease-out;
}
</style>
                <div id="main-offer-wrapper" class="c8br77-0 gdNfVj">
                  <div class="row">
                    <div class="c8br77-1 ePJrCe"></div>
                    <div class="sc-1kj1cv9-0 gfhTnO">
                      <div class="sc-1kj1cv9-1 eCdHtt">
                        <span class="sc-1kj1cv9-7 hUMNQb">
                          <div class="sc-1kj1cv9-8 dwKrQn">
                            <div class="sc-4n09jf-0 kEFYxa">
                              <div class="sc-1hk4sym-0 cuMaYk">
                                <span class="sc-1hk4sym-3 hpbwfT">
                                  <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fal"
                                    data-icon="info-circle"
                                    class="svg-inline--fa fa-info-circle fa-w-16"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                  >
                                    <path
                                      fill="currentColor"
                                      d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-36 344h12V232h-12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h48c6.627 0 12 5.373 12 12v140h12c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12h-72c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12zm36-240c-17.673 0-32 14.327-32 32s14.327 32 32 32 32-14.327 32-32-14.327-32-32-32z"
                                    ></path>
                                  </svg>
                                </span>
                                <div class="sc-1hk4sym-1 rmCeT">
                                  <div class="sc-1hk4sym-2 ikciGR tooltip">
                                    <div class="sc-1kj1cv9-9 cNhSjH">
                                      <span
                                        >This offer is promoted by the Merchant,
                                        and is not necessarily the cheapest
                                        one.</span
                                      >
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <span><span>Promoted offer</span></span>
                          </div>
                        </span>
                        <div class="sc-1kj1cv9-2 cSDLMn">
                          <span class="sc-1kj1cv9-5 hlDzFF"
                            ><span itemprop="priceCurrency" content="GBP"></span
                            ><span content="{{ $game[0]->sale_price }}"
                              >€{{ $game[0]->sale_price }}</span
                            ></span
                          >
                        </div>
                        <div id="cheapestOffer" class="xqlq5l-1 hpPFKa">
                          <strong></strong><span>offers, lowest price</span
                          ><strong>€{{ $game[0]->sale_price }}</strong>
                          <span class="xqlq5l-0 iqPcjp">
                            <svg
                              aria-hidden="true"
                              focusable="false"
                              data-prefix="fal"
                              data-icon="angle-double-down"
                              class="svg-inline--fa fa-angle-double-down fa-w-8"
                              role="img"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 256 512"
                            >
                              <path
                                fill="currentColor"
                                d="M119.5 262.9L3.5 145.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 223.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 263c-4.7 4.6-12.3 4.6-17-.1zm17 128l116-117.8c4.7-4.7 4.7-12.3 0-17l-7.1-7.1c-4.7-4.7-12.3-4.7-17 0L128 351.3 27.6 249.1c-4.7-4.7-12.3-4.7-17 0l-7.1 7.1c-4.7 4.7-4.7 12.3 0 17l116 117.8c4.7 4.6 12.3 4.6 17-.1z"
                              ></path>
                            </svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="ul9y11-0 gJySjd">
                      <div>
                        <!--<div class="ul9y11-1 jhxDfZ">-->
                        <!--   <div class="sc-1wii68x-0 gtcgmZ">-->
                        <!--      <span>Sold by</span>-->
                        <!--      <div class="sc-1wii68x-1 dTdslM">-->
                        <!--         <span>Oaghile Electronics and sales</span>-->
                        <!--         <div class="sc-1wii68x-2 bJZvSH"></div>-->
                        <!--      </div>-->
                        <!--   </div>-->
                        <!--   <div class="ul9y11-2 hhQBDJ"><span><strong>98.62%</strong> <span>of</span> <strong>3396</strong> <span>ratings are</span> <strong><span>superb</span>!</strong></span></div>-->
                        <!--</div>-->
                      </div>
                    </div>
                    <div color="#626262" class="sc-1y9yu05-2 dikeWh"></div>
                    <div class="sc-1lhgezq-0 fiJIJt">
                      <div
                        role="presentation"
                        style="display: flex; flex-grow: 1"
                      >
                        <button
                          class="sc-1lhgezq-1 fqHgjr addToCart"
                          rel="{{ $game[0]->uniqueId }}"
                        >
                          <span>Buy now</span>
                        </button>
                      </div>
                    </div>
                    <div class="c8br77-7 gtDatr">
                      <span
                        class="tip-icon"
                        data-tip="true"
                        data-for="secureTransaction"
                        currentitem="false"
                      >
                        <span class="c8br77-8 bmpQRr">
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="lock-alt"
                            class="svg-inline--fa fa-lock-alt fa-w-14"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512"
                          >
                            <path
                              fill="currentColor"
                              d="M224 420c-11 0-20-9-20-20v-64c0-11 9-20 20-20s20 9 20 20v64c0 11-9 20-20 20zm224-148v192c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V272c0-26.5 21.5-48 48-48h16v-64C64 71.6 136-.3 224.5 0 312.9.3 384 73.1 384 161.5V224h16c26.5 0 48 21.5 48 48zM96 224h256v-64c0-70.6-57.4-128-128-128S96 89.4 96 160v64zm320 240V272c0-8.8-7.2-16-16-16H48c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h352c8.8 0 16-7.2 16-16z"
                            ></path>
                          </svg>
                          <span>Your transaction is secure</span>
                        </span>
                      </span>
                      <div
                        class="__react_component_tooltip tce5899b0-93c0-41e5-a31c-32206becf3a7 place-bottom type-dark"
                        id="secureTransaction"
                        data-id="tooltip"
                      >
                        <style aria-hidden="true">
                          .tce5899b0-93c0-41e5-a31c-32206becf3a7 {
                            color: #fff;
                            background: #222;
                            border: 1px solid transparent;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-top {
                            margin-top: -10px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-top::before {
                            border-top: 8px solid transparent;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-top::after {
                            border-left: 8px solid transparent;
                            border-right: 8px solid transparent;
                            bottom: -6px;
                            left: 50%;
                            margin-left: -8px;
                            border-top-color: #222;
                            border-top-style: solid;
                            border-top-width: 6px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-bottom {
                            margin-top: 10px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-bottom::before {
                            border-bottom: 8px solid transparent;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-bottom::after {
                            border-left: 8px solid transparent;
                            border-right: 8px solid transparent;
                            top: -6px;
                            left: 50%;
                            margin-left: -8px;
                            border-bottom-color: #222;
                            border-bottom-style: solid;
                            border-bottom-width: 6px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-left {
                            margin-left: -10px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-left::before {
                            border-left: 8px solid transparent;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-left::after {
                            border-top: 5px solid transparent;
                            border-bottom: 5px solid transparent;
                            right: -6px;
                            top: 50%;
                            margin-top: -4px;
                            border-left-color: #222;
                            border-left-style: solid;
                            border-left-width: 6px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-right {
                            margin-left: 10px;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-right::before {
                            border-right: 8px solid transparent;
                          }

                          .tce5899b0-93c0-41e5-a31c-32206becf3a7.place-right::after {
                            border-top: 5px solid transparent;
                            border-bottom: 5px solid transparent;
                            left: -6px;
                            top: 50%;
                            margin-top: -4px;
                            border-right-color: #222;
                            border-right-style: solid;
                            border-right-width: 6px;
                          }
                          .modal-body {
                                     display: flex;
                                     flex-wrap: wrap;
                                      } 
                          .close_button_module {
                                     color: black;
                                     opacity: 1;
                                     margin-left: 90%;
                                  }
                                  .modal-header .close{
                                          margin: 16px;
                                  }
                         .span_font_style{
                             font-size:15px;
                         }
                         .modal-header{
                            border-bottom: 0 !important;
                         }
                         .modal-footer{
                            border-top: 0 !important;
                         }
                         .modal-content{
                             background-color: #333;
                             margin-top:10%;
                             border-radius: 30px; 
                         }
                    
                             .modal-dialog2{
                                 margin: auto;
                                 width:75%;
                             }
                             .heading_align{
                                 width:100%;
                             }
                             .modal-header{
                                 padding: 10px 0px 10px 0px !important; 
                             }
                             .modal-body{
                                 padding:0px;
                                 justify-content: center;
                             }
                             .imge_box{
                               background:#fff;  
                               margin: 0 0 20px 0;
                               border-radius: 18px;
                               padding: 10px 0;
                             }
                             .imge_box img{
                                 width:100%;
                             }
                        </style>
                        <span
                          >We're working hard to protect the security of your
                          payments. We have a dedicated Fraud Prevention and
                          Response Department working 24/7 to ensure safe
                          transactions. We have implemented custom systems to
                          screen out potential scammers and we have partnered
                          with Ravelin, one of the world's best fraud prevention
                          platforms. We don't share your payment details with
                          third-party sellers and we don't sell your information
                          with others.</span>
                      </div>
                    </div>
                    <div class="c8br77-6 fVnstf">
                      <button type="button" class="btn2 btn-info2 btn-lg" data-toggle="modal" data-target="#myModal">
                        <span class="span_font_style">Available payments</span>
                </button>
                <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="false" style="opacity: 1;">
        <div class="modal-dialog2">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
                <h3 class="text-center text-white heading_align">Available payment methods</h3>
              <button type="button" class="close close_button_module text-white"  data-dismiss="modal" >
                &times;
              </button>
            </div>
            <div class="modal-body jwAxcm">
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/bitcoin.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/dogecoin.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/ethereum.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/klarna.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/mastercard.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/paypal.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/swapxi.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/tron.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/usdt.png') }}" class="image_resize"></div>
              <div class="col-md-2 mx-3 imge_box"><img src="{{ URL('images/payment_images/visa.png') }}" class="image_resize"></div>
            </div>
            <div class="modal-footer">
              <!--<button type="button" class="btn btn-default close_button_module text-dark" data-dismiss="modal">-->
              <!--  Close-->
              <!--</button>-->
            </div>
          </div>
        </div>
      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!----- add to cart section end ---------------->
        </div>






        <!------------ comment section----------------------->
        <div class="sc-10w481k-1 iWnKBK my-5" style="display:none">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div id="reviews-list-container" class="sc-10w481k-0 dkFxJN">
                  <div class="sc-10w481k-2 hYKFKr">
                    <div class="sc-10w481k-4 ftXnzZ">
                      <div class="sc-10w481k-3 krsHQy">
                        <div>Product Rating</div>
                      </div>
                      <div class="sc-10w481k-5 cnMXjs">
                        <div class="sc-10w481k-6 bsZnoT">4.7</div>
                        <div class="sc-10w481k-7 fMUcTB">
                          <div class="sc-10w481k-8 cDNQXh">23</div>
                          <span>votes</span>
                        </div>
                      </div>
                    </div>
                    <div class="sc-10w481k-13 kifiQz">
                      <div class="sc-10w481k-12 fyPbdN">
                        <div class="sc-10w481k-14 logJmg">
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="badge-percent"
                            class="svg-inline--fa fa-badge-percent fa-w-16"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                          >
                            <path
                              fill="currentColor"
                              d="M349.66 173.65l-11.31-11.31c-3.12-3.12-8.19-3.12-11.31 0l-164.7 164.69c-3.12 3.12-3.12 8.19 0 11.31l11.31 11.31c3.12 3.12 8.19 3.12 11.31 0l164.69-164.69c3.13-3.12 3.13-8.18.01-11.31zM240 192c0-26.47-21.53-48-48-48s-48 21.53-48 48 21.53 48 48 48 48-21.53 48-48zm-64 0c0-8.83 7.19-16 16-16s16 7.17 16 16-7.19 16-16 16-16-7.17-16-16zm144 80c-26.47 0-48 21.53-48 48s21.53 48 48 48 48-21.53 48-48-21.53-48-48-48zm0 64c-8.81 0-16-7.17-16-16s7.19-16 16-16 16 7.17 16 16-7.19 16-16 16zm192-80c0-35.5-19.4-68.2-49.6-85.5 9.1-33.6-.3-70.4-25.4-95.5s-61.9-34.5-95.5-25.4C324.2 19.4 291.5 0 256 0s-68.2 19.4-85.5 49.6c-33.6-9.1-70.4.3-95.5 25.4s-34.5 61.9-25.4 95.5C19.4 187.8 0 220.5 0 256s19.4 68.2 49.6 85.5c-9.1 33.6.3 70.4 25.4 95.5 26.5 26.5 63.4 34.1 95.5 25.4 17.4 30.2 50 49.6 85.5 49.6s68.1-19.4 85.5-49.6c32.7 8.9 69.4.7 95.5-25.4 25.1-25.1 34.5-61.9 25.4-95.5 30.2-17.3 49.6-50 49.6-85.5zm-91.1 68.3c5.3 11.8 29.5 54.1-6.5 90.1-28.9 28.9-57.5 21.3-90.1 6.5C319.7 433 307 480 256 480c-52.1 0-64.7-49.5-68.3-59.1-32.6 14.8-61.3 22.2-90.1-6.5-36.8-36.7-10.9-80.5-6.5-90.1C79 319.7 32 307 32 256c0-52.1 49.5-64.7 59.1-68.3-5.3-11.8-29.5-54.1 6.5-90.1 36.8-36.9 80.8-10.7 90.1-6.5C192.3 79 205 32 256 32c52.1 0 64.7 49.5 68.3 59.1 11.8-5.3 54.1-29.5 90.1 6.5 36.8 36.7 10.9 80.5 6.5 90.1C433 192.3 480 205 480 256c0 52.1-49.5 64.7-59.1 68.3z"
                            ></path>
                          </svg>
                        </div>
                        <span
                          >Share your thoughts about this product and get 5% off
                          your next purchase</span
                        >
                      </div>
                      <button
                        class="sc-10w481k-10 iXwaSO"
                        id="addProductReview"
                      >
                        <div class="sc-10w481k-14 logJmg">
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="plus"
                            class="svg-inline--fa fa-plus fa-w-12"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 384 512"
                          >
                            <path
                              fill="currentColor"
                              d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"
                            ></path>
                          </svg>
                        </div>
                        <span>Add Product Review</span>
                      </button>
                    </div>
                  </div>

                  <div id="appendMoreComments">
                    @foreach ($comments as $com)
                    <div class="q423om-0 gaZkkV">
                      <div class="q423om-8 fCuxea col-sm-12 col-md-3">
                        <span>{{ $com->name }}</span
                        ><time datetime="1608174209969"
                          ><?php $date = date_create($com->createdDate); echo
                          date_format($date, 'd/m/Y'); ?></time
                        >
                      </div>
                      <div class="q423om-7 dyZTLB col-sm-12 col-md-9">
                        <div
                          class="q423om-6 cTsHDA col-sm-12 col-md-9 col-lg-10"
                        >
                          <span>{{ $com->comment }}</span>
                        </div>
                        <div
                          class="q423om-2 khnYpE col-sm-12 col-md-3 col-lg-2"
                        >
                          <div>
                            <div font-size="1.1" class="ln6fe9-0 iSiAcB">
                              <?php for($i=1;$i<=$com->stars;$i++) { ?>
                              <img
                                src="<?php echo URL('/images/star-rating-icon.png'); ?>"
                              />
                              <?php  }?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach

                    <div
                      class="pagination commentsPagination"
                      style="margin-left: 50%"
                    >
                      <?php echo $comments->links(); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!----- comment section--->

        <!----- other section ---->
        <div class="c-panel c-panel--fw recommended recommended_creep">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="sc-1cwd0lz-1 AJsKU othr_txt">
                  <span>Others you may like</span>
                </div>
				
				
				
				
				
                <!----            <div class="carousel slide media-carousel" id="media2">
            <div class="carousel-inner">
              <div class="item  active">
                <div class="row">
                  <?php $i = 0; foreach($other as $rec ){ ?>
                                    <div class="col-2 c-product-card" data-index="<?php echo $i;
$i++; ?>">
                                        <div>
                                            <div class="sc-4dil8t-1 gdwDyM">
                                                <?php
                                                $ex = explode(' ', $rec->title);
                                                $im = implode('-', $ex);
                                                ?>
                                                <input type="hidden" id="gameId" value="<?php echo $rec->uniqueId; ?>">
                                                <a class="sc-4dil8t-4 sc-4dil8t-5 ARZwg kMynjl" href="{{ url('showDetailPage/' . $rec->uniqueId . '/' . $im) }}">

                                                    <div class="sc-4dil8t-2 cCCTUX" style="background: url({{ '/images/' . $rec->thumbnail }}) center center / 100% 150px no-repeat;"></div>

                                                </a>
                                                <div class="sc-4dil8t-4 sc-4dil8t-6 ARZwg xFvoW">
                                                    <h3>
                                                        <a href="{{ url('showDetailPage/' . $rec->uniqueId . '/' . $im) }}">
                                                            {{ $rec->title }}
                                                        </a>
                                                    </h3>
                                                    <div class="sc-4dil8t-10 gAqARF">
                                                        <div title="steam" class="sc-1oex0f2-0 kzRKQU">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50" height="50">
                                                                <path
                                                                    d="M50.01 25.01a24.19 24.19 0 00-3.38-12.5 25.26 25.26 0 00-9.12-9.13A24.18 24.18 0 0025.01.01a24.2 24.2 0 00-12 3.07 25.73 25.73 0 00-9 8.32 24.06 24.06 0 00-4 11.61l13.4 5.52a7.42 7.42 0 014.44-1.21l5.94-8.67v-.1a9 9 0 012.78-6.65 9.48 9.48 0 0113.3 0 8.93 8.93 0 012.77 6.65 9.21 9.21 0 01-2.82 6.76 9 9 0 01-6.81 2.7l-8.47 6a7.14 7.14 0 01-7 7.36A6.85 6.85 0 0113 39.76a7.07 7.07 0 01-2.42-4l-9.67-3.9a24.18 24.18 0 004.94 9.32 24.46 24.46 0 008.52 6.5 24.87 24.87 0 0010.64 2.33 24.18 24.18 0 0012.5-3.37 25.26 25.26 0 009.12-9.13 24.18 24.18 0 003.38-12.5zm-34.28 12.9a3.83 3.83 0 003 0 3.87 3.87 0 002.12-2.12 3.58 3.58 0 000-3 4 4 0 00-2.07-2.07l-3.23-1.31a5 5 0 014 0 5.25 5.25 0 012.82 2.87 5 5 0 010 4 5.25 5.25 0 01-2.87 2.87 5 5 0 01-4 .05 5.41 5.41 0 01-2.87-2.62zm17.54-13.1a6.12 6.12 0 004.38-1.8 6.19 6.19 0 000-8.87 6.33 6.33 0 00-8.87 0 5.91 5.91 0 00-1.87 4.42 6 6 0 001.86 4.45 6.2 6.2 0 004.5 1.81zm0-1.51a4.52 4.52 0 01-3.38-1.41 4.74 4.74 0 010-6.66 4.46 4.46 0 013.33-1.41 4.74 4.74 0 013.48 8l-.11.11a4.57 4.57 0 01-3.32 1.4z"
                                                                    fill-rule="evenodd"
                                                                ></path>
                                                            </svg>
                                                        </div>
                                                        <div title="europe" class="sc-1oex0f2-0 kzRKQU">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36" width="30" height="30">
                                                                <path
                                                                    d="M18.01-.01a17.47 17.47 0 019 2.43 18.31 18.31 0 016.56 6.57 17.88 17.88 0 010 18 18.31 18.31 0 01-6.56 6.58 17.88 17.88 0 01-18 0 18.31 18.31 0 01-6.58-6.58 17.88 17.88 0 010-18 18.31 18.31 0 016.58-6.56 17.47 17.47 0 019-2.44zm14 11a15.45 15.45 0 00-3.74-4.9 14.83 14.83 0 00-5.4-3 20.32 20.32 0 013.19 7.9zm-14-8.67c-1 0-2.06.78-3.12 2.32a17.7 17.7 0 00-2.55 6.35h11.32a17.7 17.7 0 00-2.54-6.39c-1.06-1.5-2.11-2.28-3.11-2.28zm-4.86.8a14.8 14.8 0 00-5.41 3 15.71 15.71 0 00-3.73 4.87h5.93a20.87 20.87 0 013.2-7.91zM4.01 24.99a15.71 15.71 0 003.74 4.9 14.8 14.8 0 005.41 3 20.18 20.18 0 01-3.22-7.9zm14 8.71c1 0 2.06-.78 3.12-2.33a17.59 17.59 0 002.53-6.38H12.34a17.59 17.59 0 002.54 6.38c1.06 1.53 2.13 2.31 3.13 2.31zm4.94-.8a14.92 14.92 0 005.33-3 15.71 15.71 0 003.73-4.91h-5.95a21.84 21.84 0 01-3.12 7.91zM9.58 22.65H3.01a15.19 15.19 0 010-9.29h29.9a15.19 15.19 0 010 9.29zm3.12-8.06h4v1.3h-2.69v1.3h2.67v1.3h-2.67v1.3h2.67v1.3h-4v-6.5zm5.81 0h1.34v5.2h-1.34v-5.2zm1.34 5.2h2.66v1.3h-2.66v-1.3zm2.66-5.2h1.33v5.2h-1.33z"
                                                                    fill-rule="evenodd"
                                                                ></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer" class="sc-4dil8t-7 kWlmeI">
                                                        <span itemProp="priceCurrency" content="EUR"></span><span class="min" itemProp="lowPrice" content="26.18">€{{ $rec->sale_price }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php if($i % 6 == 0) {?>
                                    </div>
                                      </div>
                                      <div class="item">
                                        <div class="row">
                                    <?php }?>
                                    <?php } ?>
                                    
                                    
                  
                </div>
              </div>
              
            </div>
            <a data-slide="prev" href="#media2" class="left carousel-control">‹</a>
            <a data-slide="next" href="#media2" class="right carousel-control">›</a>
          </div>--->
		  
		  <div class="container">
	<div class="row">
		<div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
            <div class="MultiCarousel-inner">
                <div class="item">
                    @foreach($more_games as $gamelist)
                          <div class="main_swipr"> 							
                            <div class=" c-product-card">
                              <div class="sc-4dil8t-1 ivsAIb" itemscope="" itemtype="http://schema.org/Product">
                                 <a aria-label="Go to the product page of Orcs Must Die! 3 Steam CD Key" itemprop="url" class="sc-4dil8t-4 sc-4dil8t-5 ARZwg kMynjl" href="https://creepgame.com/showDetailPage/8xrA5BEIwf10/GOD-OF-WAR">
                                   <div class="sc-4dil8t-8 eocQGg">-<!-- -->67<!-- -->%</div>
                                   <div class="sc-4dil8t-2 cCCTUX" style="background: url(https://creepgame.com/images/{{ $gamelist->background_image }}) center center / 100% 150px no-repeat;"></div>
                                  </a>
                                  <div class="sc-4dil8t-4 sc-4dil8t-6 ARZwg xFvoW">
                                    <h3 itemprop="name" title="{{ $gamelist->title }}"><a href="#">{{ $gamelist->title }}</a></h3>
                                    <div class="sc-4dil8t-10 gAqARF">
                                      <div title="steam" class="sc-1oex0f2-0 kzRKQU">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50" height="50">
                                        <path d="M50.01 25.01a24.19 24.19 0 00-3.38-12.5 25.26 25.26 0 00-9.12-9.13A24.18 24.18 0 0025.01.01a24.2 24.2 0 00-12 3.07 25.73 25.73 0 00-9 8.32 24.06 24.06 0 00-4 11.61l13.4 5.52a7.42 7.42 0 014.44-1.21l5.94-8.67v-.1a9 9 0 012.78-6.65 9.48 9.48 0 0113.3 0 8.93 8.93 0 012.77 6.65 9.21 9.21 0 01-2.82 6.76 9 9 0 01-6.81 2.7l-8.47 6a7.14 7.14 0 01-7 7.36A6.85 6.85 0 0113 39.76a7.07 7.07 0 01-2.42-4l-9.67-3.9a24.18 24.18 0 004.94 9.32 24.46 24.46 0 008.52 6.5 24.87 24.87 0 0010.64 2.33 24.18 24.18 0 0012.5-3.37 25.26 25.26 0 009.12-9.13 24.18 24.18 0 003.38-12.5zm-34.28 12.9a3.83 3.83 0 003 0 3.87 3.87 0 002.12-2.12 3.58 3.58 0 000-3 4 4 0 00-2.07-2.07l-3.23-1.31a5 5 0 014 0 5.25 5.25 0 012.82 2.87 5 5 0 010 4 5.25 5.25 0 01-2.87 2.87 5 5 0 01-4 .05 5.41 5.41 0 01-2.87-2.62zm17.54-13.1a6.12 6.12 0 004.38-1.8 6.19 6.19 0 000-8.87 6.33 6.33 0 00-8.87 0 5.91 5.91 0 00-1.87 4.42 6 6 0 001.86 4.45 6.2 6.2 0 004.5 1.81zm0-1.51a4.52 4.52 0 01-3.38-1.41 4.74 4.74 0 010-6.66 4.46 4.46 0 013.33-1.41 4.74 4.74 0 013.48 8l-.11.11a4.57 4.57 0 01-3.32 1.4z" fill-rule="evenodd"></path>
                                        </svg>
                                      </div>
                                      <div title="region free" class="sc-1oex0f2-0 kzRKQU">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41 41.02" width="30" height="30">
                                         <path d="M0 20.51a20.5 20.5 0 0135-14.5l-1.25 1.17a18.79 18.79 0 00-32 13.33zm41 0a20.5 20.5 0 01-35 14.5l1.23-1.19a18.79 18.79 0 0032.06-13.31zM20.49 5.14a14.85 14.85 0 017.69 2.08 15.55 15.55 0 015.61 5.61 15.27 15.27 0 010 15.38 15.63 15.63 0 01-5.61 5.61 15.25 15.25 0 01-15.37 0 15.66 15.66 0 01-5.62-5.61 15.25 15.25 0 010-15.37 15.58 15.58 0 015.61-5.62 14.88 14.88 0 017.69-2.08zm12 9.43a13.26 13.26 0 00-3.2-4.19 12.64 12.64 0 00-4.64-2.57 17.12 17.12 0 012.72 6.76zm-6.51 6a32.93 32.93 0 00-.25-4H15.28a31.88 31.88 0 000 7.93H25.7a32.77 32.77 0 00.3-3.98zM20.49 7.13c-.86 0-1.75.66-2.66 2a15 15 0 00-2.17 5.46h9.67a15.15 15.15 0 00-2.17-5.48q-1.36-1.98-2.67-1.98zm-4.15.68a12.6 12.6 0 00-4.62 2.57 13.08 13.08 0 00-3.19 4.19h5.08a17.66 17.66 0 012.73-6.76zM7.1 20.52a13.11 13.11 0 00.62 4h5.58a32.44 32.44 0 01-.25-3.94 33.43 33.43 0 01.25-4H7.72a13 13 0 00-.62 3.94zm1.43 5.95a13.08 13.08 0 003.19 4.19 12.6 12.6 0 004.62 2.57 17.29 17.29 0 01-2.73-6.76zm12 7.44q1.31 0 2.67-2a15.22 15.22 0 002.17-5.46h-9.71a15 15 0 002.17 5.46c.91 1.34 1.79 2 2.66 2zm4.22-.68a12.68 12.68 0 004.55-2.57 13.11 13.11 0 003.2-4.19h-5.13a18.57 18.57 0 01-2.66 6.76zm3-8.74h5.58a13 13 0 000-7.94h-5.65a30.54 30.54 0 01.25 3.94 31.47 31.47 0 01-.25 4zm9.96-14.48L35 4.31l-3 3zm-34.42 21L6 36.71l3-3z" fill-rule="evenodd"></path>
                                        </svg>
                                      </div>
                                     </div>
                                     <div itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer" class="sc-4dil8t-7 kWlmeI">
                                       <!--<span itemProp="priceCurrency" content="EUR"></span><span class="original" itemProp="highPrice" content="24.99">20</span><span itemProp="priceCurrency" content="EUR"></span>-->
                                       <span class="min" itemprop="lowPrice" content="8.17">€23.00</span>
                                      </div>
                                    </div>
                                   </div>
                                  </div>
                          </div>  	
							@endforeach
                </div>
               
            </div>
            <button class="btn btn-primary leftLst"><</button>
            <button class="btn btn-primary rightLst">></button>
        </div>
	</div>
	
</div>
		  
		  
		  
                <div class="col-md-12 swiper_main_wrap swiper_main_wrap2">
                  <div data-slide="next" class="right swiper-button-prev">
                    ‹
                  </div>
                  <div data-slide="prev" class="left swiper-button-next">›</div>
                  
                  <div class="swiper_main">
                    <!-- Slider main container -->
                    <div class="swiper-container swiper-container2">
                      <!-- Additional required wrapper -->					
                      <div class="swiper-wrapper">
                        <div class="swiper-slide">
							
                        </div>
                      </div>					
	
                      <!-- If we need navigation buttons -->
                    </div>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-------------------------->

        <div class="container px-5">
          <div class="row">
            <div class="col-12">
              <div class="oicw95-0 jVapfN">
                <nav id="nav-tabs" class="sc-1osdtl1-0 jTpCVh">
                  <button
                    id="description"
                    class="sc-12l52l2-0 sc-1osdtl1-1 cIhhHJ eyyhHM showDiv my-1"
                  >
                    <span>Description</span>
                  </button>
                  <button
                    id="systemRequirements"
                    class="sc-12l52l2-0 sc-1osdtl1-1 cIhhHJ fWVJtg showDiv my-1"
                  >
                    <span>System requirements</span>
                  </button>
                  <button
                    id="activationDetails"
                    class="sc-12l52l2-0 sc-1osdtl1-1 cIhhHJ fWVJtg showDiv my-1"
                  >
                    <span>Key activation details</span>
                  </button>
                </nav>
                <div
                  id="productContent"
                  class="sc-1osdtl1-2 iuMoiE tab-description commonDiv"
                  style="display: block"
                >
                  <div class="react-reveal" style="opacity: 1">
                    <div class="roezsx-0 kdzOEp">
                      <div id="tabContent" class="roezsx-1 TfyBm">
                        <div>
                          <?php $desc = html_entity_decode($game[0]->description);
                          ?>
                          <div>
                            <?php echo $desc; ?>
                            <!--{{ $game[0]->description }}-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  id="productContent"
                  class="sc-1osdtl1-2 iuMoiE tab-systemRequirements commonDiv"
                  style="display: none"
                >
                  <div class="react-reveal" style="opacity: 1">
                    <div class="roezsx-0 kdzOEp">
                      <div id="tabContent" class="roezsx-1 TfyBm">
                        <div>
                          <?php $system = html_entity_decode($game[0]->system_requirements);
                          ?>
                          <div>
                            <?php echo $system; ?>
                            <!--{{ $game[0]->system_requirements }}-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  id="productContent"
                  class="sc-1osdtl1-2 iuMoiE tab-activationDetails commonDiv"
                  style="display: none"
                >
                  <div class="react-reveal" style="opacity: 1">
                    <div class="roezsx-0 kdzOEp">
                      <div id="tabContent" class="roezsx-1 TfyBm">
                        <div>
                          <?php $activation = html_entity_decode($game[0]->activation_details);
                          ?>
                          <div>
                            <?php echo $activation; ?>
                            <!--{{ $game[0]->activation_details }}-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--<div class="svwmk5-0 svwmk5-1 geIPJJ eQFCKE">-->
        <!--    <div class="container">-->
        <!--        <nav class="sc-1fb3v8v-0 blxSFb">-->
        <!--            <div class="sc-1fb3v8v-1 deDLjm"><button type="button">Genres</button></div>-->
        <!--            <div class="sc-1fb3v8v-1 dmplOh"><button type="button">Platforms</button></div>-->
        <!--            <div class="sc-1fb3v8v-1 sc-1fb3v8v-2 dmplOh cEKHFZ">-->
        <!--                <a href="#">-->
        <!--                    <span>Software</span>-->
        <!--                    <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="arrow-right" class="svg-inline--fa fa-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">-->
        <!--                        <path-->
        <!--                            fill="currentColor"-->
        <!--                            d="M216.464 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887L209.393 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L233.434 36.465c-4.686-4.687-12.284-4.687-16.97 0z"-->
        <!--                        ></path>-->
        <!--                    </svg>-->
        <!--                </a>-->
        <!--            </div>-->
        <!--        </nav>-->
        <!--        <div class="sc-1fb3v8v-3 bdIVdI">-->
        <!--            <div>-->
        <!--                <div class="row">-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="https://www.kinguin.net/software-games/">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/software.jpg" alt="Software" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">Software</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="https://www.kinguin.net/psn-card-games/">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/psn-card.jpg" alt="PSN Card" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">PSN Card</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="https://www.kinguin.net/fps-games/">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/fps.jpg" alt="FPS" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">FPS</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="#">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/action.jpg" alt="Action" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">Action</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="https://www.kinguin.net/adventure-games/">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/adventure.jpg" alt="Adventure" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">Adventure</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                    <div class="svwmk5-4 khwHOB col-6 col-md-4 col-xl-2">-->
        <!--                        <a class="svwmk5-2 imKyQf">-->
        <!--                            <a href="#">-->
        <!--                                <img src="https://cdns.kinguin.net/media/wysiwyg/genres/vr-games.jpg" alt="VR Games" />-->
        <!--                                <div class="svwmk5-3 gYHlRa">VR Games</div>-->
        <!--                            </a>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <br>-->
        <!--                <br>-->
        <!--                <div class="svwmk5-5 iTtuPw">-->
        <!--                    <div class="znx04n-0 ifVRLu">-->
        <!--                        <div class="znx04n-1 jqKZwb">-->
        <!--                            <button type="button" class="c-view-more__button"><span>View more</span></button>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
      </div>
    </div>

    <div class="modal cartModal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Add To Cart</h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Item added into cart successfully.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">
              <a href="{{ url('checkout') }}">Go To Checkout</a>
            </button>
            <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              <a href="{{ url('/') }}">Continue</a>
            </button>
          </div>
        </div>
      </div>
    </div>

    <div
      class="modal commentModal"
      id="exampleModalLong"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalLongTitle"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">
              Add Product Review
            </h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
              id="modalClose"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12" id="modalBody">
              <div class="msg"></div>
              <form
                accept-charset="UTF-8"
                action=""
                method="post"
                id="reviewform"
              >
                <span class="msg"></span>
                <input id="ratings-hidden" name="rating" type="hidden" />
                <textarea
                  class="form-control animated success"
                  cols="50"
                  id="new-review"
                  name="comment"
                  placeholder="Enter your review here..."
                  rows="5"
                ></textarea>
                <div class="text-right">
                  <fieldset class="rating">
                    <input id="demo-1" type="radio" name="stars" value="1" />
                    <label for="demo-1">1 star</label>
                    <input id="demo-2" type="radio" name="stars" value="2" />
                    <label for="demo-2">2 stars</label>
                    <input id="demo-3" type="radio" name="stars" value="3" />
                    <label for="demo-3">3 stars</label>
                    <input id="demo-4" type="radio" name="stars" value="4" />
                    <label for="demo-4">4 stars</label>
                    <input id="demo-5" type="radio" name="stars" value="5" />
                    <label for="demo-5">5 stars</label>

                    <div class="stars" style="text-align: left">
                      <label
                        for="demo-1"
                        aria-label="1 star"
                        title="1 star"
                      ></label>
                      <label
                        for="demo-2"
                        aria-label="2 stars"
                        title="2 stars"
                      ></label>
                      <label
                        for="demo-3"
                        aria-label="3 stars"
                        title="3 stars"
                      ></label>
                      <label
                        for="demo-4"
                        aria-label="4 stars"
                        title="4 stars"
                      ></label>
                      <label
                        for="demo-5"
                        aria-label="5 stars"
                        title="5 stars"
                      ></label>
                    </div>
                  </fieldset>
                  <a
                    class="btn btn-danger btn-sm"
                    href="#"
                    id="close-review-box"
                    style="display: none; margin-right: 10px"
                  >
                    <span class="glyphicon glyphicon-remove"></span>Cancel</a
                  >
                  <button
                    class="btn btn-success btn-lg addReview"
                    type="button"
                  >
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
          <!--<div class="modal-footer">-->
          <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
          <!--  <button type="button" class="btn btn-primary">Save changes</button>-->
          <!--</div>-->
        </div>
      </div>
    </div>
	
	
	
	




	
	
    @endsection
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
    <script
      src="https://code.jquery.com/jquery-1.12.4.min.js"
      integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
      crossorigin="anonymous"
    ></script>
	
	<script>
	$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
	</script>
	
	
    <script>
      $(document).ready(function () {
        // Swiper: Slider
        new Swiper(".swiper-container", {
          loop: true,
          paginationClickable: true,
          nextButton: ".swiper-button-next",
          prevButton: ".swiper-button-prev",
          slidesPerView: 1,
          pagination: ".swiper-pagination",
          paginationClickable: true,
          spaceBetween: 20,
          autoplay: 2000,
          speed: 800,

          breakpoints: {
            1920: {
              slidesPerView: 1,
              spaceBetween: 30,
            },
            1028: {
              slidesPerView: 1,
              spaceBetween: 30,
            },
            480: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
          },
        });
      });
    </script>
    
     <script>
      $(document).ready(function () {
        // Swiper: Slider
        new Swiper(".swiper-container2", {
          loop: true,
          paginationClickable: true,
          nextButton: ".swiper-button-next",
          prevButton: ".swiper-button-prev",
          slidesPerView: 1,
          pagination: ".swiper-pagination",
          paginationClickable: true,
          spaceBetween: 20,
          autoplay: 2000,
          speed: 800,

          breakpoints: {
            1920: {
              slidesPerView: 0,
              spaceBetween: 30,
            },
            1028: {
              slidesPerView: 0,
              spaceBetween: 30,
            },
            480: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
          },
        });
      });
    </script>
	
 
    
    
<script>    
    jQuery(document).ready(function($) {
  setTimeout(function() {
    $('#myModal2').modal('show');
  }, 1000);
  $(document).ready(function() {
    $('.lab-slide-up').find('a').attr('data-toggle', 'modal');
    $('.lab-slide-up').find('a').attr('data-target', '#myModal2');
  });

});

jQuery(document).ready(function($) {
  setTimeout(function() {
    $('#myModal').modal('show');
  }, 1000);
  $(document).ready(function() {
    $('.lab-slide-up').find('a').attr('data-toggle', 'modal');
    $('.lab-slide-up').find('a').attr('data-target', '#myModal');
  });

});

</script>

  </div>
</div>

<?php
return [

    'currency_type' => ['sol'=>1, 'betc'=>2],
    'currency_types' => [
        ['id'=>1,'name'=>'SOL'],
        ['id'=>2,'name'=>'BETC']
    ],

    'transaction_type' => ['sale'=>1, 'purchase'=>2, 'bet'=>3, 'bet_reward'=>4],

    'MINT_TOKEN_ADDRESS' => env('MINT_TOKEN_ADDRESS', '5j4u4WDrHY7DFE1969YGXVpd4hmVsxurCG4AfqWuDcHp'),

];
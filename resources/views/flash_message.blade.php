@if ($message = Session::get('success'))
<div class="top-alert alert_msg">
	<div class="alert alert-success alert-block alert_msg">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
</div>
@endif


@if ($message = Session::get('error'))
<div class="top-alert alert_msg">
	<div class="alert alert-danger alert-block alert_msg">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="top-alert alert_msg">
	<div class="alert alert-warning alert-block alert_msg">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	</div>
</div>
@endif


@if ($message = Session::get('info'))
<div class="top-alert alert_msg">
	<div class="alert alert-info alert-block alert_msg">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	</div>
</div>
@endif


@if ($errors->any())
<div class="top-alert alert_msg">
	<div class="alert alert-danger alert_msg">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		Please check the form below for errors
	</div>
</div>
@endif
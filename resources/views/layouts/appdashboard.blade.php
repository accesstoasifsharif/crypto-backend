<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf_token" content="{{ csrf_token() }}"> 
  <title>OPSsportsBook</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=1024, initial-scale=1, maximum-scale=1" name="viewport">
    <script src="{{ asset('backend/jquery/dist/jquery.min.js') }}"></script>
     <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}">
 
  <!--
  <link rel="stylesheet" href="{{ asset('/public/backend/css/style.css') }}">-->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css') }}">
  
  <!-- BEGIN: Ionicons -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- END: Ionicons -->

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('backend/css/AdminLTE.min.css') }}">
	<link href="{{asset('backend/summernote-0.8.18/summernote.css')}}" rel="stylesheet">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('backend/css/skins/_all-skins.min.css') }}">
  


   <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/iCheck/all.css') }}">
  <!-- Date Time picker -->
 
    <script src="{{asset('/js/app.js')}}"></script>
    

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap');
@font-face {
    font-family: 'Segoe UI Regular';
    font-style: normal;
    font-weight: normal;
    src: local('Segoe UI Regular'), url('Font_Family/Segoe UI.woff') format('woff');
}

@font-face {
    font-family: 'Segoe UI Bold';
    font-style: normal;
    font-weight: normal;
    src: local('Segoe UI Bold'), url('Font_Family/Segoe UI Bold.woff') format('woff');
}
  .toast.fade.show {
    /* float: right; */
    z-index: 99999;
    display: block;
    opacity: 1;
    max-width: 392px;
    overflow: hidden;
    /* font-size: 20px; */
    background-color: rgba(255,255,255,.85);
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.1);
    border-radius: .25rem;
    box-shadow: 0 0.25rem 0.75rem rgba(0,0,0,.1);
    position: relative;
    float: right;
}
.toast-header {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    padding: .25rem .75rem;
    color: #6c757d;
    background-color: rgba(255,255,255,.85);
    background-clip: padding-box;
    border-bottom: 1px solid rgba(0,0,0,.05);
}
.toast-body {
    padding: .75rem;
}
.modal-body {
    padding: 30px;
}
h1.logo_text {
    font-size: 31px;
    font-family: 'Segoe UI Bold';
    color: white;
    margin: 0;
}
h1.logo_text span {
    color: #00FFA2;
}
span.text-uppercase.font-weight-bold.short_title {
    font-size: 12px;
    letter-spacing: 0px;
    color: #FFFFFF;
    text-shadow: 5px 5px 6px #00000029;
    font-weight: 400 !important;
}
</style>

</head>
<body class="skin-red-light sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


 @include('layouts.sidebar') 
    @yield('content')
 @include('layouts.footer')
 
 </div>
<!-- ./wrapper -->

<!-- jQuery 3 -->




 
<script>
  // $(document).ready(function () {
    // $('.sidebar-menu').tree()
  // })
  $(function(){

    var url = window.location.href; 
        urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
        //console.log(url);
       
        
        $('.sidebar-menu a').each(function(){
         //console.log(this.href);
        // console.log(this.href.replace(/\/$/,''));
            // and test its normalized href against the url pathname regexp
            //if(urlRegExp.test(this.href.replace(/\/$/,''))){
            if(this.href == url){
           // console.log($(this).parent());
                $(this).parent().addClass('active');
            }
            //}
        });

});
// $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      // checkboxClass: 'icheckbox_minimal-blue',
      // radioClass   : 'iradio_minimal-blue'
    // })

$(document).ready(function () {
    $(".sidebar-toggle").click(function(){
      $("body").toggleClass("sidebar-collapse");
    });
  });
</script>
 

  <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->

   

<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!--
	<script src="{{ asset('/public/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>-->
	<!-- FastClick 
	<script src="{{ asset('/public/backend/bower_components/fastclick/lib/fastclick.js') }}"></script> -->
	<!-- AdminLTE App -->
	<!-- <script src="{{ asset('/public/backend/dist/AdminLTE.min.css') }}"></script> -->
	<!-- AdminLTE for demo purposes 
	<script src="{{ asset('/public/backend/dist/js/demo.js') }}"></script>
	<!-- iCheck 1.0.1 -->
<!-- 	<script src="{{ asset('/public/backend/plugins/iCheck/icheck.min.js') }}"></script> -->
 
  
  
  
  <script src="{{asset('backend/summernote-0.8.18/summernote.js')}}"></script>
  <!-- <script>
  $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 });
  } );
  $( function() {
    var dateFormat = "yy-mm-dd",
      from = $( "#start_date" )
        .datepicker({
          defaultDate: "+1w",
		  dateFormat: 'yy-mm-dd',
          changeMonth: true,
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#end_date" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
		dateFormat: 'yy-mm-dd',
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
  </script> -->
<script>   
  $(document).ready(function() { 
      $('.summernote').summernote({height: 300}); 
    });

</script>
</body>
</html>

@php
use App\Http\Controllers\AdminController;
$getinfo = AdminController::getuserdetials(Session::get('ad_user_id'));
$getinfo1 = AdminController::getadmindetials(Session::get('ad_user_id'));
@endphp  
  <link rel="shortcut icon" href="{ asset('backend/images/favicon.png') }}">
    <header class="main-header">
    <a href="/home" class="navbar-brand">
                    <!-- Logo Image -->
                    <!--img src="https://bootstrapious.com/i/snippets/sn-nav-logo/logo.png" width="45" alt="" class="d-inline-block align-middle mr-2"/-->
                    <h1 class="logo_text">OP<span>Sportsbook</span></h1>
                    <!-- Logo Text -->
                    <span class="text-uppercase font-weight-bold short_title">Decentralized Anonymous Global</span>
                </a>
      <!-- Logo -->
      <a href="/home" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><b><img src="{{ asset('backend/images/logo.png') }}" class="top-class-sidebar" alt="Golden Crown"></b></span> -->
        <!-- logo for regular state and mobile devices -->
        <!-- <span class="logo-lg"><img src="{{ asset('backend/images/logo.png') }}" alt="Golden Crown"></span> -->
      </a>
      <!-- Header Navbar: style can be found in header.less -->
    
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <i class="fa fa-bars toggle-icn" aria-hidden="true" alt="Toogle"></i>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
         
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          @if(!empty($getinfo->user_image))
                  <img src="backend/profile/{{$getinfo->user_image}}" class="user-image" alt="User Image">
                @elseif(!empty($getinfo1->user_image))
                 <img src="backend/profile/{{$getinfo1->user_image}}" class="user-image" alt="User Image">
		            @else
			            <img src="{{ asset('backend/profile/profile_1584621964.png') }}" class="user-image" alt="User Image">
			          @endif

                @if(!empty($getinfo))
                  <span><b></b></span><br><span class="hidden-xs">@if(!empty($getinfo)){{ $getinfo->user_firstname}} {{ $getinfo->user_lastname}}@endif</span>
                
                @elseif(!empty($getinfo1))

                <span><b></b></span><br><span class="hidden-xs">@if(!empty($getinfo1)){{ $getinfo1->name}}@endif</span>
                @endif
              </a>
              <ul class="dropdown-menu">
                <!-- Menu Footer-->
                @if(Session::get('ad_user_type') != 'A')
                <li class="user-footer">
                  <div class="pull-left">
                  
                  </div>
                  <div class="pull-right">
                    <a href="/admin/profile/{{Session::get('ad_user_id')}}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                </li>
                @endif

                <!-- Menu Footer-->
                @if(Session::get('ad_user_type') != 'A')
                <li class="user-footer">
                  <div class="pull-left">
                  
                  </div>
                  <div class="pull-right">
                    <a href="admin/changepassword" class="btn btn-default btn-flat">Change Password</a>
                  </div>
                </li>
                @endif
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                  
                  </div>
                  <div class="pull-right">
                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
              
            </li>
            <!-- Control Sidebar Toggle Button --><!--
            <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>-->
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the sidebar -->

    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="">
            <a href="/home">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
		
          @if(Session::get('ad_user_type') !=2)
		      <li class="treeview admindrop">
            <a href="/admin/manage">
            <i class="fa fa-user-o" aria-hidden="true"></i> <span> Admin</span>
            </a>
          </li>
        @endif

        <li class="treeview admindrop">
            <a href="/admin/gamestype">
            <i class="fa fa-futbol-o" aria-hidden="true"></i> <span> Manage Games Type</span>
            </a>
            <ul class="treeview-menu">
            </ul>
          </li>
        <li class="treeview admindrop">
            <a href="/admin/teams_list">
              <i class="fa fa-users" aria-hidden="true"></i> <span> Manage Teams</span>
            </a>
            <ul class="treeview-menu">
            </ul>
          </li>

          <li class="treeview admindrop">
            <a href="/admin/games">
              <i class="fa fa-gamepad"></i> <span> Manage Games</span>
            </a>
            <ul class="treeview-menu">
            </ul>
          </li>

          

          <li class="treeview admindrop">
            <a href="/admin/bet">
            <i class="fa fa-money" aria-hidden="true"></i> <span>Manage Bet</span>
            </a>
            <ul class="treeview-menu">
            </ul>
            
          </li>

          <!-- <li class="treeview admindrop">
            <a href="/admin/users/bet">
              <i class="fa fa-gamepad "></i> <span>Users Bet</span>
            </a>
            <ul class="treeview-menu">
            </ul>
            
          </li> -->
       
          <li class="treeview admindrop">
            <a href="/admin/users">
            <i class="fa fa-user-plus" aria-hidden="true"></i> <span>Manage Users</span>
            </a>
            <ul class="treeview-menu">
            </ul>
          
          </li>

          <li class="treeview admindrop">
            <a href="/admin/transactions">
              <i class="fa fa-money" aria-hidden="true"></i> <span>View Transactions</span>
            </a>
            <ul class="treeview-menu"> </ul>
          </li>

          <!-- <li class="treeview admindrop">
            <a href="/admin/matchs">
            <i class="fa fa-th-list" aria-hidden="true"></i> <span>Manage Matches</span>
            </a>
            <ul class="treeview-menu">

            </ul>
          </li>

          <li class="treeview admindrop"> 
            <a href="/admin/gameplan">
            <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <span> Manage Game Plan </span>
            </a>
            <ul class="treeview-menu">
              
           
            </ul>
          </li> -->
        
          <li class="treeview admindrop"> 
            <a href="/admin/cms">
            <i class="fa fa-user-circle" aria-hidden="true"></i>
              <span> Admin CMS </span>
            </a>
            <ul class="treeview-menu">
              
           
            </ul>
          </li>
        
	
		      <li class="">
            <a href="/logout">
              <i class="fa fa-sign-out"></i> <span>Sign out</span>
            </a>
          </li>
		
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
    <script>
 		/* $(document).ready(function(){
      $(".dropdown-toggle").on("click",function(){
        $(this).parent().toggleClass('open');
          if($(this).parent().hasClass('open')){
            $(this).attr('aria-expanded','false');
          }else{
            {
            $(this).attr('aria-expanded','true');
          }
        }
      });
 		}) */
 	</script>
 
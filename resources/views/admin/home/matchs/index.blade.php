

@extends('layouts.appdashboard')

@section('content')

<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		section.content-header{
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		
		.--switch {
		position: relative;
		display: inline-block;
		width: 87px;
		height: 21px;
		}

		.--switch input {
		display:none;
		}

		.--slider .fa-check {
			color: #FFFFFF;
			position: absolute;
			left: 0px;
			font-size: 12px;
			top: 1px;
			width:100%;
			margin-bottom: 0;
		}
		.--slider .fa-times {
			color: #FFFFFF;
			position:absolute;
			font-size:12px;
			right:0px; 
			width:100%;
			text-align: center;
			top:1px;
			margin-bottom: 0;
		}
		.fa-check:before {
			content: "\f00c";
			display:none;
		}

		.--slider {
		position: absolute;
		cursor: pointer;
		display:flex;
		align-items:center;
		top:0;
		left:0;
		right:0;
		bottom:0;
		background-color: #db3a34;
		-webkit-transition: .4s;
		transition: .4s;
		border-radius:50px;
		}

		.--slider:before {
			position: absolute;
			content: "";
			height: 19px;
			width: 19px;
			left: 1px;
			bottom: 0px;
			border-radius: 50%;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
			top: 1px;
		}

		.fa-remove:before, .fa-close:before, .fa-times:before {
			content: "\f00d";
			display:none;
		}

		.--switch input:checked + .--slider .fa-check {
		display:block;
		}

		.--switch input:checked + .--slider .fa-times {
		display:none;
		}

		.--switch input:checked + .--slider {
		background-color: #52b69a;
		}

		.--switch input:focus + .--slider {
		box-shadow: 0 0 1px #52b69a;
		}

		.--switch input:checked + .--slider:before {

		-webkit-transform: translateX(65px);
		-ms-transform: translateX(65px);
		transform: translateX(65px);
		}
		.--switch input:checked + .--slider .fa-check {
				display: block;
			}
			.--switch input:checked + .--slider .fa-times {
				display: none;
			}

			.--switch input:not(checked) + .--slider .fa-check {
				display: none;
			}
			.--switch input:not(checked) + .--slider .fa-times {
				display: block;
			}

			.alert_msg{
			margin: 0 !important;
			}
			.alert_msg .alert {
				width: 97%;
				position: relative;
				top: 15px;
				left: 0;
				right: 0;
				margin: 0 auto 15px;
			}
	</style>

    <!-- =============================================== -->

	<!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header add-more-games">
     		<h1>
       			{{__('List of Matches')}} 
       			<!-- <small>it all starts here</small> -->
      		</h1>
		  	<a href="{{url('admin/matchs/add')}}" class="Add-Matches">
				<button type="button" class="btn btn-success "> Add Matches</button>
	  		<a> 
	
    	</section>
	
    	<!-- Main content -->
    	<section class="content">
	
      		<!-- Default box -->
      		<div class="box box-primary">
	 			<br>
				<div class="row">	
					<div class="col-md-7"></div>
					<div class="col-md-5 search-form">
			            <form class="form-inline"  method="get" >
				            <div class="form-group">
					            <input type="text" class="form-control" name="search" placeholder="Search" value="" >
				            </div>
				
				            <button type="submit" class="btn  btn-info">Search</button>
				
                            <a href="/admin/matchs" class="btn  btn-info" >Reset</a> 
                            
			            </form>	
		            </div>
				</div>
        		<div class="box-header with-border table-responsive">
         
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('Team 1')}}</th>	
								<th>{{__('Team 2')}}</th>
								<th>{{__('Score')}}</th>
								<th>{{__('Winner')}}</th>
                                
                                <th>{{__('Match Date')}}</th>
								<th>{{__('Action')}}</th>
								<!--<th>Created Date</th>-->
							</tr>
						</thead>
						<tbody>
						@if(!$matches->isEmpty()) 
							@foreach($matches as $match)
								<tr>
									<th>{{$match->id }} </th>
                                        <td><img src="{{ asset('storage/Teams_images/'.$match->flag1) }}" class="teamFlag">
                                        <a  class="flex_1" href="{{$match->team1_id}}">{{$match->team1}}</a>
                                        </td>
									
                                        <td>
                                            <img src="{{ asset('storage/Teams_images/'.$match->flag2) }}" class="teamFlag">
                                            <a  class="flex_1" href="{{$match->team2_id}}">{{$match->team2}}</a>
                                        </td>

                                        @if($match->score1>=0)
                                            <td>{{$match->score1}} - {{$match->score2}}</td>
                                        @else 
                                            <td>-</td>
                                        @endif
                                        @if($match->score1 > $match->score2)
                                            <td>{{$match->team1}}</td>
                                        @elseif($match->score1 < $match->score2)
                                            <td>{{$match->team2}}</td>
                                        @elseif(($match->score1 == $match->score2) && ($match->score1 != -1))
                                            <td>{{__('Tied')}}</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                            <td>{{$match->matchDate}}</td>
                                        
                                        <td>									
                                            <a href="{{url('admin/matchs/edit')}}/{{ $match->id }}" class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></a>									
                                            <a href="" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want remove this Admin?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            
                                        </td>
                                </tr>
                            @endforeach
							@else
								<tr><td colspan="6"><b> No Record found</b></td></tr>
							@endif
						</tbody>
		  			</table>
					  {{ $matches->links('vendor.pagination.custom2') }}

                   <!-- <a href="http://127.0.0.1:8000/" class="btn btn-secondary">{{ __('See all teams')}}</a>-->
                </div>
                
        	</div>
      
            <!-- /.box-footer-->
            <!-- /.box -->
        </section>
    	<!-- /.content -->
  	</div>
   


  	
@endsection

@extends('layouts.appdashboard')

@section('content')

	<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		.col-md-6.search-form {
			width: 100%;
		}
		/* 9th april 2022 */
		section.content-header {
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		/* //9th april 2022 */
	</style>

	<!-- =============================================== -->

  	<!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
  		<div class="top-alert alert_msg">
			@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header">
		@if(empty($id))
      		<h1>
        		Create Matches
       			<!-- <small>it all starts here</small> -->
      		</h1>
			  @else
			  <h1>
        		Edit Matches
       			<!-- <small>it all starts here</small> -->
      		</h1>
			  @endif

      		<a href="{{url('admin/matchs')}}">
				<button type="button" class="btn btn-success"> Back</button>
	  		</a>
	
	  	</section>

	  	<section class="content">
			<div class="box box-primary">
				<div class="box-header with-border table-responsive">
					<form action="{{url('admin/matchs/addmatch')}}" method="post"  enctype="multipart/form-data">				
				 		
						@csrf
						<input type="hidden" name="id" value="{{$id}}">
						<div class="form-group">
							<label for="team1" class="control-label mb-1">Team 1 </label>
							<!-- <input id="team1"  name="team1_id" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required>
 -->
							<select id="game_type" class="selectpicker form-control"  name="team1_id" required>
                            <option value=''>Select</option>
							@foreach($result as $res)
							
                             <option @if($response !='') @if($res->id == $response->team1_id){{ 'selected' }}@endif @endif value="{{$res->id}}">{{$res->name}}</option>

							 @endforeach
							
							 </select>
							
                            
						</div>	
						<div class="form-group">
							<label for="team2" class="control-label mb-1">Team 2</label>
							<!-- <input id="team2"  name="team2_id" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required> -->

							<select id="game_type" class="selectpicker form-control"  name="team2_id" required>
                            <option value=''>Select</option>
							@foreach($result as $res)
							
                             <option @if($response !='') @if($res->id == $response->team2_id){{ 'selected' }}@endif @endif value="{{$res->id}}">{{$res->name}}</option>

							 @endforeach
							
							 </select>
							
						</div>
						<div class="form-group">
							<label for="score1" class="control-label mb-1">Score 1</label>
							<input id="score1"  name="score1" type="text" class="form-control" aria-required="true" aria-invalid="false" value="@if($response !='') {{$response->score1}}@endif" placeholder=" Enter the score of team 1 " required>
	
						</div>
						<div class="form-group">
							<label for="score2" class="control-label mb-1">Score 2</label>
							<input id="score2"  name="score2" type="text" class="form-control" aria-required="true" aria-invalid="false" value="@if($response !='') {{$response->score2}}@endif"  placeholder=" Enter the score of team 2 " required>
							
						</div>

						<div class="form-group">
							<label for="winner_id" class="control-label mb-1">Winner Team</label>
							<!-- <input id="winner_id"  name="winner_id" type="text" class="form-control" aria-required="true" aria-invalid="false"  value="@if($response !='') {{$response->winner}}@endif" required> -->
							<select id="game_type" class="selectpicker form-control"  name="winner_id" required>
                            <option value=''>Select</option>
							@foreach($result as $res)
							
                             <option @if($response !='') @if($res->id == $response->winner_id){{ 'selected' }}@endif @endif value="{{$res->id}}">{{$res->name}}</option>

							 @endforeach
							
							 </select>
							

							
	
						</div>

                        <div class="form-group">
							<label for="matchDate" class="control-label mb-1">Match Date </label>
							<input id="matchDate"  name="matchDate" type="datetime-local" class="form-control" aria-required="true" aria-invalid="false" value="@if($response !=''){{date('Y-m-d', strtotime($response->matchDate))}}@endif" required>
							
						</div>

						<div>
							<button id="payment-button" type="submit" class="btn btn-lg btn-info">
								Submit
							</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
					</form>
				</div>
			</div>
		</section>	<!-- END DATA TABLE-->
    </div>

@endsection
@extends('layouts.appdashboard')

@section('content')


<style>
	input.btn.btn-success {
	    margin-left: 80%;
	}
	.table-responsive {
	    width: 100%;
	}
	.onoffswitch {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
	    display: none;
	}
	.onoffswitch-label {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner:before, .onoffswitch-inner:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner:before {
	    content: "Unfreeze";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner:after {
	    content: "Freeze";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	    margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
	    right: 0px; 
	}


	.onoffswitch1 {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox1 {
	    display: none;
	}
	.onoffswitch-label1 {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner1 {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner1:before, .onoffswitch-inner1:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner1:before {
	    content: "Downgrade";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner1:after {
	    content: "Upgrade";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch1 {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
	    margin-left: 0;
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
	    right: 0px; 
	}

	

	/* 9th april 2022 */ 
	section.content-header{
	    display: flex;
	    justify-content: space-between;
	    align-items: center;
	}
	/* //9th april 2022 */
	.--switch {
      position: relative;
      display: inline-block;
      width: 87px;
      height: 21px;
    }

    .--switch input {
      display:none;
    }

    .--slider .fa-check {
        color: #FFFFFF;
        position: absolute;
        left: 15px;
        font-size: 12px;
        top: 1px;
    	width:100%;
        margin-bottom: 0;
    }
    .--slider .fa-times {
        color: #FFFFFF;
        position:absolute;
        font-size:12px;
        right:0px; 
        width:100%;
        text-align: center;
        top:1px;
        margin-bottom: 0;
    }
    .fa-check:before {
        content: "\f00c";
    	display:none;
    }

    .--slider {
      position: absolute;
      cursor: pointer;
      display:flex;
      align-items:center;
      top:0;
      left:0;
      right:0;
      bottom:0;
      background-color: #db3a34;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius:50px;
    }

    .--slider:before {
        position: absolute;
        content: "";
        height: 19px;
        width: 19px;
        left: 1px;
        bottom: 0px;
        border-radius: 50%;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        top: 1px;
    }

    .fa-remove:before, .fa-close:before, .fa-times:before {
        content: "\f00d";
    	display:none;
    }

    .--switch input:checked + .--slider .fa-check {
      display:block;
    }

    .--switch input:checked + .--slider .fa-times {
      display:none;
    }

    .--switch input:checked + .--slider {
      background-color: #52b69a;
    }

    .--switch input:focus + .--slider {
      box-shadow: 0 0 1px #52b69a;
    }

    .--switch input:checked + .--slider:before {

      -webkit-transform: translateX(65px);
      -ms-transform: translateX(65px);
      transform: translateX(65px);
    }
    .--switch input:checked + .--slider .fa-check {
		    display: block;
		}
		.--switch input:checked + .--slider .fa-times {
		    display: none;
		}

		.--switch input:not(checked) + .--slider .fa-check {
		    display: none;
		}
		.--switch input:not(checked) + .--slider .fa-times {
		    display: block;
		}

		.alert_msg{
	    margin: 0 !important;
		}
		.alert_msg .alert {
		    width: 97%;
		    position: relative;
		    top: 15px;
		    left: 0;
		    right: 0;
		    margin: 0 auto 15px;
		}
		.admin_cms_image{
			/* width: 200px; */
			height: 85px;
		}
		table.admin_cms_list tbody tr td:nth-child(4) {
			max-width: 100px;
			overflow: hidden;
			width: 100%;
			
		}
		table.admin_cms_list tbody tr td:nth-child(6) {
			display: flex;
			gap: 5px;
		}
</style>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<div class="top-alert alert_msg">
		  @include('flash_message')
		</div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Admin CMS
       <!-- <small>it all starts here</small> -->
      </h1>
	  @if(empty($rid))
      <a href="{{url('admin/create')}}">
		<button type="button" class="btn btn-success"> Add Admin CMS</button>
	  </a>
	  @else
		  <a href="{{url('admin/create')}}/{{$rid}}">
		<button type="button" class="btn btn-success"> Add Admin CMS</button>
	  </a> 
	  @endif
    </section>
	
    <!-- Main content -->
    <section class="content">
	
      <!-- Default box -->
      <div class="box box-primary">
	 		<br><div class="row">	
		<div class="col-md-7">
		</div>
		<div class="col-md-5 search-form">
			<form class="form-inline"  method="get" >
				<div class="form-group">
					<input type="text" class="form-control" name="search" placeholder="Search" value="@if($search){{$search}}@endif" >
				</div>
				
				<button type="submit" class="btn  btn-info">Search</button>
				
				@if(empty($rid))
					<a href="/admin/cms" class="btn  btn-info" >Reset</a> 
				@else
					<a href="/admin/cms/{{$rid}}" class="btn  btn-info" >Reset</a> 
				@endif
			</form>	
		</div>
		</div>
        <div class="box-header with-border table-responsive">
         
			<table class="table table-bordered  admin_cms_list">
			<thead>
			  <tr>
				
			
				<th>#</th>
				<th>Page Name</th>
				<th>Title</th>
				<th>Description</th>
				<th>Image</th>
				<th>Action</th>
				<!--<th>Created Date</th>-->
			  </tr>
			</thead>
			<tbody>
			@if(!$response->isEmpty())
			@foreach($response as $res)
			  <tr>
			  	<td>{{$res->id }} </td>
				<td>{{$res->cms_page }} </td>
				<td>{{ $res->cms_title }} </td>
				<td>{{ $res->cms_desc }} </td>
				<td width="20%"><img src="{{ asset('storage/cms_image/'.$res->cms_image) }}" class="admin_cms_image" /> </td>
				<!-- <td>
					<label class="--switch">
					<input type="checkbox" class="change-status" data-id="{{ $res->id }}" data-status="@if($res->status == 1){{'0'}}@else {{'1'}} @endif" data-val='@if(!empty($rid)){{$rid}}@endif'  @if($res->status == 1){{ 'D' }}@else {{ 'checked' }} @endif >
					<span class="--slider"><p class="fas fa-check">Inactive</p><p class="fas fa-times">Active</p></span>
				</td> -->
				
			
				<td> 
				@if(empty($rid))
				 <a href="{{url('admin/cms/edit')}}/{{ $res->id }}" class="btn btn-success btn-xs" onclick="return confirm('Are you sure you want to edit this Admin CMS?')"><i class="fa fa-edit" aria-hidden="true"></i></a>
				
				<a href="/admin/cms/delete/{{$res->id}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want remove this Admin CMS?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
				@else
				 <a href="{{url('admin/cms/edit')}}/{{ $res->id }}/{{$rid}}" class="btn btn-success btn-xs" onclick="return confirm('Are you sure you want to edit this Admin CMS?')"><i class="fa fa-edit" aria-hidden="true"></i></a>
				
				<a href="/admin/cms/delete/{{$res->id}}/{{$rid}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want remove this Admin CMS?')"><i class="fa fa-trash" aria-hidden="true"></i></a>	
				@endif
				</td>	
				
			  </tr>
			@endforeach   
			@else
				<tr><td colspan="6"><b> No Record found</b></td></tr>
			@endif
			</tbody>
		  </table>
        	{{ $response->links('vendor.pagination.custom2') }}
        </div>
      
       
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>



  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
</div>
<script>
	function addCredit(userid){
		$("#userid").val(userid);
	}
	function ReduceCredit(userid){
		$("#userid1").val(userid);
	}

	$(".btn-addevent").click(function(){
		$(".add-header-section").toggle("slow");
	});
	function myNewFunction(sel) {
	  $("#eventclassification").val(sel.options[sel.selectedIndex].text);
	}
</script>

<script>
    $(document).ready(function(){
        $('.change-status').click(function(){			
			if (confirm('Are you sure you want to change the status?')) {
				var id = $(this).attr("data-id");	
				var status = $(this).attr("data-status");
				var token=$('meta[name="csrf_token"]').attr('content');
				if($(this).attr("data-val")){
					var rid=$(this).attr("data-val");
				}else{
					var rid='';
				}
				$.ajax({     
					url: '/admin/status',
					type:"POST",  
					headers: {
					'X-CSRF-TOKEN':token
					},        
					data:'id='+id+'&status='+status+'&_token='+token,
					success:function(data){ 
					if(rid==''){
						window.location = "/admin/cms";
					}else{
						window.location = "/admin/cms/"+rid;
					}
				   
					}
				});		
				
			
			}else{
				return false;
			}
						
		});
		
		  
		 $('.test-status').click(function(){			
		
			if (confirm('Are you sure you want to change assign status?')) {
				var id = $(this).attr("data-id");	
				var status = $(this).attr("data-status");
			
				var token=$('meta[name="csrf_token"]').attr('content');
				$.ajax({     
					url: '/student/teststatus',
					type:"POST",  
					headers: {
					'X-CSRF-TOKEN':token
					},        
					data:'id='+id+'&status='+status+'&_token='+token,
					success:function(data){       
						window.location = "/admin/student";
				   
					}
				});		
				
			
			}else{
				return false;
			}
						
		});
    });
</script>
<script>
    $(document).ready(function(){
        $('.onoffswitch-checkbox').click(function(){			
		
				if($(this).prop("checked") == true){
					if (confirm('Are you sure you want to Unfreeze this user?')) {
					var status = 'A';
					}
					else{
						return false;
					}
				}
				else if($(this).prop("checked") == false){
					if (confirm('Are you sure you want to Freeze this user?')) {
				   var status = 'D';
					}else{
						return false;
					}
				}
			var id = $(this).attr("data-id");	
			var token=$('meta[name="csrf_token"]').attr('content');
				$.ajax({     
					url: '/admin/status',
					type:"POST",  
					headers: {
					'X-CSRF-TOKEN':token
					},        
					data:'id='+id+'&status='+status+'&_token='+token,
					success:function(data){       
						
						//$("#cf-module").html(data);
				   
					}
				});			
    });
    });
</script>
<script>
    $(document).ready(function(){
        $('.onoffswitch-checkbox1').click(function(){			
		
				if($(this).prop("checked") == true){
					if (confirm('Are you sure you want to Upgrade To Pro this user?')) {
					var user_type = '1';
					}
					else{
						return false;
					}
				}
				else if($(this).prop("checked") == false){
					if (confirm('Are you sure you want to Downgrade this user?')) {
				   var user_type = '0';
					}else{
						return false;
					}
				}
			var id = $(this).attr("data-id");	
			var token=$('meta[name="csrf_token"]').attr('content');
				$.ajax({     
					url: '/admin/user_type',
					type:"POST",  
					headers: {
					'X-CSRF-TOKEN':token
					},        
					data:'id='+id+'&user_type='+user_type+'&_token='+token,
					success:function(data){       
						if(data=='Upgraded'){
						alert('Upgraded Successfully');
					}else if(data=='Downgraded'){
						alert('Downgraded Successfully');	
					}else{
						console.log(data);
					}
				   
					}
				});			
    });
	
	// Reduce Credit
	 $('.redeuceBtn').click(function(){			
		
			
						
		});
	
    });
	
	// function ReduceCredit(uid){
		// if (confirm('Are you sure you want to come out credit of this user?')) {
				// var userid = uid;	
				// var credit = 1;	
				
				// var token=$('meta[name="csrf_token"]').attr('content');
				// $.ajax({     
					// url: '/reducecreditcustomer',
					// type:"POST",  
					// headers: {
					// 'X-CSRF-TOKEN':token
					// },        
					// data:'userid='+userid+'&credit='+credit+'&_token='+token,
					// success:function(data){       
						
				   // alert('Credit reduce Successfully!');
					// }
				// });	
			
		// }else{
			// return false;
		// }
	// }
</script>
 @endsection

 
 
 
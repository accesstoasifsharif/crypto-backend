@extends('layouts.appdashboard')

@section('content')

	<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		.col-md-6.search-form {
			width: 100%;
		}
		/* 9th april 2022 */
		section.content-header {
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		/* //9th april 2022 */
	</style>

	<!-- =============================================== -->

  	<!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
  		<div class="top-alert alert_msg">
			@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header">
	    	@if(empty($response))
			<h1>
				Add Admin
			<!-- <small>it all starts here</small> -->
			</h1>
			@else
			<h1>
				Edit Profile
			<!-- <small>it all starts here</small> -->
			</h1>
			@endif
			@if(empty($rid))
			<a href="{{url('/home')}}">
				<button type="button" class="btn btn-success"> Back</button>
			</a>
			@else
			<a href="{{url('/home')}}/{{$rid}}">
				<button type="button" class="btn btn-success"> Back</button>
			</a>
			@endif
	  	</section>

	  	<section class="content">
			<div class="box box-primary">
				<div class="box-header with-border table-responsive">
					<form action="{{url('admin/add')}}" method="post"  enctype="multipart/form-data">				
					<input type="hidden" name="id" value="">
					@if(!empty($response))
				 		<input type="hidden" name="id" value="{{$response->id}}">
					@endif
				
						@csrf
					
						<div class="form-group">
							<label for="name" class="control-label mb-1">Name</label>						
							<input id="name"  name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" value="@if(!empty($response)){{$response->name}}@endif" required>
							
						</div>	
						<div class="form-group">
							<label for="email" class="control-label mb-1">Email</label>						
							<input id="email"  name="email" type="text" class="form-control" aria-required="true" aria-invalid="false" value="@if(!empty($response)){{$response->email}}@endif" required>
							
						</div>
						
						
						<div>
							<button id="payment-button" type="submit" class="btn btn-lg btn-info">
								Submit
							</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
					</form>
				</div>
			</div>
		</section>	<!-- END DATA TABLE-->
    </div>

@endsection
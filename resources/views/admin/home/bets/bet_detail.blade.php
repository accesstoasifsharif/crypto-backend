@extends('layouts.appdashboard')

@section('content')


	<style>
		.table-responsive {
            width: 100%;
        }
        .bet_detail_flag{
            width: 40px;
            height: 40px;
            max-width: 40px;
            max-height: 40px;
            float: right;
        }
		
		table.gameinfo_table .gameinfo_border_tr,
		table.gameinfo_table .gameinfo_border_tr:hover{
			background-color: #ebebeb;
		}
		table.gameinfo_table tr:hover{
			background-color: #f5faff;
		}
        .cursor_disable{
            cursor: inherit;
        }

        .team-row {
            width: 100%;
            justify-content: left !important;
        }
        .my-bet-team {
            /* background: #000000 0% 0% no-repeat padding-box; */
            border-radius: 4px !important;
            padding: 4px 25px 5px 25px !important;
            display: grid;
            min-height: 60px;
            font-size: 16px;
            min-width: 70px;
            color: white;
            align-content: center;
            cursor: none;
        }
        .team-row div span {
            font-weight: bold;
        }
        .my-bet-team-black-bg {
            background: #000000 0% 0% no-repeat padding-box;
        }

        .my-bet-team-green-bg {
            background: green 0% 0% no-repeat padding-box;
        }
	</style>
	<!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  @include('flash_message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bet Detail 
       <!-- <small>it all starts here</small> -->
      </h1>
      
    </section>
	
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">

            
            <div class="box box-primary">
                <div class="box-body box-profile">
                <h4 class="text-muted text-center">User Info</h4>
                @if(!empty($bet->userdetail->image))         
                            
                    <img src="{{ asset('storage/Users_profile_photo/') }}/{{$bet->userdetail->image}}" class="profile-user-img img-responsive img-circle"> 
                        @else
                        <img src="{{ asset('storage/Users_profile_photo/avatar-s-19.png') }}" class="profile-user-img img-responsive img-circle">      
                        @endif

                

                <p class="text-muted text-center">{{$bet->userdetail->name}}</p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                    <b>Email</b> <a class="pull-right">{{$bet->userdetail->email}}</a>
                    </li>
                   
                     <li class="list-group-item">
                        <b>Wallet-Address</b> 
                        <a class="pull-right">{{formateWalletAddress($bet->wallet_address)}}</a>
                    </li>
                </ul>

    
                </div>
                
            </div>
            

            
            </div>
            
            <!-- /.col -->
            <div class="col-md-8">
            <div class="nav-tabs-custom">
            
                <div class="table-responsive box box-primary">
                    <h4 class="text-muted text-center">Game Info</h4>
                    <table class="table mb-5 gameinfo_table">
                        <tbody>
                            <tr class="gameinfo_border_tr">
                                <th>Game Type </th>
                                <th>Team1</th>
                                <th>Team2</th>     
                            </tr>
                                
                            
                            <tr>
                                <td>{{$bet->games->game_type_name}}</td> 
                                <td>{{$bet->team1detail->name}}</td>
                                <td>{{$bet->team2detail->name}}</td>														
                            </tr>
                            
                            <tr class="gameinfo_border_tr">
                                <th>Venue </th>
                                <th>Date</th>
                                <th>Result</th>   
                            </tr>
							
							<tr>
                                <td>{{$bet->games->venue}}</td> 
                                <td>{{$bet->games->game_date}}</td>
                                <td>{{formateGameResult($bet->games->result)}}</td>
                            </tr>
							
							<tr class="gameinfo_border_tr">
                                <th>Bet Limit</th>
                                <th>Amount Limit</th>
                                <th>Status</th>   
                            </tr>
							
							<tr>
                                <td>{{$bet->games->max_no_of_bets}}</td> 
                                <td>{{$bet->games->max_amount_user_bet}}</td>
                                <td>{{formateGameStatus($bet->games->status)}}</td>

                            </tr>
                            
                        </tbody>
                    </table>


                </div>
                
            </div>
            
            <!-- /.col -->
        </div>
    </section>

    <section class="content team_bet_sec">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">

                        <h4 class="text-muted text-center">Team1 Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Name</b> <a class="pull-right">{{$bet->team1detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Country</b> <a class="pull-right">{{$bet->team1detail->country}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Flag</b> <a class="pull-right"> <img src="{{ asset('storage/Teams_images/') }}/{{$bet->team1detail->flag}}" class="bet_detail_flag"></a>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><span>SPREAD</span></div>
                                    <div class="col-md-4"><span>MONEYLINE</span></div>
                                    <div class="col-md-4"><span>OVER/UNDER</span></div>
                                </div>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 1) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"><span class="">{{$bet->spread1_t1}}</span><span
                                                class="">{{$bet->spread2_t1}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 2) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"><span class="">{{$bet->moneyline_t1}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 3) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"> <span class="">{{$bet->total1_type_t1.' '.$bet->total1_t1}}</span><span
                                                class="">{{$bet->total2_t1}}</span></button></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                    <h4 class="text-muted text-center">Team2 Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Name</b> <a class="pull-right">{{$bet->team2detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Country</b> <a class="pull-right">{{$bet->team2detail->country}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Flag</b> <a class="pull-right"> <img src="{{ asset('storage/Teams_images/') }}/{{$bet->team2detail->flag}}" class="bet_detail_flag"></a>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><span>SPREAD</span></div>
                                    <div class="col-md-4"><span>MONEYLINE</span></div>
                                    <div class="col-md-4"><span>OVER/UNDER</span></div>
                                </div>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 4) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"><span class="">{{$bet->spread1_t2}}</span><span
                                                class="">{{$bet->spread2_t2}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 5) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"><span class="">{{$bet->moneyline_t2}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2 @if($bet->bet_box == 6) my-bet-team-green-bg @else  my-bet-team-black-bg @endif"> <span class="">{{$bet->total1_type_t2.' '.$bet->total1_t2}}</span><span
                                                class="">{{$bet->total2_t2}}</span></button></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h4 class="text-muted text-center">Bet Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Betteam</b> <a class="pull-right">{{$bet->betteamdetail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Amount</b> <a class="pull-right">{{$bet->bet_amount}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>To Win</b> <a class="pull-right">{{$bet->to_win}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Pay Out</b> <a class="pull-right">{{$bet->pay_out}}</a>
                            </li>                            
                            <li class="list-group-item">
                            <b>Won/Lose</b> 
                            @if($bet->win == 0)
							<a class="btn btn-primary btn-xs  pull-right cursor_disable">No Result</a>
							@elseif($bet->win == 3)
							<a class="btn btn-warning btn-xs  pull-right cursor_disable">Draw</a>
							@else
                                @if($bet->win == 2)
								<a class="btn btn-success btn-xs  pull-right cursor_disable"><i class="ion-checkmark" aria-hidden="true"></i></a>
								@else
								<a class="btn btn-danger btn-xs  pull-right cursor_disable"><i class="ion-android-close" aria-hidden="true"></i></a>
								@endif
							@endif
                            </li>
                            
                           
                            <li class="list-group-item">
                            <b>Payment</b> 
                                @if($bet->win == 2)
                                    @if($bet->settled == 1)
                                        <a class="btn btn-success btn-xs  pull-right cursor_disable"><i class="ion-checkmark" aria-hidden="true"></i></a>
                                    @else
                                        <button id="bet-reward-settlement-btn" class="btn btn-danger btn-xs reaction-btn" style="cursor:pointer;" onclick="settleBetReward({{$bet->id}})" data-toggle="tooltip"  title="Change to payment settled">Pending</button>
                                    @endif
                                @endif
                            </li>

                            
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
        </div>
    </section>
    <!-- /.content -->



     <script>
        var wallet_secret =  {!! file_get_contents(storage_path() . "/wallet/id.json") !!};
        const walletPrivateKey = new Uint8Array(wallet_secret);
        const opSportWallet = solanaWeb3.Keypair.fromSecretKey(walletPrivateKey);
        const network = "https://api.mainnet-beta.solana.com/";
        const connection = new solanaWeb3.Connection(network);

        var MINT_TOKEN_ADDRESS = `{{ config('opsports.MINT_TOKEN_ADDRESS') }}`;
        // alert(MINT_TOKEN_ADDRESS);

        async function settleBetReward(bet_id){            
            if (confirm('Are you sure you want to settle this amount?')) {
                applyProcessing();
                var token=$('meta[name="csrf_token"]').attr('content');
                $.ajax({     
                    url: '/admin/bet/get_bet_details_settlement/'+bet_id,
                    type:"GET",  
                    headers: {
                    'X-CSRF-TOKEN':token
                    },        
                    success:function(data){ 
                        if(!data.settled){
                            handleBetSettlement(data);
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        removeProcessing();
                    }   
                });		
            }else{
                return false;
            }
        };

        function applyProcessing(){
            var element = $('#bet-reward-settlement-btn');
            element.removeClass("btn-danger");    
            element.addClass("btn-default");
            element.html("Processing...");
            element.prop('disabled', true);

        }

        function removeProcessing(){
            var element = $('#bet-reward-settlement-btn');
            element.addClass("btn-danger");    
            element.removeClass("btn-default");
            element.html("Pending");
            element.prop('disabled', false);
        }



        async function handleBetSettlement(data){
            const mint = new solanaWeb3.PublicKey(MINT_TOKEN_ADDRESS);
            const fromTokenAccount = await splToken.getOrCreateAssociatedTokenAccount(
                connection,
                opSportWallet,
                mint,
                opSportWallet.publicKey
            );
            const toTokenAccount = await splToken.getOrCreateAssociatedTokenAccount(
                connection,
                opSportWallet,
                mint,
                new solanaWeb3.PublicKey(data.wallet_address)
            );
            await splToken.transfer(
					connection,
					opSportWallet,
					fromTokenAccount.address,
					toTokenAccount.address,
					opSportWallet.publicKey,
					data.payout * solanaWeb3.LAMPORTS_PER_SOL,
					[]
					).then((response) => {
                       let tokenTrnxPayload = {
							transaction_id: response,
							transaction_type: 4,
							sending_wallet_address:
								opSportWallet.publicKey.toBase58(),
							receiving_wallet_address: data.wallet_address,
							amount: data.payout + "",
							currency: 2,
                            bet_id: data.bet_id,
						};
						saveCryptoTransactionLog(tokenTrnxPayload);
                       
                    })
					.catch((error) => {
                        removeProcessing();
						console.log("tokenTrnx => ", error);
                    });
				
        };
    
        function saveCryptoTransactionLog(payload){
            let data_set = 'transaction_id=' + payload.transaction_id +'&transaction_type=' + payload.transaction_type +
                    '&sending_wallet_address=' + payload.sending_wallet_address+ '&receiving_wallet_address=' + payload.receiving_wallet_address+ '&amount=' + payload.amount + '&currency=' + payload.currency +'&bet_id='+payload.bet_id; 
            var token=$('meta[name="csrf_token"]').attr('content');
            $.ajax({     
                url: '/admin/bet/settled_bet_save_crypto_transaction',
                type:"POST",  
                headers: {
                'X-CSRF-TOKEN':token
                },
                data:data_set,
                success:function(data){ 
                    location.reload();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                       location.reload();
                }  
            });	
        };


	</script>
    


  </div>
	
@endsection

 
 
 
@extends('layouts.appdashboard')

@section('content')
<style>
	.live-bets-icn {
    height: 135px;
}
.bg-mma {
    background-color: #cd3b36;
}
.bg-football {
    background-color: #0d3799;
}
.bg-american {
    background-color: #905a4e;
}
.bg-hockey {
    background-color: #065935;
}
</style>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			Manage Games
		</h1>
		</section>

    	<!-- Main content -->
	
		<section class="content">

		  	<div class="row">
				<a href="/admin/manage_teams_by_game_type">
					<div class="col-lg-3 col-xs-6">
					<!-- small box -->
						<div class="small-box  bg-mma">
							<div class="inner">
								<h3>{{$teams_count}}</h3>
								<p>Teams By GameType</p>
							</div>
							<div class="icon">
								<img src="{{url('/backend/images/mma.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<a href="/admin/games_by_type">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-football">
							<div class="inner">
								<h3>{{$games_count}}</h3>
								<p>Games By GameType</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/football.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>			
					</div>
				</a>
				
				
				

			</div>	
		</section>
		
    	<!-- /.content -->
  	</div>
  	<!-- /.content-wrapper -->
  	<style>
  		a {
    		color: #f9fafc;
		}
  	</style>
  
@endsection
 
 
 

 
 
 
 

 
 
 
@extends('layouts.appdashboard')

@section('content')


	<style>
		.table-responsive {
            width: 100%;
        }
        .bet_detail_flag{
            width: 40px;
            height: 40px;
            max-width: 40px;
            max-height: 40px;
            float: right;
        }
        .game_detail_flag{
            width: 30px;
            height: 30px;
            max-width: 30px;
            max-height: 30px;
            float: right;
            background-color: #000;
        }
        .game_detail_view_bet{
            margin: -5px 0;
        }
        .team-row {
            width: 100%;
            justify-content: left !important;
        }
        .my-bet-team {
            background: #000000 0% 0% no-repeat padding-box;
            border-radius: 4px !important;
            padding: 4px 25px 5px 25px !important;
            display: grid;
            min-height: 60px;
            font-size: 16px;
            min-width: 70px;
            color: white;
            align-content: center;
            cursor: none;
        }
        .team-row div span {
            font-weight: bold;
        }

        .game_history_header .box-body.box-profile, .game_history_body .box-body.box-profile{
            width: 33.3%;
            float: left;
        }
        .game_history_list{
            border-top-color: #db1fff !important;
            transition: .5s;
            border-radius: 7px !important;
            display: inline-block;
            width: 100%;
            background:#fff;
            border-top: 3px solid #d2d6de;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            padding: 10px;
        }
        .game_history_list .box-primary{
            border:0 !important;
            background: inhherit;
        }
        .game_history_title{
			font-size: 24px;
    		font-weight: 400;
		}
        .game_history_updated_at{
			padding: 30px 15px;
		}

        .my-bet-team-black-bg {
            background: #000000 0% 0% no-repeat padding-box;
			cursor: pointer;
        }

        .my-bet-team-green-bg {
            background: green 0% 0% no-repeat padding-box;
			cursor: pointer;
        }
        .my-bet-team-choose-result {
            border-radius: 4px !important;
            padding: 4px 25px 5px 25px !important;
            display: grid;
            min-height: 60px;
            font-size: 16px;
            min-width: 70px;
            color: white;
            align-content: center;
        }
        .my-bet-team-choose-result:hover{
            padding: 4px 25px 5px 25px !important;
        }
        #choose_result_modal .modal-dialog {
            width: 438px;
            margin: 178px auto 0;
        }
        #choose_result_modal .modal-footer {
            margin: 39px 0 0 0;
        }
        #choose_result_modal .modal-header .close {
            margin-top: -23px;
        }
        #choose_result_modal .modal-title{
            font-size: 18px;
            font-weight: 600;
        }
	</style>
	<!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  @include('flash_message')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Game Detail 
       <!-- <small>it all starts here</small> -->
      </h1>
      
    </section>
	
    <!-- Main content -->
    <!-- <section class="content">
        <div class="row">
            <div class="col-md-4">

            
            <div class="box box-primary">
                <div class="box-body box-profile">
                <h4 class="text-muted text-center">User Info</h4>
                @if(!empty($bet->userdetail->image))         
                            
                    <img src="{{-- asset('storage/Users_profile_photo/') --}}/{{--$bet->userdetail->image--}}" class="profile-user-img img-responsive img-circle"> 
                        @else
                        <img src="{{-- asset('storage/Users_profile_photo/avatar-s-19.png') --}}" class="profile-user-img img-responsive img-circle">      
                        @endif

                

                <p class="text-muted text-center">{{--$bet->userdetail->name--}}</p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                    <b>Available Credits</b> <a class="pull-right">{{--$bet->userdetail->available_balance--}}</a>
                    </li>
                    <li class="list-group-item">
                    <b>Email</b> <a class="pull-right">{{--$bet->userdetail->email--}}</a>
                    </li>
                    <li class="list-group-item">
                    <b>Phone</b> <a class="pull-right">{{--$bet->userdetail->phone_number--}}</a>
                    </li>
                </ul>

    
                </div>
                
            </div>
            

            
            </div>
            
            
            <div class="col-md-8">
            <div class="nav-tabs-custom">
                
            
                
            
                <div class="table-responsive">
                    <h4 class="text-muted text-center">Game Info</h4>
                    <table class="table mb-5">
                        <tbody>
                            <tr>
                                <th>Game Type </th>
                                <th>Team1</th>
                                <th>Team2</th>                                                    <th></th>
                    
                            </tr>
                                
                            
                                <tr>
                                    <td>{{--$bet->games->game_type_name--}}</td> 
                                    <td>{{--$bet->team1detail->name--}}</td>
                                    <td>{{--$bet->team2detail->name--}}</td>														
                                </tr>
                            
                            
                            
                            
                        </tbody>
                    </table>
                    <table class="table mb-5">
                        <tbody>
                            <tr>
                                <th>Venue </th>
                                <th>Date</th>
                                <th>Result</th>                                                    <th></th>
                    
                            </tr>
                                
                            
                                <tr>
                                    <td>{{--$bet->games->venue--}}</td> 
                                    <td>{{--$bet->games->game_date--}}</td>
                                    <td>{{--$bet->games->result--}}</td>														
                                </tr>
                            
                            
                            
                            
                        </tbody>
                    </table>
                    <table class="table mb-5">
                        <tbody>
                            <tr>
                                <th>Bet Limit</th>
                                <th>Amount Limit</th>
                                <th>Status</th>                                                    <th></th>
                    
                            </tr>
                                
                            
                            <tr>
                                <td>{{--$bet->games->max_no_of_bets--}}</td> 
                                <td>{{--$bet->games->max_amount_user_bet--}}</td>
                                <td>{{--$bet->games->status--}}</td>														
                            </tr>
                            
                            
                            
                            
                        </tbody>
                    </table>
                    <table class="table mb-5">
                        <tbody>
                            <tr>
                                <th>Venue </th>
                                <th>American Ods</th>
                                <th>Decimal Ods</th>                                                    <th></th>
                    
                            </tr>
                                
                            
                            <tr>
                                <td>{{--$bet->games->venue--}}</td> 
                                <td>{{--$bet->games->american_ods--}}</td>
                                <td>{{--$bet->games->decimal_ods--}}</td>														
                            </tr>
                            
                            
                            
                            
                        </tbody>
                    </table>
                                            
                </div>
                
            
            </div>
            
           
        </div>
    </section> -->

    <section class="content">
        <div class="row">
        <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h4 class="text-muted text-center">Game Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Team1</b> <a class="pull-right">{{$games->team1detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Team2</b> <a class="pull-right">{{$games->team2detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Bet Limit</b> <a class="pull-right">{{$games->max_no_of_bets}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Amount Limit</b> <a class="pull-right">{{$games->max_amount_user_bet}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Result</b> <a class="pull-right">{{$games->result}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Status</b> <a class="pull-right">{{$games->status}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Date</b> <a class="pull-right">{{$games->game_date}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>American Ods</b> <a class="pull-right">{{$games->american_ods}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Decimal Ods</b> <a class="pull-right">{{$games->decimal_ods}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>View bets</b> <a href="{{url('admin/bet/'.$games->id)}}" class="btn btn-success btn-xs pull-right game_detail_view_bet"><i class="fa fa-eye" aria-hidden="true"></i></a><!-- <a class="pull-right"> #{{$games->id}}</a> -->
                            </li>
                            <li class="list-group-item">
                            <b>Game Type</b> <a class="pull-right">{{$games->game_type_name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Flag</b> <a class="pull-right"> <img src="{{ asset('storage/games_type/') }}/{{$games->game_type_details->img}}" class="game_detail_flag"></a>
                            </li>
                            
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">

                        <h4 class="text-muted text-center">Team1 Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Name</b> <a class="pull-right">{{$games->team1detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Country</b> <a class="pull-right">{{$games->team1detail->country}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Flag</b> <a class="pull-right"> <img src="{{ asset('storage/Teams_images/') }}/{{$games->team1detail->flag}}" class="bet_detail_flag"></a>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><span>SPREAD</span></div>
                                    <div class="col-md-4"><span>MONEYLINE</span></div>
                                    <div class="col-md-4"><span>OVER/UNDER</span></div>
                                </div>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"><span class="">{{$games->spread1_t1}}</span><span
                                                class="">{{$games->spread2_t1}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"><span class="">{{$games->moneyline_t1}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"> <span class="">{{$games->total1_type_t1.' '.$games->total1_t1}}</span><span
                                                class="">{{$games->total2_t1}}</span></button></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                    <h4 class="text-muted text-center">Team2 Info</h4>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                            <b>Name</b> <a class="pull-right">{{$games->team2detail->name}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Country</b> <a class="pull-right">{{$games->team2detail->country}}</a>
                            </li>
                            <li class="list-group-item">
                            <b>Flag</b> <a class="pull-right"> <img src="{{ asset('storage/Teams_images/') }}/{{$games->team2detail->flag}}" class="bet_detail_flag"></a>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><span>SPREAD</span></div>
                                    <div class="col-md-4"><span>MONEYLINE</span></div>
                                    <div class="col-md-4"><span>OVER/UNDER</span></div>
                                </div>
                            </li>
                            <li class="list-group-item team-row">
                                <div class="row team-row">
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"><span class="">{{$games->spread1_t2}}</span><span
                                                class="">{{$games->spread2_t2}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"><span class="">{{$games->moneyline_t2}}</span></button></div>
                                    <div class="col-md-4"><button type="button" class="btn my-bet-team mt-2"> <span class="">{{$games->total1_type_t2.' '.$games->total1_t2}}</span><span
                                                class="">{{$games->total2_t2}}</span></button></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div> 
            </div>
            <!-- /.col -->
            
        </div>


        <div class="game_history_list">
            <div class="row game_history_header">
                <div class="col-md-12">
                        <h5 class="game_history_title text-center">Game History</h5>
                        <div class="box box-primary">
                            <div class="box-body box-profile">

                                <h4 class="text-muted text-center">Team1 Info</h4>

                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item team-row">
                                        <div class="row team-row">
                                            <div class="col-md-4"><span>SPREAD</span></div>
                                            <div class="col-md-4"><span>MONEYLINE</span></div>
                                            <div class="col-md-4"><span>OVER/UNDER</span></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="box-body box-profile">

                                <h4 class="text-muted text-center">Team2 Info</h4>

                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item team-row">
                                        <div class="row team-row">
                                            <div class="col-md-4"><span>SPREAD</span></div>
                                            <div class="col-md-4"><span>MONEYLINE</span></div>
                                            <div class="col-md-4"><span>OVER/UNDER</span></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="box-body box-profile">

                                <h4 class="text-muted text-center">Updated At</h4>

                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item team-row">
                                        <div class="row team-row">
                                            <div class="col-md-6"><span>Updated At</span></div>
                                            <div class="col-md-6"><span>View Bets</span></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                        </div> 
                </div>
            </div>
            <div class="row game_history_body">
                @foreach($games->games_change_history as $games_change_history)
                @php
                    $bet_status_win_box_array = explode(",",$games_change_history->bet_status_win);
                @endphp
                <div class="col-md-12">
                        <div class="box box-primary">
                        <div class="box-body box-profile">

								

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_1'}}" @if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1))  @if($games_change_history->bet_status_win == 0) @endif @endif class="btn my-bet-team mt-2 @if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},1)" ><span class="">@if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) {{$games_change_history->spread1_t1}} @else {{$games->spread1_t1}} @endif</span><span
															class="">@if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) {{$games_change_history->spread2_t1}} @else {{$games->spread2_t1}} @endif</span><span id="result_{{$games_change_history->id.'_1'}}" style="font-size: 10px;">@if($bet_status_win_box_array[1-1] == '0')  @elseif($bet_status_win_box_array[1-1] == '1') Loss @elseif($bet_status_win_box_array[1-1] == '2') Win @elseif($bet_status_win_box_array[1-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_2'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->moneyline_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},2)" ><span class="">@if(!empty($games_change_history->moneyline_t1)) {{$games_change_history->moneyline_t1}} @else {{$games->moneyline_t1}} @endif</span><span id="result_{{$games_change_history->id.'_2'}}" style="font-size: 10px;">@if($bet_status_win_box_array[2-1] == '0')  @elseif($bet_status_win_box_array[2-1] == '1') Loss @elseif($bet_status_win_box_array[2-1] == '2') Win @elseif($bet_status_win_box_array[2-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_3'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},3)" > <span class="">@if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) {{$games_change_history->total1_type_t1.' '.$games_change_history->total1_t1}} @else {{$games->total1_type_t1.' '.$games->total1_t1}} @endif</span><span
															class="">@if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) {{$games_change_history->total1_t1}} @else {{$games->total1_t1}} @endif</span><span id="result_{{$games_change_history->id.'_3'}}" style="font-size: 10px;">@if($bet_status_win_box_array[3-1] == '0')  @elseif($bet_status_win_box_array[3-1] == '1') Loss @elseif($bet_status_win_box_array[3-1] == '2') Win @elseif($bet_status_win_box_array[3-1] == '3') Draw @endif</span></button></div>
											</div>
										</li>
									</ul>
								</div>
								<div class="box-body box-profile">

								

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_4'}}" @if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) @if($games_change_history->bet_status_win == 0)  @endif @endif class="btn my-bet-team mt-2 @if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},4)" ><span class="">@if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) {{$games_change_history->spread1_t2}} @else {{$games->spread1_t2}} @endif</span><span
															class="">@if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) {{$games_change_history->spread2_t2}} @else {{$games->spread2_t2}} @endif</span><span id="result_{{$games_change_history->id.'_4'}}" style="font-size: 10px;">@if($bet_status_win_box_array[4-1] == '0')  @elseif($bet_status_win_box_array[4-1] == '1') Loss @elseif($bet_status_win_box_array[4-1] == '2') Win @elseif($bet_status_win_box_array[4-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_5'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->moneyline_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},5)" ><span class="">@if(!empty($games_change_history->moneyline_t2)) {{$games_change_history->moneyline_t2}} @else {{$games->moneyline_t2}} @endif</span><span id="result_{{$games_change_history->id.'_5'}}" style="font-size: 10px;">@if($bet_status_win_box_array[5-1] == '0')  @elseif($bet_status_win_box_array[5-1] == '1') Loss @elseif($bet_status_win_box_array[5-1] == '2') Win @elseif($bet_status_win_box_array[5-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_6'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},6)" > <span class="">@if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) {{$games_change_history->total1_type_t2.' '.$games_change_history->total1_t2}} @else {{$games->total1_type_t2.' '.$games->total1_t2}} @endif</span><span
															class="">@if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) {{$games_change_history->total1_t2}} @else {{$games->total1_t2}} @endif</span><span id="result_{{$games_change_history->id.'_6'}}" style="font-size: 10px;">@if($bet_status_win_box_array[6-1] == '0')  @elseif($bet_status_win_box_array[6-1] == '1') Loss @elseif($bet_status_win_box_array[6-1] == '2') Win @elseif($bet_status_win_box_array[6-1] == '3') Draw @endif</span></button></div>
											</div>
										</li>
									</ul>
								</div>

                            <div class="box-body box-profile">

                            

                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item team-row game_history_updated_at">
                                        <div class="row team-row">
                                                <div class="col-md-6">{{$games_change_history->created_at}}</div>
												<div class="col-md-6"><a href="{{url('admin/bets/'.$games_change_history->game_id.'/'.$games_change_history->id)}}" class="btn btn-success btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                            
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                        </div> 
                </div>
                @endforeach
            </div>
        </div>

         <!-- Modal -->
         <div class="modal fade" id="choose_result_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Choose Result</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                    <div class="modal-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="hidden" className="form-control" name="games_change_history_id" id="games_change_history_id" />
                                    <input type="hidden" className="form-control" name="game_id" id="game_id" />
                                    <input type="hidden" className="form-control" name="bet_box" id="bet_box" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-success" onclick="bet_status_submit(2);"><span
                                    class=""></span><span class="">Win</span></button>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-danger" onclick="bet_status_submit(1);"><span
                                    class=""> Loss </span></button>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-warning" onclick="bet_status_submit(3);"><span
                                    class=""> Draw </span></button>
                        </div>
                    </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>

  <script>
        function bet_status(games_change_history_id, game_id, box){		
			$("#games_change_history_id").val(games_change_history_id);
            $("#game_id").val(game_id);
            $("#bet_box").val(box);
            $('#choose_result_modal').modal('toggle');
		}

        function bet_status_submit(result){
            var games_change_history_id = $("#games_change_history_id").val();
            var game_id = $("#game_id").val();
            var box = $("#bet_box").val();		
			if (confirm('Are you sure you want to change the bet-status of all bets inside it?')) {
				var token = "{{ csrf_token() }}";
				$.ajax({
					url: '/admin/change_bet_status',
					type: "POST",
					headers: {
						'X-CSRF-TOKEN': token
					},
					data: {games_change_history_id:games_change_history_id, game_id:game_id, bet_box:box, result:result, token:token},
					success: function(response) {
                        $('#choose_result_modal').modal('toggle');
						var data = JSON.parse(response);
                        if(data.message_title == 'success'){
                            if(result == 1){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-danger");
                                $("#result_"+games_change_history_id+"_"+box).text('Loss');
                            } else if(result == 2){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-success");
                                $("#result_"+games_change_history_id+"_"+box).text('Win');
                            } else if(result == 3){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-warning");
                                $("#result_"+games_change_history_id+"_"+box).text('Draw');
                            }
                            
                            alert(data.message);
                        }else{
                            alert(data.message);
                        }
                        
					}
				});	
			}else{
				return false;
			}	
		}
  </script>
	
@endsection
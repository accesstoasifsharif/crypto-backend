@extends('layouts.appdashboard')

@section('content')

<style>
	input.btn.btn-success {
	    margin-left: 80%;
	}
	.table-responsive {
	    width: 100%;
	}
	.onoffswitch {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
	    display: none;
	}
	.onoffswitch-label {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner:before, .onoffswitch-inner:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner:before {
	    content: "Unfreeze";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner:after {
	    content: "Freeze";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	    margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
	    right: 0px; 
	}


	.onoffswitch1 {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox1 {
	    display: none;
	}
	.onoffswitch-label1 {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner1 {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner1:before, .onoffswitch-inner1:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner1:before {
	    content: "Downgrade";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner1:after {
	    content: "Upgrade";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch1 {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
	    margin-left: 0;
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
	    right: 0px; 
	}

	.col-md-6.search-form {
	    width: 100%;
	}
	/* 9th april 2022 */
	section.content-header {
	    display: flex;
	    justify-content: space-between;
	    align-items: center;
	}
	.set_def {
    display: none;
}
 .input_two {
    margin-bottom: 10px;
} 
.spc_d_input {
	display:none;
    /* display: flex; */
    gap: 2rem;
    width: 71%;
    padding: 15px 0px;
    margin: 0px 50px 0px 0;
}
.set_dollar {
    display: flex;
}
.spc_row_set {
    display: flex;
    gap: 2rem;
    border: 1px solid #ccc;
    padding: 18px 0px;
    margin: 5px 0px 5px 0;
    width: 40%;
    justify-content: center;
}
.set_simple_input:focus-visible{
	border:none;
	outline:none;
}
.set_simple_input {
    border: none;
    background-color: transparent;
    width: 79px;
    display: block;
}
	/* //9th april 2022 */


	.bet_detail_flag{
            width: 40px;
            height: 40px;
            max-width: 40px;
            max-height: 40px;
            float: right;
        }
        .game_detail_flag{
            width: 30px;
            height: 30px;
            max-width: 30px;
            max-height: 30px;
            float: right;
            background-color: #000;
        }
        .game_detail_view_bet{
            margin: -5px 0;
        }
        .team-row {
            width: 100%;
            justify-content: left !important;
        }
        .my-bet-team {
            background: #000000 0% 0% no-repeat padding-box;
            border-radius: 4px !important;
            padding: 4px 25px 5px 25px !important;
            display: grid;
            min-height: 60px;
            font-size: 16px;
            min-width: 70px;
            color: white;
            align-content: center;
            cursor: none;
        }
        .team-row div span {
            font-weight: bold;
        }
        .game_history_header .box-body.box-profile, .game_history_body .box-body.box-profile{
            width: 33.3%;
            float: left;
        }
        .game_history_list{
            border-top-color: #db1fff !important;
            transition: .5s;
            border-radius: 7px !important;
            display: inline-block;
            width: 100%;
            background:#fff;
            border-top: 3px solid #d2d6de;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            padding: 10px;
        }
        .game_history_list .box-primary{
            border:0 !important;
            background: inhherit;
        }

		.game_history_title{
			font-size: 24px;
    		font-weight: 400;
		}

		.game_history_updated_at{
			padding: 30px 15px;
		}

		.my-bet-team-black-bg {
            background: #000000 0% 0% no-repeat padding-box;
			cursor: pointer;
        }

        .my-bet-team-green-bg {
            background: green 0% 0% no-repeat padding-box;
			cursor: pointer;
        }
        .my-bet-team-choose-result {
            border-radius: 4px !important;
            padding: 4px 25px 5px 25px !important;
            display: grid;
            min-height: 60px;
            font-size: 16px;
            min-width: 70px;
            color: white;
            align-content: center;
        }
        .my-bet-team-choose-result:hover{
            padding: 4px 25px 5px 25px !important;
        }
        #choose_result_modal .modal-dialog {
            width: 438px;
            margin: 178px auto 0;
        }
        #choose_result_modal .modal-footer {
            margin: 39px 0 0 0;
        }
        #choose_result_modal .modal-header .close {
            margin-top: -23px;
        }
        #choose_result_modal .modal-title{
            font-size: 18px;
            font-weight: 600;
        }
</style>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
	@if(empty($id))
      <h1>
        Add Games
       <!-- <small>it all starts here</small> -->
      </h1>
	  @else
	  <h1>
        Edit Games
       <!-- <small>it all starts here</small> -->
      </h1>
	@endif
      <a href="{{url('admin/games_by_type/games/'.$game_type)}}">
		<button type="button" class="btn btn-success"> Back</button>
	  </a>
	  </section>

	  <section class="content">
		<div class="box box-primary">
			<div class="box-header with-border table-responsive">
				<table class="table table-bordered">
					<form action="{{url('admin/games_by_type/store_games/'.$game_type)}}" method="post"  enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" value="@if(!empty($id)){{$id}}@endif">
						
						<div class="form-group">
							<label for="game_type" class="control-label mb-1">Games Type</label>
							<select id="game_type" class="selectpicker form-control"  name="game_type" required>
                            <option value=''>Select</option>
							@foreach($response as $res)
							
							@if($res->id == $game_type)
                             <option selected value="{{$res->id}}">{{$res->game_type}}</option>
							@endif

							 @endforeach
							
							 </select>
						</div>
						<div class="form-group">
							<label for="team1_id" class="control-label mb-1">Team 1 Name</label>
							<!-- <input id="team1_id"  name="team1_id" value="@if($response1 !='') {{!$response1->team1}}@endif" type="text" class="form-control" aria-required="true" aria-invalid="false" disabled required> -->
                            <select id="team1_id" class="selectpicker form-control" name="team1_id" onchange="get_team1_id(this);" required>
                            <option value=''>Select</option>
							@foreach($result as $res)
							
                             <option @if($response1 !='') @if($res->id == $response1->team1_id){{ 'selected' }}@endif @endif value="{{$res->id}}">{{$res->name}}</option>

							 @endforeach
							
							 </select>
                            
						</div>

                        <div class="form-group">
							<label for="team2_id" class="control-label mb-1">Team 2 Name</label>
							<!-- <input id="team2_id"  name="team2_id" type="text"  value="@if($response1 !='') {{!$response1->team2}}@endif" class="form-control" aria-required="true" aria-invalid="false" required> -->
                            <select id="team2_id" class="selectpicker form-control" name="team2_id" onchange="get_team2_id(this);" required>
                            <option value=''>Select</option>
							@foreach($result as $res)
							
                             <option @if($response1 !='') @if($res->id == $response1->team2_id){{ 'selected' }}@endif @endif value="{{$res->id}}">{{$res->name}}</option>

							@endforeach
							
							 </select>
                            
						</div>

                        
						
                        <div class="form-group" id="bet_calculator">
							<label for="game_plan" class="control-label mb-1"></label>
							<div>
								<div>
									<label for="bet_amount" class="control-label mb-1 input_one set_def">Bet Amount
									<input id="bet_amount"  type="text" name="bet_amount"  value="" class="bet_amount input_one plus_input" aria-required="true" aria-invalid="false" >
									</label>
									<label for="american_ods" class="control-label mb-1 input_two set_def">American Odds
									<input  type="text" name="american_ods" value="@if($response1 !=''){{ $response1->american_ods}}@endif" class="american_ods input_two plus_input_2" id="american_ods" aria-required="true" aria-invalid="false" onkeyup="american_to_decimal() ">
									</label>
								</div>
								<label for="decimal_ods" class="control-label mb-1 set_def">Decimal Odds
								<input  type="text" name="decimal_ods" value="@if($response1 !=''){{ $response1->decimal_ods}}@endif" class="decimal_ods" id="decimal_ods" aria-required="true" aria-invalid="false" onkeyup="decimal_to_american()" >
								</label>
								<label for="fractional_ods" class="control-label mb-1 set_def">Fractional Odds
								<input id="fractional_ods" name="fractional_ods" type="text"  value="@if($response1 !=''){{ $response1->fractional_ods}}@endif" class="fractional_ods" aria-required="true" onkeyup="decimal_to_fractional()" aria-invalid="false" >
								</label>
								<label for="implide_ods" class="control-label mb-1 set_def">Implied Odds
								<input id="implide_ods" name="implide_ods" type="text"  value="@if($response1 !=''){{ $response1->implide_ods}}@endif" class="implide_ods" aria-required="true" onkeyup="convert_to_impliedodds()" aria-invalid="false" >
								</label>
								<div class="spc_d_input ">
									<div class="row spc_row_set ">
									<label for="to_win" class="control-label mb-1 set_def">To Win
										<div class="set_dollar">
											<span >$</span>
											<input id="to_win"  type="text" name=""  value="" class="to_win set_simple_input" aria-required="true" onkeyup="to_win_odd()" aria-invalid="false" >  
										</div>
									</label>
								<label for="pay_out" class="control-label mb-1 set_def">Payout
									
									<div class="set_dollar" >			
										<span >$</span>
										<input id="pay_out"  type="text"  name="" value="" class="pay_out set_simple_input" aria-required="true" onkeyup="pay_out_odds()" aria-invalid="false" >
                    				</div>
								</label>
							 </div>
							</div>
							
							
						</div>

                        <!-- <div class="form-group">
							<label for="winning_percentage_team1" class="control-label mb-1">Winning Percentage of Team1 </label>
							<span id="slider_value1" style="color:red;"></span>
                            <input id="winning_percentage_team1" name="winning_percentage_team1" type="range" class="team_points" min="0" max="100" step=".1" value="@if($response1 !=''){{ $response1->winning_percentage_team1}}@endif" onchange="show_value1(this.value);">
							
							<input id="winning_percentage_team1"  name="winning_percentage_team1" value="@if($response1 !='') {{!$response1->winning_percentage_team1}}@endif" type="text" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>
						
                        <div class="form-group">
							<label for="winning_percentage_team2" class="control-label mb-1">Winning Percentage of Team2</label>										
                            <span id="slider_value" style="color:red;"></span>
							<input id="winning_percentage_team2" name="winning_percentage_team2" type="range" class="team_points" min="0" max="100" step=".1" value="@if($response1 !=''){{ $response1->winning_percentage_team2}}@endif" onchange="show_value(this.value);">
							<input id="winning_percentage_team2"   name="winning_percentage_team2" value="@if($response1 !='') {{!$response1->winning_percentage_team2}}@endif" type="text" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div> -->


						<div class="form-group">
							<input id="winning_percentage_team1"  name="winning_percentage_team1" value="@if($response1 !=''){{$response1->winning_percentage_team1}}@else{{50}}@endif" type="hidden" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>
						
                        <div class="form-group">
							<input id="winning_percentage_team2"   name="winning_percentage_team2" value="@if($response1 !=''){{$response1->winning_percentage_team2}}@else{{50}}@endif" type="hidden" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>


                        <div class="form-group">
							<label for="max_no_of_bets" class="control-label mb-1">Maximum no of bets per user</label>
							<input id="max_no_of_bets"  name="max_no_of_bets" type="text" value="@if($response1 !=''){{ $response1->max_no_of_bets}}@endif" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>

                        <div class="form-group">
							<label for="max_amount_user_bet" class="control-label mb-1">Maximum amount for bet</label>
							<input id="max_amount_user_bet"  name="max_amount_user_bet" type="text" value="@if($response1 !='') {{$response1->max_amount_user_bet}}@endif" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>

                        <div class="form-group">
							<label for="game_date" class="control-label mb-1">Game Date</label>
							@php
							$game_date = '';
							if($response1 !=''){ 
								$date = new DateTime($response1->game_date); 
								$game_date= $date->format('Y-m-d\TH:i:s');  
							}
							@endphp
							<input id="game_date"  name="game_date" type="datetime-local" class="form-control" value="@if($response1 !=''){{$game_date}}@endif" aria-required="true" aria-invalid="false" required>	
							 
						</div>

                        <div class="form-group">
							<label for="venue" class="control-label mb-1">Venue</label>
							<input id="venue"  name="venue" type="text" class="form-control" value="@if($response1 !='') {{$response1->venue}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>

                        <div class="form-group">
							<label for="result" class="control-label mb-1">Result</label>
							<!-- <input id="result"  name="result" type="text" class="form-control" value="@if($response1 !='') {{!$response1->result}}@endif" aria-required="true" aria-invalid="false" required> -->	 

							<select id="result" class="form-control" name="result" required>
                            <option value=''>Select</option>
							<option value='0' @if($response1 !='') @if($response1->result== '0') Selected @endif @endif>No Result</option>
							<option value='1' @if($response1 !='') @if($response1->result== '1') Selected @endif @endif>Team1 Won</option>
							<option value='2' @if($response1 !='') @if($response1->result== '2') Selected @endif @endif>Team2 Won</option>
							<option value='3' @if($response1 !='') @if($response1->result== '3') Selected @endif @endif>Draw</option>
							</select>

						</div>

                        <div class="form-group">
							<label for="timeline" class="control-label mb-1">Timeline</label>
							<!-- <input id="timeline"  name="timeline" type="text" value="@if($response1 !='') {{$response1->timeline}}@endif" class="form-control" aria-required="true" aria-invalid="false" required> -->
								
							<select id="timeline" class="form-control" name="timeline" required>
                            <option value=''>Select</option>
							<option value='0' @if($response1 !='') @if($response1->timeline== '0') Selected @endif @endif>Pending</option>
							<option value='1' @if($response1 !='') @if($response1->timeline== '1') Selected @endif @endif>Start</option>
							<option value='2' @if($response1 !='') @if($response1->timeline== '2') Selected @endif @endif>Completed</option>
							</select>
						</div>

						<div class="form-group">
							<label for="spread1_t1" class="control-label mb-1">spread1_t1</label>
							<input id="spread1_t1"  name="spread1_t1" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->spread1_t1}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="spread2_t1" class="control-label mb-1">spread2_t1</label>
							<input id="spread2_t1"  name="spread2_t1" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->spread2_t1}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="moneyline_t1" class="control-label mb-1">moneyline_t1</label>
							<input id="moneyline_t1"  name="moneyline_t1" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->moneyline_t1}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total1_t1" class="control-label mb-1">total1_t1</label>
							<input id="total1_t1"  name="total1_t1" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->total1_t1}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total2_t1" class="control-label mb-1">total2_t1</label>
							<input id="total2_t1"  name="total2_t1" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->total2_t1}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total1_type_t1" class="control-label mb-1">total1_type_t1</label>
							<!-- <input id="timeline"  name="timeline" type="text" value="@if($response1 !='') {{$response1->timeline}}@endif" class="form-control" aria-required="true" aria-invalid="false" required> -->
								
							<select id="total1_type_t1" class="form-control" name="total1_type_t1" required >
							<option value=''>Select</option>
							<option value='O' @if($response1 !='') @if($response1->total1_type_t1== 'O') Selected @endif @endif>Over</option>
							<option value='U' @if($response1 !='') @if($response1->total1_type_t1== 'U') Selected @endif @endif>Under</option>
							</select>
						</div>

						<div class="form-group">
							<label for="spread1_t2" class="control-label mb-1">spread1_t2</label>
							<input id="spread1_t2"  name="spread1_t2" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->spread1_t2}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="spread2_t2" class="control-label mb-1">spread2_t2</label>
							<input id="spread2_t2"  name="spread2_t2" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->spread2_t2}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="moneyline_t2" class="control-label mb-1">moneyline_t2</label>
							<input id="moneyline_t2"  name="moneyline_t2" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->moneyline_t2}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total1_t2" class="control-label mb-1">total1_t2</label>
							<input id="total1_t2"  name="total1_t2" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->total1_t2}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total2_t2" class="control-label mb-1">total2_t2</label>
							<input id="total2_t2"  name="total2_t2" type="number" step=".01" class="form-control" value="@if($response1 !=''){{$response1->total2_t2}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>
						<div class="form-group">
							<label for="total1_type_t2" class="control-label mb-1">total1_type_t2</label>
							<!-- <input id="timeline"  name="timeline" type="text" value="@if($response1 !='') {{$response1->timeline}}@endif" class="form-control" aria-required="true" aria-invalid="false" required> -->
								
							<select id="total1_type_t2" class="form-control" name="total1_type_t2" required >
							<option value=''>Select</option>
							<option value='O' @if($response1 !='') @if($response1->total1_type_t2== 'O') Selected @endif @endif>Over</option>
							<option value='U' @if($response1 !='') @if($response1->total1_type_t2== 'U') Selected @endif @endif>Under</option>
							</select>
						</div>
								
						<div>
						<button id="payment-button" type="submit" class="btn btn-lg btn-info">
							Submit
						</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
						</form>
					</div>
					</div>
			 <!-- END DATA TABLE-->
        </div>
       </table>
						</div>
					</div>
					@if($response1 !='')
					<div class="game_history_list">
				<div class="row game_history_header">
					<div class="col-md-12">
							<h5 class="game_history_title text-center">Game History</h5>
							<div class="box box-primary">
								<div class="box-body box-profile">

									<h4 class="text-muted text-center">Team1 Info</h4>

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><span>SPREAD</span></div>
												<div class="col-md-4"><span>MONEYLINE</span></div>
												<div class="col-md-4"><span>OVER/UNDER</span></div>
											</div>
										</li>
									</ul>
								</div>
								<div class="box-body box-profile">

									<h4 class="text-muted text-center">Team2 Info</h4>

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><span>SPREAD</span></div>
												<div class="col-md-4"><span>MONEYLINE</span></div>
												<div class="col-md-4"><span>OVER/UNDER</span></div>
											</div>
										</li>
									</ul>
								</div>

								<div class="box-body box-profile">

									<h4 class="text-muted text-center">Action</h4>

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
											<div class="col-md-6"><span>Updated At</span></div>
												<div class="col-md-6"><span>View Bets</span></div>
											</div>
										</li>
									</ul>
								</div>
								
							</div> 
					</div>
				</div>
				<div class="row game_history_body">
					@foreach($response1->games_change_history as $games_change_history)
                @php
                    $bet_status_win_box_array = explode(",",$games_change_history->bet_status_win);
                @endphp
					<div class="col-md-12">
							<div class="box box-primary">
							<div class="box-body box-profile">

								

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_1'}}" @if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1))  @if($games_change_history->bet_status_win == 0) @endif @endif class="btn my-bet-team mt-2 @if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},1)" ><span class="">@if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) {{$games_change_history->spread1_t1}} @else {{$response1->spread1_t1}} @endif</span><span
															class="">@if(!empty($games_change_history->spread1_t1) || !empty($games_change_history->spread2_t1)) {{$games_change_history->spread2_t1}} @else {{$response1->spread2_t1}} @endif</span><span id="result_{{$games_change_history->id.'_1'}}" style="font-size: 10px;">@if($bet_status_win_box_array[1-1] == '0')  @elseif($bet_status_win_box_array[1-1] == '1') Loss @elseif($bet_status_win_box_array[1-1] == '2') Win @elseif($bet_status_win_box_array[1-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_2'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->moneyline_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},2)" ><span class="">@if(!empty($games_change_history->moneyline_t1)) {{$games_change_history->moneyline_t1}} @else {{$response1->moneyline_t1}} @endif</span><span id="result_{{$games_change_history->id.'_2'}}" style="font-size: 10px;">@if($bet_status_win_box_array[2-1] == '0')  @elseif($bet_status_win_box_array[2-1] == '1') Loss @elseif($bet_status_win_box_array[2-1] == '2') Win @elseif($bet_status_win_box_array[2-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_3'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},3)" > <span class="">@if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) {{$games_change_history->total1_type_t1.' '.$games_change_history->total1_t1}} @else {{$response1->total1_type_t1.' '.$response1->total1_t1}} @endif</span><span
															class="">@if(!empty($games_change_history->total1_t1) || !empty($games_change_history->total2_t1)) {{$games_change_history->total1_t1}} @else {{$response1->total1_t1}} @endif</span><span id="result_{{$games_change_history->id.'_3'}}" style="font-size: 10px;">@if($bet_status_win_box_array[3-1] == '0')  @elseif($bet_status_win_box_array[3-1] == '1') Loss @elseif($bet_status_win_box_array[3-1] == '2') Win @elseif($bet_status_win_box_array[3-1] == '3') Draw @endif</span></button></div>
											</div>
										</li>
									</ul>
									</div>
									<div class="box-body box-profile">



									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row">
											<div class="row team-row">
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_4'}}" @if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) @if($games_change_history->bet_status_win == 0)  @endif @endif class="btn my-bet-team mt-2 @if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},4)" ><span class="">@if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) {{$games_change_history->spread1_t2}} @else {{$response1->spread1_t2}} @endif</span><span
															class="">@if(!empty($games_change_history->spread1_t2) || !empty($games_change_history->spread2_t2)) {{$games_change_history->spread2_t2}} @else {{$response1->spread2_t2}} @endif</span><span id="result_{{$games_change_history->id.'_4'}}" style="font-size: 10px;">@if($bet_status_win_box_array[4-1] == '0')  @elseif($bet_status_win_box_array[4-1] == '1') Loss @elseif($bet_status_win_box_array[4-1] == '2') Win @elseif($bet_status_win_box_array[4-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_5'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->moneyline_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},5)" ><span class="">@if(!empty($games_change_history->moneyline_t2)) {{$games_change_history->moneyline_t2}} @else {{$response1->moneyline_t2}} @endif</span><span id="result_{{$games_change_history->id.'_5'}}" style="font-size: 10px;">@if($bet_status_win_box_array[5-1] == '0')  @elseif($bet_status_win_box_array[5-1] == '1') Loss @elseif($bet_status_win_box_array[5-1] == '2') Win @elseif($bet_status_win_box_array[5-1] == '3') Draw @endif</span></button></div>
												<div class="col-md-4"><button type="button" id="button_{{$games_change_history->id.'_6'}}" class="btn my-bet-team mt-2 @if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) my-bet-team-green-bg @else my-bet-team-black-bg @endif"  onclick="bet_status({{$games_change_history->id}},{{$games_change_history->game_id}},6)" > <span class="">@if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) {{$games_change_history->total1_type_t2.' '.$games_change_history->total1_t2}} @else {{$response1->total1_type_t2.' '.$response1->total1_t2}} @endif</span><span
															class="">@if(!empty($games_change_history->total1_t2) || !empty($games_change_history->total2_t2)) {{$games_change_history->total1_t2}} @else {{$response1->total1_t2}} @endif</span><span id="result_{{$games_change_history->id.'_6'}}" style="font-size: 10px;">@if($bet_status_win_box_array[6-1] == '0')  @elseif($bet_status_win_box_array[6-1] == '1') Loss @elseif($bet_status_win_box_array[6-1] == '2') Win @elseif($bet_status_win_box_array[6-1] == '3') Draw @endif</span></button></div>
											</div>
										</li>
									</ul>
								</div>

								<div class="box-body box-profile">

								

									<ul class="list-group list-group-unbordered">
										<li class="list-group-item team-row game_history_updated_at">
											<div class="row team-row">
												<div class="col-md-6">{{$games_change_history->created_at}}</div>
												<div class="col-md-6"><a href="{{url('admin/bets/'.$games_change_history->game_id.'/'.$games_change_history->id)}}" class="btn btn-success btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
												
											</div>
										</li>
									</ul>
								</div>
								
							</div> 
					</div>
					@endforeach
					
				</div>
			</div>
			@endif

			 <!-- Modal -->
			 <div class="modal fade" id="choose_result_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Choose Result</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                    <div class="modal-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="hidden" className="form-control" name="games_change_history_id" id="games_change_history_id" />
                                    <input type="hidden" className="form-control" name="game_id" id="game_id" />
                                    <input type="hidden" className="form-control" name="bet_box" id="bet_box" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-success" onclick="bet_status_submit(2);"><span
                                    class=""></span><span class="">Win</span></button>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-danger" onclick="bet_status_submit(1);"><span
                                    class=""> Loss </span></button>
                        </div>
                        <div class="col-md-4"><button type="button" class="btn my-bet-team-choose-result mt-2 btn-warning" onclick="bet_status_submit(3);"><span
                                    class=""> Draw </span></button>
                        </div>
                    </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
				</section>
    </div>
    <script>
		$(document).ready(function(){
			var team1_id = $("#team1_id").val();
			var team2_id = $("#team2_id").val();
			//console.log([team1_id, team2_id]);
			if(team1_id !== '') $("#team2_id option[value='"+team1_id+"']").remove();
			if(team2_id !== '') $("#team1_id option[value='"+team2_id+"']").remove();
		});

		function get_team1_id(team){
			var team_id = team.options[team.selectedIndex].value;
				var team2_id = $('#team2_id').val();
                $('#team2_id').empty();
                $('#team2_id').append($('<option>').text("Select"));
				var response = <?php echo json_encode($result); ?>;
                $.each(response, function (key, value) {
                        $('#team2_id').append($('<option>').text(value.name).attr('value', value.id));
                });
				$('#team2_id').val(team2_id);
			$("#team2_id option[value='"+team_id+"']").remove();
			
			
		}

		function get_team2_id(team){
			var team_id = team.options[team.selectedIndex].value;
				var team1_id = $('#team1_id').val();
				$('#team1_id').empty();
                $('#team1_id').append($('<option>').text("Select"));
				var response = <?php echo json_encode($result); ?>;
                $.each(response, function (key, value) {
                        $('#team1_id').append($('<option>').text(value.name).attr('value', value.id));
                });
				$('#team1_id').val(team1_id);
			$("#team1_id option[value='"+team_id+"']").remove();
			
		}
		function bet_status(games_change_history_id, game_id, box){		
			$("#games_change_history_id").val(games_change_history_id);
            $("#game_id").val(game_id);
            $("#bet_box").val(box);
            $('#choose_result_modal').modal('toggle');
		}

        function bet_status_submit(result){
            var games_change_history_id = $("#games_change_history_id").val();
            var game_id = $("#game_id").val();
            var box = $("#bet_box").val();		
			if (confirm('Are you sure you want to change the bet-status of all bets inside it?')) {
				var token = "{{ csrf_token() }}";
				$.ajax({
					url: '/admin/change_bet_status',
					type: "POST",
					headers: {
						'X-CSRF-TOKEN': token
					},
					data: {games_change_history_id:games_change_history_id, game_id:game_id, bet_box:box, result:result, token:token},
					success: function(response) {
                        $('#choose_result_modal').modal('toggle');
						var data = JSON.parse(response);
                        if(data.message_title == 'success'){
                            if(result == 1){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-danger");
                                $("#result_"+games_change_history_id+"_"+box).text('Loss');
                            } else if(result == 2){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-success");
                                $("#result_"+games_change_history_id+"_"+box).text('Win');
                            } else if(result == 3){
                                //$("#button_"+games_change_history_id+"_"+box).addClass("btn-warning");
                                $("#result_"+games_change_history_id+"_"+box).text('Draw');
                            }
                            
                            alert(data.message);
                        }else{
                            alert(data.message);
                        }
                        
					}
				});	
			}else{
				return false;
			}	
		}
       function show_value(x)
    { 
      document.getElementById("slider_value").innerHTML=x;
	  var slide1 =parseFloat(100) - parseFloat(x);
	  slide11 =slide1.toFixed(2);
	  document.getElementById("slider_value1").innerHTML=slide11;
    }

	function show_value1(x)
    {
      document.getElementById("slider_value1").innerHTML=x;
	  var slide2 = parseFloat(100) - parseFloat(x);
	  slide12 =slide2.toFixed(2);
	  document.getElementById("slider_value").innerHTML =slide12;
	  /* var numb = to_win;
		numb1 = numb.toFixed(2);
		$('#to_win').val(numb1); */
    }
	 

	/* window.addEventListener('DOMContentLoaded', (event) => {
    	console.log('DOM fully loaded and parsed');
	}); */
	</script>
	<script>
		// $( window ).on( "load", function() {
		// 	console.log( $('#winning_percentage_team2').val() );
		// 	show_value($('#winning_percentage_team2').val());
		// });
	</script>
	<!-- <script>
		$(document).ready(()=>{
			console.log('document ready fired')
		})
	</script> -->

    <script>
		var elements = document.querySelectorAll('.team_points');
		
		
		var length = elements.length;
		var sliders = Array.prototype.slice.call(elements); // Copy of `elements` but as a real array
		var max = 100;

		function change(current) {
			"use strict";
			
			set(current);

			var input = +current.value;
			var delta = max - input;
			var sum = 0;
			var siblings = [];

			// Sum of all siblings
			sliders.forEach(function (slider) {
				if (current != slider) {
					siblings.push(slider); // Register as sibling
					sum += +slider.value;
				}
			});

			// Update all the siblings
			var partial = 0;
			siblings.forEach(function (slider, i) {
				var val = +slider.value;
				var fraction = 0;

				// Calculate fraction
				if (sum <= 0) {
					fraction = 1 / (length - 1)
				} else {
					fraction = val / sum;
				}

				// The last element will correct rounding errors
				if (i >= length - 1) {
					val = max - partial;
				} else {
					
					//val = Math.round(delta * fraction);
					val = Number.parseFloat(delta * fraction).toFixed(2);
					partial += val;
				}

				set(slider, val);
			});
		}

		// Set value on a slider
		function set(elm, val) {
			if (val) {
				elm.value = val;
			}
			// Hack to trigger CSS ::after content to be updated
			elm.setAttribute('value', elm.value);
		}

		// Add event listeners to the DOM elements
		for (var i = 0, l = elements.length; i < l; i++)  {
			
			elements[i].addEventListener('change', function (e) {
				change(this);
			}, false);
		}

</script>


<script>
						/* bet_amount
						american_ods
						decimal_ods
						fractional_ods
						implide_ods
						to_win
						pay_out */

	function american_to_decimal()
	{
		
		var americamodd= $('#american_ods').val();
		//console.log(americamodd);
		if(americamodd > 0){
			var convert = (1 + (americamodd/100));
		}else{
			var convert = (1 - (100/americamodd));
		}
		
		if(convert != 'Infinity' && convert != '-Infinity'){
			$('#decimal_ods').val(convert);	
			
		}else{	
		
		}
		to_win_odd();
		pay_out_odds();
		
	}

	function decimal_to_american()
	{
		var decimalodd= $('#decimal_ods').val();
		if(decimalodd >= 2){
			var convert = ((decimalodd-1) * 100);
		}else{
			var convert = ((-100)/(decimalodd-1));
		}
		//console.log(convert);
		if(convert != 'Infinity' && convert != '-Infinity'){
			$('#american_ods').val(convert);	
		}else{	
		
		}
		
		if(decimalodd > 0 ){
			var dec_convert = ((decimalodd -1 )/1);
		}else{
			var dec_convert = (decimalodd -1 );
		}
		$('#fractional_ods').val(dec_convert);
		pay_out_odds();
	}

	/* function decimal_to_fractional()
	{

		var fractionalods= $('#fractional_ods').val();
		if(fractionalods >=10 ){
			var convert = ((fractionalods -1 )/1);
		}else{
			var convert = (fractionalods -1 );
		}
		
		$('#decimal_ods').val(convert);	
		pay_out_odds();
	} */

	function decimal_to_fractional()
	{

		var fractionalods= $('#decimal_ods').val();
		
		var convert = ((fractionalods -1 )/1);
		
		
		$('#fractional_ods').val(convert);	
		pay_out_odds();
	}

	function convert_to_impliedodds()
	{    
		var betamount=$('#bet_amount').val();
		var decimal_ods=$('#decimal_ods').val();
		var convert_to_impliedodds= ((betamount) / (decimal_ods ));
		$('#implide_ods').val(convert_to_impliedodds);
	}

	
	function to_win_odd(){
		//alert(to_win);
		if($('#american_ods').val() > -100 ){
			var to_win = $('#american_ods').val();
		}else{
			var to_win = (100 + (100 / $('#american_ods').val()));
		}
		var numb = to_win;
		numb1 = numb.toFixed(2);
		$('#to_win').val(numb1);
	}	
	

	function pay_out_odds(){
		var decimaloddss= $('#decimal_ods').val();
		var betamount= $('#bet_amount').val();
		var pay_out_ods = ((betamount) * (decimaloddss));
		//var pay_out_ods1 = Math.round((pay_out_ods + Number.EPSILON) * 100) / 100
		var numb = pay_out_ods;
		numb = numb.toFixed(2);
		//alert(pay_out_ods);
		$('#pay_out').val(numb);

	}

</script>
                        
@endsection
@extends('layouts.appdashboard')

@section('content')
<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		

		/* 9th april 2022 */ 
		section.content-header{
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		/* //9th april 2022 */
		.--switch {
		position: relative;
		display: inline-block;
		width: 87px;
		height: 21px;
		}

		.--switch input {
		display:none;
		}

		.--slider .fa-check {
			color: #FFFFFF;
			position: absolute;
			left: 0px;
			font-size: 12px;
			top: 1px;
			width:100%;
			margin-bottom: 0;
		}
		.--slider .fa-times {
			color: #FFFFFF;
			position:absolute;
			font-size:12px;
			right:0px; 
			width:100%;
			text-align: center;
			top:1px;
			margin-bottom: 0;
		}
		.fa-check:before {
			content: "\f00c";
			display:none;
		}

		.--slider {
		position: absolute;
		cursor: pointer;
		display:flex;
		align-items:center;
		top:0;
		left:0;
		right:0;
		bottom:0;
		background-color: #db3a34;
		-webkit-transition: .4s;
		transition: .4s;
		border-radius:50px;
		}

		.--slider:before {
			position: absolute;
			content: "";
			height: 19px;
			width: 19px;
			left: 1px;
			bottom: 0px;
			border-radius: 50%;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
			top: 1px;
		}

		.fa-remove:before, .fa-close:before, .fa-times:before {
			content: "\f00d";
			display:none;
		}

		.--switch input:checked + .--slider .fa-check {
		display:block;
		}

		.--switch input:checked + .--slider .fa-times {
		display:none;
		}

		.--switch input:checked + .--slider {
		background-color: #52b69a;
		}

		.--switch input:focus + .--slider {
		box-shadow: 0 0 1px #52b69a;
		}

		.--switch input:checked + .--slider:before {

		-webkit-transform: translateX(65px);
		-ms-transform: translateX(65px);
		transform: translateX(65px);
		}
		.--switch input:checked + .--slider .fa-check {
				display: block;
			}
			.--switch input:checked + .--slider .fa-times {
				display: none;
			}

			.--switch input:not(checked) + .--slider .fa-check {
				display: none;
			}
			.--switch input:not(checked) + .--slider .fa-times {
				display: block;
			}

			.alert_msg{
			margin: 0 !important;
			}
			.alert_msg .alert {
				width: 97%;
				position: relative;
				top: 15px;
				left: 0;
				right: 0;
				margin: 0 auto 15px;
			}
	</style>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $game->stadium->name }} </div>

                    <div class="panel-body">
                        <div class="wrapper">
                            <div class="food">
                                <div class="cover" style="background-image: url({{asset('images/stadium/' .$game->name . "stadium.jpg") }})">

                                </div>
                                <div class="info recipe">
                                    <a href="#" class="recipe">
                                        <i>
                                            <svg x="0px" y="0px" width="26px" height="28px">
                                                <g>
                                                    <path d="M 8.5 20 L 8.5 21 L 17.5 21 L 17.5 20 L 8.5 20 ZM 8.5 16 L 8.5 17 L 17.5 17 L 17.5 16 L 8.5 16 ZM 8.5 12 L 8.5 13 L 17.5 13 L 17.5 12 L 8.5 12 ZM 20 0 C 19.4477 0 19 0.4477 19 1 L 19 6 C 19 6.5523 19.4477 7 20 7 C 20.5523 7 21 6.5523 21 6 L 21 1 C 21 0.4477 20.5523 0 20 0 ZM 13 0 C 12.4477 0 12 0.4477 12 1 L 12 6 C 12 6.5523 12.4477 7 13 7 C 13.5523 7 14 6.5523 14 6 L 14 1 C 14 0.4477 13.5523 0 13 0 ZM 6 0 C 5.4477 0 5 0.4477 5 1 L 5 6 C 5 6.5523 5.4477 7 6 7 C 6.5523 7 7 6.5523 7 6 L 7 1 C 7 0.4477 6.5523 0 6 0 ZM 15 4 L 18 4 L 18 3 L 15 3 L 15 4 ZM 8 4 L 11 4 L 11 3 L 8 3 L 8 4 ZM 3 4 L 4 4 L 4 3 L 3 3 C 1.3431 3 0 4.3431 0 6 L 0 25 C 0 26.6569 1.3431 28 3 28 L 23 28 C 24.6569 28 26 26.6569 26 25 L 26 6 C 26 4.3431 24.6569 3 23 3 L 22 3 L 22 4 L 23 4 C 24.1046 4 25 4.8954 25 6 L 25 25 C 25 26.1046 24.1046 27 23 27 L 3 27 C 1.8954 27 1 26.1046 1 25 L 1 6 C 1 4.8954 1.8954 4 3 4 Z" fill="#ffffff"/>
                                                </g>
                                            </svg>
                                        </i>
                                      
                                    </a>
                                    <div class="content">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <br>
                                            
                                            <tr>
                                            <th>Match  {{$game->mdate}}</th>
                                        
                                            <th>{{ $team->find($game->team1_id)->name }}</th>
                                            <th><img width="20px" height="20px" src="{{ asset('images/flags/' . $team->find($game->team1_id)->flag) }}">
                                            </th>
                                            <th>VS</th>
                                            <th><img width="20px" height="20px" src="{{ asset('images/flags/' . $team->find($game->team2_id)->flag) }}"></th>
                                            <th>{{ $team->find($game->team2_id)->name }}</th>

                                        </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
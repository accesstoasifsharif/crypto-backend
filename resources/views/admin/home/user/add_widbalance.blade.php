@extends('layouts.appdashboard')

@section('content')

<style>
	input.btn.btn-success {
	    margin-left: 80%;
	}
	.table-responsive {
	    width: 100%;
	}
	.onoffswitch {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
	    display: none;
	}
	.onoffswitch-label {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner:before, .onoffswitch-inner:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner:before {
	    content: "Unfreeze";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner:after {
	    content: "Freeze";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	    margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
	    right: 0px; 
	}


	.onoffswitch1 {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox1 {
	    display: none;
	}
	.onoffswitch-label1 {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner1 {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner1:before, .onoffswitch-inner1:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner1:before {
	    content: "Downgrade";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner1:after {
	    content: "Upgrade";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch1 {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
	    margin-left: 0;
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
	    right: 0px; 
	}

	.col-md-6.search-form {
	    width: 100%;
	}
	/* 9th april 2022 */
	section.content-header {
	    display: flex;
	    justify-content: space-between;
	    align-items: center;
	}
	.set_def {
    display: none;
}
 .input_two {
    margin-bottom: 10px;
} 
.spc_d_input {
	display:none;
    /* display: flex; */
    gap: 2rem;
    width: 71%;
    padding: 15px 0px;
    margin: 0px 50px 0px 0;
}
.set_dollar {
    display: flex;
}
.spc_row_set {
    display: flex;
    gap: 2rem;
    border: 1px solid #ccc;
    padding: 18px 0px;
    margin: 5px 0px 5px 0;
    width: 40%;
    justify-content: center;
}
.set_simple_input:focus-visible{
	border:none;
	outline:none;
}
.set_simple_input {
    border: none;
    background-color: transparent;
    width: 79px;
    display: block;
}
	/* //9th april 2022 */
</style>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>
  

    <!-- Content Header (Page header) -->
    <section class="content-header">
	
      <h1>
        Withdraw Balance
       <!-- <small>it all starts here</small> -->
      </h1>
	 
      <a href="{{url('admin/users/userwithdrawlbalance')}}/{{$id}}">
		<button type="button" class="btn btn-success"> Back</button>
	  </a>
	  </section>

	  <section class="content">
		<div class="box box-primary">
			<div class="box-header with-border table-responsive">
				<table class="table table-bordered">
					<form action="{{url('/admin/users/withdrawlbalance/store_addbalance')}}" method="post"  enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="user_id" value="@if(!empty($id)){{$id}}@endif">
						
                        


                        <div class="form-group">
							<label for="amount" class="control-label mb-1">Amount</label>
							<input id="amount"  name="amount" type="number" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>

                        
								
						<div>
						<button id="payment-button" type="submit" class="btn btn-lg btn-info">
							Submit
						</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
						</form>
					</div>
					</div>
			 <!-- END DATA TABLE-->
        </div>
       </table>
    </div>
    
                        
@endsection
@extends('layouts.appdashboard')

@section('content')

<style>
	.live-bets-icn {
    height: 135px;
}
	.bg-betting {
    background-color: #168118;
}
.bg-transaction {
    background-color: #b22d74;
}
.bg-profile {
    background-color: #212121;
}
.small-box.bg-profile{
	overflow:hidden;
	
}
.small-box.bg-profile img.live-bets-icn {
    min-width: 240px;
    object-fit: cover;
    margin: 0;
    padding: 0;
}
.live-bets-icn {
    height: 147px;
}
</style>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			User All Details
		</h1>
		</section>

    	<!-- Main content -->
	
		<section class="content">

		  	<div class="row">
				<a href="/admin/users/usersprofile/{{$id}}">
					<div class="col-lg-3 col-xs-6">
					<!-- small box -->
						<div class="small-box bg-profile">
							<div class="inner">
								<h3></h3>
								<p>Profile</p>
							</div>
							<div class="icon">
							<img src="{{asset('storage/Users_profile_photo/'.$response1->image) }}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<a href="/admin/users/usersbetting/{{$id}}">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-betting">
							<div class="inner">
								<h3></h3>
								<p>Beting Details</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/bet.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>			
					</div>
				</a>
				<!-- ./col -->
				<!-- <a href="">
					<div class="col-lg-3 col-xs-6">
						
						<div class="small-box bg-yellow">
							<div class="inner">
								<h3></h3>
								<p>Games Details</p>
							</div>
							<div class="icon">
								<i class="fa fa-user-plus"></i>
							</div>
						</div>
					</div>
				</a> -->
				<!-- ./col -->
				<!-- <a href="/admin/users/userstransactions/{{$id}}">
					<div class="col-lg-3 col-xs-6">
						<div class="small-box bg-transaction">
							<div class="inner">
								<h3></h3>
								<p>Transaction</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/transaction.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a> -->
				<!-- ./col -->
				
				

			</div>	
		</section>
		
    	<!-- /.content -->
  	</div>
  	<!-- /.content-wrapper -->
  	<style>
  		a {
    		color: #f9fafc;
		}
  	</style>
  
@endsection
 
 
 

 
 
 
 

 
 
 
@extends('layouts.appdashboard')

@section('content')
<link type="text/css" rel="stylesheet" href="{{ asset('backend/jquery_color_picker/wheelcolorpicker.css') }}" />
<script src="{{ asset('backend/jquery_color_picker/jquery.wheelcolorpicker.js') }}"></script>
<style>
	input.btn.btn-success {
	    margin-left: 80%;
	}
	.table-responsive {
	    width: 100%;
	}
	.onoffswitch {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
	    display: none;
	}
	.onoffswitch-label {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner:before, .onoffswitch-inner:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner:before {
	    content: "Unfreeze";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner:after {
	    content: "Freeze";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	    margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
	    right: 0px; 
	}


	.onoffswitch1 {
	    position: relative; width: 90px;
	    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox1 {
	    display: none;
	}
	.onoffswitch-label1 {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #ecf0f5; border-radius: 20px;
	}
	.onoffswitch-inner1 {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner1:before, .onoffswitch-inner1:after {
	    display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
	    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner1:before {
	    content: "Downgrade";
	    padding-left: 10px;
	    background-color: #00a65a; color: #FFFFFF;
	}
	.onoffswitch-inner1:after {
	    content: "Upgrade";
	    padding-right: 10px;
	   background-color: #dd4b39;
	    color: #ffffff;
	    text-align: right;
	}
	.onoffswitch-switch1 {
	    display: block; width: 20px; margin: 6px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 56px;
	    border: 2px solid #ffffff; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
	    margin-left: 0;
	}
	.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
	    right: 0px; 
	}

	.col-md-6.search-form {
	    width: 100%;
	}
	/* 9th april 2022 */
	section.content-header {
	    display: flex;
	    justify-content: space-between;
	    align-items: center;
	}
	/* //9th april 2022 */
</style>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  

    <!-- Content Header (Page header) -->
    <section class="content-header">
		@if(empty($id))
      	<h1>
        	Add Game Type
       		
      	</h1>
		@else
		<h1>
        	Edit Game Type	
      	</h1>
		  @endif
      <a href="{{url('admin/gamestype')}}">
		<button type="button" class="btn btn-success"> Back</button>
	  </a>
	  </section>

	  <section class="content">
		<div class="box box-primary">
			<div class="box-header with-border table-responsive">
				<table class="table table-bordered">
					<form action="{{url('admin/gamestype/store')}}" method="post"  enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" value="@if(!empty($id)){{$id}}@endif">
						

                        <div class="form-group">
							<label for="game_type" class="control-label mb-1">Game Type Name</label>
							<input id="game_type"  name="game_type" type="text" class="form-control" aria-required="true" aria-invalid="false" value="@if($response !='') {{$response->game_type}}@endif" required>	 
						</div>
						<div class="form-group">
							<label for="img" class="control-label mb-1">Game Type Image </label>
							<input id="img"  name="img" type="file" class="form-control" value="@if($response !=''){{$response->img}}@endif" aria-required="true" aria-invalid="false" value="">
							
						</div>

						<div class="form-group">
						<label for="img" class="control-label mb-1">Game Type Color </label>
							<div class="input-group">
								<input class="form-control input-sm" type="text" id="color" name="color" value="@if($response !='') {{$response->color}}@endif" style="background:@if($response !='') {{$response->color}}@endif;" required><span class="input-group-addon"><i class="fa fa-tint"></i></span>
							</div>
						</div>
							
						<div>
							<button id="payment-button" type="submit" class="btn btn-lg btn-info">
								Submit
							</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
						</form>
					</div>
					</div>
			 <!-- END DATA TABLE-->
        </div>
       </table>
    </div>
	<script>
		$(document).ready(function () {
			$('#color').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });
		});
	</script>
                        
@endsection
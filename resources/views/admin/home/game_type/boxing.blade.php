@extends('layouts.appdashboard')

@section('content')

<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		

		/* 9th april 2022 */ 
		section.content-header{
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		/* //9th april 2022 */
		.--switch {
		position: relative;
		display: inline-block;
		width: 87px;
		height: 21px;
		}

		.--switch input {
		display:none;
		}

		.--slider .fa-check {
			color: #FFFFFF;
			position: absolute;
			left: 0px;
			font-size: 12px;
			top: 1px;
			width:100%;
			margin-bottom: 0;
			padding: 0px 15px;
		}
		.--slider .fa-times {
			color: #FFFFFF;
			position:absolute;
			font-size:12px;
			right:0px; 
			width:100%;
			text-align: center;
			top:1px;
			margin-bottom: 0;
		}
		.fa-check:before {
			content: "\f00c";
			display:none;
		}

		.--slider {
		position: absolute;
		cursor: pointer;
		display:flex;
		align-items:center;
		top:0;
		left:0;
		right:0;
		bottom:0;
		background-color: #db3a34;
		-webkit-transition: .4s;
		transition: .4s;
		border-radius:50px;
		}

		.--slider:before {
			position: absolute;
			content: "";
			height: 19px;
			width: 19px;
			left: 1px;
			bottom: 0px;
			border-radius: 50%;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
			top: 1px;
		}

		.fa-remove:before, .fa-close:before, .fa-times:before {
			content: "\f00d";
			display:none;
		}

		.--switch input:checked + .--slider .fa-check {
		display:block;
		}

		.--switch input:checked + .--slider .fa-times {
		display:none;
		}

		.--switch input:checked + .--slider {
		background-color: #52b69a;
		}

		.--switch input:focus + .--slider {
		box-shadow: 0 0 1px #52b69a;
		}

		.--switch input:checked + .--slider:before {

		-webkit-transform: translateX(65px);
		-ms-transform: translateX(65px);
		transform: translateX(65px);
		}
		.--switch input:checked + .--slider .fa-check {
				display: block;
			}
			.--switch input:checked + .--slider .fa-times {
				display: none;
			}

			.--switch input:not(checked) + .--slider .fa-check {
				display: none;
			}
			.--switch input:not(checked) + .--slider .fa-times {
				display: block;
			}

			.alert_msg{
			margin: 0 !important;
			}
			.alert_msg .alert {
				width: 97%;
				position: relative;
				top: 15px;
				left: 0;
				right: 0;
				margin: 0 auto 15px;
			}
			td.set_spc_td {
				display: flex;
				gap: 1em;
			}
	</style>
	<!-- =============================================== -->

	<!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header">
     		<h1>
       			Boxing MMA
       			<!-- <small>it all starts here</small> -->
      		</h1>
		  <!-- 	<a href="{{url('/admin/games/add')}}">
				<button type="button" class="btn btn-success"> Add Games</button>
	  		</a> -->
			<a href="{{url('admin/livebet')}}">
				<button type="button" class="btn btn-success"> Back</button>
	  		</a> 
	
    	</section>
	
    	<!-- Main content -->
    	<section class="content">
	
      		<!-- Default box -->
      		<div class="box box-primary">
	 			<br>
				<div class="row">	
					<div class="col-md-7"></div>
					<div class="col-md-5 search-form">
						<form class="form-inline"  method="get" >
							<div class="form-group">
							<input type="text" class="form-control" name="search" placeholder="Search" value="">
							</div>
				
							<button type="submit" class="btn  btn-info search-icn"><i class="fa fa-search"></i></button>
				
								<a href="/admin/games" class="btn  btn-info" >Reset</a> 
				
						</form>	
					</div>
				</div>
        		<div class="box-header with-border table-responsive">
         
					<table class="table table-bordered">
						<thead>
							<tr>
								
							
								<th>ID</th>
								<th>Team 1</th>
								<th>Team 2</th>
								<th>Game Type</th>
								<th>Win % Team 1</th>
								<th>Win % Team 2</th>
								<!-- <th>Bet Amount</th> -->
								<th>Venu</th>
								<th>Status</th>
								<!-- <th>Action</th> -->
								
							</tr>
						</thead>
						<tbody>
						@if(!$boxing->isEmpty()) 
                        @foreach($boxing as $res)
								<tr>
									<td>{{$res->id }} </td>
									<td>{{$res->team1 }} </td>
									<td>{{$res->team2}} </td>
									<td>{{$res->game_type_name}} </td>
									<td><div id="myProgress" style=" width: 100%; background-color: grey;">
											<div id="myBar1" style="height: 30px; background-color: green; width: {{ $res->winning_percentage_team1 }}%;"></div>
										</div>
										<input type="number" id="winning_perentage_team1" onchange="$('#winning_perentage_team2').val(100 - parseFloat($('#winning_perentage_team1').val())); $('#myBar1').css('width',parseFloat($('#winning_perentage_team1').val())+'%'); $('#myBar2').css('width',100 - parseFloat($('#winning_perentage_team1').val())+'%');" value={{ $res->winning_percentage_team1}}> </td>
									<td><div id="myProgress" style=" width: 100%; background-color: grey;">
											<div id="myBar2" style="height: 30px; background-color: green; width: {{ $res->winning_percentage_team2 }}%;"></div>
										</div><input type="number" id="winning_perentage_team2"  onchange="$('#winning_perentage_team1').val(100 - parseFloat($('#winning_perentage_team2').val())); $('#myBar2').css('width',parseFloat($('#winning_perentage_team2').val())+'%');  $('#myBar1').css('width',100 - parseFloat($('#winning_perentage_team2').val())+'%');" value={{ $res->winning_percentage_team2}}></td>
									<!-- <td><input type="range" class="team_points" min="0" max="100" step="1" value={{ ! $res->winning_percentage_team1}}></td>
									<td><input type="range" class="team_points" min="0" max="100" step="1" value={{ ! $res->winning_percentage_team2}}></td> -->
									
									<td>{{$res->venu}}</td>
									<td>
									<label class="--switch">
										<input type="checkbox" class="change-status" data-id="{{ $res->id }}" data-status="@if($res->status == 1){{'0'}}@else {{'1'}} @endif" data-val='@if(!empty($rid)){{$rid}}@endif'  @if($res->status == 1){{ 'D' }}@else {{ 'checked' }} @endif >
										<span class="--slider"><p class="fas fa-check">Inactive</p><p class="fas fa-times">Active</p></span>
									</td>
								
									<!-- <td class="set_spc_td">									
										<a href="{{url('admin/games/edit')}}/{{ $res->id }}" class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></a>									
										<a href="{{url('admin/games/delete')}}/{{ $res->id }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want remove this Game?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
										
									</td>	 -->
					
								</tr>
							@endforeach  
							@else
								<tr><td colspan="6"><b> No Record found</b></td></tr>
							@endif
						</tbody>
		  			</table>
					  {{ $boxing->links('vendor.pagination.custom2') }}
        		</div>
      
       
        		<!-- /.box-footer-->
      		</div>
      		<!-- /.box -->

    	</section>
    	<!-- /.content -->
  	</div>
	  <!-- <script>
		var elements = document.querySelectorAll('.team_points');
		
		
		var length = elements.length;
		var sliders = Array.prototype.slice.call(elements); // Copy of `elements` but as a real array
		var max = 100;

		function change(current) {
			"use strict";
			
			set(current);

			var input = +current.value;
			var delta = max - input;
			var sum = 0;
			var siblings = [];

			// Sum of all siblings
			sliders.forEach(function (slider) {
				if (current != slider) {
					siblings.push(slider); // Register as sibling
					sum += +slider.value;
				}
			});

			// Update all the siblings
			var partial = 0;
			siblings.forEach(function (slider, i) {
				var val = +slider.value;
				var fraction = 0;

				// Calculate fraction
				if (sum <= 0) {
					fraction = 1 / (length - 1)
				} else {
					fraction = val / sum;
				}

				// The last element will correct rounding errors
				if (i >= length - 1) {
					val = max - partial;
				} else {
					val = Math.round(delta * fraction);
					partial += val;
				}

				set(slider, val);
			});
		}

		// Set value on a slider
		function set(elm, val) {
			if (val) {
				elm.value = val;
			}
			// Hack to trigger CSS ::after content to be updated
			elm.setAttribute('value', elm.value);
		}

		// Add event listeners to the DOM elements
		for (var i = 0, l = elements.length; i < l; i++)  {
			
			elements[i].addEventListener('change', function (e) {
				change(this);
			}, false);
		}

</script> -->




  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
	</div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script>
		$(document).ready(function(){
        $('.change-status').click(function(){			
			if (confirm('Are you sure you want to change the status?')) {
				var id = $(this).attr("data-id");	
				var status = $(this).attr("data-status");
				var token=$('meta[name="csrf_token"]').attr('content');
				if($(this).attr("data-val")){
					var rid=$(this).attr("data-val");
				}else{
					var rid='';
				}
				$.ajax({     
					url: '/admin/livebet/boxing_mma/status',
					type:"POST",  
					headers: {
					'X-CSRF-TOKEN':token
					},        
					data:'id='+id+'&status='+status+'&_token='+token,
					success:function(data){ 
					if(rid==''){
						window.location = "/admin/livebet/boxing_mma";
					}
				   
					}
				});		
				
			
			}else{
				return false;
			}
						
		});
	});
	</script>
  
@endsection
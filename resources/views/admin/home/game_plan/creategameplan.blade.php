@extends('layouts.appdashboard')

@section('content')

<link rel="stylesheet" href="backend/style.css" />

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  

    <!-- Content Header (Page header) -->
    <section class="content-header">
    @if(empty($id))
      <h1>
        Add Game Plan
       <!-- <small>it all starts here</small> -->
      </h1>
      @else

      <h1>
        Edit Game Plan
       <!-- <small>it all starts here</small> -->
      </h1>
      @endif

      <a href="{{url('admin/gameplan')}}">
		<button type="button" class="btn btn-success"> Back</button>
	  </a>
	  </section>

	  <section class="content">
		<div class="box box-primary">
			<div class="box-header with-border table-responsive">
				<table class="table table-bordered">
					<form action="{{url('admin/gameplan/store')}}" method="post"  enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" value="@if(!empty($id)){{$id}}@endif">
						

                        <div class="form-group">
							<label for="max_plan" class="control-label mb-1">American Ods</label>
							<input id="max_plan"  name="max_plan" type="text" value="@if($response !='') {{$response->max_plan}}@endif" class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div>

                        <div class="form-group">
							<label for="X" class="control-label mb-1">Bet amount x Bet plan</label>
							<input id="X"  name="X" type="text" class="form-control" value="@if($response !='') {{$response->X}}@endif" aria-required="true" aria-invalid="false" required>	 
						</div>

                        <!-- <div class="form-group">
							<label for="amount" class="control-label mb-1">Bet amount </label>
							<input id="amount"  name="amount" type="text"  class="form-control" aria-required="true" aria-invalid="false" required>	 
						</div> -->
							
						<div>
						<button id="payment-button" type="submit" class="btn btn-lg btn-info">
							Submit
						</button>
						</div>
						<!--<input type="hidden" name="id" value="{{ $id ?? ''}}"/>-->
						</form>
					</div>
					</div>
			 <!-- END DATA TABLE-->
        </div>
       </table>
    </div>
                        
@endsection
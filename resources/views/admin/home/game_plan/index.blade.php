@extends('layouts.appdashboard')

@section('content')

<link rel="stylesheet" href="backend/style.css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header add-more-games">
			<h1>
			Manage Games Plan
			<!-- <small>it all starts here</small> -->
			</h1>
			@if(empty($rid))
			<a href="{{url('/admin/gameplan/add')}}">
				<button type="button" class="btn btn-success"> Add</button>
			</a>
			@else
				<a href="{{url('/admin/gameplan/add')}}/{{$id}}">
				<button type="button" class="btn btn-success"> Add</button>
			</a> 
			@endif
    	</section>
	
    	<!-- Main content -->
    	<section class="content">
	
      		<!-- Default box -->
      		<div class="box box-primary">
	 			<br>
				<div class="row">	
					<div class="col-md-7"></div>
					<div class="col-md-5 search-form">
						<form class="form-inline"  method="get" >
							<div class="form-group">
							<input type="text" class="form-control" name="search" placeholder="Search" value="">
							</div>
				
							<button type="submit" class="btn  btn-info search-icn"><i class="fa fa-search"></i></button>
				
								<a href="/admin/gameplan" class="btn  btn-info" >Reset</a> 
				
						</form>	
					</div>
				</div>
        		<div class="box-header with-border table-responsive">
         
					<table class="table table-bordered">
						<thead>
							<tr><th>#</th>
								<th>Maximum Plan</th>
                                
								<th>Beting Plan</th>
                                <th>Action</th>
								<!--<th>Created Date</th>-->
							</tr>
						</thead>
						<tbody>
						@if(!$response->isEmpty()) 
                        @foreach($response as $res)
								<tr>
									<td>{{$res->id }} </td>
									<td>{{$res->max_plan }} </td>
									
                                    <td>{{$res->X}} %</td>
									
									<td>									
										<a href="{{url('admin/gameplan/edit')}}/{{ $res->id }}" class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></a>									
										<a href="/admin/gameplan/delete/{{$res->id}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want remove this Admin?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
										
									</td>	
					
								</tr>
							@endforeach   
							@else
								<tr><td colspan="6"><b> No Record found</b></td></tr>
							@endif
						</tbody>
		  			</table>
					  {{ $response->links('vendor.pagination.custom2') }}
        		</div>
      
       
        		<!-- /.box-footer-->
      		</div>
      		<!-- /.box -->

    	</section>
    	<!-- /.content -->
  	</div>



  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
	</div>
<!-- 	<script>
		function addCredit(userid){
			$("#userid").val(userid);
		}
		function ReduceCredit(userid){
			$("#userid1").val(userid);
		}

		$(".btn-addevent").click(function(){
			$(".add-header-section").toggle("slow");
		});
		function myNewFunction(sel) {
		$("#eventclassification").val(sel.options[sel.selectedIndex].text);
		}
	</script>

	<script>
		$(document).ready(function(){
			$('.change-status').click(function(){			
				if (confirm('Are you sure you want to change the status?')) {
					var id = $(this).attr("data-id");	
					var status = $(this).attr("data-status");
					var token=$('meta[name="csrf_token"]').attr('content');
					if($(this).attr("data-val")){
						var rid=$(this).attr("data-val");
					}else{
						var rid='';
					}
					$.ajax({     
						url: '/admin_status',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&status='+status+'&_token='+token,
						success:function(data){ 
						if(rid==''){
							window.location = "/admin/manage";
						}else{
							window.location = "/admin/manage/"+rid;
						}
					
						}
					});		
				}else{
					return false;
				}
							
			});
			
			
			$('.test-status').click(function(){			
			
				if (confirm('Are you sure you want to change assign status?')) {
					var id = $(this).attr("data-id");	
					var status = $(this).attr("data-status");
				
					var token=$('meta[name="csrf_token"]').attr('content');
					$.ajax({     
						url: '/student/teststatus',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&status='+status+'&_token='+token,
						success:function(data){       
							window.location = "/admin/student";
					
						}
					});		
					
				
				}else{
					return false;
				}
							
			});
		});
	</script> -->

		
		
	</script>
@endsection

 
@extends('layouts.appdashboard')

@section('content')


	<style>
		input.btn.btn-success {
			margin-left: 80%;
		}
		.table-responsive {
			width: 100%;
		}
		.onoffswitch {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
			display: none;
		}
		.onoffswitch-label {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner:before {
			content: "Unfreeze";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
			content: "Freeze";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px; 
		}


		.onoffswitch1 {
			position: relative; width: 90px;
			-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox1 {
			display: none;
		}
		.onoffswitch-label1 {
			display: block; overflow: hidden; cursor: pointer;
			border: 2px solid #ecf0f5; border-radius: 20px;
		}
		.onoffswitch-inner1 {
			display: block; width: 200%; margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner1:before, .onoffswitch-inner1:after {
			display: block; float: left; width: 50%; height: 25px; padding: 0; line-height: 26px;
			font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
			box-sizing: border-box;
		}
		.onoffswitch-inner1:before {
			content: "Downgrade";
			padding-left: 10px;
			background-color: #00a65a; color: #FFFFFF;
		}
		.onoffswitch-inner1:after {
			content: "Upgrade";
			padding-right: 10px;
		background-color: #dd4b39;
			color: #ffffff;
			text-align: right;
		}
		.onoffswitch-switch1 {
			display: block; width: 20px; margin: 6px;
			background: #FFFFFF;
			position: absolute; top: 0; bottom: 0;
			right: 56px;
			border: 2px solid #ffffff; border-radius: 20px;
			transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-inner1 {
			margin-left: 0;
		}
		.onoffswitch-checkbox1:checked + .onoffswitch-label1 .onoffswitch-switch1 {
			right: 0px; 
		}

		

		/* 9th april 2022 */ 
		section.content-header{
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		/* //9th april 2022 */
		.--switch {
		position: relative;
		display: inline-block;
		width: 87px;
		height: 21px;
		}

		.--switch input {
		display:none;
		}

		.--slider .fa-check {
			color: #FFFFFF;
			position: absolute;
			left: 15px;
			font-size: 12px;
			top: 1px;
			width:100%;
			margin-bottom: 0;
		}
		.--slider .fa-times {
			color: #FFFFFF;
			position:absolute;
			font-size:12px;
			right:0px; 
			width:100%;
			text-align: center;
			top:1px;
			margin-bottom: 0;
		}
		.fa-check:before {
			content: "\f00c";
			display:none;
		}

		.--slider {
		position: absolute;
		cursor: pointer;
		display:flex;
		align-items:center;
		top:0;
		left:0;
		right:0;
		bottom:0;
		background-color: #db3a34;
		-webkit-transition: .4s;
		transition: .4s;
		border-radius:50px;
		}

		.--slider:before {
			position: absolute;
			content: "";
			height: 19px;
			width: 19px;
			left: 1px;
			bottom: 0px;
			border-radius: 50%;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
			top: 1px;
		}

		.fa-remove:before, .fa-close:before, .fa-times:before {
			content: "\f00d";
			display:none;
		}

		.--switch input:checked + .--slider .fa-check {
		display:block;
		}

		.--switch input:checked + .--slider .fa-times {
		display:none;
		}

		.--switch input:checked + .--slider {
		background-color: #52b69a;
		}

		.--switch input:focus + .--slider {
		box-shadow: 0 0 1px #52b69a;
		}

		.--switch input:checked + .--slider:before {

		-webkit-transform: translateX(65px);
		-ms-transform: translateX(65px);
		transform: translateX(65px);
		}
		.--switch input:checked + .--slider .fa-check {
				display: block;
			}
			.--switch input:checked + .--slider .fa-times {
				display: none;
			}

			.--switch input:not(checked) + .--slider .fa-check {
				display: none;
			}
			.--switch input:not(checked) + .--slider .fa-times {
				display: block;
			}

			.alert_msg{
			margin: 0 !important;
			}
			.alert_msg .alert {
				width: 97%;
				position: relative;
				top: 15px;
				left: 0;
				right: 0;
				margin: 0 auto 15px;
			}
			.reaction-btn {
				padding: 0px 5px;
			}
			.reaction-btn:hover {
				padding: 0px 5px!important;
				cursor: inherit;
			}
			.cursor_enable{
				cursor: pointer !important;
				color: #fff;
				background-color: #ac2925;
				border-color: #761c19;
				padding: 4px 7px;
				border-radius: 50%;
			}
	</style>
	<!-- =============================================== -->

	<!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
  		<div class="top-alert alert_msg">
		  	@include('flash_message')
		</div>

    	<!-- Content Header (Page header) -->
    	<section class="content-header">
     		<h1>
       			Manage Bets
       			<!-- <small>it all starts here</small> -->
      		</h1>
		  	<!-- <a href="{{url('/admin/bet/add')}}">
				<button type="button" class="btn btn-success"> Add Bets</button>
	  		</a>  -->
	
    	</section>
	
    	<!-- Main content -->
    	<section class="content">
	
      		<!-- Default box -->
      		<div class="box box-primary">
	 			<br>
				<div class="row">	
					<div class="col-md-7"></div>
					<div class="col-md-5 search-form">
						<form class="form-inline"  method="get" >
							<div class="form-group">
							<input type="text" class="form-control" name="search" placeholder="Search" value="">

							</div>
				
							<button type="submit" class="btn  btn-info search-icn"><i class="fa fa-search"></i></button>
				
								<a href="/admin/bet" class="btn  btn-info" >Reset</a> 
				
						</form>	
					</div>
				</div>
        		<div class="box-header with-border table-responsive">
         
					<table class="table table-bordered">
						<thead>
							<tr>
                                <th >#</th>
                                <th >{{ __('User_id')}} </th>
                                <th >{{ __('GameType')}} </th>
                                <th >{{ __('Team 1')}} </th>
                                <th >{{ __('Team 2')}} </th>
                                <th >{{ __('Date')}} </th>
                                <th >{{ __('Bet on')}} </th>
                                <th >{{ __('Bet Amount')}} </th>
                                <th >{{ __('Won/Lose')}} </th>
                                <th >{{ __('Settlement')}} </th>
                                <!-- <th >{{ __('Status')}}</th> -->
                                <th >{{ __('Action')}}</th>
								<!--<th>Created Date</th>-->
							</tr>
						</thead>
						<tbody>
						@if(!$bets->isEmpty())
                        @foreach ($bets as $bet)

                            <tr>
                                <th >{{$bet->id}}</th>
                                <td>{{$bet->user_id}}</td>
                                <td>{{$bet->game_type_name}}</td>
                                <td>{{$bet->team1}}</td>
                                <td>{{$bet->team2}}</td>
                                <td>{{$bet->matchDate}}</td>
								<td>{{$bet->betteam}}</td>
								
                                <td>{{$bet->betAmount}}</td>
                                <!-- <td>
									@if($bet->game_result == '0')
									<span class="btn btn-primary btn-xs reaction-btn">No Result</span>
									@elseif($bet->game_result == '3')
									<span class="btn btn-warning btn-xs reaction-btn">Draw</span>
									@else
										@if($bet->game_result == $bet->bet)
										<span class="btn btn-success btn-xs reaction-btn"><i class="ion-checkmark" aria-hidden="true"></i></span>
										@else
										<span class="btn btn-danger btn-xs reaction-btn"><i class="ion-android-close" aria-hidden="true"></i></span>
										@endif
									@endif
									
								</td>
								<td>
									@if($bet->game_result == '0')
									@elseif($bet->game_result == '3')
									@else
										@if($bet->game_result == $bet->bet)
											@if($bet->settled == 1)
											<span class="btn btn-success btn-xs reaction-btn"><i class="ion-checkmark" aria-hidden="true"></i></span>
											@else
											<a href="{{url('admin/bet/settled_status')}}/{{ $bet->id }}" data-toggle="tooltip" title="Change to payment settled"   class="cursor_enable"><i class="ion-android-close" aria-hidden="true"></i></a>
											@endif
										@endif
									@endif
									
								</td> -->

								<td>
									@if($bet->win == 0)
									<span class="btn btn-primary btn-xs reaction-btn">No Result</span>
									@elseif($bet->win == 3)
									<span class="btn btn-warning btn-xs reaction-btn">Draw</span>
									@else
										@if($bet->win == 2)
										<span class="btn btn-success btn-xs reaction-btn"><i class="ion-checkmark" aria-hidden="true"></i></span>
										@else
										<span class="btn btn-danger btn-xs reaction-btn"><i class="ion-android-close" aria-hidden="true"></i></span>
										@endif
									@endif
									
								</td>
								<td>
									 @if($bet->win == '2')
										@if($bet->settled == 1)
                                            <span class="btn btn-success btn-xs reaction-btn"><i class="ion-checkmark" aria-hidden="true"></i></span>
                                        @else
                                             <button id="bet-reward-settlement-btn-<?php echo $bet->id; ?>"  class="btn btn-danger btn-xs reaction-btn" style="cursor:pointer;" onclick="settleBetReward({{$bet->id}})" data-toggle="tooltip"  title="Change to payment settled">Pending</button>
                                        @endif
									@endif
									
								</td>
                                
								
									<td>									
										<a href="{{url('admin/bet/view')}}/{{ $bet->id }}" class="btn btn-warning btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
										
									</td>	
					
								</tr>
							@endforeach   
							@else
								<tr><td colspan="6"><b> No Record found</b></td></tr>
							@endif
						</tbody>
		  			</table>
					  {{ $bets->links('vendor.pagination.custom2') }}
        		</div>
      
       
        		<!-- /.box-footer-->
      		</div>
      		<!-- /.box -->

    	</section>
    	<!-- /.content -->
  	</div>



  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
	</div>




    <script>
        var wallet_secret =  {!! file_get_contents(storage_path() . "/wallet/id.json") !!};
        const walletPrivateKey = new Uint8Array(wallet_secret);
        const opSportWallet = solanaWeb3.Keypair.fromSecretKey(walletPrivateKey);
        const network = "https://api.mainnet-beta.solana.com/";
        const connection = new solanaWeb3.Connection(network);
        var MINT_TOKEN_ADDRESS = `{{ config('opsports.MINT_TOKEN_ADDRESS') }}`;


        async function settleBetReward(bet_id){            
            if (confirm('Are you sure you want to settle this amount?')) {
                applyProcessing(bet_id);
                var token=$('meta[name="csrf_token"]').attr('content');
                $.ajax({     
                    url: '/admin/bet/get_bet_details_settlement/'+bet_id,
                    type:"GET",  
                    headers: {
                    'X-CSRF-TOKEN':token
                    },        
                    success:function(data){ 
                        if(!data.settled){
                            handleBetSettlement(data);
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        removeProcessing(bet_id);
                    }   
                });		
            }else{
                return false;
            }
        };

        function applyProcessing(bet_id){
            var element = $('#bet-reward-settlement-btn-'+bet_id);
            element.removeClass("btn-danger");    
            element.addClass("btn-default");
            element.html("Processing...");
            element.prop('disabled', true);

        }

        function removeProcessing(bet_id){
            var element = $('#bet-reward-settlement-btn-'+bet_id);
            element.addClass("btn-danger");    
            element.removeClass("btn-default");
            element.html("Pending");
            element.prop('disabled', false);
        }


        async function handleBetSettlement(data){
            const mint = new solanaWeb3.PublicKey(MINT_TOKEN_ADDRESS);
            const fromTokenAccount = await splToken.getOrCreateAssociatedTokenAccount(
                connection,
                opSportWallet,
                mint,
                opSportWallet.publicKey
            );
            const toTokenAccount = await splToken.getOrCreateAssociatedTokenAccount(
                connection,
                opSportWallet,
                mint,
                new solanaWeb3.PublicKey(data.wallet_address)
            );
            await splToken.transfer(
					connection,
					opSportWallet,
					fromTokenAccount.address,
					toTokenAccount.address,
					opSportWallet.publicKey,
					data.payout * solanaWeb3.LAMPORTS_PER_SOL,
					[]
					).then((response) => {
                       let tokenTrnxPayload = {
							transaction_id: response,
							transaction_type: 4,
							sending_wallet_address:
								opSportWallet.publicKey.toBase58(),
							receiving_wallet_address: data.wallet_address,
							amount: data.payout + "",
							currency: 2,
                            bet_id: data.bet_id,
						};
						saveCryptoTransactionLog(tokenTrnxPayload);
                       
                    })
					.catch((error) => {
                        removeProcessing();
						console.log("tokenTrnx => ", error);
                    });
				
        };
    
        function saveCryptoTransactionLog(payload){
            let data_set = 'transaction_id=' + payload.transaction_id +'&transaction_type=' + payload.transaction_type +
                    '&sending_wallet_address=' + payload.sending_wallet_address+ '&receiving_wallet_address=' + payload.receiving_wallet_address+ '&amount=' + payload.amount + '&currency=' + payload.currency +'&bet_id='+payload.bet_id; 
            var token=$('meta[name="csrf_token"]').attr('content');
            $.ajax({     
                url: '/admin/bet/settled_bet_save_crypto_transaction',
                type:"POST",  
                headers: {
                'X-CSRF-TOKEN':token
                },
                data:data_set,
                success:function(data){ 
                    location.reload();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                       location.reload();
                }  
            });	
        };


	</script>





	<script>
		function addCredit(userid){
			$("#userid").val(userid);
		}
		function ReduceCredit(userid){
			$("#userid1").val(userid);
		}

		$(".btn-addevent").click(function(){
			$(".add-header-section").toggle("slow");
		});
		function myNewFunction(sel) {
		$("#eventclassification").val(sel.options[sel.selectedIndex].text);
		}
	</script>

	<script>
		$(document).ready(function(){
			$('.change-status').click(function(){			
				if (confirm('Are you sure you want to change the status?')) {
					var id = $(this).attr("data-id");	
					var status = $(this).attr("data-status");
					var token=$('meta[name="csrf_token"]').attr('content');
					if($(this).attr("data-val")){
						var rid=$(this).attr("data-val");
					}else{
						var rid='';
					}
					$.ajax({     
						url: '/admin_status',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&status='+status+'&_token='+token,
						success:function(data){ 
						if(rid==''){
							window.location = "/admin/manage";
						}else{
							window.location = "/admin/manage/"+rid;
						}
					
						}
					});		
				}else{
					return false;
				}
							
			});
			
			
			$('.test-status').click(function(){			
			
				if (confirm('Are you sure you want to change assign status?')) {
					var id = $(this).attr("data-id");	
					var status = $(this).attr("data-status");
				
					var token=$('meta[name="csrf_token"]').attr('content');
					$.ajax({     
						url: '/student/teststatus',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&status='+status+'&_token='+token,
						success:function(data){       
							window.location = "/admin/student";
					
						}
					});		
					
				
				}else{
					return false;
				}
							
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.onoffswitch-checkbox').click(function(){			
			
					if($(this).prop("checked") == true){
						if (confirm('Are you sure you want to Unfreeze this user?')) {
						var status = 'A';
						}
						else{
							return false;
						}
					}
					else if($(this).prop("checked") == false){
						if (confirm('Are you sure you want to Freeze this user?')) {
					var status = 'D';
						}else{
							return false;
						}
					}
				var id = $(this).attr("data-id");	
				var token=$('meta[name="csrf_token"]').attr('content');
					$.ajax({     
						url: '/admin/status',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&status='+status+'&_token='+token,
						success:function(data){       
							
							//$("#cf-module").html(data);
					
						}
					});			
		});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.onoffswitch-checkbox1').click(function(){			
			
					if($(this).prop("checked") == true){
						if (confirm('Are you sure you want to Upgrade To Pro this user?')) {
						var user_type = '1';
						}
						else{
							return false;
						}
					}
					else if($(this).prop("checked") == false){
						if (confirm('Are you sure you want to Downgrade this user?')) {
					var user_type = '0';
						}else{
							return false;
						}
					}
				var id = $(this).attr("data-id");	
				var token=$('meta[name="csrf_token"]').attr('content');
					$.ajax({     
						url: '/admin/user_type',
						type:"POST",  
						headers: {
						'X-CSRF-TOKEN':token
						},        
						data:'id='+id+'&user_type='+user_type+'&_token='+token,
						success:function(data){       
							if(data=='Upgraded'){
							alert('Upgraded Successfully');
						}else if(data=='Downgraded'){
							alert('Downgraded Successfully');	
						}else{
							console.log(data);
						}
					
						}
					});			
		});
		
		// Reduce Credit
		$('.redeuceBtn').click(function(){			
			
				
							
			});
		
		});
		
		
	</script>
@endsection

 
 
 
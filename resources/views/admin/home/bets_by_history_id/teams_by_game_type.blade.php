@extends('layouts.appdashboard')

@section('content')
<style>
	.live-bets-icn {
    height: 135px;
}
.bg-mma {
    background-color: #cd3b36;
}
.bg-football {
    background-color: #0d3799;
}
.bg-american {
    background-color: #905a4e;
}
.bg-hockey {
    background-color: #065935;
}
</style>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			Teams By Game Type
		</h1>
		</section>

    	<!-- Main content -->
	
		<section class="content">

		  	<div class="row">
				@foreach($games_type as $games_by_type)
				<a href="/admin/teams_list/{{$games_by_type->id}}">
					<div class="col-lg-3 col-xs-6">
						<div class="small-box" style="background-color: {{$games_by_type->color}};">
							<div class="inner">
								<h3>{{$games_by_type->count_teams}}</h3>
								<p>{{$games_by_type->game_type}}</p>
							</div>
							<div class="icon">
								<img src="{{asset('storage/games_type/'.$games_by_type->img)}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				@endforeach
				
				<!-- <a href="/admin/teams">
					<div class="col-lg-3 col-xs-6">
						
						<div class="small-box bg-football">
							<div class="inner">
								<h3>{{-- $Soccer_League --}}</h3>
								<p>Soccer League</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/football.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>			
					</div>
				</a>
			
				<a href="/admin/teams">
					<div class="col-lg-3 col-xs-6">
						
						<div class="small-box bg-american">
							<div class="inner">
								<h3>{{-- $American_Football --}}</h3>
								<p>American Football</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/american.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				
				<a href="/admin/teams">
					<div class="col-lg-3 col-xs-6">
						
						<div class="small-box bg-hockey">
							<div class="inner">
								<h3>{{-- $Hockey --}}</h3>
								<p>Hockey</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/hockey.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a> -->
				
				
				

			</div>	
		</section>
		
    	<!-- /.content -->
  	</div>
  	<!-- /.content-wrapper -->
  	<style>
  		a {
    		color: #f9fafc;
		}
  	</style>
  
@endsection
 
 
 

 
 
 
 

 
 
 
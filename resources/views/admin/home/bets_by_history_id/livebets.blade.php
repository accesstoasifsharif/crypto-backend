@extends('layouts.appdashboard')

@section('content')
<style>
	.live-bets-icn {
    height: 135px;
}
.bg-mma {
    background-color: #cd3b36;
}
.bg-football {
    background-color: #0d3799;
}
.bg-american {
    background-color: #905a4e;
}
.bg-hockey {
    background-color: #065935;
}
</style>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			Live Bets
		</h1>
		</section>

    	<!-- Main content -->
	
		<section class="content">

		  	<div class="row">
				<a href="/admin/livebet/boxing_mma">
					<div class="col-lg-3 col-xs-6">
					<!-- small box -->
						<div class="small-box  bg-mma">
							<div class="inner">
								<h3>{{$Boxing}}</h3>
								<p>Boxing MMA</p>
							</div>
							<div class="icon">
								<img src="{{url('/backend/images/mma.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<a href="/admin/livebet/soccer_leauge">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-football">
							<div class="inner">
								<h3>{{$Soccer_League}}</h3>
								<p>Soccer League</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/football.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>			
					</div>
				</a>
				<!-- ./col -->
				<a href="/admin/livebet/american_football">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-american">
							<div class="inner">
								<h3>{{$American_Football}}</h3>
								<p>American Football</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/american.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<a href="/admin/livebet/hockey">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-hockey">
							<div class="inner">
								<h3>{{$Hockey}}</h3>
								<p>Hockey</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/hockey.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				
				

			</div>	
		</section>
		
    	<!-- /.content -->
  	</div>
  	<!-- /.content-wrapper -->
  	<style>
  		a {
    		color: #f9fafc;
		}
  	</style>
  
@endsection
 
 
 

 
 
 
 

 
 
 
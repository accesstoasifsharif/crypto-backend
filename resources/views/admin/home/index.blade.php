@extends('layouts.appdashboard')

@section('content')
<style>
	.bg-admin {
    background-color: #212121;
}
	.live-bets-icn {
    height: 85px;
}
.bg-users {
    background-color: #0ebdd5;
}
.bg-games {
    background-color: #0d3799;
}
.bg-live-bets {
    background-color: #db1fff;
}
.bg-betting {
    background-color: #168118;
}
.bg-matches {
    background-color: #cd3b36;
}
</style>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  	<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
			Dashboard
		</h1>
		</section>

    	<!-- Main content -->
	
		<section class="content">

		  	<div class="row">
			  @if(Session::get('ad_user_type') !=2)
				<a href="{{url('/admin/manage')}}">
					<div class="col-lg-3 col-xs-6">
					<!-- small box -->
						<div class="small-box bg-admin">
							<div class="inner">
								<h3>{{$admin}}</h3>
								<p>Admin</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/user.png')}}" alt="" class="live-bets-icn ">	
							</div>
						</div>
					</div>
				</a>
				@endif
				<!-- ./col -->
				<a href="{{url('admin/games')}}">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-games">
							<div class="inner">
								<h3>{{$games}}</h3>
								<p>Games</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/game.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>			
					</div>
				</a>
				<!-- ./col -->
				<a href="{{url('/admin/users')}}">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-users">
							<div class="inner">
								<h3>{{$users }}</h3>
								<p>Users</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/customer.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<a href="{{url('/admin/manage_game')}}">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-live-bets">
							<div class="inner">
								<h3></h3>
								<p>Live Bets</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/live.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				
				<!-- ./col -->
				<a href="/admin/bet">
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-betting">
							<div class="inner">
								<h3>{{$bet}}</h3>
								<p>Bets</p>
							</div>
							<div class="icon">
							<img src="{{url('/backend/images/bet.png')}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a>
				<!-- ./col -->
				<!-- <a href="/admin/matchs">
					<div class="col-lg-3 col-xs-6">
						
						<div class="small-box bg-matches">
							<div class="inner">
								<h3>{{-- $matchs --}}</h3>
								<p>Matches</p>
							</div>
							<div class="icon">
							<img src="{{-- url('/backend/images/matches.png') --}}" alt="" class="live-bets-icn ">
							</div>
						</div>
					</div>
				</a> -->
				<!-- ./col -->

			</div>	
		</section>
		
    	<!-- /.content -->
  	</div>
  	<!-- /.content-wrapper -->
  	<style>
  		a {
    		color: #f9fafc;
		}
  	</style>
  	<script>
 		$(document).ready(function(){
	 		$('.toast').toast('show');
 		})
 	</script>
@endsection
 
 
 

 
 
 
 

 
 
 
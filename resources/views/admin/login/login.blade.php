<!DOCTYPE html>
<html lang="en" >
	<head>
		<title>OPSportsBook - Login</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0">

		<!-- stylesheet -->
		<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/main_style.css')}}" media="all">
		<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/login_style.css')}}" media="all">
		<!-- //stylesheet -->
		
	   
		<meta data-react-helmet="true" name="docsearch:version" content="2.0">
		<meta data-react-helmet="true" name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover,user-scalable=0">
		
		<!-- Google fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
		<!-- //Google fonts -->
		
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<!-- //Font Awesome -->
		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<!-- //Bootstrap -->
	</head>
  <body>
	
	<div class="login_box_outer">
		
	
		<div class="login_box_inner">
			<center>
			
		  @include('flash_message')
			
			<h1>Admin Login</h1>
				<div class="login_header">
					<h1>OPSportsBook</h1>	
				</div>
				<div class="login_body">
					<form action="/admin" method="post">
						{{ csrf_field() }}
						<div class="form-group has-feedback">
							<input type="email" class="form-control" placeholder="Email" name="user_email">
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<input type="password" class="form-control" placeholder="Password" name="user_password">
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						</div>
						<div class="login_body">
							<!-- <div class="col-xs-12">
							<div class="checkbox icheck">
								<label>
								<input type="checkbox"> Remember Me
								</label>
							</div>
							</div> -->
							<!-- /.col -->
							<div class="col-xs-12 forget-cls mt-5">
								<!-- <a href="#" class="forget">Forget Password</a> -->
							</div>
						</div>
			
						<!-- /.col -->
						</div>
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary btn-block btn-flat sign">Log In</button>
						</div>
					</form>
				</div>
			</center>
		</div>
	</div>
  


  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){

	});

	$(".top-alert.alert_msg").on('click',function(){
		$(".top-alert.alert_msg").remove();
	});
</script>
  </body>
</html>
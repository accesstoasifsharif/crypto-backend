@include('admin/login/header')
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Event </b>Management</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Forgot Pasword</p>
	 @include('flash/flash-message')
    <form action="" method="post">
		 {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="user_email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="row">
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Forgot Password</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
	<a href="/"><i class="fa fa-fw fa-chevron-circle-left"></i> Back To Login</a><br>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@include('admin/login/footer')
@php
use App\Http\Controllers\AdminController;
$getinfo = AdminController::getuserdetials(Session::get('ad_user_id'));
@endphp  
  <link rel="shortcut icon" href="{ asset('backend/images/favicon.png') }}">
  <header class="main-header">
    <!-- Logo -->
    <a href="/home" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><img src="{{ asset('backend/images/logo.png') }}" class="top-class-sidebar" alt="Golden Crown"></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ asset('backend/images/logo.png') }}" alt="Golden Crown"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <img src="{{ asset('backend/images/togle_icon.png') }}" alt="Toogle">
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			@if(!empty($getinfo->user_image))
              <img src="/public/backend/profile/{{$getinfo->user_image}}" class="user-image" alt="User Image">
		  @else
			  <img src="{{ asset('/public/backend/profile/profile_1584621964.png') }}" class="user-image" alt="User Image">
			@endif
              <span><b></b></span><br><span class="hidden-xs">@if(!empty($getinfo)){{ $getinfo->user_firstname}} {{ $getinfo->user_lastname}}@endif</span>
            </a>
            <ul class="dropdown-menu">
            
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  
                </div>
                <div class="pull-right">
                  <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button --><!--
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>
 <!-- Left side column. contains the sidebar -->
 @if(empty($rid))
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="">
          <a href="/home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
		
		<li class="treeview admindrop">
          <a href="/admin/manage">
            <i class="fa fa-users"></i> <span> Manage Admin</span>
          </a>
           <ul class="treeview-menu">
             
            <li><a href="/admin/subadmin/manage">Manage Subadmin</a></li>
            <li><a href="/admin/waiter/manage">Manage Waiter</a></li>
            <li><a href="/admin/user">Manage Users</a></li>

          </ul>
          
        </li>
        
        <li class="treeview admindrop">
          <a href="/admin/restaurant">
            <i class="fa fa-users"></i> <span>Manage Restaurant</span>
          </a>
           <ul class="treeview-menu">
              <li><a href="/admin/table/manage">Manage Table</a></li>
            <li><a href="/admin/menucategory">Food Menu Category</a></li>
            <li><a href="/admin/foodmenu">Food Menu</a></li>

          </ul>
          
        </li>
        
          <li class="treeview admindrop"> 
          <a href="/admin/cms">
            <i class="fa fa-user"></i> 
            <span>Admin CMS </span>
            </a>
          <ul class="treeview-menu">
            <li><a href="/admin/subadmin/cms">Sub Admin CMS</a></li>
            <li><a href="/admin/customer/cms">Customer CMS</a></li>

          </ul>
        </li>
         
		<li class="">
          <a href="/logout">
            <i class="fa fa-sign-out"></i> <span>Sign out</span>
          </a>
        </li>
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
 
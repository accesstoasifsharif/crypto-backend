<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class SupAdminmasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sup_adminmaster')->insert([
            'user_firstname' => 'Super Admin',
            'user_email' => 'admin@gmail.com',
            'user_password' => md5('password')
        ]);
    }
}

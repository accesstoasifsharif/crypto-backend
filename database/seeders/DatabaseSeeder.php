<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            BetsSeeder::class,
            GamesSeeder::class,
            GamesTypeSeeder::class,
            SupAdminCmsSeeder::class,
            SupAdminmasterSeeder::class,
            SupAdminmasterUserTypeSeeder::class,
            SupUserSeeder::class,
            SupUsersSeeder::class,
            SupUserTypeSeeder::class,
            TeamsSeeder::class,
            EmailtemplateSeeder::class
        ]);

    }
}

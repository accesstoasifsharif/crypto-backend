<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class SupUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /* for ($i=0; $i < 20; $i++) {
            DB::table('sup_users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('password')
            ]);
        } */
        DB::table('sup_users')->insert([
            'name' => 'Alex',
            'email' => 'alextest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Robin',
            'email' => 'robintest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Wilfred',
            'email' => 'wilfred@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Joy',
            'email' => 'joy@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Peter',
            'email' => 'petertest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Richard Roy',
            'email' => 'richardroy@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Abhinav Jain',
            'email' => 'abhinavtest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Christopher',
            'email' => 'christophertest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Samuel Samson',
            'email' => 'samuelsamsontest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_users')->insert([
            'name' => 'Reina',
            'email' => 'reinatest@gmail.com',
            'password' => Hash::make('password')
        ]);
    }
}

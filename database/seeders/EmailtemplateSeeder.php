<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmailtemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('emailtemplate')->insert([
            'name' => 'admin_create_by_superadmin_adminnotification',
            'title' => 'OpSport Admin has been created successfully',
            'body' => '<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f0f0f0">
            <tbody>
               <tr>
                  <td style="padding: 30px 30px 20px 30px;">
                     <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff" style="max-width: 650px;background-image: url(https://mybodyfatscan.com/public/frontend/app-assets/images/backgrounds/MOUNTAN.png) !important;  margin: auto;">
                        <tbody>
                           <tr>
                              <td colspan="2" align="center" style=" padding: 40px 40px 60px;background: #000;">
                                <h1 class="logo_text" style="font-size: 31px;font-family: arial;color: #fff;
                                margin: 0;">OP <span style="color: #00ffa2;font-family: arial;padding: 8px 0 0 0;display: inline-block;">Sportsbook</span></h1>
                                <span class="text-uppercase font-weight-bold short_title" style="font-size: 15px;font-family: arial;color: #fff;
                                margin: 10px 0 0 0;text-transform: uppercase;display: inline-block;">Decentralized Anonymous Global</span>
                              </td>
                           </tr>
                           <tr style="box-shadow: inset 0px 3px 5px -2px rgb(0 0 0 / 10%); background-color: #fff;">
                              <td colspan="2" align="center" style="padding: 40px 50px 0px 50px;">
                                 <h1 style="padding-right: 0em; margin: 0; line-height: 40px; font-weight:600;  color: #666; text-align: left; font-family: sans-serif; padding-bottom: 1em;">Welcome to the OP Sports</h1>
                              </td>
                           </tr>
                           <tr style=" background-color: #fff; box-shadow: inset 0px 0px 0px 1px rgb(0 0 0 / 0%);">
                              <td style="text-align: left; padding: 0px 50px;" valign="top">
                                 <p style=" font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #666; text-align: left; padding-bottom: 3%;">
                                    Hi {name}
                                 </p>
                                 <b>Below is the details of admin login</b>
                                 <p> Username : {email}</p>
                                 <p> Password : {password}</p>
                              </td>
                           </tr>
                           <tr style="background-color: #fff; box-shadow: inset 0px -3px 5px -1px rgb(0 0 0 / 10%);">
                              <td style="text-align: left; padding: 30px 50px 50px 50px" valign="top">
                                 <p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;">
                                    Thanks,<br><span style="color:#00ffa2;"><br></span></p><p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;"><span style="color:#00ffa2;">Manager</span></p><p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;"><span style="color:#00ffa2;">Opsports Book</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td colspan="2" align="center" style="padding: 20px 40px 40px 40px; ">
                                 <p style="font-family: sans-serif; font-size: 12px; margin: 0; line-height: 24px;  color: #777;">
                                    ï¿½ 2022
                                    <a href="#" target="_blank" style="color: #777; text-decoration: none">Opsports Book</a>
                                    <br>
                                 </p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>'
        ]);


        DB::table('emailtemplate')->insert([
            'name' => 'admin_create_by_superadmin_superadminnotification',
            'title' => 'OpSport Admin has been created successfully by you',
            'body' => '<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f0f0f0">
            <tbody>
               <tr>
                  <td style="padding: 30px 30px 20px 30px;">
                     <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff" style="max-width: 650px;background-image: url(https://mybodyfatscan.com/public/frontend/app-assets/images/backgrounds/MOUNTAN.png) !important;  margin: auto;">
                        <tbody>
                           <tr>
                              <td colspan="2" align="center" style=" padding: 40px 40px 60px;background: #000;">
                                <h1 class="logo_text" style="font-size: 31px;font-family: arial;color: #fff;
                                margin: 0;">OP <span style="color: #00ffa2;font-family: arial;padding: 8px 0 0 0;display: inline-block;">Sportsbook</span></h1>
                                <span class="text-uppercase font-weight-bold short_title" style="font-size: 15px;font-family: arial;color: #fff;
                                margin: 10px 0 0 0;text-transform: uppercase;display: inline-block;">Decentralized Anonymous Global</span>
                              </td>
                           </tr>
                           <tr style="box-shadow: inset 0px 3px 5px -2px rgb(0 0 0 / 10%); background-color: #fff;">
                              <td colspan="2" align="center" style="padding: 40px 50px 0px 50px;">
                                 <h1 style="padding-right: 0em; margin: 0; line-height: 40px; font-weight:600;  color: #666; text-align: left; font-family: sans-serif; padding-bottom: 1em;">You have successfully added admin</h1>
                              </td>
                           </tr>
                           <tr style=" background-color: #fff; box-shadow: inset 0px 0px 0px 1px rgb(0 0 0 / 0%);">
                              <td style="text-align: left; padding: 0px 50px;" valign="top">
                                 <p style=" font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #666; text-align: left; padding-bottom: 3%;">
                                    Hi {name}
                                 </p>
                                 <b>Below is the details of admin {admin_name} login</b>
                                 <p> Username : {email}</p>
                                 <p> Password : {password}</p>
                              </td>
                           </tr>
                           <tr style="background-color: #fff; box-shadow: inset 0px -3px 5px -1px rgb(0 0 0 / 10%);">
                              <td style="text-align: left; padding: 30px 50px 50px 50px" valign="top">
                                 <p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;">
                                    Thanks,<br><span style="color:#00ffa2;"><br></span></p><p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;"><span style="color:#00ffa2;">Manager</span></p><p style="font-family: sans-serif; font-size: 18px; margin: 0; line-height: 24px;  color: #505050; text-align: left;"><span style="color:#00ffa2;">Opsports Book</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td colspan="2" align="center" style="padding: 20px 40px 40px 40px; ">
                                 <p style="font-family: sans-serif; font-size: 12px; margin: 0; line-height: 24px;  color: #777;">
                                    ï¿½ 2022
                                    <a href="#" target="_blank" style="color: #777; text-decoration: none">Opsports Book</a>
                                    <br>
                                 </p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>'
        ]);
    }
}

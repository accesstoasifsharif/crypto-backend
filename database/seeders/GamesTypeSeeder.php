<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class GamesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('games_type')->insert([
            'game_type' => 'American Football',
            'img' => '202205260343american.png',
            'color' => '#49ff3c'
        ]);

        DB::table('games_type')->insert([
            'game_type' => 'Boxing MMA',
            'img' => '202205260344mma.png',
            'color' => '#ff4a4d'
        ]);

        DB::table('games_type')->insert([
            'game_type' => 'Soccer League',
            'img' => '202205260344football.png',
            'color' => '#72afd2'
        ]);

        DB::table('games_type')->insert([
            'game_type' => 'Hockey',
            'img' => '202205260344hockey.png',
            'color' => '#065935'
        ]);
        
    }
}

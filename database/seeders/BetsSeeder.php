<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class BetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bets')->insert([
            'user_id' => 1,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 2,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 3,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 4,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 5,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 6,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 7,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 8,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 9,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);

        DB::table('bets')->insert([
            'user_id' => 10,
            'game_id' => 2,
            'games_change_history_id' => 2,
            'bet_team' => '2',
            'bet_amount' => 100,
            'to_win' => 200,
            'pay_out' => 300,
            'american_ods' => 100,
            'decimal_ods' => 2
        ]);
    }
}

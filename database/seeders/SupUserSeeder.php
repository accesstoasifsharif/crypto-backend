<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class SupUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /* for ($i=0; $i < 20; $i++) {
            DB::table('sup_user')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('password')
            ]);
        } */

       
        DB::table('sup_user')->insert([
            'name' => 'SK Test',
            'email' => 'skashyaptest@gmail.com',
            'password' => Hash::make('password')
        ]);
        
        DB::table('sup_user')->insert([
            'name' => 'MJ Jas',
            'email' => 'mjjas@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Alex Rey',
            'email' => 'alexreytest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Freddy Wilson',
            'email' => 'fredywilson@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Sanjay Jain',
            'email' => 'sanjayjain@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Abdul Basit',
            'email' => 'abdulbasit@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Rishav Jain',
            'email' => 'rishavjain@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Marc',
            'email' => 'marctest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Roman',
            'email' => 'romantest@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Kevin',
            'email' => 'kevin@gmail.com',
            'password' => Hash::make('password')
        ]);

        DB::table('sup_user')->insert([
            'name' => 'Jemison',
            'email' => 'jemisontest@gmail.com',
            'password' => Hash::make('password')
        ]);
        
    }
}

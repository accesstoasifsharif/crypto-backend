<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class SupUserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sup_user_type')->insert([
            'user_type' => 'superadmin'
        ]);
        DB::table('sup_user_type')->insert([
            'user_type' => 'admin'
        ]);
        DB::table('sup_user_type')->insert([
            'user_type' => 'user'
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('teams')->insert([
            'game_type' => 3,
            'name' => 'Real Madrid',
            'country' => 'Germany',
            'flag' => '202205260512atk.jpg'
        ]);

        DB::table('teams')->insert([
            'game_type' => 3,
            'name' => 'Barcelona FC',
            'country' => 'Brazil',
            'flag' => '202205260838Bengaluru_FC_logo.png'
        ]);

        DB::table('teams')->insert([
            'game_type' => 4,
            'name' => 'Ranchi Rays',
            'country' => 'India',
            'flag' => '202205270726ranchi.jpg'
        ]);

        DB::table('teams')->insert([
            'game_type' => 4,
            'name' => 'Punjab Warriors',
            'country' => 'India',
            'flag' => '202205270727punjab.jpg'
        ]);

        DB::table('teams')->insert([
            'game_type' => 4,
            'name' => 'Mumbai Magicians',
            'country' => 'India',
            'flag' => '202205270727mumbai.jpg'
        ]);

        DB::table('teams')->insert([
            'game_type' => 4,
            'name' => 'Delhi Wave Riders',
            'country' => 'India',
            'flag' => '202205270728delhi.jpg'
        ]);

        DB::table('teams')->insert([
            'game_type' => 4,
            'name' => 'Uttar Pradesh Wizards',
            'country' => 'India',
            'flag' => '202205270728Up.jpg'
        ]);


    }
}

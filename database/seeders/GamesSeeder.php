<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('games')->insert([
            'team1_id' => 1,
            'team2_id' => 2,
            'game_type' => 3,
            'winning_percentage_team1' => 25.40,
            'winning_percentage_team2' => 74.60,
            'max_amount_user_bet' => 1000,
            'game_date' => '2022-05-26 22:58:00',
            'venue' => 'South Wales',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);

        DB::table('games')->insert([
            'team1_id' => 3,
            'team2_id' => 4,
            'game_type' => 4,
            'winning_percentage_team1' => 80.00,
            'winning_percentage_team2' => 20.00,
            'max_amount_user_bet' => 1000,
            'game_date' => '2022-05-29 17:03:00',
            'venue' => 'JSSCA Ranchi',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);

        DB::table('games')->insert([
            'team1_id' => 4,
            'team2_id' => 5,
            'game_type' => 4,
            'winning_percentage_team1' => 77.00,
            'winning_percentage_team2' => 23.00,
            'max_amount_user_bet' => 1000,
            'game_date' => '2022-05-30 20:00:00',
            'venue' => 'Mohali',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);

        DB::table('games')->insert([
            'team1_id' => 5,
            'team2_id' => 6,
            'game_type' => 4,
            'winning_percentage_team1' => 20.00,
            'winning_percentage_team2' => 80.00,
            'max_amount_user_bet' => 800,
            'game_date' => '2022-05-31 19:00:00',
            'venue' => 'New Delhi',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);

        DB::table('games')->insert([
            'team1_id' => 7,
            'team2_id' => 3,
            'game_type' => 4,
            'winning_percentage_team1' => 9.30,
            'winning_percentage_team2' => 90.70,
            'max_amount_user_bet' => 800,
            'game_date' => '2022-06-01 20:00:00',
            'venue' => 'Lucknow',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);

        DB::table('games')->insert([
            'team1_id' => 4,
            'team2_id' => 6,
            'game_type' => 4,
            'winning_percentage_team1' => 74.50,
            'winning_percentage_team2' => 25.50,
            'max_amount_user_bet' => 1000,
            'game_date' => '2022-06-02 19:30:00',
            'venue' => 'New Delhi',
            'result' => '0',
            'timeline' => '0',
            'status' => '1',
            'max_no_of_bets' => '11',
            'american_ods' => '100',
            'decimal_ods' => '2'
        ]);
    }
}

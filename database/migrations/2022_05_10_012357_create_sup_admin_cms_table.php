<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupAdminCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_admin_cms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cms_page');
            $table->string('cms_title');
            $table->longText('cms_desc');
            $table->string('cms_image')->nullable();
            $table->enum('status',['0','1'])->comment("'0=>Deactive', '1=>Active'")->default('1');
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_admin_cms');
    }
}

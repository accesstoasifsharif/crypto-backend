<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team1_id') ;
            $table->integer('team2_id') ; 
            $table->integer('game_type') ;
            $table->float('winning_percentage_team1') ;
            $table->float('winning_percentage_team2') ;
            $table->integer('min_amount_user_bet')->nullable() ;
            $table->integer('max_amount_user_bet') ;
            $table->datetime('game_date') ;
            $table->string('venue')->nullable() ;
            $table->enum('result',['0','1','2','3'])->comment("'0=>No Result', '1=>Team1 Win','2=>Team2 Win','3=>Draw'");
            $table->enum('timeline',['0','1','2'])->comment("'0=>Pending','1=>Start','2=>Completed'") ;
            $table->enum('status',['0','1'])->comment("'0=>Deactive','1=>Active'");
            $table->integer('max_no_of_bets');
            $table->double('american_ods',[10,2])->nullable()->default('0');
            $table->double('decimal_ods',[10,2])->nullable()->default('0');
            $table->double('fractional_ods',[10,2])->nullable()->default('0');
            $table->double('implide_ods',[10,2])->nullable()->default('0');
            
            $table->float('spread1_t1',10,2)->default('0');
            $table->float('spread2_t1',10,2)->default('0');
            $table->float('moneyline_t1',10,2)->default('0');
            $table->float('total1_t1',10,2)->default('0');
            $table->float('total2_t1',10,2)->default('0');
            $table->enum('total1_type_t1',['O','U'])->default('O');

            $table->float('spread1_t2',10,2)->default('0');
            $table->float('spread2_t2',10,2)->default('0');
            $table->float('moneyline_t2',10,2)->default('0');
            $table->float('total1_t2',10,2)->default('0');
            $table->float('total2_t2',10,2)->default('0');
            $table->enum('total1_type_t2',['O','U'])->default('U');
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GamesAddHistoryTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared('
        CREATE TRIGGER games_add_history_trigger AFTER INSERT ON `games` FOR EACH ROW
            BEGIN
                INSERT INTO games_change_history (`game_id`, `american_ods`, `decimal_ods`, `fractional_ods`, `implide_ods`, `spread1_t1`, `spread2_t1`, `moneyline_t1`, `total1_t1`, `total2_t1`, `total1_type_t1`, `spread1_t2`, `spread2_t2`, `moneyline_t2`, `total1_t2`, `total2_t2`, `total1_type_t2`) 
                VALUES (new.id, new.american_ods, new.decimal_ods, new.fractional_ods, new.implide_ods, new.spread1_t1, new.spread2_t1, new.moneyline_t1, new.total1_t1, new.total2_t1, new.total1_type_t1, new.spread1_t2, new.spread2_t2, new.moneyline_t2, new.total1_t2, new.total2_t2, new.total1_type_t2);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP TRIGGER `games_add_history_trigger`');
    }
}

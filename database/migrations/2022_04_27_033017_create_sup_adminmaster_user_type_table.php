<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupAdminmasterUserTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_adminmaster_user_type', function (Blueprint $table) {
            $table->id();
            $table->string('user_type'); 
        });
        /* DB::table('sup_adminmaster_user_type')->insert([
            'user_type'=>'superadmin'
        ]);
        DB::table('sup_adminmaster_user_type')->insert([
            'user_type'=>'admin'
        ]); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_adminmaster_user_type');
    }
}

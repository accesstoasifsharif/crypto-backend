<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCryptoSwapLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_swap_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sup_user_id')->unsigned();
            $table->foreign('sup_user_id')
                ->references('id')
                ->on('sup_users')
                ->onDelete('cascade');
            $table->string('amount_to');
            $table->string('amount_from');
            $table->string('currency_to');
            $table->string('currency_from');
            $table->string('exchange_rate');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_swap_logs');
    }
}

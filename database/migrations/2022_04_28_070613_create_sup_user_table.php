<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type')->default('2');
            $table->string('name');
            $table->string('email');
            $table->datetime('email_verified_at')->nullable();
            $table->string('password');
            $table->text('remember_token')->nullable();
            $table->enum('status',['0','1'])->comment("'0=>Deactive', '1=>Active'")->default('1');
            $table->string('image')->nullable();
            $table->enum('forgot',['Y','N'])->default('N');
            $table->enum('email_conf',['Y','N'])->default('N');
            $table->string('email_hash')->nullable();
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_user');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GamesChangeHistoryTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared('
        CREATE TRIGGER games_change_history_trigger AFTER UPDATE ON `games` FOR EACH ROW
            BEGIN
            IF NEW.spread1_t1 <> OLD.spread1_t1 || NEW.spread2_t1 <> OLD.spread2_t1 || NEW.spread1_t2 <> OLD.spread1_t2 || NEW.spread2_t2 <> OLD.spread2_t2  THEN
                IF (NEW.spread1_t1 <> OLD.spread1_t1 || NEW.spread2_t1 <> OLD.spread2_t1) && (NEW.spread1_t2 <> OLD.spread1_t2 || NEW.spread2_t2 <> OLD.spread2_t2) THEN
                INSERT INTO games_change_history (`game_id`, `american_ods`, `decimal_ods`, `fractional_ods`, `implide_ods`, `spread1_t1`, `spread2_t1`, `spread1_t2`, `spread2_t2`) 
                VALUES (new.id, new.american_ods, new.decimal_ods, new.fractional_ods, new.implide_ods, new.spread1_t1, new.spread2_t1, new.spread1_t2, new.spread2_t2);
                ELSEIF NEW.spread1_t1 <> OLD.spread1_t1 || NEW.spread2_t1 <> OLD.spread2_t1 THEN
                INSERT INTO games_change_history (`game_id`, `american_ods`, `decimal_ods`, `fractional_ods`, `implide_ods`, `spread1_t1`, `spread2_t1`) 
                VALUES (new.id, new.american_ods, new.decimal_ods, new.fractional_ods, new.implide_ods, new.spread1_t1, new.spread2_t1);
                ELSE
                INSERT INTO games_change_history (`game_id`, `american_ods`, `decimal_ods`, `fractional_ods`, `implide_ods`, `spread1_t2`, `spread2_t2`) 
                VALUES (new.id, new.american_ods, new.decimal_ods, new.fractional_ods, new.implide_ods, new.spread1_t2, new.spread2_t2);
                END IF;
            END IF;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP TRIGGER `games_change_history_trigger`');
    }
}

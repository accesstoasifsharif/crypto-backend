<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('bet_id')->nullable()->default('0');
            //$table->json('transaction_details')->nullable()->default('{}');
            $table->json('transaction_details')/* ->nullable()->default('{}') */;
            $table->enum('transaction',['C','D','W','U'])->comment("'C=>Credit','D=>Debit','W=>Withdrawal','U=>Used'");
            $table->double('amount',[10,2]);
            $table->enum('status',['0','1'])->comment("'0=>Pending','1=>Approved'");
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}

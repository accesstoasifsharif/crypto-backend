<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('game_id');
            $table->integer('games_change_history_id');
            $table->enum('bet_team',['1','2'])->comment("'1=>Team1', '2=>Team2'");
            $table->double('bet_amount',[10,2]);
            $table->double('to_win',[10,2]);
            $table->double('pay_out',[10,2]);
            $table->double('american_ods',[10,2])->nullable()->default('0');
            $table->double('decimal_ods',[10,2])->nullable()->default('0');
            $table->double('fractional_ods',[10,2])->nullable()->default('0');
            $table->double('implide_ods',[10,2])->nullable()->default('0');

            $table->float('spread1_t1',10,2)->default('0');
            $table->float('spread2_t1',10,2)->default('0');
            $table->float('moneyline_t1',10,2)->default('0');
            $table->float('total1_t1',10,2)->default('0');
            $table->float('total2_t1',10,2)->default('0');
            $table->enum('total1_type_t1',['O','U'])->default('O');

            $table->float('spread1_t2',10,2)->default('0');
            $table->float('spread2_t2',10,2)->default('0');
            $table->float('moneyline_t2',10,2)->default('0');
            $table->float('total1_t2',10,2)->default('0');
            $table->float('total2_t2',10,2)->default('0');
            $table->enum('total1_type_t2',['O','U'])->default('U');
            $table->integer('win')->nullable()->default('0')->comment("'0=>No Result', '1=>Lost', '2=>Won', '3=>Draw'");
            $table->integer('settled')->nullable()->default('0');
            $table->string('wallet_address')->nullable();
            $table->integer('bet_box')->nullable()->default('1');
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets');
    }
}

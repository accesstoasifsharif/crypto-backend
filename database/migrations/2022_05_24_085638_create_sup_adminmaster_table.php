<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupAdminmasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
                Schema::create('sup_adminmaster', function (Blueprint $table) {
                    $table->id('user_id');
                    $table->string('user_firstname');
                    $table->string('user_lastname')->nullable();
                    $table->string('user_email');
                    $table->string('user_password');
                    $table->string('user_image')->nullable();
                    $table->datetime('user_lastlogin')->default(DB::raw('CURRENT_TIMESTAMP'));
                    $table->integer('user_type')->default('1');
                    $table->enum('user_forgotstatus', ['N', 'Y'])->default('N');
                    $table->string('api_token')->nullable();
                    $table->string('image')->nullable();
                    $table->enum('forgot',['Y','N'])->default('N');
                    $table->enum('email_conf',['Y','N'])->default('N');
                    $table->string('email_hash')->nullable();
                    //$table->timestamps();
                    $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                    $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            
        });

        /* DB::table('sup_adminmaster')->insert([
            'user_firstname'=>'Super Admin', 
            'user_email'=>'jasimnft@gmail.com',
            'user_password'=> md5('123456')
        ]); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_adminmaster');
    }
}

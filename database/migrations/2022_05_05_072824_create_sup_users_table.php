<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type')->default('3');
            $table->string('name');
            $table->string('email')->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone_number')->nullable();
            $table->string('account_id')->nullable();
            $table->text('remember_token')->nullable();
            $table->double('available_balance',[10,2])->default('0');
            $table->double('current_balance',[10,2])->default('0');
            $table->enum('status',['0','1'])->comment("'0=>Deactive', '1=>Active'")->default('1');
            $table->string('image')->nullable();
            $table->enum('forgot',['Y','N'])->default('N');
            $table->enum('email_conf',['Y','N'])->default('N');
            $table->string('email_hash')->nullable();
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GamesChangeHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('games_change_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id') ;
            $table->double('american_ods',[10,2])->nullable()->default('0');
            $table->double('decimal_ods',[10,2])->nullable()->default('0');
            $table->double('fractional_ods',[10,2])->nullable()->default('0');
            $table->double('implide_ods',[10,2])->nullable()->default('0');
            
            $table->float('spread1_t1',10,2)->default('0');
            $table->float('spread2_t1',10,2)->default('0');
            $table->float('moneyline_t1',10,2)->default('0');
            $table->float('total1_t1',10,2)->default('0');
            $table->float('total2_t1',10,2)->default('0');
            $table->enum('total1_type_t1',['O','U'])->default('O');

            $table->float('spread1_t2',10,2)->default('0');
            $table->float('spread2_t2',10,2)->default('0');
            $table->float('moneyline_t2',10,2)->default('0');
            $table->float('total1_t2',10,2)->default('0');
            $table->float('total2_t2',10,2)->default('0');
            $table->enum('total1_type_t2',['O','U'])->default('U');

            $table->string('bet_status_win')->nullable()->default('0,0,0,0,0,0');
            //$table->timestamps();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('games_change_history');
    }
}

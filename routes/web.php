<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CmsController;
//use App\Http\Middleware\checklogin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('/', [AdminController::class, 'index']); 


//Route::get('/admin', 'AdminController@index'); 
	Route::get('/admin', [AdminController::class, 'index']); 
	Route::post('/admin', [AdminController::class, 'dologin']); 
    //Route::post('/', [AdminController::class, 'adminlogin']); 
	Route::get('/logout', [AdminController::class, 'logout']);
	
	Route::group(['middleware' => ['checklogin']], function () {

		Route::get('/home', [AdminController::class, 'home']);
		/*--------------------User----------------------------------*/
		//Route::get('/home', [AdminController::class, 'home']);
		/*--------------------------- Manage Admin----------------------------------*/
		Route::get('/admin/manage', [AdminController::class, 'admin_list']);
		Route::get('/admin/add', [AdminController::class, 'add_admin']);
		Route::post('/admin/add', [AdminController::class, 'submit_admin']);
        Route::get('/admin/edit/{id}', [AdminController::class, 'edit_admin']);
        Route::get('/admin/delete/{id}', [AdminController::class, 'delete_admin']);
        Route::get('/admin/profile/{id}', [AdminController::class, 'admin_profile']);
        Route::get('/admin/changepassword', [AdminController::class, 'changepassword']);
        Route::post('/admin/changepassword', [AdminController::class, 'store_changepassword']);
        Route::post('/admin/manage/status', [AdminController::class, 'admin_status']);

        /*------------------------------Manage Teams-----------------------------------------*/

        Route::get('/admin/teams_list/{game_type?}', [AdminController::class, 'team_list']);
        Route::get('/admin/teams/add/{game_type?}',[AdminController::class,'addteams']);
        Route::post('/admin/teams/addteams/{game_type?}',[AdminController::class, 'submit_addteams']);
        Route::get('/admin/teams/edit/{id}/{game_type?}', [AdminController::class, 'edit_addteams']);
        Route::get('/admin/teams/delete/{id}/{game_type?}',[AdminController::class,'delete_addteams']); 


        
        /*--------------------------- Manage Games----------------------------------*/
        Route::get('/admin/games', [AdminController::class, 'games_list']);
        Route::get('/admin/games/view/{id}', [AdminController::class, 'game_detail']);
        Route::post('/admin/games/teams_by_game_type', [AdminController::class, 'teams_by_game_type']);


        Route::get('/admin/gamestype', [AdminController::class, 'gamestype']);
        Route::get('/admin/gamestype/add', [AdminController::class, 'gamestype_add']);
        Route::post('/admin/gamestype/store', [AdminController::class, 'gamestype_store']);
        Route::get('/admin/gamestype/edit/{id}', [AdminController::class, 'edit_gamestype']);
        Route::get('/admin/gamestype/delete/{id}',[AdminController::class,'delete_gamestype']); 

        Route::post('/admin/games/status', [AdminController::class, 'gmae_status']);

        //Route::get('/admin/games/{id}', [AdminController::class, 'game_show']);
        Route::get('/admin/games/add', [AdminController::class, 'add_gamme']);
        Route::post('/admin/games/store_games', [AdminController::class, 'store_games']);
	    Route::get('/admin/games/edit/{id}', [AdminController::class, 'edit_gmaes']);
	    Route::post('/admin/change_bet_status', [AdminController::class, 'change_bet_status']);
        Route::get('/admin/games/delete/{id}',[AdminController::class,'delete_games']); 
        /*--------------------------- Manage Bet----------------------------------*/

        Route::get('/admin/bet/{game_id?}', [AdminController::class, 'bet_index']);
        Route::get('/admin/bets/{game_id}/{games_change_history_id}', [AdminController::class, 'bet_by_gameid_and_historyid_index']);
        Route::get('/admin/bet/view/{id}', [AdminController::class, 'bet_detail']);
        Route::post('/admin/bet/status', [AdminController::class, 'bet_status']);
        Route::get('/admin/bet/delete/{id}',[AdminController::class,'delete_bets']); 
        Route::get('/admin/bet/settled_status/{id}',[AdminController::class,'settled_status']); 
        Route::get('/admin/users/bet', [AdminController::class, 'bet_store']); 
       // Route::get('/admin/bet/add',  [AdminController::class,'createBetForMatch']);
        Route::get('/user/allbets/{id}',  [AdminController::class,'seeBetsOfUser']);


        Route::get('/admin/bet/get_bet_details_settlement/{bet_id}',[AdminController::class,'bet_details_settlement']);

        Route::post('/admin/bet/settled_bet_save_crypto_transaction',[AdminController::class,'settled_bet_save_crypto_transaction']);

        
        Route::get('/admin/manage_game', [AdminController::class, 'manage_game']);
        Route::get('/admin/manage_teams_by_game_type', [AdminController::class, 'teamsByGameType']);
        Route::get('/admin/games_by_type', [AdminController::class, 'games_by_type']);
        
        Route::get('/admin/games_by_type/games/{game_type?}', [AdminController::class, 'games_list_by_type']);
        Route::get('/admin/games_by_type/games/view/{id}/{game_type?}', [AdminController::class, 'games_list_by_type_game_detail']);
        Route::post('/admin/games_by_type/status', [AdminController::class, 'gmae_status_by_type']);
        Route::get('/admin/games_by_type/add/{game_type?}', [AdminController::class, 'add_gamme_by_type']);
        Route::post('/admin/games_by_type/store_games/{game_type?}', [AdminController::class, 'store_games_by_type']);
	    Route::get('/admin/games_by_type/edit/{id}/{game_type?}', [AdminController::class, 'edit_gmaes_by_type']);
        Route::get('/admin/games_by_type/delete/{id}/{game_type?}',[AdminController::class,'delete_games_by_type']); 



        Route::get('/admin/livebet', [AdminController::class, 'livebet']);
        Route::get('/admin/livebet/boxing_mma', [AdminController::class, 'boxing_mma']);
        Route::post('/admin/livebet/boxing_mma/status', [AdminController::class, 'boxing_mma_status']);
        Route::get('/admin/livebet/soccer_leauge', [AdminController::class, 'soccer_leauge']);
        Route::post('/admin/livebet/soccer_leauge/status', [AdminController::class, 'soccer_leauge_status']);
        Route::get('/admin/livebet/american_football', [AdminController::class, 'american_football']);
        Route::post('/admin/livebet/american_football/status', [AdminController::class, 'soccer_leauge_status']);
        Route::get('/admin/livebet/hockey', [AdminController::class, 'hockey']);
        Route::post('/admin/livebet/hockey/status', [AdminController::class, 'soccer_leauge_status']);

        /*--------------------------- Manage User----------------------------------*/
        Route::get('/admin/users', [AdminController::class, 'user_index']);
        Route::get('/admin/users/{id}', [AdminController::class, 'user_details']);
        Route::get('/admin/users/bet', [AdminController::class, 'createBetForMatch']);
        Route::post('/admin/users/status', [AdminController::class, 'user_status']);

        Route::get('/admin/users/usersprofile/{id}', [AdminController::class, 'usersprofile']);
        Route::get('/admin/users/usersprofile/edit/{id}', [AdminController::class, 'edituserprofile']);
        Route::post('/admin/users/usersprofile/store', [AdminController::class, 'edituserprofile_store']);
        Route::get('/admin/users/usersprofile/delete/{id}',[AdminController::class,'delete_userinfon']); 



        Route::get('/admin/users/usersbetting/{id}', [AdminController::class, 'usersbetting']);
        Route::get('/admin/users/userstransactions/{id}', [AdminController::class, 'userstransactions']);
        Route::post('/admin/users/userstransactions/status/{id}', [AdminController::class, 'userstransactions_status']);



        Route::get('/admin/users/useraddbalance/{id}', [AdminController::class, 'useraddbalance']);
        Route::post('/admin/users/useraddbalance/status/{id}', [AdminController::class, 'addbal_status']);
        Route::get('/admin/users/addbalance/add/{id}', [AdminController::class, 'add_addbalance']);
        Route::post('/admin/users/addbalance/store_addbalance', [AdminController::class, 'store_addbalance']);
	
        
        Route::get('/admin/users/userwithdrawlbalance/{id}', [AdminController::class, 'userwidbalance']);
        Route::post('/admin/users/userwithdrawlbalance/status/{id}', [AdminController::class, 'widbal_status']);
        Route::get('/admin/users/withdrawlbalance/add/{id}', [AdminController::class, 'add_widbalance']);
        Route::post('/admin/users/withdrawlbalance/store_addbalance', [AdminController::class, 'store_widbalance']);


        Route::get('/admin/users/userdebitbalance/{id}', [AdminController::class, 'userdebbalance']);
        Route::post('/admin/users/userdebitbalance/status/{id}', [AdminController::class, 'debbal_status']);
        Route::get('/admin/users/userdebitbalance/add/{id}', [AdminController::class, 'add_debbalance']);
        Route::post('/admin/users/userdebitbalance/store_addbalance', [AdminController::class, 'store_debbalance']);
        
        

        /*------------------------------Manage Matchs-----------------------------*/
        Route::get('/admin/matchs',[AdminController::class,'match_index']);
        Route::get('/admin/matchs/add',[AdminController::class,'addmatch']);
        Route::get('/admin/matchs/edit/{id}', [AdminController::class, 'edit_match']);
        Route::post('/admin/matchs/addmatch',[AdminController::class, 'submit_addmatch']);

        /*-----------------------------------Game Plan--------------------------*/
        
        Route::get('/admin/gameplan',[AdminController::class,'gameplan']);
        Route::get('/admin/gameplan/add',[AdminController::class,'add_gameplan']);
        Route::post('/admin/gameplan/store',[AdminController::class, 'submit_gameplan']);
        Route::get('/admin/gameplan/edit/{id}', [AdminController::class, 'edit_gamplan']);
        Route::get('/admin/gameplan/delete/{id}',[AdminController::class,'delete_gamplan']); 

        /*------------------------------Admin CMS-----------------------------*/
        Route::get('/admin/cms',[AdminController::class,'admincms']);
        Route::get('/admin/create/{rid?}', [AdminController::class, 'create_admincms']);
        Route::post('/admin/submit', [AdminController::class, 'submit_admincms']);
        Route::post('/admin/status', [AdminController::class, 'admin_cms_status']);
        Route::get('/admin/cms/edit/{id}/{rid?}', [AdminController::class, 'edit_admincms']);
        Route::get('/admin/cms/delete/{id}/{rid?}', [AdminController::class, 'delete_admincms']);
       
        Route::get('/admin/transactions', [AdminController::class, 'get_transactions']);

	});

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\SupUserController;
use App\Http\Controllers\CryptoController;

//use App\Http\Middleware\DomainCheckMiddleware;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//SuperAdmin
Route::group([
    'middleware' => [/* 'DomainCheckMiddleware',  */'api'],
    'prefix' => 'v1/superadmin'
], function ($router) {
    //For SuperAdminAuth
    //Route::post('register', [SuperAdminController::class, 'register']);
    Route::post('login', [SuperAdminController::class, 'login']);
    Route::post('logout', [SuperAdminController::class, 'logout']);
    Route::post('refresh', [SuperAdminController::class, 'refresh']);
    //Route::post('addcredit', [SuperAdminController::class, 'addCredit']);
    Route::post('profile', [SuperAdminController::class, 'userProfile']);
    Route::post('profile_update', [SuperAdminController::class, 'userProfileUpdate']);

    

}); 


//SupUser

Route::group([
    'middleware' => [/* 'DomainCheckMiddleware',  */'api'],
    'prefix' => 'v1/user'
], function ($router) {
    //For SuperAdminAuth
    Route::post('register', [SupUserController::class, 'register']);
    Route::post('login', [SupUserController::class, 'login']);
    Route::post('logout', [SupUserController::class, 'logout']);
    Route::post('change-password',[SupUserController::class, 'change_password']);
    Route::post('refresh', [SupUserController::class, 'refresh']);
    //Route::post('addcredit', [SupUserController::class, 'addCredit']);
    Route::post('profile', [SupUserController::class, 'userProfile']);
    Route::post('profile_update', [SupUserController::class, 'userProfileUpdate']);
    Route::post('change_password', [SupUserController::class, 'change_password_new']);
    /* Route::get('allgames', [SupUserController::class, 'allgames']);
    Route::get('allgames/{game_type}', [SupUserController::class, 'allgames_type']);
    Route::get('usersbetting/{id}', [SupUserController::class, 'usersbetting']);
    Route::get('usersbetting/{id}/show', [SupUserController::class, 'usersbettingshow']);
    Route::get('transaction', [SupUserController::class, 'userstransaction']);
    Route::get('transaction/addbalance', [SupUserController::class, 'usersaddbalance']);
    Route::get('transaction/withdrawlbalance', [SupUserController::class, 'withdrawl_bal']);
    Route::get('allbet', [SupUserController::class, 'allbet']);
    Route::post('bet', [SupUserController::class, 'bet']);
    Route::get('upcoming_bet', [SupUserController::class, 'upcoming_bet']); */
    Route::post('games', [SupUserController::class, 'games']);
    Route::post('games_s', [SupUserController::class, 'games_s']);
    Route::post('games_by_game_type', [SupUserController::class, 'games_by_game_type']);
    Route::post('gamebyid', [SupUserController::class, 'gamebyid']);
    Route::post('bets', [SupUserController::class, 'bets']);
    Route::post('bets/add', [SupUserController::class, 'bets_add']);
    Route::post('game_type', [SupUserController::class, 'game_type']);
    Route::post('transactions', [SupUserController::class, 'transactions']);
    Route::post('total_investment_earned', [SupUserController::class, 'total_investment_earned']);
    Route::post('admin_cms_by_page', [SupUserController::class, 'admin_cms_by_page']);
    Route::post('test', [SupUserController::class, 'test']);

    Route::post('bets/checkUserCanBetMore', [SupUserController::class, 'checkUserCanBetMore']);

    // Code Added by Asif
    Route::post('save_crypto_transaction', [CryptoController::class, 'save_crypto_transaction']);
    Route::get('get_user_all_crypto_transactions', [CryptoController::class, 'get_user_all_crypto_transactions']);
    Route::post('save_user_crypto_wallets', [CryptoController::class, 'save_user_crypto_wallets']);
    Route::get('get_user_crypto_wallets/{sup_users_id}', [CryptoController::class, 'get_user_crypto_wallets']);
    Route::post('save_crypto_swap_logs', [CryptoController::class, 'save_crypto_swap_logs']);
    Route::get('get_crypto_swap_logs/{sup_users_id}/{currency_type_id}', [CryptoController::class, 'get_crypto_swap_logs']);
    Route::get('get_user_stats', [CryptoController::class, 'get_user_stats']);

    Route::get('getBetsData', [SupUserController::class, 'getBetsData']);
    

}); 








FROM php:7.4


# Update packages
#RUN apt-get update
RUN apt-get update && apt-get install -y git && apt-get install -y --no-install-recommends apt-utils

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev  libz-dev libzip-dev libonig-dev
#==========================================================================================
 
RUN apt-get install -my wget gnupg

#RUN  apt-get install -y --no-install-recommends apt-utils
# install node.js and npm

#RUN curl -sL https://deb.nodesource.com/setup_12.x |   bash -
#RUN apt-get install -y nodejs   
#RUN npm install npm@latest -g
#RUN node -v
#RUN npm -v 
#=============================================================================================
# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
RUN pecl install mcrypt-1.0.4 \
	&& docker-php-ext-enable mcrypt
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install  pdo_mysql zip gd mbstring exif

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
 
# Install Laravel Envoy
RUN composer global require laravel/envoy


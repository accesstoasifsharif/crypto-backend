<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CryptoSwapLog extends Model
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    protected $fillable = [ 'sup_user_id', 'amount_to', 'amount_from', 'currency_to', 'currency_from', 'exchange_rate' ];

    protected $appends = [
        'currency_to_name', 'currency_from_name'
    ];

    public function getCurrencyToNameAttribute()
    {
        if ($this->currency_to == 1) {
            return 'SOL';
        }
        if ($this->currency_to == 2) {
            return 'BETC';
        }
    }
    public function getCurrencyFromNameAttribute()
    {
        if ($this->currency_from == 1) {
            return 'SOL';
        }
        if ($this->currency_from == 2) {
            return 'BETC';
        }
    }

    public function supUser()
    {
        return $this->belongsTo(SupUser::class);
    }
}
<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;   
use Illuminate\Support\Facades\DB;
use App\Models\CryptoTransaction;

class AdminModel extends Model
{
	function getAllData($table){
    	$results = DB::table($table)->get();
		return $results;
	}
	function getAllDataOrderBy($table,$id,$orderby){
    	$results = DB::table($table)->orderBy($id,$orderby)->get();
		return $results;
	}
	function getAllDataSelect($table,$select){
    	$results = DB::table($table)->select($select)->get();
		return $results;
	}
	function getRowById($table,$where){
        $row = DB::table($table)->where($where)->first();
        return $row;
	}
	
	function getResultById($table, $where){
        $result = DB::table($table)->where($where)->get();
        return $result;
	}
	function update_row($table, $data, $where){
		$result = DB::table($table)->where($where)->update($data);
		//return $result;	
		if($result){
			return true;
		}else{
			return false;
		} 
	} 
	
	function getResultByLike($date){
        $result = DB::table('ad_timeline')->where('created_date_time','like',"%$date%")->get();
        return $result;
	}
	function getLastRowById($table,$where,$id,$orderby){
        $last_row=DB::table($table)->where($where)->orderBy($id,$orderby)->first();
        return $last_row;
	}

	

	function deleteRowById($table, $where){
		$result = DB::table($table)->where($where)->delete();
		return $result;	
	}


	function insert($table, $data){
		$result = DB::table($table)->insert($data);
		//return $result;
		return DB::getPdo()->lastInsertId();
	}
	function update_table($table,$id,$data){
		$result = DB::table($table)->where('id',$id)->update($data);
		return $result;		
	}
	function fetch_row($table,$id){
		$result = DB::table($table)->where('id',$id)->first();
		return $result;		
	}
	function fetch_all($table,$search){
	if(empty($search)){
		$result = DB::table($table)->paginate(10)->withQueryString();
	}else{
		$result = DB::table($table)->where('cms_title', 'Like', '%' .$search. '%')->orwhere('cms_page', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
	}
		return $result;		
	}

	function fetch_all_byrest($table,$search){
		if(empty($search)){
			$result = DB::table($table)->where('games_id',$games_id)->paginate(10)->withQueryString();
		}else{
			$result = DB::table($table)->where('games_id',$games_id)->where('cms_title', 'Like', '%' .$search. '%')->orwhere('cms_page', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
			return $result;		
		}
	
	function delete_row($table,$id){
		$result = DB::table($table)->where('id', $id)->delete();
		return $result;	
	}
	
	function userlogin($username,$password){
		$results= DB::table('sup_adminmaster')
			->where('user_email','=',$username)
			->where('user_password','=',md5($password))
			->first();
		return $results;	
	}
	function adminlogin($username,$password){
		$results= DB::table('sup_user')
			->where('email','=',$username)
			
			->where('user_type','=','2')
			->where('status','=','1')
			->first();
		return $results;	
	}
	
	function tablecount($table){
		$results = DB::table($table)->count();
			return $results;	
	}

	function tableWhereCount($table,$where){
		$results = DB::table($table)->where($where)->count();
			return $results;	
	}

	function getusrinfo($id){
		$results= DB::table('ad_adminmaster')
			->where('id','=',$id)
			->first();
			return $results;	
	}

	
	/*-------------------------------------Manage admin--------------------------------*/
	function admin_list($search){
		if(empty($search)){
			$results= DB::table('sup_user')->where('user_type','2')->paginate(10)->withQueryString();
		}else{
			$results= DB::table('sup_user')->where('user_type','2')->where('name', 'Like', '%' .$search. '%')->orwhere('email', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $results;
	}

	function admin_status($id,$data){
		$result = DB::table('sup_user')->where('id',$id)->update($data);
		return $result;		
	}
	
	function add_admin(){
		$result = DB::table('sup_user')->where('status','1')->get(); 
		return $result;		
	}
	function admin_row($email){
		$result=DB::table('sup_user')->where('user_type','2')->where('email',$email)->first();
		return $result;
	}
	

	function admin_edit($id){
		$result=DB::table('sup_user')->where('id',$id)->where('user_type','2')->first();
		return $result;
	}

	function admin_profile($id){
		$results= DB::table('sup_user')->where('id',$id)->first();
		//echo "<pre>"; print_r($results); die;
		return $results;
	}

	function delete_admin($id){
		$result = DB::table('sup_user')->where('id', $id)->delete();
		return $result;	
	}
	

	/*--------------------------------------Manage Teams----------------------------*/ 

	function team_list($search,$id=null){
		if($id){
			if(empty($search)){
				$results= DB::table('teams')->where('game_type',$id)->paginate(10)->withQueryString();
			}else{
				$results= DB::table('teams')->where('game_type',$id)->where('name', 'Like', '%' .$search. '%')->orwhere('country', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
			}
		}else{
			if(empty($search)){
				$results= DB::table('teams')->paginate(10)->withQueryString();
			}else{
				$results= DB::table('teams')->where('name', 'Like', '%' .$search. '%')->orwhere('country', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
			}
		}
		
		return $results;
	}

	function addteams(){
		$result = DB::table('teams')->get(); 
		return $result;		
	}

	function editteams($data,$id){
		$result=DB::table('teams')->where('id',$id)->update($data);
		return $result;
	}

	function edit_addteams($id){
		$result=DB::table('teams')->where('id',$id)
		->first();
		return $result;
	}

	function delete_addteams($id){
		$result = DB::table('teams')->where('id', $id)->delete();
		return $result;	
	} 
	

	/*-------------------------------------Games Model----------------------------------*/
	function games_list($search, $game_type=null){
		if($game_type){
			if(empty($search)){
				$results = DB::table('games')
				->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
				->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
				->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
				//->leftjoin('bets as b1', '', '=', 'b1.id')
				->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))
				//->where('g1.game_type','=','Booxing MMA')
				->where('games.game_type',$game_type)
				->paginate(10)->withQueryString();
			}else{
				$results = DB::table('games')
				->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
				->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
				->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
				//->leftjoin('bets as b1', '', '=', 'b1.id')
				->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))->/* where('games.game_type',$game_type)->or */where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venue', 'Like', '%' .$search. '%')->where('games.game_type',$game_type)
				
				->paginate(10)->withQueryString();
			}
		}else{
			if(empty($search)){
				$results = DB::table('games')
				->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
				->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
				->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
				//->leftjoin('bets as b1', '', '=', 'b1.id')
				->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))
				//->where('g1.game_type','=','Booxing MMA')
				->paginate(10)->withQueryString();
			}else{
				$results = DB::table('games')
				->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
				->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
				->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
				//->leftjoin('bets as b1', '', '=', 'b1.id')
				->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venue', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
			}
		}
		
		return $results;
	}
	
	
	function game_list(){
		
		$results= DB::table('games')->get();;
		
		return $results;
	}
	function gmae_status($id,$data){
		$result = DB::table('games')->where('id',$id)->update($data);
		return $result;		
	}
	
	function gamestype($search){
		if(empty($search)){
			$results= DB::table('games_type')->paginate(10)->withQueryString();
		}else{
			$results= DB::table('games_type')->where('game_type', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $results;
	}

	function gamestype_add(){
		$result = DB::table('games_type')->get(); 
		return $result;		
	}

	function edit_gamestype($id){
		$result=DB::table('games_type')->where('id',$id)
		->first();
		return $result;
	}
	function delete_gamestype($id){
		$result = DB::table('games_type')->where('id', $id)->delete();
		return $result;	
	} 
	function editGamestype($data,$id){
		$result=DB::table('games_type')->where('id',$id)->update($data);
		return $result;
	}

	function add_gamme(){
		$result = DB::table('games')
		->get(); 
		return $result;		
	}

	function editGames($data,$id){
	
		$result=DB::table('games')->where('id',$id)->update($data);
		return $result;
	}

	function getGames($id){
		$result=DB::table('games')
		->join('teams as t1', 'games.team1_id', '=', 't1.id')
		->join('teams as t2', 'games.team2_id', '=', 't2.id')
		->select('t1.name as team1', 't2.name as team2','games.*')
		->where('games.id',$id)->first();
		return $result;
	}

	function delete_games($id){
		$result = DB::table('games')->where('id', $id)->delete();
		return $result;	
	} 
	
	/*-----------------------------------------User Model---------------------------------------*/
	function user_index($search){
		if(empty($search)){
			$results= DB::table('sup_users')->paginate(10)->withQueryString();
		}else{
			$results= DB::table('sup_users')->where('user_type','3')->where('name', 'Like', '%' .$search. '%')->orwhere('email', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $results;
	}

	function user_status($id,$data){
		$result = DB::table('sup_users')->where('id',$id)->update($data);
		return $result;		
	}

	function usersprofile($id){
		
		$results= DB::table('sup_users')->where('id',$id)->first();
		return $results;
		
	}

	function usersbetting($id,$search){
		if(empty($search)){
			$bets = DB::table('bets')
			->join('games as g', 'bets.game_id', '=', 'g.id')
			->join('teams as t1', 'g.team1_id', '=', 't1.id')
			->join('teams as t2', 'g.team2_id', '=', 't2.id')
			//->join('matchs as m', 'bets.match_id', '=', 'm.id')
			//->join('teams as t3', 'bets.bet', '=', 't3.id')
			->select('t1.name as team1', 't2.name as team2',/* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount',/* 'm.id as matchid', */ 'bets.id')
			->where('user_id',$id)
			->paginate(10)->withQueryString();
			//echo '<pre>'; print_r($bets);die();
			//return view('admin/user/')->with('bets',$bets);
		}else{
			$bets = DB::table('bets')
			->join('games as g', 'bets.game_id', '=', 'g.id')
			->join('teams as t1', 'g.team1_id', '=', 't1.id')
			->join('teams as t2', 'g.team2_id', '=', 't2.id')
			//->join('matchs as m', 'bets.match_id', '=', 'm.id')
			//->join('teams as t3', 'bets.bet', '=', 't3.id')
			->select('t1.name as team1', 't2.name as team2',/* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount',/* 'm.id as matchid', */ 'bets.id')
			->where('user_id',$id)
			->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')/* ->orwhere('t3.name', 'Like', '%' .$search. '%') */->paginate(10)->withQueryString();
		}
		foreach($bets as &$bet){
			if($bet->bet == '1'){
				$betteamdetail = $this->getRowById('teams',array('id'=>$bet->team1_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}else{
				$betteamdetail = $this->getRowById('teams',array('id'=>$bet->team2_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}
		}
		//echo "<pre>"; print_r($bets); die;
		return $bets;
		
	}

	function edituserprofile($id){
		$result=DB::table('sup_users')->where('id',$id)
		->first();
		return $result;
	}

	function userprofile_edit($data,$id){
		$result=DB::table('sup_users')->where('id',$id)->update($data);
		return $result;
	}

	function delete_userinfon($id){
		$result = DB::table('sup_users')->where('id', $id)->delete();
		return $result;	
	}

	function userstransactions($id,$search){
		/* if(empty($search)){
			$trans_bal = DB::table('transactions')
			->leftjoin('bets','bets.user_id', '=','transactions.user_id')
			->leftjoin('teams as t3', 'bets.bet', '=', 't3.id')
			->leftjoin('games','bets.match_id','=','games.match_id')
			->leftjoin('games_plan','games_plan.id','=','games.game_plan')
			->select('t3.name as betteam','transactions.*','games.game_plan','games_plan.X as gamerate')
			->where('transactions.user_id',$id)
			->paginate(10)->withQueryString();
			//->tosql();
			//echo '<pre>'; print_r($trans_bal);die();
		}else{
			$trans_bal = DB::table('transactions')
			->leftjoin('bets','bets.user_id', '=','transactions.user_id')
			->leftjoin('teams as t3', 'bets.bet', '=', 't3.id')
			->leftjoin('games','bets.match_id','=','games.match_id')
			->leftjoin('games_plan','games_plan.id','=','games.game_plan')
			->select('t3.name as betteam','transactions.*','games.game_plan','games_plan.X as gamerate')
			->where('transactions.id',$id)
			->where('t3.name', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		} */
		if(empty($search)){
			$trans_bal = DB::table('transactions')
			->where('transaction', 'U')
			->where('transactions.user_id',$id)
			->paginate(10)->withQueryString();
			//->tosql();
			//echo '<pre>'; print_r($trans_bal);die();
		}else{
			$trans_bal = DB::table('transactions')
			->where('transaction', 'U')
			->where('amount', 'Like', '%' .$search. '%')
			->where('transactions.user_id',$id)
			/* ->where('t3.name', 'Like', '%' .$search. '%') */->paginate(10)->withQueryString();
		}
		//echo "<pre>"; print_r($trans_bal); die;
		return $trans_bal;	
	}

	function userstransactions_status($id,$data){
	
		$result = DB::table('transactions')->where('id',$id)->update($data);
	
		return $result;		
	}


	function useraddbalance($id,$search){
		if(empty($search)){
			$add_bal = DB::table('transactions')
			->where('transaction', 'C')
			//->select('id', 'amount', 'via', 'too', 'frm', 'trx')
			//->orderBy('id', 'ASC')
			->paginate(10)->withQueryString();
			//echo '<pre>'; print_r($add_bal);die();
		}else{
			$add_bal = DB::table('transactions')
			->where('transaction', 'C')
			->where('amount', 'Like', '%' .$search. '%')
			/* ->orwhere('via', 'Like', '%' .$search. '%')->orwhere('frm', 'Like', '%' .$search. '%')->orwhere('too', 'Like', '%' .$search. '%') */->paginate(10)->withQueryString();
		}
		return $add_bal;	
	}

	function addbal_status($id,$data){
		$result = DB::table('add_bal')->where('id',$id)->update($data);
		return $result;		
	}

	function userwidbalance($id,$search){
		if(empty($search)){
			$wid_bal = DB::table('transactions')
			->where('transaction', 'W')
			->paginate(10)->withQueryString();
		}else{
			$wid_bal = DB::table('transactions')
			->where('transaction', 'W')
			->where('amount', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $wid_bal;
		
	}

	function widbal_status($id,$data){
		$result = DB::table('withdrawl_bal')->where('id',$id)->update($data);
		return $result;		
	}

	function userdebbalance($id,$search){
		if(empty($search)){
			$wid_bal = DB::table('transactions')
			->where('transaction', 'D')
			->paginate(10)->withQueryString();
		}else{
			$wid_bal = DB::table('transactions')
			->where('transaction', 'D')
			->where('amount', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $wid_bal;
		
	}
	/*---------------------------------------Manage Match------------------------------------*/
	function addmatch(){
		$result = DB::table('matchs')->get(); 
		return $result;		
	}

	function editmatch($data,$id){
		$result=DB::table('matchs')->where('id',$id)->update($data);
		return $result;
	}

	function edit_match($id){
		$result=DB::table('matchs')
		->join('teams as a', 'team1_id', '=', 'a.id')
		->join('teams as b', 'team2_id', '=', 'b.id')
		->join('teams as c', 'winner_id', '=', 'c.id')	
		->leftJoin('stats as s', 's.match_id', '=', 'matchs.id')
		->select('matchs.*', 'a.id as team1_id', 'b.id as team2_id', 'a.name as team1', 'b.name as team2', 'b.flag as flag2', 'a.flag as flag1', 'c.name as winner', 's.id as stat_id')
		->where('matchs.id',$id)->first();
		
		return $result;
	}

	/*-------------------------------------------Bet-----------------------------------------*/

	function make_bet(){
		$result = DB::table('matchs')->get(); 
		return $result;		
	}

	function delete_bets($id){
		$result = DB::table('bets')->where('id', $id)->delete();
		return $result;	
	} 
	
	function bet_status($id,$data){
		$result = DB::table('bets')->where('id',$id)->update($data);
		return $result;		
	}

	/*------------------------------------------Admin Cms-----------------------------------*/
	function admin_cms(){
		$result = DB::table('games')
		->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
      	->select('g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','games.id')
		->where('status','1')->get(); 
		//echo '<pre>';print_r($result);die();
		return $result;		
	}

	/*-----------------------------------------Game Plan------------------------------------*/ 
	
	function gameplan($search){
		if(empty($search)){
			$gameplan = DB::table('games_plan')->paginate(10)->withQueryString();
			
		}else{
			$gameplan = DB::table('games_plan')->where('max_plan', 'Like', '%' .$search. '%')->orwhere('X', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		return $gameplan;
		
	}

	function add_gameplan(){
		$result = DB::table('games_plan')->get(); 
		return $result;		
	}

	function edit_gamplan($id){
		$result=DB::table('games_plan')->where('id',$id)->first();
		return $result;
	}

	function editgameplansubmit($data,$id){
		$result=DB::table('games_plan')->where('id',$id)->update($data);
		return $result;
	}

	function delete_gamplan($id){
		$result = DB::table('games_plan')->where('id', $id)->delete();
		return $result;	
	}
	
	 /******************************************Manage Game type******************************************** */
	 function boxing_mma($search){
		if(empty($search)){
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Booxing MMA')
			->paginate(10)->withQueryString();
		}else{
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Booxing MMA')
			->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venu', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		
		return $results;
	}
	
	function soccer_leauge($search){
		if(empty($search)){
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Soccer League')
			->paginate(10)->withQueryString();
		}else{
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Soccer League')
			->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venu', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		
		return $results;
	} 
	
	
	function american_football($search){
		if(empty($search)){
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','American Football')
			->paginate(10)->withQueryString();
		}else{
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','American Football')
			->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venu', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		
		return $results;
	} 

	function hockey($search){
		if(empty($search)){
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Hockey')
			->paginate(10)->withQueryString();
		}else{
			$results = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->leftjoin('bets as b1', '', '=', 'b1.id')
			->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
			->where('g1.game_type','=','Hockey')
			->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venu', 'Like', '%' .$search. '%')->paginate(10)->withQueryString();
		}
		
		return $results;
	}

	// Asif code starts
	function get_transactions($search){
		if(empty($search)){
			$results = CryptoTransaction::paginate(10);
		}else{
			$results = CryptoTransaction::where('sending_wallet_address', 'Like', '%' .$search. '%')
			->orwhere('receiving_wallet_address', 'Like', '%' .$search. '%')->paginate(10);
		}
		return $results;
	}	
	// Asif code ends

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWallet extends Model
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    protected $fillable = [ 'sup_user_id', 'wallet_address' ];

    public function supUser()
    {
        return $this->belongsTo(SupUser::class);
    }
}
<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

//class Mbfcustomer extends Model
class AllGames extends Model
{
    protected $fillable = [
       'team1', 'team2','game_date','game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status',
    ];
	protected $table = 'games';	
	protected $primaryKey = 'id';
   
}
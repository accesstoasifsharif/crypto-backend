<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

//class Mbfcustomer extends Model
class UserBetting extends Model
{
    protected $fillable = [

    'team1', 'team2','betteam', 'user_id', 'betAmount','matchid',

    ];

	protected $table = 'bets';
	
	protected $primaryKey = 'id';
	
	
    
}
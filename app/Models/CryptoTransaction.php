<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CryptoTransaction extends Model
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    protected $fillable = [ 'user_id', 'transaction_id', 'transaction_type', 'amount', 'currency', 'sending_wallet_address', 'receiving_wallet_address', 'reference_id' ];


    protected $appends = [
        'formatted_amout', 'transaction_type_name', 'formatted_date', 'currency_name'
    ];
    
    public function getTransactionTypeNameAttribute()
    {
        switch ($this->transaction_type) {
            case 1:
                return 'Sale';
                break;
            case 2:
                return 'Purchase';
                break;
            case 3:
                return 'Bet';
                break;
            case 4:
                return 'Bet Reward';
                break;
            default:
                return 'Bet';
                break;
        }
    }

    public function getFormattedAmoutAttribute()
    {
        if ($this->transaction_type == 3) {
            return '<span class="text-danger"> -' . $this->amount . '</span>';
        }
        else if ($this->transaction_type == 4) {
            return '<span class="text-success"> +' . $this->amount . '</span>';
        }
        else
        {
            return '<span>' . $this->amount . '</span>';
        }
    }

    public function getFormattedDateAttribute()
    {
        return $this->created_at->format('M-d-Y (h:mA)');
    }

    public function getCurrencyNameAttribute()
    {
        if ($this->currency == 1) {
            return 'Solana';
        }
        if ($this->currency == 2) {
            return 'BetCoin';
        }
    }
}
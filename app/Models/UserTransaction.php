<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

//class Mbfcustomer extends Model
class UserTransaction extends Model
{
    protected $fillable = [

    'user_id','amount','via','too','frm','trx','status',

    ];
    protected $table = 'transactions';
	protected $primaryKey = 'id';
 
}
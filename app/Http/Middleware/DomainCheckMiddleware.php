<?php

namespace App\Http\Middleware;

use Closure;

class DomainCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	
    public function handle($request, Closure $next)
    {
        $allowedHosts = explode(',', env('ALLOWED_DOMAINS'));

        $requestHost = parse_url($request->headers->get('origin'),  PHP_URL_HOST);
		//return $requestHost; 
		if(in_array($request->getClientIp(), $allowedHosts)) return $next($request);
        if(!app()->runningUnitTests()) {
            if(!\in_array($requestHost, $allowedHosts, false)) {
                $requestInfo = [
                    'host' => $requestHost,
                    'ip' => $request->getClientIp(),
                    'url' => $request->getRequestUri(),
                    'agent' => $request->header('User-Agent'),
                ];
                //event(new UnauthorizedAccess($requestInfo));
				//  $data = $requestInfo;
                // //echo $request->getClientIp();
				// return response()->json([
				// 	'requestHost' => $data,
				// 	'allowedHosts' => $allowedHosts,
				// 	'host' => $request->getHost()
				// ]);
				//Log::warning('access_from_unauthorized_domain_' . date('Y-m-d_H:i:s'), $data);
                //throw new SuspiciousOperationException('This host is not allowed');
				return response()->json([
					'message_type' => 'error',
					'message' => 'This host is unauthorized'
				]);
			}
        }

        return $next($request);
    }
	
	
}

<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserWallet;
use App\Models\SupUser;
use App\Models\CryptoSwapLog;
use App\Models\CryptoTransaction;
use Config;

class CryptoController extends Controller
{
    public function __construct() {
        Config::set('jwt.user', 'App\Models\SupUser'); 
        Config::set('auth.providers.users.model', \App\Models\SupUser::class);
        Config::set('jwt.ttl', 60 * 24);
        $this->guard = "api_sup_user";
        $this->middleware('auth:api');
    }

	// user wallets code starts here
	public function save_user_crypto_wallets(Request $request)
	{
		$sup_user_id = $request->sup_user_id;
		$wallet_address = $request->wallet_address;

		$oldUserWallet = UserWallet::where('sup_user_id', $sup_user_id)->where('wallet_address', $wallet_address)->first();
		if (!isset($oldUserWallet))
		{
	        $newUserWallet = UserWallet::create([
	            'sup_user_id' => $request->sup_user_id,
	            'wallet_address' => $request->wallet_address,
	        ]);
	        return $newUserWallet;
		}
		return $oldUserWallet;
	}
	public function get_user_crypto_wallets($sup_users_id)
	{
		$userWallets = UserWallet::where('sup_user_id', $sup_users_id)->with('supUser')->get();
		return $userWallets;
	}

	// swap log code starts here
	public function save_crypto_swap_logs(Request $request)
	{
        $newCryptoSwapLog = CryptoSwapLog::create([
            'sup_user_id' => $request->sup_user_id,
            'amount_from' => $request->amount_from,
            'amount_to' => $request->amount_to,
            'currency_from' => (isset($request->currency_from)) ? $request->currency_from : config('opsports.currency_type.sol'),
            'currency_to' => (isset($request->currency_to)) ? $request->currency_to : config('opsports.currency_type.betc'),
            'exchange_rate' => (isset($request->exchange_rate)) ? $request->exchange_rate : 0,
        ]);

        return $newCryptoSwapLog;
	}
	public function get_crypto_swap_logs($sup_users_id, $currency_type_id)
	{
		$cryptoSwapLogs = CryptoSwapLog::where('sup_user_id', $sup_users_id)->where('currency_from', $currency_type_id)->get();
		return $cryptoSwapLogs;
	}

	// transactions code starts here
	public function save_crypto_transaction(Request $request)
	{
		$user = auth($this->guard)->user();
		$oldCryptoTransaction = CryptoTransaction::where('transaction_id', $request->transaction_id)->first();

		if (!isset($oldCryptoTransaction)) {
	        $newCryptoTransaction = CryptoTransaction::create([
		        'transaction_id' => $request->transaction_id,
		        'transaction_type' => $request->transaction_type,
				'sending_wallet_address' => $request->sending_wallet_address, 
				'receiving_wallet_address' => $request->receiving_wallet_address, 
				'amount' => $request->amount, 
				'currency' => $request->currency, 
				'user_id' => $user['id'],
				'reference_id' => (isset($request->reference_id)) ? $request->reference_id : null
			]);
	        return $newCryptoTransaction;
		}
		return $oldCryptoTransaction;
	}
	public function get_user_all_crypto_transactions()
	{
		$user = auth($this->guard)->user();
		$user_wallets = $this->get_user_crypto_wallets($user['id']);
		// return $user_wallets;
		$userAllCryptoTransactions = [];

		foreach ($user_wallets as $user_wallet)
		{
			$sending_wallet_address = $user_wallet->wallet_address;
			
			$cryptoBetTransactions = CryptoTransaction::where('user_id', $user['id'])->where('transaction_type', config('opsports.transaction_type.bet'))->where(function ($query) use ($sending_wallet_address) {
				$query->where('sending_wallet_address',$sending_wallet_address)
				->get();
			})
			->get();
			foreach ($cryptoBetTransactions as $cryptoBetTransaction) {
				array_push($userAllCryptoTransactions, $cryptoBetTransaction);
			}
		}

		foreach ($user_wallets as $user_wallet)
		{
			$receiving_wallet_address = $user_wallet->wallet_address;

			$cryptoBetRewardTransactions = CryptoTransaction::where('user_id', $user['id'])->where('transaction_type', config('opsports.transaction_type.bet_reward'))->where(function ($query) use ( $receiving_wallet_address ) {
				$query->where('receiving_wallet_address',$receiving_wallet_address)
				->get();
			})
			->get();
			foreach ($cryptoBetRewardTransactions as $cryptoBetRewardTransaction) {
				array_push($userAllCryptoTransactions, $cryptoBetRewardTransaction);
			}
		}

		$uniqueUserAllCryptoTransactions = [];
		$uniquekeys = array();
		foreach ($userAllCryptoTransactions as $key => $userCryptoTransaction) {
			if (!in_array($userCryptoTransaction->id, $uniquekeys)) {
				$uniquekeys[] = $userCryptoTransaction->id;
				$uniqueUserAllCryptoTransactions[] = $userCryptoTransaction;
			}
		}

		return $uniqueUserAllCryptoTransactions;
	}

	public function get_user_stats()
	{
		$userAllCryptoTransactions = $this->get_user_all_crypto_transactions();
		
		$total_investment = 0;
		$total_earned = 0;
		foreach ($userAllCryptoTransactions as $userCryptoTransaction) {
			if ($userCryptoTransaction->transaction_type == 3)
			{
				$total_investment += $userCryptoTransaction->amount;
			}
			if ($userCryptoTransaction->transaction_type == 4)
			{
				$total_earned += $userCryptoTransaction->amount;
			}
		}
        $data_set = [
            'total_investment' => $total_investment,
            'total_earned' => $total_earned
        ];

        return $data_set;
	}

}
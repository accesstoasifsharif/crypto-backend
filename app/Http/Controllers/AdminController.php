<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\AdminModel;
use App\Models\CryptoTransaction;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class AdminController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	public function __construct(AdminModel $AdminModel) {
        $this->AdminModel = $AdminModel;	
    }
	
	public function index(){
		if(empty(Session::get('ad_user_id'))){
			return view('admin/login/login');
		}else{
			return redirect('/home');	
		}
		
	}
	
	public function  dologin(Request $request){

		$user_email = $request->user_email;		
		$user_password =$request->user_password;
		$data = $this->AdminModel->userlogin($user_email,$user_password);
		if(empty($data)){
				$admindata = $this->AdminModel->adminlogin($user_email,$user_password);
				if(empty($admindata)){
				return back()->with('error','Invalid Login details');
			}else{
				if (Hash::check($user_password, $admindata->password)){
				Session::put('ad_user_id',$admindata->id);
				Session::put('ad_user_type',$admindata->user_type);
				return redirect('/home');
				}else{
					return back()->with('error','Invalid Login details');
				}
			}
			
		}else{
		   Session::put('ad_user_id',$data->user_id);
		   Session::put('ad_user_type',$data->user_type);
		   return redirect('/home');	
		}
    }
	/* public function  adminlogin(Request $request){
		$user_email = $request->user_email;		
		$user_password =$request->user_password;
		$data = $this->AdminModel->adminlogin($user_email,$user_password);
		if(empty($data)){
			return back()->with('error','Invalid Login details');
		}else{
		   Session::put('ad_user_id',$data->id);
		   Session::put('ad_user_type',$data->user_type);
		   return redirect('/home');	
		}
    } */
	
	public function home($id=null){
		$admin = DB::table('sup_user')->where('user_type','=','2')->count();
		$bet = DB::table('bets')->count();
		//$matchs = DB::table('matchs')->count();
		$matchs = 0;
		$games = DB::table('games')->count();
		$users = DB::table('sup_users')->count();
		return view('admin/home/index',compact('admin','bet','matchs','games','users'));
	}
	
	public static function getuserdetials($id){
		
		$results= DB::table('sup_adminmaster')
			->where('user_id','=',$id)
			->where('user_type','=',Session::get('ad_user_type'))
			->first();
		
		return $results;	
	}

	public static function getadmindetials($id){
	
		$results= DB::table('sup_user')
		->where('id','=',$id)
		->where('user_type','=',Session::get('ad_user_type'))
		->first();
		return $results;	
	}
	
	public function logout()
    {    
		 Session::flush();
         return redirect('/admin');		 
    }
	
	/* --------------------------- Manage  Admin ------------- ---------------*/
	public function admin_list(Request $request,$id=null){
		
		$search='';
		$search=$request->search;
		$response = $this->AdminModel->admin_list($search);
		
		return view('admin/home/manage_admin/index',compact('response','search'));
	}

	public function add_admin(){
		$rid='';
		$response='';
		$addadmin = $this->AdminModel->add_admin();
		return view('admin/home/manage_admin/create',compact('rid','addadmin','response'));
	}

	public function admin_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->admin_status($id,$data);
		
	}

	

	public function edit_admin($id){
		$rid='';
		$response='';
		if(!empty($id)){
			$response= $this->AdminModel->admin_edit($id);
		}
		return view('admin/home/manage_admin/create',compact('rid','response'));
	
	}

	public function submit_admin(Request $request){
		$ad_user_id = Session::get('ad_user_id');
		$sup_admin_detail = $this->AdminModel->getRowById("sup_adminmaster", array("user_id"=>$ad_user_id));
		$rid='';
		$name=$request->name;
		$email=$request->email;
		$password=$request->password;
		$user_type='2';
		if(empty($request->id)){
			$admin_row = $this->AdminModel->admin_row($email);
			if(empty($admin_row)){
				$data = [
				"name"=>$name,
				"email"=>$email,
				"password"=>Hash::make($password),
				"user_type"=>2,
			];
			
			$this->AdminModel->insert('sup_user', $data);
			 //$to = $email;
			 //$subject = "Admin details";
			 
			 //$message = "<b>Below is the details of admin login</b>";
			 //$message .= "<p> Username : ".$email."</p>";
			 //$message .= "<p> Password : ".$password."</p>";
			 
			
			// $header = "MIME-Version: 1.0\r\n";
			// $header .= "Content-type: text/html\r\n";
			 
			 //$retval = mail ($to,$subject,$message,$header);
				$email_template_admin = $this->AdminModel->getRowById('emailtemplate',array('name'=>'admin_create_by_superadmin_adminnotification'));
				if($email_template_admin){
					$html=str_replace(array('{name}','{email}','{password}'), array($request->name,$request->email,$request->password), $email_template_admin->body);
					$this->sendPMEmail($request->email,$email_template_admin->title,$html);
				}

				$email_template_superadmin = $this->AdminModel->getRowById('emailtemplate',array('name'=>'admin_create_by_superadmin_superadminnotification'));
				if($email_template_admin){
					$html=str_replace(array('{name}','{admin_name}','{email}','{password}'), array($sup_admin_detail->user_firstname,$request->name,$request->email,$request->password), $email_template_superadmin->body);
					$this->sendPMEmail($sup_admin_detail->user_email,$email_template_superadmin->title,$html);
				}
				//echo "<pre>"; print_r([$email_template_admin,$email_template_superadmin]); die;
				

				return redirect('/admin/manage')->with('success','Admin details added successfully');
			}else{
				return redirect()->back()->with('error','Admin already exist');
			}
		}else{
			$id=$request->id;
			$data = [
				"name"=>$name,
				"email"=>$email,
				
			];
			$admin_row = $this->AdminModel->admin_row($email);
			if(!empty($admin_row) && $admin_row->id!=$id){
				return redirect()->back()->with('error','Admin already exist ');
					
			}else{
				$this->AdminModel->update_table('sup_user',$id, $data);
				return redirect('/admin/manage')->with('success','Admin details updated successfully');
			}
		}
	
	}

	public function admin_profile(Request $request , $id){
		$response = $this->AdminModel->admin_profile($id);
		//echo "<pre>"; print_r($response); die;
		return view('admin/home/manage_admin/aboutadmin',compact('response'));
	}

	public function changepassword()
    {
        return view('admin/home/manage_admin/changepassword');
    } 

	public function store_changepassword(Request $request)
    {  //print_r($_POST);die();
        $request->validate([
            'current_password' => ['required'],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
		
		$password=$request->current_password;
		$data = [
			"password"=>Hash::make($request->new_password)
			
		];
		$passwordid=Session::get('ad_user_id');
		$hashedPassword=DB::table('sup_user')->where('id','=',$passwordid)->first();
		//echo $hashedPassword->password;die();
		if ($hashedPassword && Hash::check($password, $hashedPassword->password)) {	
			$this->AdminModel->update_table('sup_user',$passwordid,$data);
			return redirect('/admin/changepassword')->with('success', 'Password change successfully.');
		}else{

			return redirect()->back()->with('error', 'Wrong current password.');

		}
    }

	public function delete_admin(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_admin($id);
	 return redirect('admin/manage')->with('error','Admin Deleted successfully');
	}


	/*-------------------------------------Teams---------------------------------------------------*/ 
	public function team_list(Request $request,$game_type=null){
		$search='';
		if($request->search) $search=$request->search;
		if($game_type) $response = $this->AdminModel->team_list($search,$game_type);
		else $response = $this->AdminModel->team_list($search);
		
		return view('admin/home/teams/index',compact('response','search','game_type'));
	}

	public function addteams($game_type=null){
		$id='';
		$response='';
		$addteams = $this->AdminModel->addteams();
		$game_types_list = $this->AdminModel->getAllData('games_type');
		// echo "<pre>"; print_r($game_types_list);
		// var_dump($game_type); die;
		return view('admin/home/teams/createteams',compact('id','addteams','response','game_types_list','game_type'));
	}

	public function submit_addteams(Request $request,$game_type=null)
	{
		/* $data = $request->post();
		//echo "<pre>"; print_r($data); die;
        if($request->file('flag')){
            $file= $request->file('flag');
            $filename= date('YmdHi').$file->getClientOriginalName();
            //$file-> move(public_path('public/uploads/Teams_images'), $filename);
            $file-> move(public_path('uploads/Teams_images'), $filename);
            $data['flag']= $filename;
        } */
		//$data= new Postimage();
		$data = $request->post();
        if($request->hasFile('flag')){
			$image = $request->file('flag');
			$filename    = date('YmdHi').$image->getClientOriginalName();
			$image_resize = Image::make($image->getRealPath());            
			$image_resize->resize(442, 442, function ($constraint) {
						$constraint->aspectRatio();
			});
			$image_resize->resizeCanvas(442, 442, 'center', false, array(255, 255, 255, 0));
			// $tets = $image_resize->save(public_path('uploads/Teams_images/' .$filename));
			$tets = $image_resize->save(storage_path('app/public/Teams_images/' .$filename));
			$data['flag'] = $filename;
        }

       //$data->save();
	   $id =$request->id;
	   unset($data['_token']);
	   unset($data['id']);
 
	  if(empty($id)){
		$insert_id = $this->AdminModel->insert('teams',$data);
		if($insert_id){
			if($game_type) return redirect('admin/teams_list/'.$game_type)->with('success', 'Team added successfully');
			else return redirect('admin/teams_list')->with('success', 'Team added successfully');
		}else{
			return back()->with('error','Unable to add ');
		}
		}else{
			$this -> AdminModel-> editteams($data,$id);
			if($game_type) return redirect('admin/teams_list/'.$game_type)->with('success', 'TEAM Edited successfully');
			else return redirect('admin/teams_list')->with('success', 'TEAM Edited successfully');
		}
	}

	/* public function edit_addteams($id){
		$response='';
		if(!empty($id)){
			$response= $this->AdminModel->edit_addteams($id);
		}
		return view('admin/home/teams/createteams',compact('id','response'));
	
	}  */

	public function edit_addteams($id,$game_type=null){
		$response='';
		/* $result = DB::table('teams')->get();
		$response= $this->AdminModel->gamestype(); */
		if(!empty($id)){
			$response= $this->AdminModel->edit_addteams($id);
			
		}
		$game_types_list = $this->AdminModel->getAllData('games_type');
		//echo "<pre>";
		//print_r($response1); die();
		return view('admin/home/teams/createteams',compact('id','response','game_types_list','game_type'));
	
	} 

	public function delete_addteams(Request $request,$game_type=null){
		$id = $request->id;
		$response = $this->AdminModel->delete_addteams($id);
	 	if($game_type) return redirect('admin/teams_list/'.$game_type)->with('error','Team Deleted successfully');
	 	else return redirect('admin/teams_list')->with('error','Team Deleted successfully');
	}


	/*------------------------------------Game Controller------------------------------------------------*/
	public function gamestype_add(){
		$response='';
		$gamestype_add = $this->AdminModel->gamestype_add();
		return view('admin/home/game_type/create_game_type',compact('gamestype_add','response'));
	}
	
	public function gamestype(Request $request,$id=null)
    {
		$search='';
		$search=$request->search;
    	$response = $this->AdminModel->gamestype($search);
		
		return view('admin/home/game_type/index',compact('response','search')); 
    }

	public function edit_gamestype($id){
		$response='';
		/* $result = DB::table('teams')->get();
		$response= $this->AdminModel->gamestype(); */
		if(!empty($id)){
			$response= $this->AdminModel->edit_gamestype($id);
			
		}
		//echo "<pre>";
		//print_r($response1); die();
		return view('admin/home/game_type/create_game_type',compact('id','response'));
	
	} 

	public function delete_gamestype(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_gamestype($id);
	 return redirect('admin/gamestype')->with('error','GameType Deleted successfully');
	}

	public function gamestype_store(Request $request){
		$data = $request->post();
		
        if($request->hasFile('img')){
			/*
            $file= $request->file('img');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('uploads/games_type'), $filename);
            $data['img']= $filename;
			*/
			$image = $request->file('img');
			$filename    = date('YmdHi').$image->getClientOriginalName();
			$image_resize = Image::make($image->getRealPath());            
			$image_resize->resize(420, 420, function ($constraint) {
						$constraint->aspectRatio();
			});
			$image_resize->resizeCanvas(420, 420, 'center', false, array(255, 255, 255, 0));
			// $tets = $image_resize->save(public_path('uploads/games_type/' .$filename));
			$tets = $image_resize->save(storage_path('app/public/games_type/' .$filename));
			$data['img'] = $filename;
        
        }
		 
		$id=$request->id;
		unset($data['_token']);
		unset($data['id']);
		if(empty($id)){
		$insert_id = $this->AdminModel->insert('games_type',$data);
		if($insert_id){
			return redirect('admin/gamestype')->with('success', 'Games Type added successfully');
		}else{
			return back()->with('error','Unable to add games');
		}
		}else{
			$this->AdminModel->editGamestype($data,$id);
			return redirect('admin/gamestype')->with('success', 'Games Type Edited successfully');
		}
	}

	public function games_list(Request $request,$id=null)
    {
		$search='';
		if($request->search) $search=$request->search;
    	$games = $this->AdminModel->games_list($search);
		//echo "<pre>"; print_r($games); die;
		return view('admin/home/games/game_index',compact('games','search'));
       
    //   $games = DB::table('games')
    //   ->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
    //   ->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
	//   ->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
    //   ->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
    //   ->get();

     // return view('admin/home/games/game_index')->with('games',$games);
    }
	
	public function game_detail(Request $request,$id)
    {
		
    	$games = $this->AdminModel->getRowById("games",array("id"=>$id));
		
		if($games){
            $games_type = $this->AdminModel->getRowById("games_type",array("id"=>$games->game_type));
            $team1detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team1_id));
            $team2detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team2_id));
            $games_change_history = $this->AdminModel->getResultById("games_change_history",array("game_id"=>$id));
            $games->game_type_name = $games_type->game_type;
            $games->game_type_details = $games_type;
            //$bet->games = $games;
            $games->team1detail = $team1detail;
            $games->team2detail = $team2detail;
            $games->games_change_history = $games_change_history;
            // if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
            // else $bet->betteamdetail = $team2detail;
        } 
		//echo '<pre>'; print_r($games); die;
      	return view('admin/home/games/game_detail',compact('games','id'));
    }
	
	public function teams_by_game_type(Request $request){
		$teams = $this->AdminModel->getResultById("teams",array("game_type"=>$request->game_type));
		echo json_encode($teams);
	}
	/* public function game_show($id)
    {	
		$res_id=$id; 
		$game='';
		$stadiums='';
		$team='';
		$players1='';
		$players2='';

		$game=DB::table('opsgames')->find($id);
		$stadiums = DB::table('stadium')->get();
		$team = DB::table('opsgames')->get();
		//$team1 = DB::table('opsgames')->find($game->team1_id);
		//$team2 = DB::table('opsgames')->find($game->team2_id);
		//$players1 = DB::table('player')->whereIn("team_id", $team1)->get();
		//$players2 = DB::table('player')->whereIn("team_id", $team2)->get();
   
		return view('admin/home/games/game_show', compact('res_id','game', 'stadiums', 'team', 'players1', 'players2' ));
		
    } */

	public function gmae_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->gmae_status($id,$data);
		
	}

	public function add_gamme(Request $request){
		
		$data = $request->post();
		//echo "<pre>"; print_r($data); die;
		$response1='';
		$search=$request->search;
		$result = DB::table('teams')->get();
		$addgame = $this->AdminModel->add_gamme();
		$response = $this->AdminModel->gamestype($search);
		//$gameplan = $this->AdminModel->add_gameplan();
		//echo "<pre>"; print_r($result); die;
		return view('admin/home/games/creategames',compact('addgame','response1','response','result'));
	}

	public function store_games(Request $request){
		$data = $request->post();
		
		//echo "<pre>"; print_r($data); die;
		$id=$request->id;
		unset($data['_token']);
		unset($data['id']);
		unset($data['bet_amount']);

		// if(empty($data['spread1_t1'])) unset($data['spread1_t1']);
		// if(empty($data['spread2_t1'])) unset($data['spread2_t1']);
		// if(empty($data['moneyline_t1'])) unset($data['moneyline_t1']);
		// if(empty($data['total1_t1'])) unset($data['total1_t1']);
		// if(empty($data['total2_t1'])) unset($data['total2_t1']);
		// if(empty($data['total1_type_t1'])) unset($data['total1_type_t1']);

		// if(empty($data['spread1_t2'])) unset($data['spread1_t2']);
		// if(empty($data['spread2_t2'])) unset($data['spread2_t2']);
		// if(empty($data['moneyline_t2'])) unset($data['moneyline_t2']);
		// if(empty($data['total1_t2'])) unset($data['total1_t2']);
		// if(empty($data['total2_t2'])) unset($data['total2_t2']);
		// if(empty($data['total1_type_t2'])) unset($data['total1_type_t2']);
		
		if(empty($id)){
			//echo "<pre>"; print_r($data); die;
			if(($data['spread1_t1'] > 0 && $data['spread1_t2'] < 0) || ($data['spread1_t1'] < 0 && $data['spread1_t2'] > 0)){
				$insert_id = $this->AdminModel->insert('games',$data);
				if($insert_id){
					return redirect('admin/games')->with('success', 'Games added successfully');
				}else{
					return back()->with('error','Unable to add games');
				}
			}else{
				return back()->with('error','Please add respective positive or negative value for spread1_t1, spread1_t2');
			}
		}else{
			if(($data['spread1_t1'] > 0 && $data['spread1_t2'] < 0) || ($data['spread1_t1'] < 0 && $data['spread1_t2'] > 0)){
				if(isset($data['timeline']) && $data['timeline'] == '2'){
					$gamedetail = $this->AdminModel->getRowById("games",array("id"=>$id));
					$betdetails = $this->AdminModel->getResultById("bets",array("game_id"=>$id));
					foreach($betdetails as $bet){
						$bet->game_result = $gamedetail->result;
						$bet->bet_result = 'No Result';
						/* $bet->win = 0;
						//if($bet->bet_team == $gamedetail->result)  $bet_result = 'No Result';
						if(in_array($bet->bet_box,["1","4"])){
							if($bet->game_result == '0'){
								$bet->bet_result = 'No Result';
							}
							elseif($bet->game_result == '3'){
								$bet->bet_result = 'Draw';
							}
							else{
								if(($bet->games_change_history_current->spread1_t1 > 0 && $bet->games_change_history_current->spread1_t2 < 0) || ($bet->games_change_history_current->spread1_t1 < 0 && $bet->games_change_history_current->spread1_t2 > 0)){
										
				
									if($bet->bet_box == '1') $spread = $bet->games_change_history_current->spread1_t1;
									else $spread = $bet->games_change_history_current->spread1_t2;
			
									if($spread > 0) $bet_on = 'Underdog';
									else $bet_on = 'Favorite';
			
									if($bet->games_change_history_last->spread1_t1 > 0 && $bet->games_change_history_last->spread1_t2 < 0){
										$Underdog = $bet->games_change_history_last->spread1_t1;
										$Favorite = $bet->games_change_history_last->spread1_t2;
									}else{
										$Underdog = $bet->games_change_history_last->spread1_t2;
										$Favorite = $bet->games_change_history_last->spread1_t1;
									}
			
									if($bet_on == 'Underdog'){
										if($Underdog + $spread > $Favorite) $bet->bet_result = 'Won';
										elseif($Underdog + $spread < $Favorite) $bet->bet_result = 'Lost';
										else $bet->bet_result = 'Draw';
									}else{
										if($Favorite - $spread > $Underdog) $bet->bet_result = 'Won';
										elseif($Favorite - $spread < $Underdog) $bet->bet_result = 'Lost';
										else $bet->bet_result = 'Draw';
									}
			
			
			
								}else{
									$bet->bet_result = 'Lost';
								}
								
							}
						}else{
							if($bet->game_result == '0'){
								$bet->bet_result = 'No Result';
								$bet->win = 0;
							}
							elseif($bet->game_result == '3'){
								$bet->bet_result = 'Draw';
								$bet->win = 3;
							}
							else{
								if($bet->game_result == $bet->bet_team){
									$bet->bet_result = 'Won';
									$bet->win = 2;
								}else{
									$bet->bet_result = 'Lost';
									$bet->win = 1;
								}
							}
				
				
						}

						$this->AdminModel->update_row("bets",array("win"=>$bet->win),array("id"=>$bet->id)); */
					}
				}
				$this->AdminModel->editGames($data,$id);
				return redirect('admin/games')->with('success', 'Games Edited successfully');	
			}else{
				return back()->with('error','Please add respective positive or negative value for spread1_t1, spread1_t2');
			}
		}
	}

	 public function edit_gmaes(Request $request,$id){
		$response='';
		$search=$request->search;
		$result = DB::table('teams')->get();
		$response= $this->AdminModel->gamestype($search);
		//$gameplan = $this->AdminModel->add_gameplan();
		if(!empty($id)){
			$response1= $this->AdminModel->getGames($id);
			$games_change_history = $this->AdminModel->getResultById("games_change_history",array("game_id"=>$id));
			$response1->games_change_history = $games_change_history;
			// $bets = DB::table('bets')
			// //->select('bets.*')
			// ->where(array("game_id"=>$id))
			// ->get()
			// ->groupBy('spread1_t1','spread2_t1','moneyline_t1','total1_t1','total2_t1','total1_type_t1','spread1_t2','spread2_t2','moneyline_t2','total1_t2','total2_t2','total1_type_t2');
		}
		// echo "<pre>";
		// print_r($bets); die();
		return view('admin/home/games/creategames',compact('id','response1','response','result'));
	
	} 

	public function delete_games(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_games($id);
	 return redirect('admin/games')->with('error','Game Deleted successfully');
	}




	/*----------------------------------------------Game By GameType Controller-----------------------------------------------*/
	
	public function games_by_type(){
		// $Boxing = DB::table('games')->where('game_type','=','1')->count();
		// $Soccer_League = DB::table('games')->where('game_type','=','2')->count();
		// $American_Football= DB::table('games')->where('game_type','=','3')->count();
		// $Hockey = DB::table('games')->where('game_type','=','4')->count();
		$games_type = $this->AdminModel->getAllData('games_type');
		foreach($games_type as &$g_type){
			$count_games_by_game_type = $this->AdminModel->tableWhereCount('games',array("game_type"=>$g_type->id));
			$g_type->count_games = $count_games_by_game_type;
		}
		//echo "<pre>"; print_r($games_type); die;
		return view('admin/home/games_by_type/games_by_game_type',compact('games_type'));
	}
	
	public function games_list_by_type(Request $request,$game_type=null)
    {
		$search='';
		if($request->search) $search=$request->search;
    	$games = $this->AdminModel->games_list($search, $game_type);
		//echo "<pre>"; print_r($games); die;
		return view('admin/home/games_by_type/game_index',compact('games','search','game_type'));
       
    //   $games = DB::table('games')
    //   ->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
    //   ->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
	//   ->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
    //   ->select('t1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','loosing_percentage_team1','loosing_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
    //   ->get();

     // return view('admin/home/games/game_index')->with('games',$games);
    }
	
	public function games_list_by_type_game_detail(Request $request,$id,$game_type=null)
    {
		
    	$games = $this->AdminModel->getRowById("games",array("id"=>$id));
		
		if($games){
            $games_type = $this->AdminModel->getRowById("games_type",array("id"=>$games->game_type));
            $team1detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team1_id));
            $team2detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team2_id));
            $games_change_history = $this->AdminModel->getResultById("games_change_history",array("game_id"=>$id));
			$games->game_type_name = $games_type->game_type;
            $games->game_type_details = $games_type;
            //$bet->games = $games;
            $games->team1detail = $team1detail;
            $games->team2detail = $team2detail;
			$games->games_change_history = $games_change_history;
            // if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
            // else $bet->betteamdetail = $team2detail;
        } 
		//echo '<pre>'; print_r($games); die;
      	return view('admin/home/games_by_type/game_detail',compact('games','id'));
    }

	public function gmae_status_by_type(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->gmae_status($id,$data);
		
	}

	public function add_gamme_by_type(Request $request, $game_type=null){
		
		$data = $request->post();
		//echo "<pre>"; print_r($data); die;
		$response1='';
		$search='';
		if($request->search) $search=$request->search;
		//$result = DB::table('teams')->get();
		$result = $this->AdminModel->getResultById("teams",array("game_type"=>$game_type));
		//$addgame = $this->AdminModel->add_gamme();
		//$response = $this->AdminModel->gamestype($search);
		$response = $this->AdminModel->getResultById("games_type",array("id"=>$game_type));
		//$gameplan = $this->AdminModel->add_gameplan();
		//echo "<pre>"; print_r($result); die;
		return view('admin/home/games_by_type/creategames',compact('response1','response','result','game_type'));
	}

	public function store_games_by_type(Request $request, $game_type=null){
		$data = $request->post();
		//echo "<pre>"; print_r($data); die;
		//echo "<pre>"; print_r($data); die;
		$id=$request->id;
		unset($data['_token']);
		unset($data['id']);
		unset($data['bet_amount']);
		// if(empty($data['spread1_t1'])) unset($data['spread1_t1']);
		// if(empty($data['spread2_t1'])) unset($data['spread2_t1']);
		// if(empty($data['moneyline_t1'])) unset($data['moneyline_t1']);
		// if(empty($data['total1_t1'])) unset($data['total1_t1']);
		// if(empty($data['total2_t1'])) unset($data['total2_t1']);
		// if(empty($data['total1_type_t1'])) unset($data['total1_type_t1']);

		// if(empty($data['spread1_t2'])) unset($data['spread1_t2']);
		// if(empty($data['spread2_t2'])) unset($data['spread2_t2']);
		// if(empty($data['moneyline_t2'])) unset($data['moneyline_t2']);
		// if(empty($data['total1_t2'])) unset($data['total1_t2']);
		// if(empty($data['total2_t2'])) unset($data['total2_t2']);
		// if(empty($data['total1_type_t2'])) unset($data['total1_type_t2']);

		if(empty($id)){
			//echo "<pre>"; print_r($data); die;
			
			if(($data['spread1_t1'] > 0 && $data['spread1_t2'] < 0) || ($data['spread1_t1'] < 0 && $data['spread1_t2'] > 0)){
				$insert_id = $this->AdminModel->insert('games',$data);
				if($insert_id){
					return redirect('admin/games_by_type/games/'.$game_type)->with('success', 'Games added successfully');
				}else{
					return back()->with('error','Unable to add games');
				}
			}else{
				return back()->with('error','Please add respective positive or negative value for spread1_t1, spread1_t2');
			}
		}else{
			if(($data['spread1_t1'] > 0 && $data['spread1_t2'] < 0) || ($data['spread1_t1'] < 0 && $data['spread1_t2'] > 0)){
				$this->AdminModel->editGames($data,$id);
				return redirect('admin/games_by_type/games/'.$game_type)->with('success', 'Games Edited successfully');
			}else{
				return back()->with('error','Please add respective positive or negative value for spread1_t1, spread1_t2');
			}
			
		}
	}

	 public function edit_gmaes_by_type(Request $request, $id, $game_type=null){
		$response='';
		$search=$request->search;
		$result = DB::table('teams')->get();
		$response= $this->AdminModel->gamestype($search);
		//$gameplan = $this->AdminModel->add_gameplan();
		if(!empty($id)){
			$response1= $this->AdminModel->getGames($id);
			$games_change_history = $this->AdminModel->getResultById("games_change_history",array("game_id"=>$id));
			$response1->games_change_history = $games_change_history;
		}
		// echo "<pre>";
		// print_r($response1); die();
		return view('admin/home/games_by_type/creategames',compact('id','response1','response','result','game_type'));
	
	} 

	public function delete_games_by_type(Request $request, $game_type=null){
		$id = $request->id;
		$response = $this->AdminModel->delete_games($id);
	 return redirect('admin/games_by_type/games/'.$game_type)->with('error','Game Deleted successfully');
	}






	
	
	
	/*----------------------------------------------Bet Controller-----------------------------------------------*/

	public function bet_index(Request $request,$game_id=null)
    {	
		$search='';
		if($request->search) $search=$request->search;
		if($game_id){
			if(empty($search)){
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();
			}else{
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')/* ->orwhere('t3.name', 'Like', '%' .$search. '%') */
				->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();	
			}
			//echo '<pre>'; print_r($bets); die;
		}else{
			if(empty($search)){
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				//->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();
			}else{
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')/* ->orwhere('t3.name', 'Like', '%' .$search. '%') */
				//->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();	
			}
			//echo '<pre>'; print_r($bets); die;
		}

		foreach($bets as &$bet){
			if($bet->bet == '1'){
				$betteamdetail = $this->AdminModel->getRowById('teams',array('id'=>$bet->team1_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}else{
				$betteamdetail = $this->AdminModel->getRowById('teams',array('id'=>$bet->team2_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}

			$games_change_history_last = $this->AdminModel->getLastRowById('games_change_history',array('game_id'=>$bet->game_id),"id","DESC");
			$bet->games_change_history_last = $games_change_history_last;
			$games_change_history_current = $this->AdminModel->getRowById('games_change_history',array('id'=>$bet->games_change_history_id));
			$bet->games_change_history_current = $games_change_history_current;
			/* $bet->bet_result = 'No Result';
			if(in_array($bet->bet_box,["1","4"])){
				if($bet->game_result == '0'){
					$bet->bet_result = 'No Result';
				}
				elseif($bet->game_result == '3'){
					$bet->bet_result = 'Draw';
				}
				else{
					if($bet->game_result == $bet->bet){
						$bet->bet_result = 'Won';
					}else{
						if(($bet->games_change_history_current->spread1_t1 > 0 && $bet->games_change_history_current->spread1_t2 < 0) || ($bet->games_change_history_current->spread1_t1 < 0 && $bet->games_change_history_current->spread1_t2 > 0)){
							

							if($bet->bet_box == '1') $spread = $bet->games_change_history_current->spread1_t1;
							else $spread = $bet->games_change_history_current->spread1_t2;

							if($spread > 0) $bet_on = 'Underdog';
							else $bet_on = 'Favorite';

							if($bet->games_change_history_last->spread1_t1 > 0 && $bet->games_change_history_last->spread1_t2 < 0){
								$Underdog = $bet->games_change_history_last->spread1_t1;
								$Favorite = $bet->games_change_history_last->spread1_t2;
							}else{
								$Underdog = $bet->games_change_history_last->spread1_t2;
								$Favorite = $bet->games_change_history_last->spread1_t1;
							}

							if($bet_on == 'Underdog'){
								if($Underdog + $spread > $Favorite) $bet->bet_result = 'Won';
								elseif($Underdog + $spread < $Favorite) $bet->bet_result = 'Lost';
								else $bet->bet_result = 'Draw';
							}else{
								if($Favorite - $spread > $Underdog) $bet->bet_result = 'Won';
								elseif($Favorite - $spread < $Underdog) $bet->bet_result = 'Lost';
								else $bet->bet_result = 'Draw';
							}



						}else{
							$bet->bet_result = 'Lost';
						}
					}
				}
			}else{
				if($bet->game_result == '0'){
					$bet->bet_result = 'No Result';
				}
				elseif($bet->game_result == '3'){
					$bet->bet_result = 'Draw';
				}
				else{
					if($bet->game_result == $bet->bet){
						$bet->bet_result = 'Won';
					}else{
						$bet->bet_result = 'Lost';
					}
				}


			} */

			if($bet->win == 0) $bet->bet_result = 'No Result';
			elseif($bet->win == 1) $bet->bet_result = 'Lost';
			else $bet->bet_result = 'Won';
		}
		
	 //->tosql();
	//echo '<pre>'; print_r($bets);die();
	 
      return view('admin/home/bets/allbets',compact('bets','game_id'));
    }

	public function bet_by_gameid_and_historyid_index(Request $request,$game_id=null, $games_change_history_id=null)
    {	
		$search='';
		if($request->search) $search=$request->search;
		if($game_id){
			if(empty($search)){
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where(array('bets.game_id'=>$game_id, 'bets.games_change_history_id'=>$games_change_history_id))
				->paginate(10)->withQueryString();
			}else{
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')/* ->orwhere('t3.name', 'Like', '%' .$search. '%') */
				->where(array('bets.game_id'=>$game_id, 'bets.games_change_history_id'=>$games_change_history_id))
				->paginate(10)->withQueryString();	
			}
			//echo '<pre>'; print_r($bets); die;
		}else{
			if(empty($search)){
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				//->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();
			}else{
				$bets = DB::table('bets')
				->join('games as g', 'bets.game_id', '=', 'g.id')
				->join('teams as t1', 'g.team1_id', '=', 't1.id')
				->join('teams as t2', 'g.team2_id', '=', 't2.id')
				->join('games_type as g1', 'g.game_type', '=', 'g1.id')
				//->join('teams as t3', 'bets.bet_team_id', '=', 't3.id')
				
				->select('bets.*', 't1.name as team1', 't2.name as team2', /* 't3.name as betteam', */ 'user_id', 'g.team1_id', 'g.team2_id','bet_team as bet', 'bet_amount as betAmount','game_date as matchDate', 'g1.game_type as game_type_name', 'g.result as game_result')
				->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')/* ->orwhere('t3.name', 'Like', '%' .$search. '%') */
				//->where('bets.game_id',$game_id)
				->paginate(10)->withQueryString();	
			}
			//echo '<pre>'; print_r($bets); die;
		}

		foreach($bets as &$bet){
			if($bet->bet == '1'){
				$betteamdetail = $this->AdminModel->getRowById('teams',array('id'=>$bet->team1_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}else{
				$betteamdetail = $this->AdminModel->getRowById('teams',array('id'=>$bet->team2_id));
				$bet->betteam = $betteamdetail->name;
				$bet->betteamdetail = $betteamdetail;
			}
			$games_change_history_last = $this->AdminModel->getLastRowById('games_change_history',array('game_id'=>$bet->game_id),"id","DESC");
			$bet->games_change_history_last = $games_change_history_last;
			$games_change_history_current = $this->AdminModel->getRowById('games_change_history',array('id'=>$bet->games_change_history_id));
			$bet->games_change_history_current = $games_change_history_current;
			/* $bet->bet_result = 'No Result';
			if(in_array($bet->bet_box,["1","4"])){
				if($bet->game_result == '0'){
					$bet->bet_result = 'No Result';
				}
				elseif($bet->game_result == '3'){
					$bet->bet_result = 'Draw';
				}
				else{
					if($bet->game_result == $bet->bet){
						$bet->bet_result = 'Won';
					}else{
						if(($bet->games_change_history_current->spread1_t1 > 0 && $bet->games_change_history_current->spread1_t2 < 0) || ($bet->games_change_history_current->spread1_t1 < 0 && $bet->games_change_history_current->spread1_t2 > 0)){
							

							if($bet->bet_box == '1') $spread = $bet->games_change_history_current->spread1_t1;
							else $spread = $bet->games_change_history_current->spread1_t2;

							if($spread > 0) $bet_on = 'Underdog';
							else $bet_on = 'Favorite';

							if($bet->games_change_history_last->spread1_t1 > 0 && $bet->games_change_history_last->spread1_t2 < 0){
								$Underdog = $bet->games_change_history_last->spread1_t1;
								$Favorite = $bet->games_change_history_last->spread1_t2;
							}else{
								$Underdog = $bet->games_change_history_last->spread1_t2;
								$Favorite = $bet->games_change_history_last->spread1_t1;
							}

							if($bet_on == 'Underdog'){
								if($Underdog + $spread > $Favorite) $bet->bet_result = 'Won';
								elseif($Underdog + $spread < $Favorite) $bet->bet_result = 'Lost';
								else $bet->bet_result = 'Draw';
							}else{
								if($Favorite - $spread > $Underdog) $bet->bet_result = 'Won';
								elseif($Favorite - $spread < $Underdog) $bet->bet_result = 'Lost';
								else $bet->bet_result = 'Draw';
							}



						}else{
							$bet->bet_result = 'Lost';
						}
					}
				}
			}else{
				if($bet->game_result == '0'){
					$bet->bet_result = 'No Result';
				}
				elseif($bet->game_result == '3'){
					$bet->bet_result = 'Draw';
				}
				else{
					if($bet->game_result == $bet->bet){
						$bet->bet_result = 'Won';
					}else{
						$bet->bet_result = 'Lost';
					}
				}


			} */
			if($bet->win == 0) $bet->bet_result = 'No Result';
			elseif($bet->win == 1) $bet->bet_result = 'Lost';
			else $bet->bet_result = 'Won';

		}
		
	 //->tosql();
	//echo '<pre>'; print_r($bets);die();
	 
      return view('admin/home/bets_by_history_id/allbets',compact('bets','game_id'));
    }

	public function bet_detail(Request $request,$id)
    {	
		$bet = $this->AdminModel->getRowById("bets", array("id"=>$id));
		$userdetail = $this->AdminModel->getRowById("sup_users", array("id"=>$bet->user_id));
		// $gamedetail = $this->AdminModel->getRowById("games", array("id"=>$bet->game_id));
		$games = $this->AdminModel->getRowById("games",array("id"=>$bet->game_id));
        if($games){
            $games_type = $this->AdminModel->getRowById("games_type",array("id"=>$games->game_type));
            $team1detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team1_id));
            $team2detail = $this->AdminModel->getRowById("teams",array("id"=>$games->team2_id));
            $games->game_type_name = $games_type->game_type;
            $games->game_type_details = $games_type;
            $bet->games = $games;
            $bet->team1detail = $team1detail;
            $bet->team2detail = $team2detail;
            if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
            else $bet->betteamdetail = $team2detail;
            $bet->game_result = $games->result;
        } 
		$bet->userdetail = $userdetail;

		$games_change_history_last = $this->AdminModel->getLastRowById('games_change_history',array('game_id'=>$bet->game_id),"id","DESC");
		$bet->games_change_history_last = $games_change_history_last;
		$games_change_history_current = $this->AdminModel->getRowById('games_change_history',array('id'=>$bet->games_change_history_id));
		$bet->games_change_history_current = $games_change_history_current;
		/* $bet->bet_result = 'No Result';
		if(in_array($bet->bet_box,["1","4"])){
			if($bet->game_result == '0'){
				$bet->bet_result = 'No Result';
			}
			elseif($bet->game_result == '3'){
				$bet->bet_result = 'Draw';
			}
			else{
				if($bet->game_result == $bet->bet_team){
					$bet->bet_result = 'Won';
				}else{
					if(($bet->games_change_history_current->spread1_t1 > 0 && $bet->games_change_history_current->spread1_t2 < 0) || ($bet->games_change_history_current->spread1_t1 < 0 && $bet->games_change_history_current->spread1_t2 > 0)){
						

						if($bet->bet_box == '1') $spread = $bet->games_change_history_current->spread1_t1;
						else $spread = $bet->games_change_history_current->spread1_t2;

						if($spread > 0) $bet_on = 'Underdog';
						else $bet_on = 'Favorite';

						if($bet->games_change_history_last->spread1_t1 > 0 && $bet->games_change_history_last->spread1_t2 < 0){
							$Underdog = $bet->games_change_history_last->spread1_t1;
							$Favorite = $bet->games_change_history_last->spread1_t2;
						}else{
							$Underdog = $bet->games_change_history_last->spread1_t2;
							$Favorite = $bet->games_change_history_last->spread1_t1;
						}

						if($bet_on == 'Underdog'){
							if($Underdog + $spread > $Favorite) $bet->bet_result = 'Won';
							elseif($Underdog + $spread < $Favorite) $bet->bet_result = 'Lost';
							else $bet->bet_result = 'Draw';
						}else{
							if($Favorite - $spread > $Underdog) $bet->bet_result = 'Won';
							elseif($Favorite - $spread < $Underdog) $bet->bet_result = 'Lost';
							else $bet->bet_result = 'Draw';
						}



					}else{
						$bet->bet_result = 'Lost';
					}
				}
			}
		}else{
			if($bet->game_result == '0'){
				$bet->bet_result = 'No Result';
			}
			elseif($bet->game_result == '3'){
				$bet->bet_result = 'Draw';
			}
			else{
				if($bet->game_result == $bet->bet_team){
					$bet->bet_result = 'Won';
				}else{
					$bet->bet_result = 'Lost';
				}
			}


		} */

		if($bet->win == 0) $bet->bet_result = 'No Result';
		elseif($bet->win == 1) $bet->bet_result = 'Lost';
		else $bet->bet_result = 'Won';

		//echo '<pre>'; print_r($bet); die;
      	return view('admin/home/bets/bet_detail',compact('bet','id'));
    }

    public function seeBetsOfUser($id)
    {
      if (id == $id) {
        $bets = DB::table('bets')
        ->join('teams as t1', 'bets.team1', '=', 't1.id')
        ->join('teams as t2', 'bets.team2', '=', 't2.id')
        ->join('matchs as m', 'bets.match_id', '=', 'm.id')
        ->select('t1.name as team1', 't2.name as team2', 'user_id', 'bet', 'betAmount','matchDate', 'bets.id')
        ->where('user_id',$id)
        ->get();
        return view('admin/user/')->with('bets',$bets);
      } else {
        return redirect('');
      }
    }

    public function createBetForMatch()
    {
      $match = DB::table('matchs')
      ->join('teams as t1', 'matchs.team1_id', '=', 't1.id')
      ->join('teams as t2', 'matchs.team2_id', '=', 't2.id')
     
      ->select('matchs.id as match_id','matchs.matchDate', 't1.name as name1','t2.name as name2','t1.id as team1_id','t2.id as team2_id')
      
      ->get();
	//echo '<pre>';
	//print_r( $match);die();
      return view('admin/home/user/bet')->with('match', $match);
    }

    public function store(Request $request)
    {
      
      $bet->user_id = $request->input('user_id');
      $bet->match_id = $request->input('match_id');
      $bet->team1 = $request->input('team1_id');
      $bet->team2 = $request->input('team2_id');
      $bet->bet = $request->input('bet');
      $bet->betAmount = $request->input('bet_amount');
      $bet->save();

      return redirect('');
    }
	public function make_bet(Request $request){
		
		$response = $this->AdminModel->make_bet();
		
		return view('admin/home/user/bet',compact('response'));
	}


	public function delete_bets(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_bets($id);
	 	return redirect('admin/bet')->with('error','Bet Deleted successfully');
	}

	public function settled_status(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->update_row("bets", array("settled"=>1), array("id"=>$id));
		return back()->with('success','Amount settled successfully');
	}


	public function bet_details_settlement($bet_id){
		$id = $bet_id;
        $bet = $this->AdminModel->getRowById("bets", array("id"=>$id));
        $user = $this->AdminModel->getRowById("users", array("id"=>$bet->user_id));
        
        $data_set = [
            'settled'=> ($bet->settled==0)?false:true,
            'payout'=> $bet->pay_out,
            'bet_id'=> $bet->id,
            'wallet_address'=> $bet->wallet_address,
        ];
		return response()->json($data_set);
	}



    
	public function settled_bet_save_crypto_transaction(Request $request){
        $response = $this->AdminModel->update_row("bets", array("settled"=>1), array("id"=>$request->bet_id));

        $oldCryptoTransaction = CryptoTransaction::where('transaction_id', $request->transaction_id)->first();
		if (!isset($oldCryptoTransaction)) {
	        $newCryptoTransaction = CryptoTransaction::create([
		        'transaction_id' => $request->transaction_id,
		        'transaction_type' => $request->transaction_type,
				'sending_wallet_address' => $request->sending_wallet_address, 
				'receiving_wallet_address' => $request->receiving_wallet_address, 
				'amount' => $request->amount, 
				'currency' => $request->currency, 
				'reference_id' => (isset($request->reference_id)) ? $request->reference_id : null
			]);
	        return true;
		}
		return true;   
    }



	public function store_bets(Request $request){

		$result = DB::table('bets')->where('user_id', '=', Auth::id())->where('match_id', '=', $incase->id)->get();

		if(count($result) > 10){
			$incase = Incase::find($id);
			$bets->match_id = $incase->id;
			$data = $request->post();
			$id=$request->id;
			unset($data['_token']);
			unset($data['id']);
			if(empty($id)){
				$insert_id = $this->AdminModel->insert('bets',$data);
				if($insert_id){
					return redirect('admin/users/bet')->with('success', 'Bet added successfully');
				}else{
					return back()->with('error','Unable to add games');
				}
			}else{
				$this->AdminModel->editbet($data,$id);
				return redirect('admin/users/bet')->with('success', 'Bet Edited successfully');
			}
			return redirect('admin/users/bet')->with('success', 'Bet added successfully');
		}else{
			return redirect('admin/users/bet')->with('warning','Maximum bet added limit reached');
		}
		
	}

	public function bet_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->bet_status($id,$data);
		
	}



	/* --------------------------- Manage  User ------------- ---------------*/
	public function user_index(Request $request){
		$search='';
		$search=$request->search;
		$response = $this->AdminModel->user_index($search);
		//echo '<pre>'; print_r($response);die();
		return view('admin/home/user/index',compact('response'));
	}

	public function user_details(Request $request ,$id){
		$response='';
		$response1 = $this->AdminModel->usersprofile($id);
		/* $bets = DB::table('sup_users')
		->join('teams as t1', 'bets.team1', '=', 't1.id')
		->join('teams as t2', 'bets.team2', '=', 't2.id')
		->join('teams as t3', 'bets.bet', '=', 't3.id')
		->join('matchs as m', 'bets.match_id', '=', 'm.id')
		->select('t1.name as team1', 't2.name as team2', 't3.name as betteam', 'user_id','bet', 'betAmount','matchDate', 'bets.id')
		->get(); */
	   //->tosql();
	   //echo '<pre>'; print_r($response1);die();
	   
	   return view('admin/home/user/userdetails',compact('response','id','response1'));
	}

	public function user_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->user_status($id,$data);
		
	}
	public function usersprofile(Request $request){
		$id = $request->id;
		$response=$this->AdminModel->usersprofile($id);
		//echo '<pre>'; print_r($response);die();
		return view('admin/home/user/userprofile',compact('response','id'));
	}

	public function usersbetting(Request $request){
		$id = $request->id;
		$search='';
		if($request->search) $search=$request->search;
		$response=$this->AdminModel->usersbetting($id,$search);
		return view('admin/home/user/userbeting',compact('response','id'));
	}
 
	public function edituserprofile($id){
		//$id = $request->id;
		if(!empty($id)){
			$response= $this->AdminModel->edituserprofile($id);
		}
		return view('admin/home/user/edituserprofile',compact('response','id'));
	
	}

	public function edituserprofile_store(Request $request){
		$data = $request->post();
		//echo "<pre>"; print_r($data); die;
		if($request->hasFile('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            //$file-> move(public_path('public/uploads/Users_profile_photo'), $filename);
            // $file-> move(public_path('uploads/Users_profile_photo'), $filename);
            $file-> move(storage_path('app/public/Users_profile_photo'), $filename);
            $data['image']= $filename;
        }
		$id=$request->id;
		unset($data['_token']);
		unset($data['id']);
		if(empty($id)){
		$insert_id = $this->AdminModel->insert('sup_users',$data);
		if($insert_id){
			return redirect('admin/gamestype')->with('success', 'User added successfully');
		}else{
			return back()->with('error','Unable to add games');
		}
		}else{
			$this->AdminModel->userprofile_edit($data,$id);
			return back()->with('success','User edited successfully');
		}
	}

	public function delete_userinfon(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_userinfon($id);
		//return back()->with('error','User Deleted successfully');
		return redirect('/admin/users/')->with('success', 'User Deleted successfully');
	}
/*-------------------------------------User Transactions-------------------------*/ 
	public function userstransactions(Request $request ,$id){
		$search='';
		$search=$request->search;
		$response=$this->AdminModel->userstransactions($id,$search);
		$userdetail=$this->AdminModel->getRowById("sup_users", array("id"=>$id));
		
		//echo '<pre>'; print_r($response);die();
	
		return view('admin/home/user/userTransaction',compact('response','id','userdetail'));
	}


public function userstransactions_status(Request $request){
	$id = $request->id;
	$transaction='C';
	$data = [
			"status"=>$request->status,
			"transaction"=>C,
		];
		
	$response = $this->AdminModel->userstransactions_status($id,$data);

}

public function useraddbalance(Request $request ,$id){
	$id = $request->id;
	$search='';
	if($request->search) $search=$request->search;
	$response=$this->AdminModel->useraddbalance($id,$search);
   	//echo '<pre>'; print_r($response); die;
   
   return view('admin/home/user/userAddbalance',compact('response','id'));
}

public function addbal_status(Request $request){
	$id = $request->id;
	$data = [
			"status"=>$request->status
		];
		
	$response = $this->AdminModel->update_row("transactions",$data,array("id"=>$id));
	if($response){
		echo json_encode(array("message"=>"success", "message_title"=>"Status changed successfully"));
	}else{
		echo json_encode(array("message"=>"error", "message_title"=>"Unable to change status"));
	}
	
}

public function add_addbalance(Request $request, $id){
		
	$data = $request->post();
	//echo "<pre>"; print_r($data); die;
	$response1='';
	$search=$request->search;
	$result = DB::table('teams')->get();
	$addgame = $this->AdminModel->add_gamme();
	$response = $this->AdminModel->gamestype($search);
	//$gameplan = $this->AdminModel->add_gameplan();
	//echo "<pre>"; print_r($result); die;
	return view('admin/home/user/add_addbalance',compact('addgame','response1','response','result','id'));
}

public function store_addbalance(Request $request){
	//$data = $request->post();
	$userById = $this->AdminModel->getRowById('sup_users',array("id"=>$request->user_id));
	$data = array(
		"user_id"=>$request->user_id,
		"amount"=>$request->amount,
		"transaction_details"=>json_encode(array(
			"amount"=>$request->amount,
			"message"=>"Credit",
			"message_description"=>"Amount credited by Admin.",
		)),
		"transaction"=>"C",
		"status"=>"0",
	);
	// echo "<pre>"; print_r($data); 
	// echo "<pre>"; print_r($userById); 
	// die;
	$insert_id = $this->AdminModel->insert('transactions',$data);
	$update_user = $this->AdminModel->update_row('sup_users',array("available_balance"=>$userById->available_balance + $request->amount, "current_balance"=>$userById->current_balance + $request->amount),array("id"=>$request->user_id));
	if($insert_id){
		return redirect('/admin/users/useraddbalance/'.$request->user_id)->with('success', 'Balance added successfully');
	}else{
		return back()->with('error','Unable to add balance');
	}
	
}



public function userwidbalance(Request $request ,$id){
	$id = $request->id;
	$search='';
	$search=$request->search;
	$response=$this->AdminModel->userwidbalance($id,$search);
   //echo '<pre>'; print_r($userwidbalance);die();
   
   return view('admin/home/user/userWithdrawlbal',compact('response','id'));
}

public function widbal_status(Request $request){
	$id = $request->id;
	$data = [
			"status"=>$request->status
		];
		
	$response = $this->AdminModel->widbal_status($id,$data);
	
}


public function add_widbalance(Request $request, $id){
		
	$data = $request->post();
	//echo "<pre>"; print_r($data); die;
	$response1='';
	$search=$request->search;
	$result = DB::table('teams')->get();
	$addgame = $this->AdminModel->add_gamme();
	$response = $this->AdminModel->gamestype($search);
	//$gameplan = $this->AdminModel->add_gameplan();
	//echo "<pre>"; print_r($result); die;
	return view('admin/home/user/add_widbalance',compact('addgame','response1','response','result','id'));
}

public function store_widbalance(Request $request){
	$data = $request->post();
	
	$userById = $this->AdminModel->getRowById('sup_users',array("id"=>$request->user_id));
	if($request->amount < $userById->available_balance){
		//echo "<pre>"; print_r($data); die;
		$data = array(
			"user_id"=>$request->user_id,
			"amount"=>$request->amount,
			"transaction_details"=>json_encode(array(
				"amount"=>$request->amount,
				"message"=>"Withdraw",
				"message_description"=>"Amount withdraw by Admin.",
			)),
			"transaction"=>"W",
			"status"=>"0",
		);
		// echo "<pre>"; print_r($data); 
		// echo "<pre>"; print_r($userById); 
		// die;
		$insert_id = $this->AdminModel->insert('transactions',$data);
		$update_user = $this->AdminModel->update_row('sup_users',array("available_balance"=>$userById->available_balance - $request->amount, "current_balance"=>$userById->current_balance - $request->amount),array("id"=>$request->user_id));
		if($insert_id){
			return redirect('/admin/users/userwithdrawlbalance/'.$request->user_id)->with('success', 'Balance withdraw successfully');
		}else{
			return back()->with('error','Unable to withdraw balance');
		}
	}else{
		//echo "<pre>"; print_r($data); die;
		return back()->with('error','Insufficient balance');
	}
	
}

public function userdebbalance(Request $request ,$id){
	$id = $request->id;
	$search='';
	$search=$request->search;
	$response=$this->AdminModel->userdebbalance($id,$search);
   //echo '<pre>'; print_r($userwidbalance);die();
   
   return view('admin/home/user/userDebitbal',compact('response','id'));
}

public function debbal_status(Request $request){
	$id = $request->id;
	$data = [
			"status"=>$request->status
		];
		
	$response = $this->AdminModel->widbal_status($id,$data);
	
}


public function add_debbalance(Request $request, $id){
		
	$data = $request->post();
	//echo "<pre>"; print_r($data); die;
	$response1='';
	$search=$request->search;
	$result = DB::table('teams')->get();
	$addgame = $this->AdminModel->add_gamme();
	$response = $this->AdminModel->gamestype($search);
	//$gameplan = $this->AdminModel->add_gameplan();
	//echo "<pre>"; print_r($result); die;
	return view('admin/home/user/add_debbalance',compact('addgame','response1','response','result','id'));
}

public function store_debbalance(Request $request){
	$data = $request->post();
	
	$userById = $this->AdminModel->getRowById('sup_users',array("id"=>$request->user_id));
	if($request->amount < $userById->available_balance){
		//echo "<pre>"; print_r($data); die;
		$data = array(
			"user_id"=>$request->user_id,
			"amount"=>$request->amount,
			"transaction_details"=>json_encode(array(
				"amount"=>$request->amount,
				"message"=>"Debit",
				"message_description"=>"Amount debit by Admin.",
			)),
			"transaction"=>"D",
			"status"=>"0",
		);
		// echo "<pre>"; print_r($data); 
		// echo "<pre>"; print_r($userById); 
		// die;
		$insert_id = $this->AdminModel->insert('transactions',$data);
		$update_user = $this->AdminModel->update_row('sup_users',array("available_balance"=>$userById->available_balance - $request->amount, "current_balance"=>$userById->current_balance - $request->amount),array("id"=>$request->user_id));
		if($insert_id){
			return redirect('/admin/users/userdebitbalance/'.$request->user_id)->with('success', 'Balance debit successfully');
		}else{
			return back()->with('error','Unable to debit balance');
		}
	}else{
		//echo "<pre>"; print_r($data); die;
		return back()->with('error','Insufficient balance');
	}
	
}

/*--------------------------------------------Bet-----------------------------------*/

	public function bet_store(Request $request){
		
		
		$bet->user_id= $request->input('user_id');
		$bet->match_id=$request->input('match_id');
		$bet->team1=$request->input('team1_id');
		$bet->team2=$request->input('team2_id');
		$bet->bet=$request->input('bet');
		$bet->betAmount=$request->input('bet_amount');
		$bet->save();

		return redirect('admin/home/');
	}

	public function manage_game(){
		$games_count = DB::table('games')->count();
		$teams_count = DB::table('teams')->count();
		return view('admin/home/bets/manage',compact('games_count','teams_count'));
	}

	public function teamsByGameType(){
		// $Boxing = DB::table('games')->where('game_type','=','1')->count();
		// $Soccer_League = DB::table('games')->where('game_type','=','2')->count();
		// $American_Football= DB::table('games')->where('game_type','=','3')->count();
		// $Hockey = DB::table('games')->where('game_type','=','4')->count();
		$games_type = $this->AdminModel->getAllData('games_type');
		foreach($games_type as &$g_type){
			$count_teams_by_game_type = $this->AdminModel->tableWhereCount('teams',array("game_type"=>$g_type->id));
			$g_type->count_teams = $count_teams_by_game_type;
		}
		//echo "<pre>"; print_r($games_type); die;
		return view('admin/home/bets/teams_by_game_type',compact('games_type'));
	}

	public function livebet(){
		$Boxing = DB::table('games')->where('game_type','=','1')->count();
		$Soccer_League = DB::table('games')->where('game_type','=','2')->count();
		$American_Football= DB::table('games')->where('game_type','=','3')->count();
		$Hockey = DB::table('games')->where('game_type','=','4')->count();
		return view('admin/home/bets/livebets',compact('Boxing','Soccer_League','American_Football','Hockey'));
	}

	public function boxing_mma(Request $request,$id=null)
    {
		$search='';
		$search=$request->search;
    	$boxing = $this->AdminModel->boxing_mma($search);
		//echo '<pre>';print_r($boxing);die();
		return view('admin/home/game_type/boxing',compact('boxing','search'));
	}

	public function soccer_leauge(Request $request,$id=null)
    {
		$search='';
		$search=$request->search;
    	$soccer_leauge = $this->AdminModel->soccer_leauge($search);
		//echo '<pre>';print_r($soccer_leauge);die();
		return view('admin/home/game_type/soccer',compact('soccer_leauge','search'));
	}

	public function american_football(Request $request,$id=null)
    {
		$search='';
		$search=$request->search;
    	$american_football = $this->AdminModel->american_football($search);
		//echo '<pre>';print_r($boxing);die();
		return view('admin/home/game_type/american',compact('american_football','search'));
	}

	public function hockey(Request $request,$id=null)
    {
		$search='';
		$search=$request->search;
    	$hockey = $this->AdminModel->hockey($search);
		//echo '<pre>';print_r($boxing);die();
		return view('admin/home/game_type/hockey',compact('hockey','search'));
	}

	public function boxing_mma_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->gmae_status($id,$data);
		
	}

	public function soccer_leauge_status(Request $request){
		$id = $request->id;
		$data = [
				"status"=>$request->status
			];
			
		$response = $this->AdminModel->gmae_status($id,$data);
		
	}
	
	/*------------------------------Manage Match-----------------------------*/ 

	public function match_index(Request $request,$id=null){
		$search='';
		$search=$request->search;
		if(empty($search)){
			$matches = DB::table('matchs')
			->join('teams as a', 'team1_id', '=', 'a.id')
			->join('teams as b', 'team2_id', '=', 'b.id')
			->join('teams as c', 'winner_id', '=', 'c.id')
			
			->leftJoin('stats as s', 's.match_id', '=', 'matchs.id')
			->select('matchs.*', 'a.id as team1_id', 'b.id as team2_id', 'a.name as team1', 'b.name as team2', 'b.flag as flag2', 'a.flag as flag1', 'c.name as winner', 's.id as stat_id')
			->orderBy('id','ASC')
			->paginate(5)->withQueryString();
		}else{
			$matches = DB::table('matchs')
			->join('teams as a', 'team1_id', '=', 'a.id')
			->join('teams as b', 'team2_id', '=', 'b.id')
			->join('teams as c', 'winner_id', '=', 'c.id')
			
			->leftJoin('stats as s', 's.match_id', '=', 'matchs.id')
			->select('matchs.*', 'a.id as team1_id', 'b.id as team2_id', 'a.name as team1', 'b.name as team2', 'b.flag as flag2', 'a.flag as flag1', 'c.name as winner', 's.id as stat_id')
			->orderBy('id','ASC')
			->where('a.name', 'Like', '%' .$search. '%')->orwhere('b.name', 'Like', '%' .$search. '%')->orwhere('c.name', 'Like', '%' .$search. '%')->paginate(5)->withQueryString();	
		}
		//echo '<pre>';print_r($matches);die();
		return view('admin/home/matchs/index')->with('matches', $matches);
	}
	
	public function addmatch(Request $request){
		$id='';
		$response1='';
		$search=$request->search;
		$result = DB::table('teams')->get();
		$addmatch = $this->AdminModel->addmatch();
		$response = $this->AdminModel->gamestype($search);
		return view('admin/home/matchs/addmatches',compact('id','addmatch','response1','response','result'));
	}

	public function edit_match($id){
		//$id = $request->id;
		$result = DB::table('teams')->get();
		if(!empty($id)){
			$response= $this->AdminModel->edit_match($id);
		}
		//  echo "<pre>";
		//  print_r($result); die();
		return view('admin/home/matchs/addmatches',compact('response','id','result'));
	
	}


	public function edit_addmatch(Request $request,$id){
		$response='';
		$search=$request->search;
		$result = DB::table('teams')->get();
		$response= $this->AdminModel->gamestype($search);
		if(!empty($id)){
			$response1= $this->AdminModel->getGames($id);
			
		}
		// echo "<pre>";
		// print_r($response1); die();
		return view('admin/home/matchs/addmatches',compact('id','response1','response','result'));
	
	} 

	public function submit_addmatch(Request $request)
	{
	  $data = $request->post();
	  $id =$request->id;
	  unset($data['_token']);
	  unset($data['id']);
	  if(empty($id)){
		$insert_id = $this->AdminModel->insert('matchs',$data);
		if($insert_id){
			return redirect('admin/matchs')->with('success', ' added successfully');
		}else{
			return back()->with('error','Unable to add ');
		}
		}else{
			$this -> AdminModel-> editmatch($data,$id);
			return redirect('admin/matchs')->with('success', ' Edited successfully');
		}
	}

	/*----------------------------------------ADMIN CMS----------------------- */

	public function admincms(Request $request,$rid=''){
		$search=$request->search;
		if(empty($rid)){
			$response = $this->AdminModel->fetch_all('sup_admin_cms',$search);
		}else{
			$response = $this->AdminModel->fetch_all_byrest('sup_admin_cms',$search,$rid);
		}
		return view('admin/home/admin_cms/index',compact('response','search','rid'));
	}

	public function create_admincms(Request $request,$rid=''){
		$response='';
		$games = $this->AdminModel->admin_cms();
		//echo '<pre>';print_r($games);die();
		return view('admin/home/admin_cms/createcms',compact('response','games','rid'));
	}

	public function edit_admincms($id,$rid=''){
		$response='';
		if(!empty($id)){
			
			$response=$this->AdminModel->fetch_row('sup_admin_cms',$id);
			//echo '<pre>';print_r($response);die();
		}
		$games = $this->AdminModel->admin_cms();
		return view('admin/home/admin_cms/createcms',compact('response','games','rid'));
	}
	
	public function submit_admincms(Request $request){
		$data=$_POST;
		if($request->id){
			$id=$request->id;
		}
		if($request->rid){
			$rid=$request->rid;
		}
		
		if($request->hasFile('cms_image')){
			$image = $request->file('cms_image');
			$filename    = date('YmdHi').$image->getClientOriginalName();
			$image_resize = Image::make($image->getRealPath());            
			$image_resize->resize(1326, 666, function ($constraint) {
				//$constraint->aspectRatio();
			});
			$image_resize->resizeCanvas(1326, 666, 'center', false, array(255, 255, 255, 0));
			// $image_resize->save(public_path('uploads/cms_image/' .$filename));
			$image_resize->save(storage_path('app/public/cms_image/' .$filename));
			$data['cms_image'] = $filename;
        }
		unset($data['_token']);
		unset($data['id']);
		unset($data['rid']);
		if(empty($rid)){
			if(!empty($id)){
				$this->AdminModel->update_table('sup_admin_cms',$id, $data);
			 return redirect('admin/cms')->with('success','Admin CMS updated successfully');
			
			}else{
				$this->AdminModel->insert('sup_admin_cms', $data);
				 return redirect('admin/cms')->with('success','Admin CMS added successfully');
			}
		}else{
			if(!empty($id)){
				$this->AdminModel->update_table('sup_admin_cms',$id, $data);
			 return redirect('admin/cms/'.$rid)->with('success','Admin CMS updated successfully');
			
			}else{
				$this->AdminModel->insert('sup_admin_cms', $data);
				 return redirect('admin/cms/'.$rid)->with('success','Admin CMS added successfully');
			}

		
		
		}
	}
	public function admin_cms_status(Request $request){
		$id = $request->id;
			$data = [
				"status"=>$request->status
			];
			$this->AdminModel->update_table('sup_admin_cms',$id,$data);
	
	
	}
	
	public function delete_admincms($id,$rid=''){
		$response = $this->AdminModel->delete_row('sup_admin_cms',$id);
		if(!empty($rid)){
			return redirect('admin/cms/'.$rid)->with('error','Admin CMS Deleted successfully');
		}else{
			return redirect('admin/cms')->with('error','Admin CMS Deleted successfully');
		
		}
	}

	
	/*--------------------------------------------------Game Plan-----------------------------------------*/ 

	public function gameplan(Request $request,$id=null){
		$id='';
		$search='';
		$search=$request->search;
		$response=$this->AdminModel->gameplan($search);
		//$addmatch = $this->AdminModel->addmatch();
		//return view('admin/home/game_plan/index');
		return view('admin/home/game_plan/index',compact('id','response'));
	}

	public function add_gameplan(){
		$id='';
		$response='';
		$add_gameplan = $this->AdminModel->add_gameplan();
		return view('admin/home/game_plan/creategameplan',compact('id','add_gameplan','response'));
	}

	public function submit_gameplan(Request $request)
	{
	  $data = $request->post();
	  $id =$request->id;
	  unset($data['_token']);
	  unset($data['id']);
	  if(empty($id)){
		$insert_id = $this->AdminModel->insert('games_plan',$data);
		if($insert_id){
			return redirect('admin/gameplan')->with('success', 'Game Plan added successfully');
		}else{
			return back()->with('error','Unable to add ');
		}
		}else{
			$this -> AdminModel-> editgameplansubmit($data,$id);
			return redirect('admin/gameplan')->with('success', ' Game Plan Edited successfully');
		}
	}
	
	public function edit_gamplan($id){
		//$id = $request->id;
		if(!empty($id)){
			$response= $this->AdminModel->edit_gamplan($id);
		}
		// print_r($response);
		// exit;
		return view('admin/home/game_plan/creategameplan',compact('response','id'));
	
	}

	public function delete_gamplan(Request $request){
		$id = $request->id;
		$response = $this->AdminModel->delete_gamplan($id);
		return back()->with('error','User Deleted successfully');
	}

	public function sendPMEmail($email,$subject,$content) {

		$mail = new PHPMailer(true); 
        
        try {

            //$mail->SMTPDebug = 4; 
            $mail->isSMTP(); 
            // $mail->IsMail();
            $mail->Host = env('MAIL_HOST'); 
            $mail->SMTPAuth = true; 
            $mail->Username = env('MAIL_USERNAME'); 
            $mail->Password = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port = env('MAIL_PORT');
            
            $mail->setFrom(env('MAIL_FROM_ADDRESS'));
            $mail->addAddress($email); 
            $mail->addReplyTo($email);
            

                /* $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );  */

            $mail->isHTML(true); 

            $mail->Subject = $subject;
            $mail->Body    = $content;
            
        
            if( !$mail->send() ) {
            // echo 'Message could not be sent.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                //return back()->with('success',$flash);
                return;
            }

        } catch (Exception $e) {
            // return back()->with('error','Message could not be sent.');
        }
        // exit;

		
	}


	public function get_transactions(Request $request){
		$search='';
		$search=$request->search;
		$response = $this->AdminModel->get_transactions($search);
		//echo '<pre>'; print_r($response);die();
		return view('admin/home/transaction/index',compact('response'));
	}
	public function change_bet_status(Request $request){
		$games_change_history_id = $request->games_change_history_id;
		$game_id = $request->game_id;
		$bet_box = $request->bet_box;
		$result = $request->result;
		$data = array('win' => $result);
		$games_change_history_byid = $this->AdminModel->getRowById("games_change_history", array("id"=>$games_change_history_id));
		$bet_status_win_box_array = explode(",",$games_change_history_byid->bet_status_win);
		$bet_status_win_box_array[$bet_box - 1] = $result;
		$bet_status_win = implode(",",$bet_status_win_box_array);
		$getbets = $this->AdminModel->getResultById("bets", array("game_id"=>$game_id, "games_change_history_id"=>$games_change_history_id, "bet_box"=>$bet_box));
		if(count($getbets)>0){
			$updatebets = $this->AdminModel->update_row("bets", $data, array("game_id"=>$game_id, "games_change_history_id"=>$games_change_history_id, "bet_box"=>$bet_box));
			$games_change_history_update = $this->AdminModel->update_row("games_change_history", array("bet_status_win"=>$bet_status_win), array("id"=>$games_change_history_id));
			$result_response = array("message_title"=>"success", "message"=>"All bets status under this are changed");
		}else{
            $games_change_history_update = $this->AdminModel->update_row("games_change_history", array("bet_status_win"=>$bet_status_win), array("id"=>$games_change_history_id));
			$result_response = array("message_title"=>"error", "message"=>"There are no bet betted on this particular button");
		}

		echo json_encode($result_response);
		
		
		
	}
} 
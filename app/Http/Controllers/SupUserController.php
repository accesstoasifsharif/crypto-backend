<?php
namespace App\Http\Controllers;
#use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Models\SupUser;
//use App\Models\User;
use App\Models\AllGames;
use App\Models\UserBetting;
// use App\Models\UserTransaction;
use App\Models\SupUserModel;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Config;
use File;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SupUserController extends Controller
{
    public function __construct(SupUserModel $SupUserModel) {
        Config::set('jwt.user', 'App\Models\SupUser'); 
        Config::set('auth.providers.users.model', \App\Models\SupUser::class);
        Config::set('jwt.ttl', 60 * 24);
        $this->guard = "api_sup_user";
        $this->SupUserModel = $SupUserModel;
        $this->middleware('auth:api', ['except' => ['login', 'register', 'game_type','games_by_game_type','admin_cms_by_page','games_s','gamebyid']]);
        //$this->middleware('auth:api', ['except' => ['login', 'register', 'logout', 'refresh', 'userProfile', 'addCredit', 'userProfileUpdate']]);
    }


    /**
     * Register Super Admin User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        // return false;
        //echo "<pre>"; print_r($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:sup_users',
            //'user_id' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:6',
            //'password_confirmation' => 'required|string|confirmed|min:6',
            'account_id' => 'required|string|min:6',
            //'phone_number' => 'required|string|min:11',
        ]);

        // if($validator->fails()){
        //     return response()->json($validator->errors()->toJson(), 400);
        // }
        if($validator->fails()){
            return response()->json([
                'message'=>'Authenticated',
				'message_title' => "error",
				'message_description' => $validator->errors()
			]);
        }
        $user = SupUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
		$token = JWTAuth::fromUser($user);
		$data = array();
        $data['remember_token'] = $token;    
        $data['user_type'] = 3;
        $data['account_id'] = $request->account_id;
        //$data['phone_number'] = $request->phone_number;
        $this->SupUserModel->update_row('sup_users',$data,array("id"=>$user['id']));
		$user1 = $this->SupUserModel->getRowById("sup_users",array("id"=>$user['id']));
		$user['remember_token'] = $user1->remember_token;
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'success',
			'message_description'=>'User successfully registered',
			'id'=> $user['id'],
            'user' => $user1,
            'token' => $token
        ], 200);  
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth($this->guard)->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 3) return response()->json(['error' => 'Unauthorized'], 401);
        if($user['status'] != '1'){
            return response()->json([
                'message'=>'Authenticated',
                'message_title'=>'error',
                'message_description' => "User blocked"
            ]);
        }
        // Fill user model
        $user->fill([
            'remember_token' => $token
        ]);

        // Save user to database
        $user->save(); 
        return $this->createNewToken($token);
    }

    


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $user = auth($this->guard)->user();
        if($user['user_type'] != 3) return response()->json(['error' => 'Unauthorized'], 401);
        auth($this->guard)->logout();
        return response()->json(['message'=>'Authenticated','message_title' => 'User successfully signed out']);
    }

    public function change_password(Request $request)
    {  //print_r($_POST);die();
        $request->validate([
            'current_password' => ['required'],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
		
		$password=$request->current_password;
		$data = [
			"password"=>Hash::make($request->new_password)
			
		];
        $user = auth($this->guard)->user();

        $passwordid = $user->id;
       //dd($passwordid);
		
		$hashedPassword=DB::table('sup_users')->where('id','=',$passwordid)->first();
		//echo $hashedPassword->password;die();
		if ($hashedPassword && Hash::check($password, $hashedPassword->password)) {	
			$this->SupUserModel->userpasswordupdate('sup_users',$passwordid,$data);
            DB::table('sup_users')->where('id','=',$passwordid)->update($data);
			
            return response()->json(['message'=>'Authenticated','message_title' => 'User Password changed successfully Successfully']);
		}else{
            return response()->json(['error' => 'Unauthorized'], 401);
			

		}
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request) {
        $user = auth($this->guard)->user();
        if($user['user_type'] != 3) return response()->json(['error' => 'Unauthorized'], 401);
        return $this->createNewToken(auth($this->guard)->refresh(),'refresh',$user);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(Request $request) {
		$user = auth($this->guard)->user();
        if($user['user_type'] != 3) return response()->json(['error' => 'Unauthorized'], 401);
        return response()->json(['message'=>'Authenticated','message_title'=>'success','user'=>$user]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token,$method='',$user1=''){
		$user = auth($this->guard)->user();
		$user['remember_token'] = $token;
		if(!empty($method) && $method == 'refresh'){
			$data = array(
                'remember_token' => $token
            );
            $user['id'] = $user1['id'];
            $this->SupUserModel->update_row('sup_users',$data,array("id"=>$user1['id']));
		}
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'success',
			'id' => $user['id'],
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth($this->guard)->factory()->getTTL() * 60,
            'user' => $user
        ]);
		
    }

   

    public function userProfileUpdate(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'string|between:2,100',
			'email' => 'email',
            'image' => 'nullable|image'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if(!isset($request->email) && !isset($request->name)){
            return response()->json(['error' => 'Atleast one from name, email is required'], 400);
        }
        if(isset($request->email)){
            $userdetail1 = $this->SupUserModel->getRowById("sup_users",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $user['id']){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
        $data = array();
        if($request->hasFile('image')){
            if($user->image){
                // dd(storage_path('/Users_profile_photo'));
             // $old_path=public_path('/public/uploads/Users_profile_photo'.$user->image);
             $old_path=storage_path('/app/public/Users_profile_photo'.$user->image);
             //dd($old_path);
                if(File::exists($old_path)){
                    File::delete($old_path);
                }         
            } 

            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            // $file-> move(public_path('uploads/Users_profile_photo'), $filename);
            $file-> move(storage_path('app/public/Users_profile_photo'), $filename);
            $data['image']= $filename;
           // dd($data);
           
        }else{
            
        }
        
		if(isset($request->name)) $data['name'] = $request->name;
		if(isset($request->email)) $data['email'] = $request->email;
        $this->SupUserModel->update_row('sup_users',$data,array("id"=>$user['id']));
        $userdetail = $this->SupUserModel->getRowById('sup_users',array("id"=>$user['id']));
		return response()->json(['message'=>'Authenticated','message_title' => 'User profile updated successfully','user'=>$userdetail]);
    }


    public function change_password_new(Request $request) {
		$validator = Validator::make($request->all(), [
			'password' => 'required|string|confirmed|min:6',
			'password_old' => 'required|string|min:6'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if(!Hash::check($request->password_old, $user->password)){
            return response()->json(['error' => 'Incorrect Old password'], 400);
        }
        /* if(!isset($request->email) && !isset($request->name)){
            return response()->json(['error' => 'Atleast one from name, email is required'], 400);
        }
        if(isset($request->email)){
            $userdetail1 = $this->SupUserModel->getRowById("sup_users",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $user['id']){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
        $data = array();
        if($request->hasFile('image')){
            if($user->image){
                
             $old_path=public_path('/public/uploads/Users_profile_photo'.$user->image);
             //dd($old_path);
                if(File::exists($old_path)){
                    File::delete($old_path);
                }         
            } 

            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('uploads/Users_profile_photo'), $filename);
            $data['image']= $filename;
           // dd($data);
           
        }else{
            
        }
        
		if(isset($request->name)) $data['name'] = $request->name;
		if(isset($request->email)) $data['email'] = $request->email; */
        $this->SupUserModel->update_row('sup_users',array("password"=>Hash::make($request->password)),array("id"=>$user['id']));
        $userdetail = $this->SupUserModel->getRowById('sup_users',array("id"=>$user['id']));
		return response()->json(['message'=>'Authenticated','message_title' => 'Password changed successfully','user'=>$userdetail]);
    }
 

    
    /* public function allgames(Request $request){
        
        // $allgames = AllGames:: all();

        $allgames = DB::table('games')
			->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
			->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
			->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
			//->select('t1.name as team1', 't2.name as team2','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id')
            ->select('t1.name as team1', 't2.name as team2','game_date','g1.game_type as game_type_name','game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','venu','result','timeline','status','games.id', \DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))
            ->get();


        return response()->json(['allgames'=> $allgames],200);

       
        
    }

    public function usersbetting(Request $request,$id){
        $bets = DB::table('bets')
			->join('teams as t1', 'bets.team1', '=', 't1.id')
			->join('teams as t2', 'bets.team2', '=', 't2.id')
			->join('matchs as m', 'bets.match_id', '=', 'm.id')
			->join('teams as t3', 'bets.bet', '=', 't3.id')
			->select('t1.name as team1', 't2.name as team2','t3.name as betteam', 'user_id','betAmount','m.id as matchid', 'bets.id')
			->where('user_id',$id)
            ->get();

        return response()->json(['bets'=> $bets],200);       
            
    }

    public function usersbettingshow(Request $request,$id){
        $bets = DB::table('bets')
			->join('teams as t1', 'bets.team1', '=', 't1.id')
			->join('teams as t2', 'bets.team2', '=', 't2.id')
			->join('matchs as m', 'bets.match_id', '=', 'm.id')
			->join('teams as t3', 'bets.bet', '=', 't3.id')
			->select('t1.name as team1', 't2.name as team2','t3.name as betteam', 'user_id', 'bet', 'betAmount','m.id as matchid', 'bets.id')
			->where('user_id',$id)
            ->find($id);

            if(!empty($id)){
                return response()->json(['bets'=> $bets],200);
            }else{
                return response()->json(['message'=>'No Betting details Found'],404);
            }
            
    }

    public function userstransaction(Request $request){
        $transaction = DB::table('transactions')
            ->leftjoin('sup_users as s', 'transactions.user_id', '=', 's.id')
            ->select('s.name as username','transactions.*',DB::raw('(CASE 
            WHEN transactions.transaction = "C" THEN "Credit" 
            WHEN transactions.transaction = "D" THEN "Debit" 
            WHEN transactions.transaction = "W" THEN "Withdrawal" 
            ELSE "Null" 
            END) AS transactions_lable'))
            ->get();
            return response()->json(['transaction'=> $transaction],200); 
    }
    
    // ,\DB::raw('(CASE 
    // WHEN transactions.status = "1" THEN "Approve"
    // ELSE "Pending" 
    // END) AS status _lable')


    public function usersaddbalance(Request $request){
        $transaction = DB::table('add_bal')
            ->leftjoin('sup_users as s', 'add_bal.user_id', '=', 's.id')
            ->select('s.name as username','amount','via','too','frm','trx')
            ->get();
            return response()->json(['transaction'=> $transaction],200); 
    }

    public function withdrawl_bal(Request $request){
        $transaction = DB::table('withdrawl_bal')
            ->leftjoin('sup_users as s', 'withdrawl_bal.user_id', '=', 's.id')
            ->select('s.name as username','amount','via','too','frm','trx')
            ->get();
            return response()->json(['transaction'=> $transaction],200); 
    }

    public function allbet(Request $request){
        $allbets = DB::table('bets')
			->join('teams as t1', 'bets.team1', '=', 't1.id')
			->join('teams as t2', 'bets.team2', '=', 't2.id')
			->join('matchs as m', 'bets.match_id', '=', 'm.id')
			->join('teams as t3', 'bets.bet', '=', 't3.id')
            ->join('sup_users as s', 'bets.user_id', '=', 's.id')
			->select('t1.name as team1', 't2.name as team2','t3.name as betteam', 's.name as username','betAmount','m.id as matchid', 'bets.id')
			//->where('user_id',$id)
            ->get();

        return response()->json(['allbets'=> $allbets],200);       
            
    } */

    public function games(Request $request){
        if(isset($request->timeline)/*  && !empty($request->id) */){
            //$games = $this->SupUserModel->getResultById("games",array("timeline"=>$request->timeline));
            $games = DB::table("games")->where(array("status"=>"1"))->whereIn("timeline",["0", "1"])->get();
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
            
        }else{
            $games = $this->SupUserModel->getAllData("games");
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
        }
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $games
        ], 200);  
    }

    public function games_s(Request $request){
        $search='';
		if($request->search && !empty($request->search)) $search=$request->search;
        if(isset($request->timeline) && !empty($search)){
            //$games = $this->SupUserModel->getResultById("games",array("timeline"=>$request->timeline));
            $games = DB::table('games')
                    ->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
                    ->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
                    ->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
                    //->leftjoin('bets as b1', '', '=', 'b1.id')
                    ->select('games.*', 't1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))
                    
                    ->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venue', 'Like', '%' .$search. '%')
                    
                    ->get();
                    //echo "<pre>"; print_r($games); die;
            foreach($games as &$games1){
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                    $games1->game_type_name = $games_type->game_type;
                    $games1->game_type_details = $games_type;
                    $games1->team1detail = $team1detail;
                    $games1->team2detail = $team2detail;
                
            }
            $games_array = array();
            foreach($games as $games_k=>$games_v){
                if($games_v->timeline != '2' && $games_v->status == '1') $games_array[] = $games_v;
            }

            $games = $games_array;
            
            
        }else{
            //$games = $this->SupUserModel->getAllData("games");
            $games = DB::table('games')
                    ->leftjoin('teams as t1', 'games.team1_id', '=', 't1.id')
                    ->leftjoin('teams as t2', 'games.team2_id', '=', 't2.id')
                    ->leftjoin('games_type as g1', 'games.game_type', '=', 'g1.id')
                    //->leftjoin('bets as b1', '', '=', 'b1.id')
                    ->select('games.*', 't1.name as team1', 't2.name as team2', 'min_amount_user_bet','game_date','g1.game_type as game_type_name','games.game_type as game_plan','winning_percentage_team1','winning_percentage_team2','min_amount_user_bet','max_amount_user_bet','games.venue as venu','result','timeline','status','games.id',\DB::raw('DATEDIFF(games.game_date, NOW()) as remaining_match_day'))
                    ->where(array("status"=>"1"))
                    ->whereIn("games.timeline",["0","1"])
                    /* ->where('t1.name', 'Like', '%' .$search. '%')->orwhere('t2.name', 'Like', '%' .$search. '%')->orwhere('g1.game_type', 'Like', '%' .$search. '%')->orwhere('venue', 'Like', '%' .$search. '%') */
                    
                    ->get();
                    //echo "<pre>"; print_r($games); die;
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
        }
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $games
        ], 200);  
    }

    public function games_by_game_type(Request $request){
        if(isset($request->timeline) && !empty($request->game_type)){
            //$games = $this->SupUserModel->getResultById("games",array("timeline"=>$request->timeline, "game_type"=>$request->game_type));
            $games = DB::table("games")->where(array("status"=>"1"))->whereIn("timeline",["0", "1"])->where(array("game_type"=>$request->game_type))->get();
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
        }elseif(isset($request->timeline)/*  && !empty($request->id) */){
            //$games = $this->SupUserModel->getResultById("games",array("timeline"=>$request->timeline));
            $games = DB::table("games")->where(array("status"=>"1"))->whereIn("timeline",["0", "1"])->get();
            
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
            
        }else{
            $games = $this->SupUserModel->getAllData("games");
            foreach($games as &$games1){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games1->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games1->team2_id));
                $games1->game_type_name = $games_type->game_type;
                $games1->game_type_details = $games_type;
                $games1->team1detail = $team1detail;
                $games1->team2detail = $team2detail;
            }
        }
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $games
        ], 200);  
    }

    public function gamebyid(Request $request){
            $game = $this->SupUserModel->getRowById("games",array("id"=>$request->id));
            if($game){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$game->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$game->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$game->team2_id));
                $game->game_type_name = $games_type->game_type;
                $game->game_type_details = $games_type;
                $game->team1detail = $team1detail;
                $game->team2detail = $team2detail;
            }
            
            return response()->json([
                'message'=>'Authenticated',
                'message_title'=>'success',
                'results'=> $game
            ], 200);  
    }
    public function bets(Request $request){
        
        $user = auth($this->guard)->user();
        $bets1 = array();
        if((isset($request->timeline))){
            if($request->timeline != '2'){
                $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
                foreach($bets as $bet_k=>&$bet){
                    //$games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id, "timeline"=>$request->timeline));
                    $games = DB::table("games")->where(array("id"=>$bet->game_id))->whereIn("timeline",explode(",",$request->timeline))->first();
                    if($games)
                    {
                        $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                        $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                        $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                        $games->game_type_name = $games_type->game_type;
                        $games->game_type_details = $games_type;
                        $bet->games = $games;
                        $bet->team1detail = $team1detail;
                        $bet->team2detail = $team2detail;
                        if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                        else $bet->betteamdetail = $team2detail;
                    } 
                    //else unset($bets[$bet_k]);
                }
                foreach($bets as $bett){
                    if(isset($bett->games)){
                        $bets1[] = $bett;
                    }
                }
                $bets = $bets1;
            }else{
                $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
                foreach($bets as $bet_k=>&$bet){
                    //$games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id, "timeline"=>$request->timeline));
                    
                    $games = DB::table("games")->where(array("id"=>$bet->game_id))->whereIn("timeline",explode(",",$request->timeline))->first();
                    if($games){
                        $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                        $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                        $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                        $games->game_type_name = $games_type->game_type;
                        $games->game_type_details = $games_type;
                        $bet->games = $games;
                        $bet->team1detail = $team1detail;
                        $bet->team2detail = $team2detail;
                        if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                        else $bet->betteamdetail = $team2detail;
                        $bet->game_result = $games->result;
                    } 
                    //else unset($bets[$bet_k]);
                    $games_change_history_last = $this->SupUserModel->getLastRowById('games_change_history',array('game_id'=>$bet->game_id),"id","DESC");
                    $bet->games_change_history_last = $games_change_history_last;
                    $games_change_history_current = $this->SupUserModel->getRowById('games_change_history',array('id'=>$bet->games_change_history_id));
                    $bet->games_change_history_current = $games_change_history_current;
                    $bet->bet_result = 'No Result';
                    if(!isset($bet->game_result)){
                        $bet->game_result = '0';
                    }
                    
                    
                    /* if(in_array($bet->bet_box,["1","4"])){
                        if($bet->game_result == '0'){
                            $bet->bet_result = 'No Result';
                        }
                        elseif($bet->game_result == '3'){
                            $bet->bet_result = 'Draw';
                        }
                        else{
                            if($bet->game_result == $bet->bet_team){
                                $bet->bet_result = 'Won';
                            }else{
                                if(($bet->games_change_history_current->spread1_t1 > 0 && $bet->games_change_history_current->spread1_t2 < 0) || ($bet->games_change_history_current->spread1_t1 < 0 && $bet->games_change_history_current->spread1_t2 > 0)){
                                    

                                    if($bet->bet_box == '1') $spread = $bet->games_change_history_current->spread1_t1;
                                    else $spread = $bet->games_change_history_current->spread1_t2;

                                    if($spread > 0) $bet_on = 'Underdog';
                                    else $bet_on = 'Favorite';

                                    if($bet->games_change_history_last->spread1_t1 > 0 && $bet->games_change_history_last->spread1_t2 < 0){
                                        $Underdog = $bet->games_change_history_last->spread1_t1;
                                        $Favorite = $bet->games_change_history_last->spread1_t2;
                                    }else{
                                        $Underdog = $bet->games_change_history_last->spread1_t2;
                                        $Favorite = $bet->games_change_history_last->spread1_t1;
                                    }

                                    if($bet_on == 'Underdog'){
                                        if($Underdog + $spread > $Favorite) $bet->bet_result = 'Won';
                                        elseif($Underdog + $spread < $Favorite) $bet->bet_result = 'Lost';
                                        else $bet->bet_result = 'Draw';
                                    }else{
                                        if($Favorite - $spread > $Underdog) $bet->bet_result = 'Won';
                                        elseif($Favorite - $spread < $Underdog) $bet->bet_result = 'Lost';
                                        else $bet->bet_result = 'Draw';
                                    }



                                }else{
                                    $bet->bet_result = 'Lost';
                                }
                            }
                        }

                    }else{
                        if($bet->game_result == '0'){
                            $bet->bet_result = 'No Result';
                        }
                        elseif($bet->game_result == '3'){
                            $bet->bet_result = 'Draw';
                        }
                        else{
                            if($bet->game_result == $bet->bet_team){
                                $bet->bet_result = 'Won';
                            }else{
                                $bet->bet_result = 'Lost';
                            }
                        }
            
            
                    } */

                    if($bet->win == 0) $bet->bet_result = 'No Result';
                    elseif($bet->win == 1) $bet->bet_result = 'Lost';
                    else $bet->bet_result = 'Won';


                }
                foreach($bets as $bett){
                    if(isset($bett->games)){
                        $bets1[] = $bett;
                    }
                }
                $bets = $bets1;
            }
        }else{
            $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
            foreach($bets as $bet_k=>&$bet){
                $games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id));
                if($games){
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                    $games->game_type_name = $games_type->game_type;
                    $games->game_type_details = $games_type;
                    $bet->games = $games;
                    $bet->team1detail = $team1detail;
                    $bet->team2detail = $team2detail;
                    if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                    else $bet->betteamdetail = $team2detail;
                } 
                //else unset($bets[$bet_k]);
                foreach($bets as $bett){
                    if(isset($bett->games)){
                        $bets1[] = $bett;
                    }
                }
                $bets = $bets1;
            }
        }
        
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $bets
        ], 200);  
    }

    public function game_type(Request $request){
        if(isset($request->id)/*  && !empty($request->id) */){
            $game_type = $this->SupUserModel->getResultById("games_type",array("id"=>$request->id));
        }else{
            $game_type = $this->SupUserModel->getAllData("games_type");
        }
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $game_type
        ], 200);  
    }


    public function checkUserCanBetMore(Request $request)
    { $user = auth($this->guard)->user();
        $gamesById = $this->SupUserModel->getRowById("games",array('id'=>$request->game_id));
        $betsCountById = $this->SupUserModel->tablecountwhere("bets",array('user_id'=>$user->id, 'game_id'=>$request->game_id));
        if($betsCountById >= $gamesById->max_no_of_bets){
            return response()->json([
                'allow'=>0,
                'message'=>'Authenticated',
                'message_title'=>'warning',
                'results'=> 'Max betting limit Per User is already reached i.e. '.$gamesById->max_no_of_bets
            ], 200);
        }
        return response()->json([
                'allow'=>1,
            ], 200);
    }


    public function bets_add(Request $request){
		//echo "<pre>"; print_r($request->all()); die;
        $user = auth($this->guard)->user();
		$data = $request->all();
        $gamesById = $this->SupUserModel->getRowById("games",array('id'=>$request->game_id));
        $games_change_historyByGameId = $this->SupUserModel->getLastRowById("games_change_history",array('game_id'=>$request->game_id),"id","DESC");
        $usersById = $this->SupUserModel->getRowById("sup_users",array('id'=>$user->id));
        $betsCountById = $this->SupUserModel->tablecountwhere("bets",array('user_id'=>$user->id, 'game_id'=>$request->game_id));
		// if($request->bet_amount > $user->current_balance){
        //     return response()->json([
        //         'message'=>'Authenticated',
        //         'message_title'=>'warning',
        //         'results'=> 'Insufficient current balance in wallet'
        //     ], 200);
            
        // }else
        
        if($betsCountById >= $gamesById->max_no_of_bets){
            return response()->json([
                'message'=>'Authenticated',
                'message_title'=>'warning',
                'results'=> 'Max betting limit Per User is already reached i.e. '.$gamesById->max_no_of_bets
            ], 200);
            
        }elseif($request->bet_amount > $gamesById->max_amount_user_bet){
            return response()->json([
                'message'=>'Authenticated',
                'message_title'=>'warning',
                'results'=> 'Bet amount is greater than Max amount user can bet i.e. '.$gamesById->max_amount_user_bet
            ], 200);
        }else{
            $data['user_id'] = $user->id;
            $data['games_change_history_id'] = $games_change_historyByGameId->id;
            
            $to_win = 0;
            $pay_out = 0;

            if ($request->american_ods > 0){
                $to_win = ($request->american_ods/100)*$request->bet_amount;
                $pay_out = $to_win + $request->bet_amount;
            }
            
            if ($request->american_ods < 0){
                $to_win = (100/$request->american_ods)*$request->bet_amount * (-1);
                $pay_out = $to_win + $request->bet_amount;
            }
            
            $data['pay_out'] = number_format((float)$pay_out, 2, '.', '');
            $data['to_win'] = number_format((float)$to_win, 2, '.', '');


            
            $data['american_ods'] = $request->american_ods;
            $data['decimal_ods'] = $request->decimal_ods;
            $data['fractional_ods'] = $request->fractional_ods;
            $data['implide_ods'] = $request->implide_ods;
            // $data['fractional_ods'] = $gamesById->fractional_ods;
            // $data['implide_ods'] = $gamesById->implide_ods;

            $data['spread1_t1'] = $gamesById->spread1_t1;
            $data['spread2_t1'] = $gamesById->spread2_t1;
            $data['moneyline_t1'] = $gamesById->moneyline_t1;
            $data['total1_t1'] = $gamesById->total1_t1;
            $data['total2_t1'] = $gamesById->total2_t1;
            $data['total1_type_t1'] = $gamesById->total1_type_t1;

            $data['spread1_t2'] = $gamesById->spread1_t2;
            $data['spread2_t2'] = $gamesById->spread2_t2;
            $data['moneyline_t2'] = $gamesById->moneyline_t2;
            $data['total1_t2'] = $gamesById->total1_t2;
            $data['total2_t2'] = $gamesById->total2_t2;
            $data['total1_type_t2'] = $gamesById->total1_type_t2;
            
            $bets_add = $this->SupUserModel->insert("bets",$data);
            $data1 = array(
                "user_id"=>$user->id,
                "bet_id"=>$bets_add,
                "transaction_details"=>json_encode($data),
                "transaction"=>"U",
                "amount"=>$request->bet_amount,
                "status"=>"1",
            );
            $update_user_wallet = $this->SupUserModel->insert("transactions",$data1);
            // $transactions_add = $this->SupUserModel->update_row("sup_users",array("available_balance"=>$usersById->available_balance - $request->bet_amount, "current_balance"=>$usersById->current_balance - $request->bet_amount),array("id"=>$user->id));
            $upcoming_bets = $this->this_bets('0');
            return response()->json([
                'message'=>'Authenticated',
                'message_title'=>'success',
                'results'=> $upcoming_bets
            ], 200);  
        }
        
	}

    public function this_bets($timeline=''){
        $user = auth($this->guard)->user();
        $bets1 = array();
        if($timeline != ''){
            $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
            foreach($bets as $bet_k=>&$bet){
                $games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id, "timeline"=>$timeline));
                if($games){
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                    $games->game_type_name = $games_type->game_type;
                    $games->game_type_details = $games_type;
                    $bet->games = $games;
                    $bet->team1detail = $team1detail;
                    $bet->team2detail = $team2detail;
                    if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                    else $bet->betteamdetail = $team2detail;
                } 
                //else unset($bets[$bet_k]);
            }
            foreach($bets as $bett){
                if(isset($bett->games)){
                    $bets1[] = $bett;
                }
            }
            $bets = $bets1;
        }else{
            $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
            foreach($bets as $bet_k=>&$bet){
                $games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id));
                if($games){
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                    $games->game_type_name = $games_type->game_type;
                    $games->game_type_details = $games_type;
                    $bet->games = $games;
                    $bet->team1detail = $team1detail;
                    $bet->team2detail = $team2detail;
                    if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                    else $bet->betteamdetail = $team2detail;
                } 
                //else unset($bets[$bet_k]);
                foreach($bets as $bett){
                    if(isset($bett->games)){
                        $bets1[] = $bett;
                    }
                }
                $bets = $bets1;
            }
        }
        return $bets;
    }

    public function sendPMEmail($email,$subject,$content) {
        /* header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); */
            //require dirname(__DIR__, 3) .'/vendor/autoload.php'; 

        $mail = new PHPMailer(true); 
        
        try {

            $mail->SMTPDebug = 4; 
            $mail->isSMTP(); 
            // $mail->IsMail();
            $mail->Host = env('MAIL_HOST'); 
            $mail->SMTPAuth = true; 
            $mail->Username = env('MAIL_USERNAME'); 
            $mail->Password = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port = env('MAIL_PORT');
            
            $mail->setFrom(env('MAIL_FROM_ADDRESS'));
            $mail->addAddress($email); 
            $mail->addReplyTo($email);
            

                /* $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );  */

            $mail->isHTML(true); 

            $mail->Subject = $subject;
            $mail->Body    = $content;
            
        
            if( !$mail->send() ) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                //return back()->with('success',$flash);
                return;
            }

        } catch (Exception $e) {
            // return back()->with('error','Message could not be sent.');
        }
        exit;

        
    }

    public function test(){
        $this->sendPMEmail("jasimnft@gmail.com","Test","Test"); 
    }

    public function transactions(Request $request){
        $user = auth($this->guard)->user();
        $transactions = $this->SupUserModel->getResultById("transactions",array("user_id"=>$user->id)); 
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            'results'=> $transactions
        ], 200);  
    }

    public function total_investment_earned(Request $request){
        $user = auth($this->guard)->user();
        $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id)); 
        $bets1 = array();
        $bets = $this->SupUserModel->getResultById("bets",array("user_id"=>$user->id));
        foreach($bets as $bet_k=>&$bet){
            $games = $this->SupUserModel->getRowById("games",array("id"=>$bet->game_id/* , "timeline"=>$request->timeline */));
            if($games){
                $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                $games->game_type_name = $games_type->game_type;
                $games->game_type_details = $games_type;
                $bet->games = $games;
                $bet->team1detail = $team1detail;
                $bet->team2detail = $team2detail;
                if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                else $bet->betteamdetail = $team2detail;
            } 
            //else unset($bets[$bet_k]);
        }
        foreach($bets as $bett){
            if(isset($bett->games)){
                $bets1[] = $bett;
            }
        }
        $bets = $bets1;
        $total_investment = 0;
        $total_earned = 0;
        foreach($bets as $bet_v){
            $total_investment = $total_investment + $bet_v->bet_amount;
            if($bet_v->games->timeline == '2'){
                $total_earned = $total_earned + $bet_v->pay_out;
            } 
        }
        $total_investment_earned_array = array(
            "total_investment"=>$total_investment,
            "total_earned"=>$total_earned
        );
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            //'results'=> $bets
            'results'=> $total_investment_earned_array
        ], 200);  
    }

    public function admin_cms_by_page(Request $request){
        //$user = auth($this->guard)->user();
        $sup_admin_cms = $this->SupUserModel->getResultById("sup_admin_cms",array("cms_page"=>$request->page));
        return response()->json([
            'message'=>'Authenticated',
            'message_title'=>'success',
            //'results'=> $bets
            'results'=> $sup_admin_cms
        ], 200);  
    }

    // added by asif 
    public function getBetsData(Request $request)
    {
        $user = auth($this->guard)->user();
        $bets1 = array();

        if ($request->status == 0)
        {
            $bets = UserBetting::where('user_id', $user->id)->where('win', $request->status)->get();
            foreach($bets as $bet_k=>&$bet){
                $games = AllGames::find($bet->game_id);
                // $games = DB::table("games")->where(array("id"=>$bet->game_id))->whereIn("timeline",explode(",",$request->timeline))->first();
                if(isset($games))
                {
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                    $games->game_type_name = $games_type->game_type;
                    $games->game_type_details = $games_type;
                    $bet->bet_on = getBotBox($bet);
                    $bet->games = $games;
                    $bet->team1detail = $team1detail;
                    $bet->team2detail = $team2detail;
                    if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                    else $bet->betteamdetail = $team2detail;
                } 
                //else unset($bets[$bet_k]);
            }
            foreach($bets as $bett){
                if(isset($bett->games)){
                    $bets1[] = $bett;
                }
            }
            $bets = $bets1;
        }
        else
        {
            $bets = UserBetting::where('user_id', $user->id)->where('win', 1)->orWhere('win', 2)->orWhere('win', 3)->get();
            foreach($bets as $bet_k=>&$bet){
                $games = AllGames::find($bet->game_id);
                // $games = DB::table("games")->where(array("id"=>$bet->game_id))->whereIn("timeline",explode(",",$request->timeline))->first();
                if(isset($games))
                {
                    $games_type = $this->SupUserModel->getRowById("games_type",array("id"=>$games->game_type));
                    $team1detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team1_id));
                    $team2detail = $this->SupUserModel->getRowById("teams",array("id"=>$games->team2_id));
                    $games->game_type_name = $games_type->game_type;
                    $games->game_type_details = $games_type;
                    $bet->bet_on = getBotBox($bet);
                    $bet->games = $games;
                    $bet->team1detail = $team1detail;
                    $bet->team2detail = $team2detail;
                    if($bet->bet_team == '1') $bet->betteamdetail = $team1detail;
                    else $bet->betteamdetail = $team2detail;
                } 
                //else unset($bets[$bet_k]);
            }
            foreach($bets as $bett){
                if(isset($bett->games)){
                    $bets1[] = $bett;
                }
            }
            $bets = $bets1;
        }
        return $bets;
    }
}


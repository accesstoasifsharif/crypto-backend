<?php
namespace App\Http\Controllers;
#use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Models\ResUser;
use App\Models\SuperAdminModel;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Config;

class SuperAdminController extends Controller
{
    public function __construct(SuperAdminModel $SuperAdminModel) {
        Config::set('jwt.user', 'App\Models\ResUser'); 
        Config::set('auth.providers.users.model', \App\Models\ResUser::class);
        $this->guard = "api_res_user";
        $this->SuperAdminModel = $SuperAdminModel;
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        //$this->middleware('auth:api', ['except' => ['login', 'register', 'logout', 'refresh', 'userProfile', 'addCredit', 'userProfileUpdate']]);
    }


    /**
     * Register Super Admin User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_firstname' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:sup_adminmaster',
            //'user_id' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:6',
            //'password_confirmation' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = ResUser::create([
            'user_firstname' => $request->user_firstname,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
		$token = JWTAuth::fromUser($user);
		$data = array();
        //$data['remember_token'] = $token;    
        $data['user_type'] = 1;
        $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("user_id"=>$user['user_id']));
		//$user1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("user_id"=>$user['user_id']));
		//$user['remember_token'] = $user1->remember_token;
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'User successfully registered',
			'id'=> $user['user_id'],
            'user' => $user,
            'token' => $token
        ], 200);  
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth($this->guard)->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        // Fill user model
        $user->fill([
            'remember_token' => $token
        ]);

        // Save user to database
        $user->save(); 
        return $this->createNewToken($token);
    }

    


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        auth($this->guard)->logout();
        return response()->json(['message'=>'Authenticated','message_title' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request) {
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        return $this->createNewToken(auth($this->guard)->refresh(),'refresh',$user);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(Request $request) {
		$user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        return response()->json(['message'=>'Authenticated','message_title'=>'success','user'=>$user]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token,$method='',$user1=''){
		$user = auth($this->guard)->user();
		$user['remember_token'] = $token;
		if(!empty($method) && $method == 'refresh'){
			$data = array(
                'remember_token' => $token
            );
            $user['id'] = $user1['id'];
            $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("id"=>$user1['id']));
		}
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'success',
			'id' => $user['id'],
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth($this->guard)->factory()->getTTL() * 60,
            'user' => $user
        ]);
		
    }
	
	
	public function userProfileUpdate(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'string|between:2,100',
			'email' => 'email'
            //'email' => 'required|string|email|max:100'
            //'email' => 'string|email|max:100|unique:mbf_customer'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        if(!isset($request->email) && !isset($request->name)){
            return response()->json(['error' => 'Atleast one from name, email is required'], 400);
        }
        if(isset($request->email)){
            $userdetail1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $user['id']){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
		$data = array();
		if(isset($request->name)) $data['name'] = $request->name;
		if(isset($request->email)) $data['email'] = $request->email;
        $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("id"=>$user['id']));
		return response()->json(['message'=>'Authenticated','message_title' => 'User profile updated successfully']);
    }


    public function restaurant_add(Request $request){
        $validator = Validator::make($request->all(), [
            'restaurant_name' => 'required|string|between:2,255',
            'owner_name' => 'required|string|between:2,255',
            'street' => 'required|string|between:2,255',
            'city' => 'required|string|between:2,255',
            'state' => 'required|string|between:2,255',
            'country' => 'required|string|between:2,255',
            'zipcode' => 'required|string|between:2,8',
            'contact' => 'required|string|between:2,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = $this->SuperAdminModel->insert('restaurant', $data);
        if($id){
            return response()->json(['message'=>'Authenticated','message_title' => 'Restaurant added successfully','id'=>$id]);
        }
    }

    public function restaurant_edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        unset($data['id']);
        $result = $this->SuperAdminModel->update_row('restaurant', $data, array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Restaurant edited successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to edit restaurant']);
        }
    }

    public function restaurant_delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        $result = $this->SuperAdminModel->deleteRowById('restaurant', array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Restaurant deleted successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to delete restaurant']);
        }
        
    }

    public function restaurant_admin_add(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:sup_adminmaster',
            //'user_id' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:6',
            //'password_confirmation' => 'required|string|confirmed|min:6',
            'restaurant_id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);

        $restaurantdetail = $this->SuperAdminModel->getRowById("restaurant",array("id"=>(int) $request->restaurant_id));
        if(empty($restaurantdetail)) return response()->json(['error' => 'Unauthorized'], 401);
        $user = ResUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
		$token = JWTAuth::fromUser($user);
		$data = array();
        $data['remember_token'] = $token;    
        $data['user_type'] = 2;
        $data['restaurant_id'] = (int) $request->restaurant_id;
        $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("id"=>$user['id']));
		$user1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$user['id']));
		$user['remember_token'] = $user1->remember_token;
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'Admin successfully added',
			'id'=> $user['id'],
            'user' => $user,
            'token' => $token
        ], 200);  
    }

    public function restaurant_admin_edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
            'name' => 'string|between:2,100',
            'email' => 'string|email|max:100',
            'password' => 'string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        if(isset($data['password'])) $data['password'] = Hash::make($request->password);
        if(isset($request->email)){
            $userdetail1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $id){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
        unset($data['id']);
        $result = $this->SuperAdminModel->update_row('sup_adminmaster', $data, array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Admin edited successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to edit admin']);
        }
        
    }

    public function restaurant_admin_delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        $admindetail = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$id));
        if(!empty($admindetail) && $admindetail->user_type != 2) return response()->json(['error' => 'Unauthorized'], 401);
        $result = $this->SuperAdminModel->deleteRowById('sup_adminmaster', array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Admin deleted successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to delete admin']);
        }
    }

    public function restaurant_subadmin_add(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:sup_adminmaster',
            //'user_id' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:6',
            //'password_confirmation' => 'required|string|confirmed|min:6',
            'restaurant_id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $restaurantdetail = $this->SuperAdminModel->getRowById("restaurant",array("id"=>(int) $request->restaurant_id));
        if(empty($restaurantdetail)) return response()->json(['error' => 'Unauthorized'], 401);
        $user = ResUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
		$token = JWTAuth::fromUser($user);
		$data = array();
        $data['remember_token'] = $token;    
        $data['user_type'] = 3;
        $data['restaurant_id'] = (int) $request->restaurant_id;
        $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("id"=>$user['id']));
		$user1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$user['id']));
		$user['remember_token'] = $user1->remember_token;
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'SubAdmin successfully added',
			'id'=> $user['id'],
            'user' => $user,
            'token' => $token
        ], 200);  
    }

    public function restaurant_subadmin_edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
            'name' => 'string|between:2,100',
            'email' => 'string|email|max:100',
            'password' => 'string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        if(isset($data['password'])) $data['password'] = Hash::make($request->password);
        if(isset($request->email)){
            $userdetail1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $id){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
        unset($data['id']);
        $result = $this->SuperAdminModel->update_row('sup_adminmaster', $data, array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'SubAdmin edited successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to edit subadmin']);
        }
    }

    public function restaurant_subadmin_delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        $admindetail = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$id));
        if(!empty($admindetail) && $admindetail->user_type != 3) return response()->json(['error' => 'Unauthorized'], 401);
        $result = $this->SuperAdminModel->deleteRowById('sup_adminmaster', array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'SubAdmin deleted successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to delete subadmin']);
        }
    }

    public function restaurant_waiter_add(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:sup_adminmaster',
            //'user_id' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:6',
            //'password_confirmation' => 'required|string|confirmed|min:6',
            'restaurant_id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $restaurantdetail = $this->SuperAdminModel->getRowById("restaurant",array("id"=>(int) $request->restaurant_id));
        if(empty($restaurantdetail)) return response()->json(['error' => 'Unauthorized'], 401);
        $user = ResUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
		$token = JWTAuth::fromUser($user);
		$data = array();
        $data['remember_token'] = $token;    
        $data['user_type'] = 4;
        $data['restaurant_id'] = (int) $request->restaurant_id;
        $this->SuperAdminModel->update_row('sup_adminmaster',$data,array("id"=>$user['id']));
		$user1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$user['id']));
		$user['remember_token'] = $user1->remember_token;
        return response()->json([
			'message'=>'Authenticated',
			'message_title'=>'Waiter successfully added',
			'id'=> $user['id'],
            'user' => $user,
            'token' => $token
        ], 200);  
    }

    public function restaurant_waiter_edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
            'name' => 'string|between:2,100',
            'email' => 'string|email|max:100',
            'password' => 'string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        if(isset($data['password'])) $data['password'] = Hash::make($request->password);
        if(isset($request->email)){
            $userdetail1 = $this->SuperAdminModel->getRowById("sup_adminmaster",array("email"=>$request->email));
            if(!empty($userdetail1)){
                if($userdetail1->id != $id){
                    return response()->json(['error' => 'Email already used by other user from website'], 400);
                }
            }
        }
        unset($data['id']);
        $result = $this->SuperAdminModel->update_row('sup_adminmaster', $data, array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Waiter edited successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to edit waiter']);
        }
    }

    public function restaurant_waiter_delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|between:1,255',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            //return response()->json($validator->errors(), 400);
        }
        $user = auth($this->guard)->user();
        if($user['user_type'] != 1) return response()->json(['error' => 'Unauthorized'], 401);
        $data = $request->all();
        $id = (int) $data['id'];
        $admindetail = $this->SuperAdminModel->getRowById("sup_adminmaster",array("id"=>$id));
        if(!empty($admindetail) && $admindetail->user_type != 3) return response()->json(['error' => 'Unauthorized'], 401);
        $result = $this->SuperAdminModel->deleteRowById('sup_adminmaster', array('id'=>$id));
        if($result){
            return response()->json(['message'=>'Authenticated','message_title' => 'Waiter deleted successfully']);
        }else{
            return response()->json(['message'=>'Authenticated','message_title' => 'Unable to delete waiter']);
        }
    }


}


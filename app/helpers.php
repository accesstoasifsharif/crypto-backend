<?php


if (! function_exists('formateWalletAddress')) {
    function formateWalletAddress($address) {
        return substr($address,0,6) . "...." .substr($address,strlen($address)-4,4);
    }
}


if (! function_exists('formateGameResult')) {
    function formateGameResult($val) {
        if($val == 0){
            return 'No Result';
        }
        if($val == 1){
            return 'Team1 Win';
        }
        if($val == 2){
            return 'Team2 Win';
        }
        if($val == 3){
            return 'Draw';
        }
        return "";
    }
}



if (! function_exists('formateGameStatus')) {
    function formateGameStatus($val) {

        if($val == 0){
            return 'Deactive';
        }
        if($val == 1){
            return 'Active';
        }        return "";
    }
}


if (! function_exists('getBotBox')) {
    function getBotBox($bet ) {
        if($bet->bet_box == 1)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">SPREAD <br/>'. $bet->spread1_t1 . '<br/>' . $bet->spread2_t1 . '</p>';
        }
        if($bet->bet_box == 2)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">MONEYLINE <br/>' . $bet->moneyline_t1 . '</p>';
        }
        if($bet->bet_box == 3)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">OVER/UNDER ' . $bet->total1_t1 . '<br/>' . $bet->total2_t1 . '<br/>' . $bet->total1_type_t1 . '</p>';
        }
        if($bet->bet_box == 4)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">SPREAD <br/>' . $bet->spread1_t2 . '<br/>' . $bet->spread2_t2 . '</p>';
        }
        if($bet->bet_box == 5)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">MONEYLINE <br/>' . '<br/>' . $bet->moneyline_t2 . '</p>';
        }
        if($bet->bet_box == 6)
        {
            return '<p style="  background-color: black; color: white; padding: 5px; text-align: center;">OVER/UNDER ' . $bet->total1_t2 . '<br/>' . $bet->total2_t2 . '<br/>' . $bet->total1_type_t2 . '</p>';
        }
        return "";
    }
}

